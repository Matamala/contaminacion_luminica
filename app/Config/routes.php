<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'Monitoreo', 'action' => 'index'));
	Router::connect('/Contacto', array('controller' => 'Monitoreo', 'action' => 'contact'));
	Router::connect('/Sobre-Nosotros', array('controller' => 'Monitoreo', 'action' => 'about'));

	Router::connect('/Dashboard', array('controller' => 'Dashboards', 'action' => 'index'));
	Router::connect('/Dashboard/Mis-Nodos', array('controller' => 'Dashboards', 'action' => 'misZonas'));
	Router::connect('/Dashboard/Mis-Nodos/Nuevo', array('controller' => 'UserRanges', 'action' => 'add'));
	Router::connect('/Dashboard/Nodos-Monitoreadas', array('controller' => 'Dashboards', 'action' => 'zonasMonitoreadas'));
	Router::connect('/Mi-Perfil/*', array('controller' => 'Users', 'action' => 'editUser'));
	Router::connect('/Dashboard/Galeria/Imagenes', array('controller' => 'Dashboards', 'action' => 'galeryImages'));
	
	Router::connect('/Dashboard/Ciudades', array('controller' => 'Citys', 'action' => 'index'));
	Router::connect('/Dashboard/Ciudades/Nuevo', array('controller' => 'Citys', 'action' => 'add'));
	Router::connect('/Dashboard/Ciudades/Editar/*', array('controller' => 'Citys', 'action' => 'edit'));

	Router::connect('/Dashboard/Comunas', array('controller' => 'Communes', 'action' => 'index'));
	Router::connect('/Dashboard/Comunas/Nuevo', array('controller' => 'Communes', 'action' => 'add'));
	Router::connect('/Dashboard/Comunas/Editar/*', array('controller' => 'Communes', 'action' => 'edit'));

	Router::connect('/Dashboard/Regiones', array('controller' => 'Regions', 'action' => 'index'));
	Router::connect('/Dashboard/Regiones/Nuevo', array('controller' => 'Regions', 'action' => 'add'));
	Router::connect('/Dashboard/Regiones/Editar/*', array('controller' => 'Regions', 'action' => 'edit'));

	Router::connect('/Dashboard/Usuarios', array('controller' => 'Users', 'action' => 'index'));
	Router::connect('/Dashboard/Usuarios/Nuevo', array('controller' => 'Users', 'action' => 'addUser'));
	Router::connect('/Dashboard/Usuarios/Editar/*', array('controller' => 'Users', 'action' => 'editar'));

	Router::connect('/Dashboard/Nodos', array('controller' => 'Zones', 'action' => 'index'));
	Router::connect('/Dashboard/Nodos/Nuevo', array('controller' => 'Zones', 'action' => 'add'));
	Router::connect('/Dashboard/Nodos/Editar/*', array('controller' => 'Zones', 'action' => 'edit'));

	

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
