function validaFormulario(){

	var error_formulario = false;
	$('.form-control').removeClass('panel-bordered-danger');

	var rut_cliente = $.trim($('#rut_cliente').val());
	var nombre_cliente = $.trim($('#nombre_cliente').val());
	var segmento_cliente = $.trim($('#segmento_cliente').val());
	var tipo_formulario = $.trim($('#tipo_formulario').val());
	var numero_formulario = $.trim($('#numero_formulario').val());
	var servicio_seleccionado = $.trim($('#servicio').val());

	if(rut_cliente.length==0){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe ingresar el rut del cliente.');
		$('#rut_cliente').addClass('panel-bordered-danger');
		$('#rut_cliente').select();
		$('#rut_cliente').focus();
		return false;
	}

	if(nombre_cliente.length==0){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe ingresar el nombre del cliente.');
		$('#nombre_cliente').addClass('panel-bordered-danger');
		$('#nombre_cliente').select();
		$('#nombre_cliente').focus();
		return false;
	}

	if(segmento_cliente.length==0 || segmento_cliente=='0'){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe seleccionar el segmento a la cual pertenece el cliente.');
		$('#segmento_cliente').addClass('panel-bordered-danger');
		$('#segmento_cliente').select();
		$('#segmento_cliente').focus();
		return false;
	}

	if(tipo_formulario.length==0 || tipo_formulario==0){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe seleccionar el tipo de formulario.');
		$('#tipo_formulario').addClass('panel-bordered-danger');
		$('#tipo_formulario').select();
		$('#tipo_formulario').focus();
		return false;
	}

	var tpform = "";
	if(tipo_formulario==1){
		tpform = "USV"; 
	}

	if(tipo_formulario==2){
		tpform = "BPO";	
	}

	if(numero_formulario.length==0){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe ingresar el número del '+tpform+'.');
		$('#numero_formulario').addClass('panel-bordered-danger');
		$('#numero_formulario').select();
		$('#numero_formulario').focus();
		return false;
	}

    if(servicio_seleccionado.length==0 || servicio_seleccionado==0){
		error_formulario = true;
		alertFail('Datos Cliente', 'Debe seleccionar el servicio asociado al formulario.');
		$('#servicio').addClass('panel-bordered-danger');
		$('#servicio').select();
		$('#servicio').focus();
		return false;
	}

	/* Internet Dedicado */
	var cantidad1 = $.trim($('#cantidad_intenetDedicado_filas').val());
		var cantidad111 = $.trim($('#tipo_intenetDedicado_filas').val());
	
	var cantidad2 = $.trim($('#cantidad_mpls_instalacion_filas').val());
	var cantidad3 = $.trim($('#cantidad_mpls_upgrade_filas').val());
	var cantidad4 = $.trim($('#cantidad_mpls_downgrade_filas').val());
	var cantidad5 = $.trim($('#cantidad_mpls_traslado_filas').val());
	var cantidad6 = $.trim($('#cantidad_mpls_cambioAcceso_filas').val());
	var cantidad7 = $.trim($('#cantidad_mpls_renegociacion_filas').val());

	var cantidad8 = $.trim($('#cantidad_trunckCD_instalacion_filas').val());
	var cantidad9 = $.trim($('#cantidad_trunckCD_upgrade_filas').val());
		var cantidad91 = $.trim($('#cantidad_trunckCD_upgrade_telefonia_filas').val());
		var cantidad92 = $.trim($('#cantidad_trunckCD_upgrade_internetDedicado_filas').val());
		var cantidad93 = $.trim($('#cantidad_trunckCD_upgrade_accesoMpls_filas').val());
	var cantidad10 = $.trim($('#cantidad_trunckCD_downgrade_filas').val());
		var cantidad101 = $.trim($('#cantidad_trunckCD_downgrade_telefonia_filas').val());
		var cantidad102 = $.trim($('#cantidad_trunckCD_downgrade_internetDedicado_filas').val());
		var cantidad103 = $.trim($('#cantidad_trunckCD_downgrade_accesoMpls_filas').val());
	var cantidad11 = $.trim($('#cantidad_trunckCD_traslado_filas').val());	
	var cantidad12 = $.trim($('#cantidad_trunckCD_cambioAcceso_filas').val());
	var cantidad13 = $.trim($('#cantidad_trunckCD_renegociacion_filas').val());
	var cantidad14 = $.trim($('#cantidad_trunckCD_cambioProducto_filas').val());

	var cantidad15 = $.trim($('#cantidad_trunckSD_instalacion_filas').val());
	var cantidad16 = $.trim($('#cantidad_trunckSD_upgrade_filas').val());
		var cantidad161 = $.trim($('#cantidad_trunckSD_upgrade_telefonia_filas').val());
		var cantidad162 = $.trim($('#cantidad_trunckSD_upgrade_internetDedicado_filas').val());
		var cantidad163 = $.trim($('#cantidad_trunckSD_upgrade_accesoMpls_filas').val());
	var cantidad17 = $.trim($('#cantidad_trunckSD_downgrade_filas').val());
		var cantidad171 = $.trim($('#cantidad_trunckSD_downgrade_telefonia_filas').val());
		var cantidad172 = $.trim($('#cantidad_trunckSD_downgrade_internetDedicado_filas').val());
		var cantidad173 = $.trim($('#cantidad_trunckSD_downgrade_accesoMpls_filas').val());
	var cantidad18 = $.trim($('#cantidad_trunckSD_traslado_filas').val());
	var cantidad19 = $.trim($('#cantidad_trunckSD_cambioAcceso_filas').val());
	var cantidad20 = $.trim($('#cantidad_trunckSD_renegociacion_filas').val());
	var cantidad21 = $.trim($('#cantidad_trunckSD_cambioProducto_filas').val());

    var cantidad22 = $.trim($("#cantidad_telefonia_filas").val());


    //Internet Dedicado
    if(servicio_seleccionado==1){
    	if(cantidad1==0){
    		error_formulario = true;
			alertFail('Internet Dedicado', 'Debe a lo menos ingresar 1 item.');
			return false;
    	}
    }

    if(servicio_seleccionado==2){
    	if(cantidad2==0 && cantidad3==0 && cantidad4==0 && cantidad5==0 && cantidad6==0 && cantidad7==0){
    		error_formulario = true;
			alertFail('MPLS', 'Debe a lo menos ingresar 1 item.');
			return false;
    	}
    }

    if(servicio_seleccionado==3){
    	if(cantidad8==0 && cantidad9==0 && cantidad10==0 && cantidad11==0 && cantidad12==0 && cantidad13==0 && cantidad14==0){
    		error_formulario = true;
			alertFail('Trunck Con Datos', 'Debe a lo menos ingresar 1 item.');
			return false;
    	}
    }

    if(servicio_seleccionado==4){
    	if(cantidad15==0 && cantidad16==0 && cantidad17==0 && cantidad18==0 && cantidad19==0 && cantidad20==0 && cantidad21==0){
    		error_formulario = true;
			alertFail('Trunck Sin Datos', 'Debe a lo menos ingresar 1 item.');
			return false;
    	}
    }

    if(servicio_seleccionado==5){
    	if(cantidad22==0){
    		error_formulario = true;
			alertFail('Telefonía', 'Debe a lo menos ingresar 1 item.');
			return false;
    	}
    }

	if(cantidad1>0 || cantidad2>0 || cantidad3>0 || cantidad4>0 || cantidad5>0 || cantidad6>0 || cantidad7>0 || cantidad8>0 || cantidad9>0 || cantidad10>0 || cantidad11>0 || cantidad12>0 || cantidad13>0 || cantidad14>0 || cantidad15>0 || cantidad16>0 || cantidad17>0 || cantidad18>0 || cantidad19>0 || cantidad20>0 || cantidad21>0 || cantidad22>0){	



		/* Internet Dedicado */
		if(servicio_seleccionado==1){
			if(cantidad1.length>0 && cantidad1>0){
				for(var i=1;i<=cantidad1;i++){

					//Instalación
					if(cantidad111==1){
						
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idprecioobjeetivo = $.trim($('#idprecioobjeetivo_'+i).val());
						var idplazo = $.trim($('#idplazo_'+i).val());

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el número de oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}
						
						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}
						
						if(idprecioobjeetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#idprecioobjeetivo_'+i).addClass('panel-bordered-danger');
							$('#idprecioobjeetivo_'+i).select();
							$('#idprecioobjeetivo_'+i).focus();
							return false;
						}

						if(idplazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idplazo_'+i).addClass('panel-bordered-danger');
							$('#idplazo_'+i).select();
							$('#idplazo_'+i).focus();
							return false;
						}	
						
					}

					//Upgrade
					if(cantidad111==2){
						
						var idcodigoservicio = $.trim($('#idcodigoservicio_'+i).val());
						var idusvcontratovigente = $.trim($('#idusvcontratovigente_'+i).val());
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idprecioobjeetivo = $.trim($('#idprecioobjeetivo_'+i).val());
						var idplazo = $.trim($('#idplazo_'+i).val());
						var idcuentafacturacion = $.trim($('#idcuentafacturacion_'+i).val());

						if(idcodigoservicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#idcodigoservicio_'+i).addClass('panel-bordered-danger');
							$('#idcodigoservicio_'+i).select();
							$('#idcodigoservicio_'+i).focus();
							return false;
						}

						if(idusvcontratovigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de contrato vigente.');
							$('#idusvcontratovigente_'+i).addClass('panel-bordered-danger');
							$('#idusvcontratovigente_'+i).select();
							$('#idusvcontratovigente_'+i).focus();
							return false;
						}

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de Oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}

						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}

						if(idprecioobjeetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar precio objetivo.');
							$('#idprecioobjeetivo_'+i).addClass('panel-bordered-danger');
							$('#idprecioobjeetivo_'+i).select();
							$('#idprecioobjeetivo_'+i).focus();
							return false;
						}

						if(idplazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idplazo_'+i).addClass('panel-bordered-danger');
							$('#idplazo_'+i).select();
							$('#idplazo_'+i).focus();
							return false;
						}

						/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/

					}

					//Traslado
					if(cantidad111==3){

						var idcodigoservicio = $.trim($('#idcodigoservicio_'+i).val());
						var idusvcontratovigente = $.trim($('#idusvcontratovigente_'+i).val());
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idcuentafacturacion = $.trim($('#idcuentafacturacion_'+i).val());
					
						if(idcodigoservicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idcodigoservicio_'+i).addClass('panel-bordered-danger');
							$('#idcodigoservicio_'+i).select();
							$('#idcodigoservicio_'+i).focus();
							return false;
						}

						if(idusvcontratovigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de contrato vigente.');
							$('#idusvcontratovigente_'+i).addClass('panel-bordered-danger');
							$('#idusvcontratovigente_'+i).select();
							$('#idusvcontratovigente_'+i).focus();
							return false;
						}

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de Oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}

						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}

						/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/

					}

					//Cambio de Acceso
					if(cantidad111==4){
						
						var idcodigoservicio = $.trim($('#idcodigoservicio_'+i).val());
						var idusvcontratovigente = $.trim($('#idusvcontratovigente_'+i).val());
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idcuentafacturacion = $.trim($('#idcuentafacturacion_'+i).val());

						if(idcodigoservicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idcodigoservicio_'+i).addClass('panel-bordered-danger');
							$('#idcodigoservicio_'+i).select();
							$('#idcodigoservicio_'+i).focus();
							return false;
						}

						if(idusvcontratovigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de contrato vigente.');
							$('#idusvcontratovigente_'+i).addClass('panel-bordered-danger');
							$('#idusvcontratovigente_'+i).select();
							$('#idusvcontratovigente_'+i).focus();
							return false;
						}

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de Oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}

						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}

						/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/

					}

					//Renegociación
					if(cantidad111==5){
						
						var idcodigoservicio = $.trim($('#idcodigoservicio_'+i).val());
						var idusvcontratovigente = $.trim($('#idusvcontratovigente_'+i).val());
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idprecioobjeetivo = $.trim($('#idprecioobjeetivo_'+i).val());
						var idplazo = $.trim($('#idplazo_'+i).val());
						var idplazorenegociacion = $.trim($('#idplazorenegociacion_'+i).val());
						var idcuentafacturacion = $.trim($('#idcuentafacturacion_'+i).val());

						if(idcodigoservicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#idcodigoservicio_'+i).addClass('panel-bordered-danger');
							$('#idcodigoservicio_'+i).select();
							$('#idcodigoservicio_'+i).focus();
							return false;
						}

						if(idusvcontratovigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de contrato vigente.');
							$('#idusvcontratovigente_'+i).addClass('panel-bordered-danger');
							$('#idusvcontratovigente_'+i).select();
							$('#idusvcontratovigente_'+i).focus();
							return false;
						}

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de Oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}

						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}

						if(idprecioobjeetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar precio objetivo.');
							$('#idprecioobjeetivo_'+i).addClass('panel-bordered-danger');
							$('#idprecioobjeetivo_'+i).select();
							$('#idprecioobjeetivo_'+i).focus();
							return false;
						}

						if(idplazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idplazo_'+i).addClass('panel-bordered-danger');
							$('#idplazo_'+i).select();
							$('#idplazo_'+i).focus();
							return false;
						}

						if(idplazorenegociacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo de negociación en número de meses.');
							$('#idplazorenegociacion_'+i).addClass('panel-bordered-danger');
							$('#idplazorenegociacion_'+i).select();
							$('#idplazorenegociacion_'+i).focus();
							return false;
						}

						/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/

					}

					//downgrade
					if(cantidad111==6){
						
						var idcodigoservicio = $.trim($('#idcodigoservicio_'+i).val());
						var idusvcontratovigente = $.trim($('#idusvcontratovigente_'+i).val());
						var idnumerooft = $.trim($('#idnumerooft_'+i).val());
						var idvelnacional = $.trim($('#idvelnacional_'+i).val());
						var idvelinternacional = $.trim($('#idvelinternacional_'+i).val());
						var idprecioobjeetivo = $.trim($('#idprecioobjeetivo_'+i).val());
						var idplazo = $.trim($('#idplazo_'+i).val());
						var idcuentafacturacion = $.trim($('#idcuentafacturacion_'+i).val());

						if(idcodigoservicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#idcodigoservicio_'+i).addClass('panel-bordered-danger');
							$('#idcodigoservicio_'+i).select();
							$('#idcodigoservicio_'+i).focus();
							return false;
						}

						if(idusvcontratovigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de contrato vigente.');
							$('#idusvcontratovigente_'+i).addClass('panel-bordered-danger');
							$('#idusvcontratovigente_'+i).select();
							$('#idusvcontratovigente_'+i).focus();
							return false;
						}

						if(idnumerooft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar número de Oft.');
							$('#idnumerooft_'+i).addClass('panel-bordered-danger');
							$('#idnumerooft_'+i).select();
							$('#idnumerooft_'+i).focus();
							return false;
						}

						if(idvelnacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#idvelnacional_'+i).addClass('panel-bordered-danger');
							$('#idvelnacional_'+i).select();
							$('#idvelnacional_'+i).focus();
							return false;
						}

						if(idvelinternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#idvelinternacional_'+i).addClass('panel-bordered-danger');
							$('#idvelinternacional_'+i).select();
							$('#idvelinternacional_'+i).focus();
							return false;
						}

						if(idprecioobjeetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar precio objetivo.');
							$('#idprecioobjeetivo_'+i).addClass('panel-bordered-danger');
							$('#idprecioobjeetivo_'+i).select();
							$('#idprecioobjeetivo_'+i).focus();
							return false;
						}

						if(idplazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar el plazo en número de meses.');
							$('#idplazo_'+i).addClass('panel-bordered-danger');
							$('#idplazo_'+i).select();
							$('#idplazo_'+i).focus();
							return false;
						}

						/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/
					
					}

				}	
			}	
		}

		/* MPLS */
		if(servicio_seleccionado==2){
			if(cantidad2.length>0 && cantidad2>0){
				
				var mplsInstalacionConCasaMatriz = $('#mpls_instalacion_check_codigo_casa_matriz').is(':checked');
				if(mplsInstalacionConCasaMatriz){
					var mplsInstalacionNumeroCasaMatriz = $('#mpls_instalacion_codigo_casa_matriz').val();
					if(mplsInstalacionNumeroCasaMatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS/Instalación', 'Debe ingresar código de casa matriz 1.');
						$('#mpls_instalacion_codigo_casa_matriz').addClass('panel-bordered-danger');
						$('#mpls_instalacion_codigo_casa_matriz').select();
						$('#mpls_instalacion_codigo_casa_matriz').focus();
						return false;
					}
				}

				for(var i=1;i<=cantidad2;i++){
					var mpls_numero_oft = $.trim($('#mpls_numero_oft_1_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_1_'+i).val());
					//var mpls_casamatriz = $.trim($('#mpls_casamatriz_1_'+i).val());
					var mpls_precioobjetivo = $.trim($('#mpls_precioobjetivo_1_'+i).val());
					var mpls_plazo = $.trim($('#mpls_plazo_1_'+i).val());

					if(mpls_numero_oft.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de Oft.');
						$('#mpls_numero_oft_1_'+i).addClass('panel-bordered-danger');
						$('#mpls_numero_oft_1_'+i).select();
						$('#mpls_numero_oft_1_'+i).focus();
						return false;
					}

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_1_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_1_'+i).select();
						$('#mpls_velocidad_1_'+i).focus();
						return false;
					}

					/*
					if(mpls_casamatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el Código de servicio de la Casa Matriz.');
						$('#mpls_casamatriz_1_'+i).addClass('panel-bordered-danger');
						return false;
					}
					*/

					if(mpls_precioobjetivo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el precio objetivo.');
						$('#mpls_precioobjetivo_1_'+i).addClass('panel-bordered-danger');
						$('#mpls_precioobjetivo_1_'+i).select();
						$('#mpls_precioobjetivo_1_'+i).focus();
						return false;
					}

					if(mpls_plazo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el plazo en número de meses.');
						$('#mpls_plazo_1_'+i).addClass('panel-bordered-danger');
						$('#mpls_plazo_1_'+i).select();
						$('#mpls_plazo_1_'+i).focus();
						return false;
					}
				}
			}
			
			/* Upgrade */
			if(cantidad3.length>0 && cantidad3>0){
				for(var i=1;i<=cantidad3;i++){		
					var mpls_codigo_servicio = $.trim($('#mpls_codigo_servicio_2_'+i).val());
					var mpls_usv_vigente = $.trim($('#mpls_usv_vigente_2_'+i).val());
					var mpls_numero_oft = $.trim($('#mpls_numero_oft_2_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_2_'+i).val());
					var mpls_casamatriz = $.trim($('#mpls_casamatriz_2_'+i).val());
					var mpls_cta_factura = $.trim($('#mpls_cta_factura_2_'+i).val());
				

					if(mpls_codigo_servicio.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el código de servicio.');
						$('#mpls_codigo_servicio_2_'+i).addClass('panel-bordered-danger');
						$('#mpls_codigo_servicio_2_'+i).select();
						$('#mpls_codigo_servicio_2_'+i).focus();
						return false;
					}

					if(mpls_usv_vigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de USV vigente.');
						$('#mpls_usv_vigente_2_'+i).addClass('panel-bordered-danger');
						$('#mpls_usv_vigente_2_'+i).select();
						$('#mpls_usv_vigente_2_'+i).focus();
						return false;
					}

					if(mpls_numero_oft.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de Oft.');
						$('#mpls_numero_oft_2_'+i).addClass('panel-bordered-danger');
						$('#mpls_numero_oft_2_'+i).select();
						$('#mpls_numero_oft_2_'+i).focus();
						return false;
					}

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_2_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_2_'+i).select();
						$('#mpls_velocidad_2_'+i).focus();
						return false;
					}

					if(mpls_casamatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el Código de servicio de la Casa Matriz.');
						$('#mpls_casamatriz_2_'+i).addClass('panel-bordered-danger');
						$('#mpls_casamatriz_2_'+i).select();
						$('#mpls_casamatriz_2_'+i).focus();
						return false;
					}

					/*
						if(idcuentafacturacion.length==0){
							$('#menuPrincipal li:eq(0) a').tab('show');
							alertFail('Internet Dedicado', 'Debe ingresar la cuenta de facturación.');
							$('#idcuentafacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
					*/
				}
			}

			/* Downgrade */
			if(cantidad4.length>0 && cantidad4>0){
				for(var i=1;i<=cantidad4;i++){
					var mpls_codigo_servicio = $.trim($('#mpls_codigo_servicio_6_'+i).val());
					var mpls_usv_vigente = $.trim($('#mpls_usv_vigente_6_'+i).val());
					var mpls_numero_oft = $.trim($('#mpls_numero_oft_6_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_6_'+i).val());
					var mpls_casamatriz = $.trim($('#mpls_casamatriz_6_'+i).val());
					var mpls_cta_factura = $.trim($('#mpls_cta_factura_6_'+i).val());
				
					if(mpls_codigo_servicio.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el código de servicio.');
						$('#mpls_codigo_servicio_6_'+i).addClass('panel-bordered-danger');
						$('#mpls_codigo_servicio_6_'+i).select();
						$('#mpls_codigo_servicio_6_'+i).focus();
						return false;
					}

					if(mpls_usv_vigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de USV vigente.');
						$('#mpls_usv_vigente_6_'+i).addClass('panel-bordered-danger');
						$('#mpls_usv_vigente_6_'+i).select();
						$('#mpls_usv_vigente_6_'+i).focus();
						return false;
					}

					/*
					if(mpls_numero_oft.length==0){
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de Oft.');
						$('#mpls_numero_oft_6_'+i).addClass('panel-bordered-danger');
						return false;
					}
					*/

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_6_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_6_'+i).select();
						$('#mpls_velocidad_6_'+i).focus();
						return false;
					}

					if(mpls_casamatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el Código de servicio de la Casa Matriz.');
						$('#mpls_cta_factura_6_'+i).addClass('panel-bordered-danger');
						$('#mpls_cta_factura_6_'+i).select();
						$('#mpls_cta_factura_6_'+i).focus();
						return false;
					}
				}
			}

			if(cantidad5.length>0 && cantidad5>0){
				for(var i=1;i<=cantidad5;i++){
					var mpls_codigo_servicio = $.trim($('#mpls_codigo_servicio_3_'+i).val());
					var mpls_usv_vigente = $.trim($('#mpls_usv_vigente_3_'+i).val());
					var mpls_numero_oft = $.trim($('#mpls_numero_oft_3_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_3_'+i).val());
					var mpls_casamatriz = $.trim($('#mpls_casamatriz_3_'+i).val());
					var mpls_cta_factura = $.trim($('#mpls_cta_factura_3_'+i).val());

					if(mpls_codigo_servicio.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el código de servicio.');
						$('#mpls_codigo_servicio_3_'+i).addClass('panel-bordered-danger');
						$('#mpls_codigo_servicio_3_'+i).select();
						$('#mpls_codigo_servicio_3_'+i).focus();
						return false;
					}

					if(mpls_usv_vigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de USV vigente.');
						$('#mpls_usv_vigente_3_'+i).addClass('panel-bordered-danger');
						$('#mpls_usv_vigente_3_'+i).select();
						$('#mpls_usv_vigente_3_'+i).focus();
						return false;
					}

					if(mpls_numero_oft.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de Oft.');
						$('#mpls_numero_oft_3_'+i).addClass('panel-bordered-danger');
						$('#mpls_numero_oft_3_'+i).select();
						$('#mpls_numero_oft_3_'+i).focus();
						return false;
					}

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_3_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_3_'+i).select();
						$('#mpls_velocidad_3_'+i).focus();
						return false;
					}

					if(mpls_casamatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el Código de servicio de la Casa Matriz.');
						$('#mpls_casamatriz_3_'+i).addClass('panel-bordered-danger');
						$('#mpls_casamatriz_3_'+i).select();
						$('#mpls_casamatriz_3_'+i).focus();
						return false;
					}
				}
			}
				
			if(cantidad6.length>0 && cantidad6>0){
				for(var i=1;i<=cantidad6;i++){
					var mpls_codigo_servicio = $.trim($('#mpls_codigo_servicio_4_'+i).val());
					var mpls_usv_vigente = $.trim($('#mpls_usv_vigente_4_'+i).val());
					var mpls_numero_oft = $.trim($('#mpls_numero_oft_4_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_4_'+i).val());
					var mpls_casamatriz = $.trim($('#mpls_casamatriz_4_'+i).val());
					var mpls_cta_factura = $.trim($('#mpls_cta_factura_4_'+i).val());
				
					if(mpls_codigo_servicio.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el código de servicio.');
						$('#mpls_codigo_servicio_4_'+i).addClass('panel-bordered-danger');
						$('#mpls_codigo_servicio_4_'+i).select();
						$('#mpls_codigo_servicio_4_'+i).focus();
						return false;
					}

					if(mpls_usv_vigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de USV vigente.');
						$('#mpls_usv_vigente_4_'+i).addClass('panel-bordered-danger');
						$('#mpls_usv_vigente_4_'+i).select();
						$('#mpls_usv_vigente_4_'+i).focus();
						return false;
					}

					if(mpls_numero_oft.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de Oft.');
						$('#mpls_numero_oft_4_'+i).addClass('panel-bordered-danger');
						$('#mpls_numero_oft_4_'+i).select();
						$('#mpls_numero_oft_4_'+i).focus();
						return false;
					}

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_4_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_4_'+i).select();
						$('#mpls_velocidad_4_'+i).focus();
						return false;
					}

					if(mpls_casamatriz.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el Código de servicio de la Casa Matriz.');
						$('#mpls_casamatriz_4_'+i).addClass('panel-bordered-danger');
						$('#mpls_casamatriz_4_'+i).select();
						$('#mpls_casamatriz_4_'+i).focus();
						return false;
					}
				}
			}

			if(cantidad7.length>0 && cantidad7>0){
				for(var i=1;i<=cantidad7;i++){
					var mpls_codigo_servicio = $.trim($('#mpls_codigo_servicio_5_'+i).val());
					var mpls_usv_vigente = $.trim($('#mpls_usv_vigente_5_'+i).val());
					var mpls_velocidad = $.trim($('#mpls_velocidad_5_'+i).val());
					var mpls_precioobjetivo = $.trim($('#mpls_precioobjetivo_5_'+i).val());
					var mpls_plazo = $.trim($('#mpls_plazo_5_'+i).val());
					var mpls_cta_factura = $.trim($('#mpls_cta_factura_5_'+i).val());
				
					if(mpls_codigo_servicio.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el código de servicio.');
						$('#mpls_codigo_servicio_5_'+i).addClass('panel-bordered-danger');
						$('#mpls_codigo_servicio_5_'+i).select();
						$('#mpls_codigo_servicio_5_'+i).focus();
						return false;
					}

					if(mpls_usv_vigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el número de USV vigente.');
						$('#mpls_usv_vigente_5_'+i).addClass('panel-bordered-danger');
						$('#mpls_usv_vigente_5_'+i).select();
						$('#mpls_usv_vigente_5_'+i).focus();
						return false;
					}

					if(mpls_velocidad.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar la velocidad.');
						$('#mpls_velocidad_5_'+i).addClass('panel-bordered-danger');
						$('#mpls_velocidad_5_'+i).select();
						$('#mpls_velocidad_5_'+i).focus();
						return false;
					}

					if(mpls_precioobjetivo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el precio objetivo.');
						$('#mpls_precioobjetivo_5_'+i).addClass('panel-bordered-danger');
						$('#mpls_precioobjetivo_5_'+i).select();
						$('#mpls_precioobjetivo_5_'+i).focus();
						return false;
					}

					if(mpls_plazo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(1) a').tab('show');
						alertFail('MPLS', 'Debe ingresar el plazo en número de meses.');
						$('#mpls_plazo_5_'+i).addClass('panel-bordered-danger');
						$('#mpls_plazo_5_'+i).select();
						$('#mpls_plazo_5_'+i).focus();
						return false;
					}
				}
			}
		}	

		/********************** TRUNCK CD ************************/
		if(servicio_seleccionado==3){
			if(cantidad8.length>0 && cantidad8>0){
				for(var i=1;i<=cantidad8;i++){
					
					//Solicitud
						var tcd_inst_solicitud_oft = $.trim($('#tcd_inst_solicitud_oft_'+i).val());
						if(tcd_inst_solicitud_oft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Solicitud', 'Debe ingresar el número de OFT.');
							$('#tcd_inst_solicitud_oft_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_solicitud_oft_'+i).select();
							$('#tcd_inst_solicitud_oft_'+i).focus();
							return false;
						}

						var checkIsCentral = $('#isTcdCentralHomecheck_'+i).is(':checked');
						if(!checkIsCentral){
							var numeroDeCasaMatrizCDInstalacion = $('#isTcdCentralHomeNumber_'+i).val();
							if(numeroDeCasaMatrizCDInstalacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Instalación/Solicitud', 'Debe ingresar la código de la casa matriz.');
								$('#isTcdCentralHomeNumber_'+i).addClass('panel-bordered-danger');
								$('#isTcdCentralHomeNumber_'+i).select();
								$('#isTcdCentralHomeNumber_'+i).focus();
								return false;
							}
						}
					
					//Telefonia
						var tcd_inst_telefonia_cantCanales = $.trim($('#tcd_inst_telefonia_cantCanales_'+i).val());
						if(tcd_inst_telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Teléfonia', 'Debe ingresar la cantidad de canales.');
							$('#tcd_inst_telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_telefonia_cantCanales_'+i).select();
							$('#tcd_inst_telefonia_cantCanales_'+i).focus();
							return false;
						}

						var tcd_inst_telefonia_minSlm = $.trim($('#tcd_inst_telefonia_minSlm_'+i).val());
						if(tcd_inst_telefonia_minSlm.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Teléfonia', 'Debe ingresar la cantidad de minutos SLM.');
							$('#tcd_inst_telefonia_minSlm_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_telefonia_minSlm_'+i).select();
							$('#tcd_inst_telefonia_minSlm_'+i).focus();
							return false;
						}

						var tcd_inst_telefonia_minMovil = $.trim($('#tcd_inst_telefonia_minMovil_'+i).val());
						if(tcd_inst_telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Teléfonia', 'Debe ingresar la cantidad de minutos Móvil.');
							$('#tcd_inst_telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_telefonia_minMovil_'+i).select();
							$('#tcd_inst_telefonia_minMovil_'+i).focus();
							return false;
						}

						var tcd_inst_telefonia_minAdicional = $.trim($('#tcd_inst_telefonia_minAdicional_'+i).val());
						if(tcd_inst_telefonia_minAdicional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Teléfonia', 'Debe ingresar la cantidad de minutos adicionales.');
							$('#tcd_inst_telefonia_minAdicional_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_telefonia_minAdicional_'+i).select();
							$('#tcd_inst_telefonia_minAdicional_'+i).focus();
							return false;
						}


					//Interet Dedicado
						var tcd_inst_InternDedicado_minNacional = $.trim($('#tcd_inst_InternDedicado_minNacional_'+i).val());
						if(tcd_inst_InternDedicado_minNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Interet Dedicado', 'Debe ingresar los minutos nacionales.');
							$('#tcd_inst_InternDedicado_minNacional_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_InternDedicado_minNacional_'+i).select();
							$('#tcd_inst_InternDedicado_minNacional_'+i).focus();
							return false;
						}

						var tcd_inst_InternDedicado_minInteracional = $.trim($('#tcd_inst_InternDedicado_minInteracional_'+i).val());
						if(tcd_inst_InternDedicado_minInteracional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Interet Dedicado', 'Debe ingresar los minutos internacionales.');
							$('#tcd_inst_InternDedicado_minInteracional_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_InternDedicado_minInteracional_'+i).select();
							$('#tcd_inst_InternDedicado_minInteracional_'+i).focus();
							return false;
						}

						var tcd_inst_InternDedicado_precioObjetivoNacional = $.trim($('#tcd_inst_InternDedicado_precioObjetivoNacional_'+i).val());
						if(tcd_inst_InternDedicado_precioObjetivoNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Interet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#tcd_inst_InternDedicado_precioObjetivoNacional_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_InternDedicado_precioObjetivoNacional_'+i).select();
							$('#tcd_inst_InternDedicado_precioObjetivoNacional_'+i).focus();
							return false;
						}

						var tcd_inst_InternDedicado_plazo = $.trim($('#tcd_inst_InternDedicado_plazo_'+i).val());
						if(tcd_inst_InternDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Interet Dedicado', 'Debe ingresar el plazo.');
							$('#tcd_inst_InternDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_InternDedicado_plazo_'+i).select();
							$('#tcd_inst_InternDedicado_plazo_'+i).focus();
							return false;
						}

					//Datos Velocidad
						var tcd_inst_datos_velocidad = $.trim($('#tcd_inst_datos_velocidad_'+i).val());
						if(tcd_inst_datos_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Datos Velocidad', 'Debe ingresar la velocidad.');
							$('#tcd_inst_datos_velocidad_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_datos_velocidad_'+i).select();
							$('#tcd_inst_datos_velocidad_'+i).focus();
							return false;
						}

						var tcd_inst_datos_codServicio = $.trim($('#tcd_inst_datos_codServicio_'+i).val());
						if(tcd_inst_datos_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Datos Velocidad', 'Debe ingresar el código de servicio.');
							$('#tcd_inst_datos_codServicio_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_datos_codServicio_'+i).select();
							$('#tcd_inst_datos_codServicio_'+i).focus();
							return false;
						}

						var tcd_inst_datos_precioObjetivo = $.trim($('#tcd_inst_datos_precioObjetivo_'+i).val());
						if(tcd_inst_datos_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Datos Velocidad', 'Debe ingresar el precio objetivo.');
							$('#tcd_inst_datos_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_datos_precioObjetivo_'+i).select();
							$('#tcd_inst_datos_precioObjetivo_'+i).focus();
							return false;
						}

						var tcd_inst_datos_plazo = $.trim($('#tcd_inst_datos_plazo_'+i).val());
						if(tcd_inst_datos_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Instalación/Datos Velocidad', 'Debe ingresar el plazo.');
							$('#tcd_inst_datos_plazo_'+i).addClass('panel-bordered-danger');
							$('#tcd_inst_datos_plazo_'+i).select();
							$('#tcd_inst_datos_plazo_'+i).focus();
							return false;
						}
				}	
			}	
			
			if(cantidad9.length>0 && cantidad9>0){
				if(cantidad91.length>0 && cantidad91>0){
					for(var i=1;i<=cantidad91;i++){
						
						var trunckCD_upgrade_telefonia_numOft = $.trim($('#trunckCD_upgrade_telefonia_numOft_'+i).val());
						if(trunckCD_upgrade_telefonia_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Updrade/Telefonía', 'Debe ingresar el numero de OFT.');
							$('#trunckCD_upgrade_telefonia_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_numOft_'+i).select();
							$('#trunckCD_upgrade_telefonia_numOft_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_telefonia_CodServicio = $.trim($('#trunckCD_upgrade_telefonia_CodServicio_'+i).val());
						if(trunckCD_upgrade_telefonia_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar el código de servicio.');
							$('#trunckCD_upgrade_telefonia_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_CodServicio_'+i).select();
							$('#trunckCD_upgrade_telefonia_CodServicio_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_telefonia_contVigente = $.trim($('#trunckCD_upgrade_telefonia_contVigente_'+i).val());
						if(trunckCD_upgrade_telefonia_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_upgrade_telefonia_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_contVigente_'+i).select();
							$('#trunckCD_upgrade_telefonia_contVigente_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_telefonia_ctaFacturacion = $.trim($('#trunckCD_upgrade_telefonia_ctaFacturacion_'+i).val());
						if(trunckCD_upgrade_telefonia_ctaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la Cuenta Facturación.');
							$('#trunckCD_upgrade_telefonia_ctaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_ctaFacturacion_'+i).select();
							$('#trunckCD_upgrade_telefonia_ctaFacturacion_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_telefonia_cantCanales_actual = $.trim($('#trunckCD_upgrade_telefonia_cantCanales_actual_'+i).val());
						if(trunckCD_upgrade_telefonia_cantCanales_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de canales actuales.');
							$('#trunckCD_upgrade_telefonia_cantCanales_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_cantCanales_actual_'+i).select();
							$('#trunckCD_upgrade_telefonia_cantCanales_actual_'+i).focus();
							return false;
						}		

						var trunckCD_upgrade_telefonia_cantCanales_solicitada = $.trim($('#trunckCD_upgrade_telefonia_cantCanales_solicitada_'+i).val());
						if(trunckCD_upgrade_telefonia_cantCanales_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de canales solicitada.');
							$('#trunckCD_upgrade_telefonia_cantCanales_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_cantCanales_solicitada_'+i).select();
							$('#trunckCD_upgrade_telefonia_cantCanales_solicitada_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_telefonia_minSLM_actual = $.trim($('#trunckCD_upgrade_telefonia_minSLM_actual_'+i).val());
						if(trunckCD_upgrade_telefonia_minSLM_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM actaules.');
							$('#trunckCD_upgrade_telefonia_minSLM_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_minSLM_actual_'+i).select();
							$('#trunckCD_upgrade_telefonia_minSLM_actual_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_telefonia_minSLM_solicitada = $.trim($('#trunckCD_upgrade_telefonia_minSLM_solicitada_'+i).val());
						if(trunckCD_upgrade_telefonia_minSLM_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM solicitada.');
							$('#trunckCD_upgrade_telefonia_minSLM_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_minSLM_solicitada_'+i).select();
							$('#trunckCD_upgrade_telefonia_minSLM_solicitada_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_telefonia_minMovil_actual = $.trim($('#trunckCD_upgrade_telefonia_minMovil_actual_'+i).val());
						if(trunckCD_upgrade_telefonia_minMovil_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos moviles actuales.');
							$('#trunckCD_upgrade_telefonia_minMovil_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_minMovil_actual_'+i).select();
							$('#trunckCD_upgrade_telefonia_minMovil_actual_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_telefonia_minMovil_solicitada = $.trim($('#trunckCD_upgrade_telefonia_minMovil_solicitada_'+i).val());
						if(trunckCD_upgrade_telefonia_minMovil_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos moviles solicitada.');
							$('#trunckCD_upgrade_telefonia_minMovil_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_telefonia_minMovil_solicitada_'+i).select();
							$('#trunckCD_upgrade_telefonia_minMovil_solicitada_'+i).focus();
							return false;
						}

						/*
							var trunckCD_upgrade_telefonia_comentario = $.trim($('#trunckCD_upgrade_telefonia_comentario_'+i).val());
							if(trunckCD_upgrade_telefonia_minSLM_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM solicitada.');
								$('#trunckCD_upgrade_telefonia_minSLM_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}	
				}

				if(cantidad92.length>0 && cantidad92>0){
					for(var i=1;i<=cantidad92;i++){
						
						var trunckCD_upgrade_InternetDedicado_numOft = $.trim($('#trunckCD_upgrade_InternetDedicado_numOft_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar el número de OFT.');
							$('#trunckCD_upgrade_InternetDedicado_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_numOft_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_numOft_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_InternetDedicado_CodServicio = $.trim($('#trunckCD_upgrade_InternetDedicado_CodServicio_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#trunckCD_upgrade_InternetDedicado_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_CodServicio_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_CodServicio_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_InternetDedicado_contVigente = $.trim($('#trunckCD_upgrade_InternetDedicado_contVigente_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_upgrade_InternetDedicado_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_contVigente_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_contVigente_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_InternetDedicado_ctaFacturacion = $.trim($('#trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_ctaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar la Cuenta Facturación.');
							$('#trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_InternetDedicado_velNacional_solicitada = $.trim($('#trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_velNacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar velocidad nacional solicitada.');
							$('#trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_InternetDedicado_velInternacional_solicitada = $.trim($('#trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).val());
						if(trunckCD_upgrade_InternetDedicado_velInternacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
							$('#trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).select();
							$('#trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).focus();
							return false;
						}

						/*
							var trunckCD_upgrade_InternetDedicado_comentario_solicitada = $.trim($('#trunckCD_upgrade_InternetDedicado_comentario_solicitada_'+i).val());
							if(trunckCD_upgrade_InternetDedicado_comentario_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Upgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
								$('#trunckCD_upgrade_InternetDedicado_comentario_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}
				}

				if(cantidad93.length>0 && cantidad93>0){
					for(var i=1;i<=cantidad93;i++){

						var trunckCD_upgrade_accesoMPLS_numOft = $.trim($('#trunckCD_upgrade_accesoMPLS_numOft_'+i).val());
						if(trunckCD_upgrade_accesoMPLS_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar el número de OFT.');
							$('#trunckCD_upgrade_accesoMPLS_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_accesoMPLS_numOft_'+i).select();
							$('#trunckCD_upgrade_accesoMPLS_numOft_'+i).focus();
							return false;
						}	

						var trunckCD_upgrade_accesoMPLS_codServicio = $.trim($('#trunckCD_upgrade_accesoMPLS_codServicio_'+i).val());
						if(trunckCD_upgrade_accesoMPLS_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar el código de servicio.');
							$('#trunckCD_upgrade_accesoMPLS_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_accesoMPLS_codServicio_'+i).select();
							$('#trunckCD_upgrade_accesoMPLS_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_accesoMPLS_contratoVigente = $.trim($('#trunckCD_upgrade_accesoMPLS_contratoVigente_'+i).val());
						if(trunckCD_upgrade_accesoMPLS_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_upgrade_accesoMPLS_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_accesoMPLS_contratoVigente_'+i).select();
							$('#trunckCD_upgrade_accesoMPLS_contratoVigente_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_accesoMPLS_cuentaFacturacion = $.trim($('#trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+i).val());
						if(trunckCD_upgrade_accesoMPLS_cuentaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar la cuenta de facturación.');
							$('#trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+i).select();
							$('#trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+i).focus();
							return false;
						}

						var trunckCD_upgrade_accesoMPLS_velocidad = $.trim($('#trunckCD_upgrade_accesoMPLS_velocidad_'+i).val());
						if(trunckCD_upgrade_accesoMPLS_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar la velocidad solicitada.');
							$('#trunckCD_upgrade_accesoMPLS_velocidad_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_upgrade_accesoMPLS_velocidad_'+i).select();
							$('#trunckCD_upgrade_accesoMPLS_velocidad_'+i).focus();
							return false;
						}

						/*
							var trunckCD_upgrade_accesoMPLS_comentario = $.trim($('#trunckCD_upgrade_accesoMPLS_comentario_'+i).val());
							if(trunckCD_upgrade_accesoMPLS_comentario.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Upgrade/acceso MPLS', 'Debe ingresar la cuenta de facturación.');
								$('#trunckCD_upgrade_accesoMPLS_comentario_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}	
				}
			}

			if(cantidad10.length>0 && cantidad10>0){

				if(cantidad101.length>0 && cantidad101>0){
					for(var i=1;i<=cantidad101;i++){
						
						var trunckCD_downgrade_telefonia_numOft = $.trim($('#trunckCD_downgrade_telefonia_numOft_'+i).val());
						if(trunckCD_downgrade_telefonia_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar el número de OFT.');
							$('#trunckCD_downgrade_telefonia_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_numOft_'+i).select();
							$('#trunckCD_downgrade_telefonia_numOft_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_CodServicio = $.trim($('#trunckCD_downgrade_telefonia_CodServicio_'+i).val());
						if(trunckCD_downgrade_telefonia_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar el código servicio.');
							$('#trunckCD_downgrade_telefonia_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_CodServicio_'+i).select();
							$('#trunckCD_downgrade_telefonia_CodServicio_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_contVigente = $.trim($('#trunckCD_downgrade_telefonia_contVigente_'+i).val());
						if(trunckCD_downgrade_telefonia_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_downgrade_telefonia_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_contVigente_'+i).select();
							$('#trunckCD_downgrade_telefonia_contVigente_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_ctaFacturacion = $.trim($('#trunckCD_downgrade_telefonia_ctaFacturacion_'+i).val());
						if(trunckCD_downgrade_telefonia_ctaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cuenta de Facturación.');
							$('#trunckCD_downgrade_telefonia_ctaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_ctaFacturacion_'+i).select();
							$('#trunckCD_downgrade_telefonia_ctaFacturacion_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_cantCanales_actual = $.trim($('#trunckCD_downgrade_telefonia_cantCanales_actual_'+i).val());
						if(trunckCD_downgrade_telefonia_cantCanales_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de canales actuales.');
							$('#trunckCD_downgrade_telefonia_cantCanales_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_cantCanales_actual_'+i).select();
							$('#trunckCD_downgrade_telefonia_cantCanales_actual_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_cantCanales_solicitada = $.trim($('#trunckCD_downgrade_telefonia_cantCanales_solicitada_'+i).val());
						if(trunckCD_downgrade_telefonia_cantCanales_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de canales solicitada.');
							$('#trunckCD_downgrade_telefonia_cantCanales_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_cantCanales_solicitada_'+i).select();
							$('#trunckCD_downgrade_telefonia_cantCanales_solicitada_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_minSLM_actual = $.trim($('#trunckCD_downgrade_telefonia_minSLM_actual_'+i).val());
						if(trunckCD_downgrade_telefonia_minSLM_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM actuales.');
							$('#trunckCD_downgrade_telefonia_minSLM_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_minSLM_actual_'+i).select();
							$('#trunckCD_downgrade_telefonia_minSLM_actual_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_minSLM_solicitada = $.trim($('#trunckCD_downgrade_telefonia_minSLM_solicitada_'+i).val());
						if(trunckCD_downgrade_telefonia_minSLM_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM solicitada.');
							$('#trunckCD_downgrade_telefonia_minSLM_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_minSLM_solicitada_'+i).select();
							$('#trunckCD_downgrade_telefonia_minSLM_solicitada_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_minMovil_actual = $.trim($('#trunckCD_downgrade_telefonia_minMovil_actual_'+i).val());
						if(trunckCD_downgrade_telefonia_minMovil_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles actuales.');
							$('#trunckCD_downgrade_telefonia_minMovil_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_minMovil_actual_'+i).select();
							$('#trunckCD_downgrade_telefonia_minMovil_actual_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_telefonia_minMovil_solicitada = $.trim($('#trunckCD_downgrade_telefonia_minMovil_solicitada_'+i).val());
						if(trunckCD_downgrade_telefonia_minMovil_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles solicitada.');
							$('#trunckCD_downgrade_telefonia_minMovil_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_telefonia_minMovil_solicitada_'+i).select();
							$('#trunckCD_downgrade_telefonia_minMovil_solicitada_'+i).focus();
							return false;
						}

						//var trunckCD_downgrade_telefonia_comentario = $.trim($('#trunckCD_downgrade_telefonia_comentario_'+i).val());
					}	
				}

				if(cantidad102.length>0 && cantidad102>0){
					for(var i=1;i<=cantidad102;i++){
						
						var trunckCD_downgrade_InternetDedicado_numOft = $.trim($('#trunckCD_downgrade_InternetDedicado_numOft_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar el número de OFT.');
							$('#trunckCD_downgrade_InternetDedicado_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_numOft_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_numOft_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_InternetDedicado_CodServicio = $.trim($('#trunckCD_downgrade_InternetDedicado_CodServicio_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar código de servicio.');
							$('#trunckCD_downgrade_InternetDedicado_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_CodServicio_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_CodServicio_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_InternetDedicado_contVigente = $.trim($('#trunckCD_downgrade_InternetDedicado_contVigente_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar el número USV Contrato Vigente.');
							$('#trunckCD_downgrade_InternetDedicado_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_contVigente_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_contVigente_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_InternetDedicado_ctaFacturacion = $.trim($('#trunckCD_downgrade_InternetDedicado_ctaFacturacion_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_ctaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar el número de Cuenta Facturación.');
							$('#trunckCD_downgrade_InternetDedicado_ctaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_ctaFacturacion_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_ctaFacturacion_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_InternetDedicado_velNacional_solicitada = $.trim($('#trunckCD_downgrade_InternetDedicado_velNacional_solicitada_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_velNacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad nacional solicitada.');
							$('#trunckCD_downgrade_InternetDedicado_velNacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_velNacional_solicitada_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_velNacional_solicitada_'+i).focus();
							return false;
						}

						var trunckCD_downgrade_InternetDedicado_velInternacional_solicitada = $.trim($('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).val());
						if(trunckCD_downgrade_InternetDedicado_velInternacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
							$('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).focus();
							return false;
						}

						/*
							var trunckCD_downgrade_InternetDedicado_comentario_solicitada = $.trim($('#trunckCD_downgrade_InternetDedicado_comentario_solicitada_'+i).val());
							if(trunckCD_downgrade_InternetDedicado_comentario_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
								$('#trunckCD_downgrade_InternetDedicado_comentario_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}
				}

				if(cantidad103.length>0 && cantidad103>0){
					for(var i=1;i<=cantidad103;i++){

					}	
				}
			}

			if(cantidad11.length>0 && cantidad11>0){
				for(var i=1;i<=cantidad11;i++){
					
					/* Solicitud */
						var trunckCD_Traslado_Solicitud_numOft = $.trim($('#trunckCD_Traslado_Solicitud_numOft_'+i).val());
						if(trunckCD_Traslado_Solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckCD_Traslado_Solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Solicitud_numOft_'+i).select();
							$('#trunckCD_Traslado_Solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Solicitud_codServicio = $.trim($('#trunckCD_Traslado_Solicitud_codServicio_'+i).val());
						if(trunckCD_Traslado_Solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Solicitud', 'Debe ingresar código de servicio.');
							$('#trunckCD_Traslado_Solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Solicitud_codServicio_'+i).select();
							$('#trunckCD_Traslado_Solicitud_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Solicitud_cuentaFacturacion = $.trim($('#trunckCD_Traslado_Solicitud_cuentaFacturacion_'+i).val()); 
						if(trunckCD_Traslado_Solicitud_cuentaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Solicitud', 'Debe ingresar la cuenta de facturación.');
							$('#trunckCD_Traslado_Solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Solicitud_cuentaFacturacion_'+i).select();
							$('#trunckCD_Traslado_Solicitud_cuentaFacturacion_'+i).focus();
							return false;
						}

					/* Telefonia */
						var trunckCD_Traslado_Telefonia_cantCanales = $.trim($('#trunckCD_Traslado_Telefonia_cantCanales_'+i).val());
						if(trunckCD_Traslado_Telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckCD_Traslado_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Telefonia_cantCanales_'+i).select();
							$('#trunckCD_Traslado_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Telefonia_minSLM = $.trim($('#trunckCD_Traslado_Telefonia_minSLM_'+i).val());
						if(trunckCD_Traslado_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckCD_Traslado_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Telefonia_minSLM_'+i).select();
							$('#trunckCD_Traslado_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Telefonia_minMovil = $.trim($('#trunckCD_Traslado_Telefonia_minMovil_'+i).val());
						if(trunckCD_Traslado_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckCD_Traslado_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Telefonia_minMovil_'+i).select();
							$('#trunckCD_Traslado_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Telefonia_minAdicional = $.trim($('#trunckCD_Traslado_Telefonia_minAdicional_'+i).val());
						if(trunckCD_Traslado_Telefonia_minAdicional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Telefonía', 'Debe ingresar los minutos adicionales.');
							$('#trunckCD_Traslado_Telefonia_minAdicional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Telefonia_minAdicional_'+i).select();
							$('#trunckCD_Traslado_Telefonia_minAdicional_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckCD_Traslado_InternetDedicado_velNacional = $.trim($('#trunckCD_Traslado_InternetDedicado_velNacional_'+i).val());
						if(trunckCD_Traslado_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckCD_Traslado_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_InternetDedicado_velNacional_'+i).select();
							$('#trunckCD_Traslado_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_InternetDedicado_velInternacional = $.trim($('#trunckCD_Traslado_InternetDedicado_velInternacional_'+i).val());
						if(trunckCD_Traslado_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckCD_Traslado_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_InternetDedicado_velInternacional_'+i).select();
							$('#trunckCD_Traslado_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_InternetDedicado_precioObjetivo = $.trim($('#trunckCD_Traslado_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckCD_Traslado_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_Traslado_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckCD_Traslado_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_InternetDedicado_plazo = $.trim($('#trunckCD_Traslado_InternetDedicado_plazo_'+i).val());
						if(trunckCD_Traslado_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckCD_Traslado_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_InternetDedicado_plazo_'+i).select();
							$('#trunckCD_Traslado_InternetDedicado_plazo_'+i).focus();
							return false;
						}

					/* Datos */
						var trunckCD_Traslado_Datos_velocidad = $.trim($('#trunckCD_Traslado_Datos_velocidad_'+i).val());
						if(trunckCD_Traslado_Datos_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Datos', 'Debe ingresar la velocidad.');
							$('#trunckCD_Traslado_Datos_velocidad_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Datos_velocidad_'+i).select();
							$('#trunckCD_Traslado_Datos_velocidad_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Datos_codServicio = $.trim($('#trunckCD_Traslado_Datos_codServicio_'+i).val());
						if(trunckCD_Traslado_Datos_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#trunckCD_Traslado_Datos_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Datos_codServicio_'+i).select();
							$('#trunckCD_Traslado_Datos_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Datos_precioObjetivo = $.trim($('#trunckCD_Traslado_Datos_precioObjetivo_'+i).val());
						if(trunckCD_Traslado_Datos_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_Traslado_Datos_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Datos_precioObjetivo_'+i).select();
							$('#trunckCD_Traslado_Datos_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_Traslado_Datos_plazo = $.trim($('#trunckCD_Traslado_Datos_plazo_'+i).val());
						if(trunckCD_Traslado_Datos_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Traslado/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckCD_Traslado_Datos_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_Traslado_Datos_plazo_'+i).select();
							$('#trunckCD_Traslado_Datos_plazo_'+i).focus();
							return false;
						}
				}	
			}	

			if(cantidad12.length>0 && cantidad12>0){
				for(var i=1;i<=cantidad12;i++){
					
					/* Solicitud */
						var trunckCD_cambio_acceso_solicitud_numOft = $.trim($('#trunckCD_cambio_acceso_solicitud_numOft_'+i).val());
						if(trunckCD_cambio_acceso_solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckCD_cambio_acceso_solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_solicitud_numOft_'+i).select();
							$('#trunckCD_cambio_acceso_solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_solicitud_codServicio = $.trim($('#trunckCD_cambio_acceso_solicitud_codServicio_'+i).val());
						if(trunckCD_cambio_acceso_solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckCD_cambio_acceso_solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_solicitud_codServicio_'+i).select();
							$('#trunckCD_cambio_acceso_solicitud_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_solicitud_contratoVigente = $.trim($('#trunckCD_cambio_acceso_solicitud_contratoVigente_'+i).val()); 
						if(trunckCD_cambio_acceso_solicitud_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/solicitud', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_cambio_acceso_solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_solicitud_contratoVigente_'+i).select();
							$('#trunckCD_cambio_acceso_solicitud_contratoVigente_'+i).focus();
							return false;
						}

						/*
							var trunckCD_cambio_acceso_solicitud_cuentaFacturacion = $.trim($('#trunckCD_cambio_acceso_solicitud_cuentaFacturacion_'+i).val()); 
							if(trunckCD_cambio_acceso_solicitud_cuentaFacturacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Cambio Acceso/solicitud', 'Debe ingresar la cuenta de facturación.');
								$('#trunckCD_cambio_acceso_solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/

					/* Telefonia */
						var trunckCD_cambio_acceso_Telefonia_cantCanales = $.trim($('#trunckCD_cambio_acceso_Telefonia_cantCanales_'+i).val());
						if(trunckCD_cambio_acceso_Telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckCD_cambio_acceso_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Telefonia_cantCanales_'+i).select();
							$('#trunckCD_cambio_acceso_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Telefonia_minSLM = $.trim($('#trunckCD_cambio_acceso_Telefonia_minSLM_'+i).val());
						if(trunckCD_cambio_acceso_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Telefonía', 'Debe ingresar la cantidad de minutos SLM.');
							$('#trunckCD_cambio_acceso_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Telefonia_minSLM_'+i).select();
							$('#trunckCD_cambio_acceso_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Telefonia_minMovil = $.trim($('#trunckCD_cambio_acceso_Telefonia_minMovil_'+i).val());
						if(trunckCD_cambio_acceso_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Telefonía', 'Debe ingresar la cantidad de minutos móviles.');
							$('#trunckCD_cambio_acceso_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Telefonia_minMovil_'+i).select();
							$('#trunckCD_cambio_acceso_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Telefonia_contratoVigente = $.trim($('#trunckCD_cambio_acceso_Telefonia_contratoVigente_'+i).val());
						if(trunckCD_cambio_acceso_Telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_cambio_acceso_Telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Telefonia_contratoVigente_'+i).select();
							$('#trunckCD_cambio_acceso_Telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Telefonia_contratoTelefonia = $.trim($('#trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+i).val());
						if(trunckCD_cambio_acceso_Telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+i).select();
							$('#trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckCD_cambio_acceso_internetDedicado_velNacional = $.trim($('#trunckCD_cambio_acceso_internetDedicado_velNacional_'+i).val());
						if(trunckCD_cambio_acceso_internetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckCD_cambio_acceso_internetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_internetDedicado_velNacional_'+i).select();
							$('#trunckCD_cambio_acceso_internetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_internetDedicado_velInternacional = $.trim($('#trunckCD_cambio_acceso_internetDedicado_velInternacional_'+i).val());
						if(trunckCD_cambio_acceso_internetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckCD_cambio_acceso_internetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_internetDedicado_velInternacional_'+i).select();
							$('#trunckCD_cambio_acceso_internetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_internetDedicado_precioObjetivo = $.trim($('#trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+i).val());
						if(trunckCD_cambio_acceso_internetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+i).select();
							$('#trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_internetDedicado_plazo = $.trim($('#trunckCD_cambio_acceso_internetDedicado_plazo_'+i).val());
						if(trunckCD_cambio_acceso_internetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckCD_cambio_acceso_internetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_internetDedicado_plazo_'+i).select();
							$('#trunckCD_cambio_acceso_internetDedicado_plazo_'+i).focus();
							return false;
						}

					/* Datos */
						var trunckCD_cambio_acceso_Datos_velocidad = $.trim($('#trunckCD_cambio_acceso_Datos_velocidad_'+i).val());
						if(trunckCD_cambio_acceso_Datos_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Datos', 'Debe ingresar la velocidad.');
							$('#trunckCD_cambio_acceso_Datos_velocidad_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Datos_velocidad_'+i).select();
							$('#trunckCD_cambio_acceso_Datos_velocidad_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Datos_codServicio = $.trim($('#trunckCD_cambio_acceso_Datos_codServicio_'+i).val());
						if(trunckCD_cambio_acceso_Datos_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Datos', 'Debe ingresar el código de servicio.');
							$('#trunckCD_cambio_acceso_Datos_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Datos_codServicio_'+i).select();
							$('#trunckCD_cambio_acceso_Datos_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Datos_precioObjetivo = $.trim($('#trunckCD_cambio_acceso_Datos_precioObjetivo_'+i).val());
						if(trunckCD_cambio_acceso_Datos_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Datos', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_cambio_acceso_Datos_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Datos_precioObjetivo_'+i).select();
							$('#trunckCD_cambio_acceso_Datos_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_cambio_acceso_Datos_plazo = $.trim($('#trunckCD_cambio_acceso_Datos_plazo_'+i).val());
						if(trunckCD_cambio_acceso_Datos_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Acceso/Datos', 'Debe ingresar el plazo.');
							$('#trunckCD_cambio_acceso_Datos_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambio_acceso_Datos_plazo_'+i).select();
							$('#trunckCD_cambio_acceso_Datos_plazo_'+i).focus();
							return false;
						}

				}	
			}	
			
			if(cantidad13.length>0 && cantidad13>0){
				for(var i=1;i<=cantidad13;i++){
					
					/* Solicitud */
						var trunckCD_renegociacion_solicitud_numOft = $.trim($('#trunckCD_renegociacion_solicitud_numOft_'+i).val());
						if(trunckCD_renegociacion_solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckCD_renegociacion_solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_solicitud_numOft_'+i).select();
							$('#trunckCD_renegociacion_solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_solicitud_codServicio = $.trim($('#trunckCD_renegociacion_solicitud_codServicio_'+i).val());
						if(trunckCD_renegociacion_solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckCD_renegociacion_solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_solicitud_codServicio_'+i).select();
							$('#trunckCD_renegociacion_solicitud_codServicio_'+i).focus();
							return false;
						}	

						var trunckCD_renegociacion_solicitud_contratoVigente = $.trim($('#trunckCD_renegociacion_solicitud_contratoVigente_'+i).val()); 
						if(trunckCD_renegociacion_solicitud_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Solicitud', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_renegociacion_solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_solicitud_contratoVigente_'+i).select();
							$('#trunckCD_renegociacion_solicitud_contratoVigente_'+i).focus();
							return false;
						}	

						/*
						var trunckCD_renegociacion_solicitud_cuentaFacturacion = $.trim($('#trunckCD_renegociacion_solicitud_cuentaFacturacion_'+i).val()); 
						if(trunckCD_renegociacion_solicitud_cuentaFacturacion.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckCD_renegociacion_solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
							return false;
						}
						*/	

					/* Telefonia */
						var trunckCD_renegociacion_Telefonia_cantCanales = $.trim($('#trunckCD_renegociacion_Telefonia_cantCanales_'+i).val());
						if(trunckCD_renegociacion_Telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckCD_renegociacion_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_Telefonia_cantCanales_'+i).select();
							$('#trunckCD_renegociacion_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_Telefonia_minSLM = $.trim($('#trunckCD_renegociacion_Telefonia_minSLM_'+i).val());
						if(trunckCD_renegociacion_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Telefonía', 'Debe ingresar la cantidad de minutos SLM.');
							$('#trunckCD_renegociacion_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_Telefonia_minSLM_'+i).select();
							$('#trunckCD_renegociacion_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_Telefonia_minMovil = $.trim($('#trunckCD_renegociacion_Telefonia_minMovil_'+i).val());
						if(trunckCD_renegociacion_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Telefonía', 'Debe ingresar la cantidad de minutos móviles.');
							$('#trunckCD_renegociacion_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_Telefonia_minMovil_'+i).select();
							$('#trunckCD_renegociacion_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_Telefonia_contratoVigente = $.trim($('#trunckCD_renegociacion_Telefonia_contratoVigente_'+i).val());
						if(trunckCD_renegociacion_Telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_renegociacion_Telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_Telefonia_contratoVigente_'+i).select();
							$('#trunckCD_renegociacion_Telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_Telefonia_contratoTelefonia = $.trim($('#trunckCD_renegociacion_Telefonia_contratoTelefonia_'+i).val());
						if(trunckCD_renegociacion_Telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckCD_renegociacion_Telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_Telefonia_contratoTelefonia_'+i).select();
							$('#trunckCD_renegociacion_Telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckCD_renegociacion_InternetDedicado_velNacional = $.trim($('#trunckCD_renegociacion_InternetDedicado_velNacional_'+i).val());
						if(trunckCD_renegociacion_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckCD_renegociacion_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_InternetDedicado_velNacional_'+i).select();
							$('#trunckCD_renegociacion_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_InternetDedicado_velInternacional = $.trim($('#trunckCD_renegociacion_InternetDedicado_velInternacional_'+i).val());
						if(trunckCD_renegociacion_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckCD_renegociacion_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_InternetDedicado_velInternacional_'+i).select();
							$('#trunckCD_renegociacion_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_InternetDedicado_precioObjetivo = $.trim($('#trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckCD_renegociacion_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_InternetDedicado_plazo = $.trim($('#trunckCD_renegociacion_InternetDedicado_plazo_'+i).val());
						if(trunckCD_renegociacion_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckCD_renegociacion_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_InternetDedicado_plazo_'+i).select();
							$('#trunckCD_renegociacion_InternetDedicado_plazo_'+i).focus();
							return false;
						}

					/* Datos */
						var trunckCD_renegociacion_datos_velocidad = $.trim($('#trunckCD_renegociacion_datos_velocidad_'+i).val());
						if(trunckCD_renegociacion_datos_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Datos', 'Debe ingresar la velocidad.');
							$('#trunckCD_renegociacion_datos_velocidad_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_datos_velocidad_'+i).select();
							$('#trunckCD_renegociacion_datos_velocidad_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_datos_codServicio = $.trim($('#trunckCD_renegociacion_datos_codServicio_'+i).val());
						if(trunckCD_renegociacion_datos_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Datos', 'Debe ingresar el código de servicio.');
							$('#trunckCD_renegociacion_datos_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_datos_codServicio_'+i).select();
							$('#trunckCD_renegociacion_datos_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_datos_precioObjetivo = $.trim($('#trunckCD_renegociacion_datos_precioObjetivo_'+i).val());
						if(trunckCD_renegociacion_datos_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Datos', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_renegociacion_datos_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_datos_precioObjetivo_'+i).select();
							$('#trunckCD_renegociacion_datos_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_renegociacion_datos_plazo = $.trim($('#trunckCD_renegociacion_datos_plazo_'+i).val());
						if(trunckCD_renegociacion_datos_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Renegociación/Datos', 'Debe ingresar el plazo.');
							$('#trunckCD_renegociacion_datos_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_renegociacion_datos_plazo_'+i).select();
							$('#trunckCD_renegociacion_datos_plazo_'+i).focus();
							return false;
						}
				}
			}

			if(cantidad14.length>0 && cantidad14>0){
				for(var i=1;i<=cantidad14;i++){
					
					/* Solicitud */
						var trunckCD_cambioProducto_Solicitud_numOft = $.trim($('#trunckCD_cambioProducto_Solicitud_numOft_'+i).val());
						if(trunckCD_renegociacion_solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckCD_cambioProducto_Solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_Solicitud_numOft_'+i).select();
							$('#trunckCD_cambioProducto_Solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_Solicitud_codServicio = $.trim($('#trunckCD_cambioProducto_Solicitud_codServicio_'+i).val());
						if(trunckCD_cambioProducto_Solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckCD_cambioProducto_Solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_Solicitud_codServicio_'+i).select();
							$('#trunckCD_cambioProducto_Solicitud_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_Solicitud_contratoVigente = $.trim($('#trunckCD_cambioProducto_Solicitud_contratoVigente_'+i).val()); 
						if(trunckCD_cambioProducto_Solicitud_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Solicitud', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckCD_cambioProducto_Solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_Solicitud_contratoVigente_'+i).select();
							$('#trunckCD_cambioProducto_Solicitud_contratoVigente_'+i).focus();
							return false;
						}

						/*
							var trunckCD_cambioProducto_Solicitud_cuentaFacturacion = $.trim($('#trunckCD_cambioProducto_Solicitud_cuentaFacturacion_'+i).val()); 
							if(trunckCD_cambioProducto_Solicitud_cuentaFacturacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(2) a').tab('show');
								alertFail('Trunck Con Datos/Cambio Producto/Solicitud', 'Debe ingresar la cuenta de facturación.');
								$('#trunckCD_cambioProducto_Solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/

					/* Telefonia */
						var trunckCD_cambioProducto_telefonia_cantCanales = $.trim($('#trunckCD_cambioProducto_telefonia_cantCanales_'+i).val());
						if(trunckCD_cambioProducto_telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckCD_cambioProducto_telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_telefonia_cantCanales_'+i).select();
							$('#trunckCD_cambioProducto_telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_telefonia_minSLM = $.trim($('#trunckCD_cambioProducto_telefonia_minSLM_'+i).val());
						if(trunckCD_cambioProducto_telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckCD_cambioProducto_telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_telefonia_minSLM_'+i).select();
							$('#trunckCD_cambioProducto_telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_telefonia_minMovil = $.trim($('#trunckCD_cambioProducto_telefonia_minMovil_'+i).val());
						if(trunckCD_cambioProducto_telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckCD_cambioProducto_telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_telefonia_minMovil_'+i).select();
							$('#trunckCD_cambioProducto_telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_telefonia_contratoVigente = $.trim($('#trunckCD_cambioProducto_telefonia_contratoVigente_'+i).val());
						if(trunckCD_cambioProducto_telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Telefonía', 'Debe ingresar el USV Contrato Central.');
							$('#trunckCD_cambioProducto_telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_telefonia_contratoVigente_'+i).select();
							$('#trunckCD_cambioProducto_telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_telefonia_contratoTelefonia = $.trim($('#trunckCD_cambioProducto_telefonia_contratoTelefonia_'+i).val());
						if(trunckCD_cambioProducto_telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckCD_cambioProducto_telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_telefonia_contratoTelefonia_'+i).select();
							$('#trunckCD_cambioProducto_telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckCD_cambioProducto_InternetDedicado_velNacional = $.trim($('#trunckCD_cambioProducto_InternetDedicado_velNacional_'+i).val());
						if(trunckCD_cambioProducto_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckCD_cambioProducto_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_InternetDedicado_velNacional_'+i).select();
							$('#trunckCD_cambioProducto_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_InternetDedicado_velInternacional = $.trim($('#trunckCD_cambioProducto_InternetDedicado_velInternacional_'+i).val());
						if(trunckCD_cambioProducto_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckCD_cambioProducto_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_InternetDedicado_velInternacional_'+i).select();
							$('#trunckCD_cambioProducto_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_InternetDedicado_precioObjetivo = $.trim($('#trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckCD_cambioProducto_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_InternetDedicado_plazo = $.trim($('#trunckCD_cambioProducto_InternetDedicado_plazo_'+i).val());
						if(trunckCD_cambioProducto_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckCD_cambioProducto_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_InternetDedicado_plazo_'+i).select();
							$('#trunckCD_cambioProducto_InternetDedicado_plazo_'+i).focus();
							return false;
						}

					/* Datos */
						var trunckCD_cambioProducto_datos_velocidad = $.trim($('#trunckCD_cambioProducto_datos_velocidad_'+i).val());
						if(trunckCD_cambioProducto_datos_velocidad.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Datos', 'Debe ingresar la velocidad.');
							$('#trunckCD_cambioProducto_datos_velocidad_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_datos_velocidad_'+i).select();
							$('#trunckCD_cambioProducto_datos_velocidad_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_datos_codServicio = $.trim($('#trunckCD_cambioProducto_datos_codServicio_'+i).val());
						if(trunckCD_cambioProducto_datos_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Datos', 'Debe ingresar el código de servicio.');
							$('#trunckCD_cambioProducto_datos_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_datos_codServicio_'+i).select();
							$('#trunckCD_cambioProducto_datos_codServicio_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_datos_precioObjetivo = $.trim($('#trunckCD_cambioProducto_datos_precioObjetivo_'+i).val());
						if(trunckCD_cambioProducto_datos_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Datos', 'Debe ingresar el precio objetivo.');
							$('#trunckCD_cambioProducto_datos_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_datos_precioObjetivo_'+i).select();
							$('#trunckCD_cambioProducto_datos_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckCD_cambioProducto_datos_plazo = $.trim($('#trunckCD_cambioProducto_datos_plazo_'+i).val());
						if(trunckCD_cambioProducto_datos_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(2) a').tab('show');
							alertFail('Trunck Con Datos/Cambio Producto/Datos', 'Debe ingresar el plazo.');
							$('#trunckCD_cambioProducto_datos_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_cambioProducto_datos_plazo_'+i).select();
							$('#trunckCD_cambioProducto_datos_plazo_'+i).focus();
							return false;
						}

				}
			}
		}
		/********************** TRUNCK SD ****************************************************************************************************************************************************************************************************/
		if(servicio_seleccionado==4){
			if(cantidad15.length>0 && cantidad15>0){
				for(var i=1;i<=cantidad15;i++){
					
					var tsd_inst_solicitud_oft = $.trim($('#tsd_inst_solicitud_oft_'+i).val());
					if(tsd_inst_solicitud_oft.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar el número de OFT.');
						$('#tsd_inst_solicitud_oft_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_solicitud_oft_'+i).select();
						$('#tsd_inst_solicitud_oft_'+i).focus();
						return false;
					}
					
					var tsd_inst_telefonia_cantCanales = $.trim($('#tsd_inst_telefonia_cantCanales_'+i).val());
					if(tsd_inst_telefonia_cantCanales.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar la cantidad de canales.');
						$('#tsd_inst_telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_telefonia_cantCanales_'+i).select();
						$('#tsd_inst_telefonia_cantCanales_'+i).focus();
						return false;
					}

					var tsd_inst_telefonia_minSlm = $.trim($('#tsd_inst_telefonia_minSlm_'+i).val());
					if(tsd_inst_telefonia_minSlm.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar los minutos SLM.');
						$('#tsd_inst_telefonia_minSlm_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_telefonia_minSlm_'+i).select();
						$('#tsd_inst_telefonia_minSlm_'+i).focus();
						return false;
					}

					var tsd_inst_telefonia_minMovil = $.trim($('#tsd_inst_telefonia_minMovil_'+i).val());
					if(tsd_inst_telefonia_minMovil.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar los minutos móviles.');
						$('#tsd_inst_telefonia_minMovil_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_telefonia_minMovil_'+i).select();
						$('#tsd_inst_telefonia_minMovil_'+i).focus();
						return false;
					}

					var tsd_inst_telefonia_minAdicional = $.trim($('#tsd_inst_telefonia_minAdicional_'+i).val());
					if(tsd_inst_telefonia_minAdicional.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar los minutos móviles.');
						$('#tsd_inst_telefonia_minAdicional_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_telefonia_minAdicional_'+i).select();
						$('#tsd_inst_telefonia_minAdicional_'+i).focus();
						return false;
					}

					var tsd_inst_InternDedicado_minNacional = $.trim($('#tsd_inst_InternDedicado_minNacional_'+i).val());
					if(tsd_inst_InternDedicado_minNacional.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar la cantidad de minutos nacionales.');
						$('#tsd_inst_InternDedicado_minNacional_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_InternDedicado_minNacional_'+i).select();
						$('#tsd_inst_InternDedicado_minNacional_'+i).focus();
						return false;
					}

					var tsd_inst_InternDedicado_minInteracional = $.trim($('#tsd_inst_InternDedicado_minInteracional_'+i).val());
					if(tsd_inst_InternDedicado_minInteracional.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar la cantidad de minutos internacionales.');
						$('#tsd_inst_InternDedicado_minInteracional_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_InternDedicado_minInteracional_'+i).select();
						$('#tsd_inst_InternDedicado_minInteracional_'+i).focus();
						return false;
					}

					var tsd_inst_InternDedicado_precioObjetivoNacional = $.trim($('#tsd_inst_InternDedicado_precioObjetivoNacional_'+i).val());
					if(tsd_inst_InternDedicado_precioObjetivoNacional.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar el precio objetivo.');
						$('#tsd_inst_InternDedicado_precioObjetivoNacional_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_InternDedicado_precioObjetivoNacional_'+i).select();
						$('#tsd_inst_InternDedicado_precioObjetivoNacional_'+i).focus();
						return false;
					}

					var tsd_inst_InternDedicado_plazo = $.trim($('#tsd_inst_InternDedicado_plazo_'+i).val());
					if(tsd_inst_InternDedicado_plazo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(3) a').tab('show');
						alertFail('Trunck Sin Datos/Instalación', 'Debe ingresar el plazo.');
						$('#tsd_inst_InternDedicado_plazo_'+i).addClass('panel-bordered-danger');
						$('#tsd_inst_InternDedicado_plazo_'+i).select();
						$('#tsd_inst_InternDedicado_plazo_'+i).focus();
						return false;
					}

					//var tsd_inst_datos_velocidad = $.trim($('#tsd_inst_datos_velocidad_'+i).val());
					//var tsd_inst_datos_codServicio = $.trim($('#tsd_inst_datos_codServicio_'+i).val());
					//var tsd_inst_datos_precioObjetivo = $.trim($('#tsd_inst_datos_precioObjetivo_'+i).val());
					//var tsd_inst_datos_plazo = $.trim($('#tsd_inst_datos_plazo_'+i).val());
				}	
			}	

			if(cantidad16.length>0 && cantidad16>0){
				if(cantidad161.length>0 && cantidad161>0){
					for(var i=1;i<=cantidad161;i++){
						var trunckSD_upgrade_telefonia_numOft = $.trim($('#trunckSD_upgrade_telefonia_numOft_'+i).val());
						if(trunckSD_upgrade_telefonia_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar el número de OFT.');
							$('#trunckSD_upgrade_telefonia_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_numOft_'+i).select();
							$('#trunckSD_upgrade_telefonia_numOft_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_CodServicio = $.trim($('#trunckSD_upgrade_telefonia_CodServicio_'+i).val());
						if(trunckSD_upgrade_telefonia_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar el código de servicio.');
							$('#trunckSD_upgrade_telefonia_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_CodServicio_'+i).select();
							$('#trunckSD_upgrade_telefonia_CodServicio_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_contVigente = $.trim($('#trunckSD_upgrade_telefonia_contVigente_'+i).val());
						if(trunckSD_upgrade_telefonia_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_upgrade_telefonia_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_contVigente_'+i).select();
							$('#trunckSD_upgrade_telefonia_contVigente_'+i).focus();
							return false;
						}

						/*
							var trunckSD_upgrade_telefonia_ctaFacturacion = $.trim($('#trunckSD_upgrade_telefonia_ctaFacturacion_'+i).val());
							if(trunckSD_upgrade_telefonia_contVigente.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
								$('#trunckSD_upgrade_telefonia_contVigente_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/

						var trunckSD_upgrade_telefonia_cantCanales_actual = $.trim($('#trunckSD_upgrade_telefonia_cantCanales_actual_'+i).val());
						if(trunckSD_upgrade_telefonia_cantCanales_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de canales actual.');
							$('#trunckSD_upgrade_telefonia_cantCanales_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_cantCanales_actual_'+i).select();
							$('#trunckSD_upgrade_telefonia_cantCanales_actual_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_cantCanales_solicitada = $.trim($('#trunckSD_upgrade_telefonia_cantCanales_solicitada_'+i).val());
						if(trunckSD_upgrade_telefonia_cantCanales_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de canales solicitada.');
							$('#trunckSD_upgrade_telefonia_cantCanales_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_cantCanales_solicitada_'+i).select();
							$('#trunckSD_upgrade_telefonia_cantCanales_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_minSLM_actual = $.trim($('#trunckSD_upgrade_telefonia_minSLM_actual_'+i).val());
						if(trunckSD_upgrade_telefonia_minSLM_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos actuales.');
							$('#trunckSD_upgrade_telefonia_minSLM_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_minSLM_actual_'+i).select();
							$('#trunckSD_upgrade_telefonia_minSLM_actual_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_minSLM_solicitada = $.trim($('#trunckSD_upgrade_telefonia_minSLM_solicitada_'+i).val());
						if(trunckSD_upgrade_telefonia_minSLM_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos solicitada.');
							$('#trunckSD_upgrade_telefonia_minSLM_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_minSLM_solicitada_'+i).select();
							$('#trunckSD_upgrade_telefonia_minSLM_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_minMovil_actual = $.trim($('#trunckSD_upgrade_telefonia_minMovil_actual_'+i).val());
						if(trunckSD_upgrade_telefonia_minMovil_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles actuales.');
							$('#trunckSD_upgrade_telefonia_minMovil_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_minMovil_actual_'+i).select();
							$('#trunckSD_upgrade_telefonia_minMovil_actual_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_telefonia_minMovil_solicitada = $.trim($('#trunckSD_upgrade_telefonia_minMovil_solicitada_'+i).val());
						if(trunckSD_upgrade_telefonia_minMovil_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles solicitada.');
							$('#trunckSD_upgrade_telefonia_minMovil_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_telefonia_minMovil_solicitada_'+i).select();
							$('#trunckSD_upgrade_telefonia_minMovil_solicitada_'+i).focus();
							return false;
						}

						/*
							var trunckSD_upgrade_telefonia_comentario = $.trim($('#trunckSD_upgrade_telefonia_comentario_'+i).val());
							if(trunckSD_upgrade_telefonia_minMovil_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Upgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles solicitada.');
								$('#trunckSD_upgrade_telefonia_minMovil_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}	
				}

				if(cantidad162.length>0 && cantidad162>0){
					for(var i=1;i<=cantidad162;i++){
						var trunckSD_upgrade_InternetDedicado_numOft = $.trim($('#trunckSD_upgrade_InternetDedicado_numOft_'+i).val());
						if(trunckSD_upgrade_InternetDedicado_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar el número de OFT.');
							$('#trunckSD_upgrade_InternetDedicado_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_InternetDedicado_numOft_'+i).select();
							$('#trunckSD_upgrade_InternetDedicado_numOft_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_InternetDedicado_CodServicio = $.trim($('#trunckSD_upgrade_InternetDedicado_CodServicio_'+i).val());
						if(trunckSD_upgrade_InternetDedicado_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#trunckSD_upgrade_InternetDedicado_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_InternetDedicado_CodServicio_'+i).select();
							$('#trunckSD_upgrade_InternetDedicado_CodServicio_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_InternetDedicado_contVigente = $.trim($('#trunckSD_upgrade_InternetDedicado_contVigente_'+i).val());
						if(trunckSD_upgrade_InternetDedicado_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_upgrade_InternetDedicado_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_InternetDedicado_contVigente_'+i).select();
							$('#trunckSD_upgrade_InternetDedicado_contVigente_'+i).focus();
							return false;
						}

						//var trunckSD_upgrade_InternetDedicado_ctaFacturacion = $.trim($('#trunckSD_upgrade_InternetDedicado_ctaFacturacion_'+i).val());
						
						var trunckSD_upgrade_InternetDedicado_velNacional_solicitada = $.trim($('#trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+i).val());
						if(trunckSD_upgrade_InternetDedicado_velNacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar la velocidad nacional solicitada.');
							$('#trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+i).select();
							$('#trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_upgrade_InternetDedicado_velInternacional_solicitada = $.trim($('#trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).val());
						if(trunckSD_upgrade_InternetDedicado_velInternacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
							$('#trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).select();
							$('#trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+i).focus();
							return false;
						}

						
						/*
							var trunckSD_upgrade_InternetDedicado_comentario_solicitada = $.trim($('#trunckSD_upgrade_InternetDedicado_comentario_solicitada_'+i).val());
							if(trunckSD_upgrade_InternetDedicado_comentario_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Upgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
								$('#trunckSD_upgrade_InternetDedicado_comentario_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}
				}
			}

			if(cantidad17.length>0 && cantidad17>0){
				if(cantidad171.length>0 && cantidad171>0){
					for(var i=1;i<=cantidad171;i++){

						var trunckSD_downgrade_telefonia_numOft = $.trim($('#trunckSD_downgrade_telefonia_numOft_'+i).val());
						if(trunckSD_downgrade_telefonia_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar el número de OFT.');
							$('#trunckSD_downgrade_telefonia_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_numOft_'+i).select();
							$('#trunckSD_downgrade_telefonia_numOft_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_CodServicio = $.trim($('#trunckSD_downgrade_telefonia_CodServicio_'+i).val());
						if(trunckSD_downgrade_telefonia_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar el código de servicio.');
							$('#trunckSD_downgrade_telefonia_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_CodServicio_'+i).select();
							$('#trunckSD_downgrade_telefonia_CodServicio_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_contVigente = $.trim($('#trunckSD_downgrade_telefonia_contVigente_'+i).val());
						if(trunckSD_downgrade_telefonia_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_downgrade_telefonia_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_contVigente_'+i).select();
							$('#trunckSD_downgrade_telefonia_contVigente_'+i).focus();
							return false;
						}

						//var trunckSD_downgrade_telefonia_ctaFacturacion = $.trim($('#trunckSD_downgrade_telefonia_ctaFacturacion_'+i).val());
						var trunckSD_downgrade_telefonia_cantCanales_actual = $.trim($('#trunckSD_downgrade_telefonia_cantCanales_actual_'+i).val());
						if(trunckSD_downgrade_telefonia_cantCanales_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de canales actuales.');
							$('#trunckSD_downgrade_telefonia_cantCanales_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_cantCanales_actual_'+i).select();
							$('#trunckSD_downgrade_telefonia_cantCanales_actual_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_cantCanales_solicitada = $.trim($('#trunckSD_downgrade_telefonia_cantCanales_solicitada_'+i).val());
						if(trunckSD_downgrade_telefonia_cantCanales_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de canales solicitada.');
							$('#trunckSD_downgrade_telefonia_cantCanales_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_cantCanales_solicitada_'+i).select();
							$('#trunckSD_downgrade_telefonia_cantCanales_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_minSLM_actual = $.trim($('#trunckSD_downgrade_telefonia_minSLM_actual_'+i).val());
						if(trunckSD_downgrade_telefonia_minSLM_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM actuales.');
							$('#trunckSD_downgrade_telefonia_minSLM_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_minSLM_actual_'+i).select();
							$('#trunckSD_downgrade_telefonia_minSLM_actual_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_minSLM_solicitada = $.trim($('#trunckSD_downgrade_telefonia_minSLM_solicitada_'+i).val());
						if(trunckSD_downgrade_telefonia_minSLM_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos SLM solicitada.');
							$('#trunckSD_downgrade_telefonia_minSLM_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_minSLM_solicitada_'+i).select();
							$('#trunckSD_downgrade_telefonia_minSLM_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_minMovil_actual = $.trim($('#trunckSD_downgrade_telefonia_minMovil_actual_'+i).val());
						if(trunckSD_downgrade_telefonia_minMovil_actual.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles actuales.');
							$('#trunckSD_downgrade_telefonia_minMovil_actual_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_minMovil_actual_'+i).select();
							$('#trunckSD_downgrade_telefonia_minMovil_actual_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_telefonia_minMovil_solicitada = $.trim($('#trunckSD_downgrade_telefonia_minMovil_solicitada_'+i).val());
						if(trunckSD_downgrade_telefonia_minMovil_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Telefonía', 'Debe ingresar la cantidad de minutos móviles solicitada.');
							$('#trunckSD_downgrade_telefonia_minMovil_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_telefonia_minMovil_solicitada_'+i).select();
							$('#trunckSD_downgrade_telefonia_minMovil_solicitada_'+i).focus();
							return false;
						}

						//var trunckSD_downgrade_telefonia_comentario = $.trim($('#trunckSD_downgrade_telefonia_comentario_'+i).val());
					}	
				}

				if(cantidad172.length>0 && cantidad172>0){
					for(var i=1;i<=cantidad172;i++){
						
						var trunckSD_downgrade_InternetDedicado_numOft = $.trim($('#trunckSD_downgrade_InternetDedicado_numOft_'+i).val());
						if(trunckSD_downgrade_InternetDedicado_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar el número de OFT.');
							$('#trunckSD_downgrade_InternetDedicado_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_InternetDedicado_numOft_'+i).select();
							$('#trunckSD_downgrade_InternetDedicado_numOft_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_InternetDedicado_CodServicio = $.trim($('#trunckSD_downgrade_InternetDedicado_CodServicio_'+i).val());
						if(trunckSD_downgrade_InternetDedicado_CodServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar el código de servicio.');
							$('#trunckSD_downgrade_InternetDedicado_CodServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_InternetDedicado_CodServicio_'+i).select();
							$('#trunckSD_downgrade_InternetDedicado_CodServicio_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_InternetDedicado_contVigente = $.trim($('#trunckSD_downgrade_InternetDedicado_contVigente_'+i).val());
						if(trunckSD_downgrade_InternetDedicado_contVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_downgrade_InternetDedicado_contVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_InternetDedicado_contVigente_'+i).select();
							$('#trunckSD_downgrade_InternetDedicado_contVigente_'+i).focus();
							return false;
						}

						/*
							var trunckSD_downgrade_InternetDedicado_ctaFacturacion = $.trim($('#trunckSD_downgrade_InternetDedicado_ctaFacturacion_'+i).val());
							if(trunckSD_downgrade_InternetDedicado_ctaFacturacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar el USV Contrato Vigente.');
								$('#trunckSD_downgrade_InternetDedicado_ctaFacturacion_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/

						var trunckSD_downgrade_InternetDedicado_velNacional_solicitada = $.trim($('#trunckSD_downgrade_InternetDedicado_velNacional_solicitada_'+i).val());
						if(trunckSD_downgrade_InternetDedicado_velNacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad nacional solicitada.');
							$('#trunckSD_downgrade_InternetDedicado_velNacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_downgrade_InternetDedicado_velNacional_solicitada_'+i).select();
							$('#trunckSD_downgrade_InternetDedicado_velNacional_solicitada_'+i).focus();
							return false;
						}

						var trunckSD_downgrade_InternetDedicado_velInternacional_solicitada = $.trim($('#trunckSD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).val());
						if(trunckSD_downgrade_InternetDedicado_velInternacional_solicitada.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
							$('#trunckSD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).addClass('panel-bordered-danger');
							$('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).select();
							$('#trunckCD_downgrade_InternetDedicado_velInternacional_solicitada_'+i).focus();
							return false;
						}

						
						/*
							var trunckSD_downgrade_InternetDedicado_comentario_solicitada = $.trim($('#trunckSD_downgrade_InternetDedicado_comentario_solicitada_'+i).val());
							if(trunckSD_downgrade_InternetDedicado_comentario_solicitada.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Downgrade/Internet Dedicado', 'Debe ingresar la velocidad internacional solicitada.');
								$('#trunckSD_downgrade_InternetDedicado_comentario_solicitada_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/
					}
				}
			}

			if(cantidad18.length>0 && cantidad18>0){
				for(var i=1;i<=cantidad18;i++){
					
					/* Solicitud */
						var trunckSD_Traslado_Solicitud_numOft = $.trim($('#trunckSD_Traslado_Solicitud_numOft_'+i).val());
						if(trunckSD_Traslado_Solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckSD_Traslado_Solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Solicitud_numOft_'+i).select();
							$('#trunckSD_Traslado_Solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_Solicitud_codServicio = $.trim($('#trunckSD_Traslado_Solicitud_codServicio_'+i).val());
						if(trunckSD_Traslado_Solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckSD_Traslado_Solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Solicitud_codServicio_'+i).select();
							$('#trunckSD_Traslado_Solicitud_codServicio_'+i).focus();
							return false;
						}

						/*
							var trunckSD_Traslado_Solicitud_cuentaFacturacion = $.trim($('#trunckSD_Traslado_Solicitud_cuentaFacturacion_'+i).val()); 
							if(trunckSD_Traslado_Solicitud_cuentaFacturacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Traslado/Solicitud', 'Debe ingresar el código de servicio.');
								$('#trunckSD_Traslado_Solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/

					/* Telefonia */
						var trunckSD_Traslado_Telefonia_cantCanales = $.trim($('#trunckSD_Traslado_Telefonia_cantCanales_'+i).val());
						if(trunckSD_Traslado_Telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckSD_Traslado_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Telefonia_cantCanales_'+i).select();
							$('#trunckSD_Traslado_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_Telefonia_minSLM = $.trim($('#trunckSD_Traslado_Telefonia_minSLM_'+i).val());
						if(trunckSD_Traslado_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckSD_Traslado_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Telefonia_minSLM_'+i).select();
							$('#trunckSD_Traslado_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_Telefonia_minMovil = $.trim($('#trunckSD_Traslado_Telefonia_minMovil_'+i).val());
						if(trunckSD_Traslado_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckSD_Traslado_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Telefonia_minMovil_'+i).select();
							$('#trunckSD_Traslado_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_Telefonia_minAdicional = $.trim($('#trunckSD_Traslado_Telefonia_minAdicional_'+i).val());
						if(trunckSD_Traslado_Telefonia_minAdicional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Telefonía', 'Debe ingresar los minutos adicionales.');
							$('#trunckSD_Traslado_Telefonia_minAdicional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_Telefonia_minAdicional_'+i).select();
							$('#trunckSD_Traslado_Telefonia_minAdicional_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckSD_Traslado_InternetDedicado_velNacional = $.trim($('#trunckSD_Traslado_InternetDedicado_velNacional_'+i).val());
						if(trunckSD_Traslado_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckSD_Traslado_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_InternetDedicado_velNacional_'+i).select();
							$('#trunckSD_Traslado_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_InternetDedicado_velInternacional = $.trim($('#trunckSD_Traslado_InternetDedicado_velInternacional_'+i).val());
						if(trunckSD_Traslado_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckSD_Traslado_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_InternetDedicado_velInternacional_'+i).select();
							$('#trunckSD_Traslado_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_InternetDedicado_precioObjetivo = $.trim($('#trunckSD_Traslado_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckSD_Traslado_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckSD_Traslado_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckSD_Traslado_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckSD_Traslado_InternetDedicado_plazo = $.trim($('#trunckSD_Traslado_InternetDedicado_plazo_'+i).val());
						if(trunckSD_Traslado_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Traslado/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckSD_Traslado_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_Traslado_InternetDedicado_plazo_'+i).select();
							$('#trunckSD_Traslado_InternetDedicado_plazo_'+i).focus();
							return false;
						}

				}	
			}	

			if(cantidad19.length>0 && cantidad19>0){
				for(var i=1;i<=cantidad19;i++){
					
					/* Solicitud */
						var trunckSD_cambio_acceso_solicitud_numOft = $.trim($('#trunckSD_cambio_acceso_solicitud_numOft_'+i).val());
						if(trunckSD_cambio_acceso_solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Solicitud', 'Debe ingresar número de OFT.');
							$('#trunckSD_cambio_acceso_solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_solicitud_numOft_'+i).select();
							$('#trunckSD_cambio_acceso_solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_solicitud_codServicio = $.trim($('#trunckSD_cambio_acceso_solicitud_codServicio_'+i).val());
						if(trunckSD_cambio_acceso_solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckSD_cambio_acceso_solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_solicitud_codServicio_'+i).select();
							$('#trunckSD_cambio_acceso_solicitud_codServicio_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_solicitud_contratoVigente_ = $.trim($('#trunckSD_cambio_acceso_solicitud_contratoVigente_'+i).val()); 
						if(trunckSD_cambio_acceso_solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckSD_cambio_acceso_solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_solicitud_contratoVigente_'+i).select();
							$('#trunckSD_cambio_acceso_solicitud_contratoVigente_'+i).focus();
							return false;
						}

						//var trunckSD_cambio_acceso_solicitud_cuentaFacturacion_ = $.trim($('#trunckSD_cambio_acceso_solicitud_cuentaFacturacion_'+i).val()); 

					/* Telefonia */
						var trunckSD_cambio_acceso_Telefonia_cantCanales_ = $.trim($('#trunckSD_cambio_acceso_Telefonia_cantCanales_'+i).val());
						if(trunckSD_cambio_acceso_Telefonia_cantCanales_.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckSD_cambio_acceso_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_Telefonia_cantCanales_'+i).select();
							$('#trunckSD_cambio_acceso_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_Telefonia_minSLM = $.trim($('#trunckSD_cambio_acceso_Telefonia_minSLM_'+i).val());
						if(trunckSD_cambio_acceso_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckSD_cambio_acceso_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_Telefonia_minSLM_'+i).select();
							$('#trunckSD_cambio_acceso_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_Telefonia_minMovil = $.trim($('#trunckSD_cambio_acceso_Telefonia_minMovil_'+i).val());
						if(trunckSD_cambio_acceso_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckSD_cambio_acceso_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_Telefonia_minMovil_'+i).select();
							$('#trunckSD_cambio_acceso_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_Telefonia_contratoVigente = $.trim($('#trunckSD_cambio_acceso_Telefonia_contratoVigente_'+i).val());
						if(trunckSD_cambio_acceso_Telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_cambio_acceso_Telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_Telefonia_contratoVigente_'+i).select();
							$('#trunckSD_cambio_acceso_Telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_Telefonia_contratoTelefonia = $.trim($('#trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+i).val());
						if(trunckSD_cambio_acceso_Telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+i).select();
							$('#trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckSD_cambio_acceso_internetDedicado_velNacional = $.trim($('#trunckSD_cambio_acceso_internetDedicado_velNacional_'+i).val());
						if(trunckSD_cambio_acceso_internetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckSD_cambio_acceso_internetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_internetDedicado_velNacional_'+i).select();
							$('#trunckSD_cambio_acceso_internetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_internetDedicado_velInternacional = $.trim($('#trunckSD_cambio_acceso_internetDedicado_velInternacional_'+i).val());
						if(trunckSD_cambio_acceso_internetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckSD_cambio_acceso_internetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_internetDedicado_velInternacional_'+i).select();
							$('#trunckSD_cambio_acceso_internetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_internetDedicado_precioObjetivo = $.trim($('#trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+i).val());
						if(trunckSD_cambio_acceso_internetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+i).select();
							$('#trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckSD_cambio_acceso_internetDedicado_plazo = $.trim($('#trunckSD_cambio_acceso_internetDedicado_plazo_'+i).val());
						if(trunckSD_cambio_acceso_internetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Acceso/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckSD_cambio_acceso_internetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambio_acceso_internetDedicado_plazo_'+i).select();
							$('#trunckSD_cambio_acceso_internetDedicado_plazo_'+i).focus();
							return false;
						}

				}	
			}

			if(cantidad20.length>0 && cantidad20>0){
				for(var i=1;i<=cantidad20;i++){
					
					/* Solicitud */
						var trunckSD_renegociacion_solicitud_numOft = $.trim($('#trunckSD_renegociacion_solicitud_numOft_'+i).val());
						if(trunckSD_cambio_acceso_internetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Solicitud', 'Debe ingresar el número de OFT.');
							$('#trunckSD_renegociacion_solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_solicitud_numOft_'+i).select();
							$('#trunckSD_renegociacion_solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_solicitud_codServicio = $.trim($('#trunckSD_renegociacion_solicitud_codServicio_'+i).val());
						if(trunckSD_renegociacion_solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckSD_renegociacion_solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_solicitud_codServicio_'+i).select();
							$('#trunckSD_renegociacion_solicitud_codServicio_'+i).focus();
							return false;
						}


						var trunckSD_renegociacion_solicitud_contratoVigente = $.trim($('#trunckSD_renegociacion_solicitud_contratoVigente_'+i).val()); 
						if(trunckSD_renegociacion_solicitud_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Solicitud', 'Debe ingresar el código de servicio.');
							$('#trunckSD_renegociacion_solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_solicitud_contratoVigente_'+i).select();
							$('#trunckSD_renegociacion_solicitud_contratoVigente_'+i).focus();
							return false;
						}

						/*
							var trunckSD_renegociacion_solicitud_cuentaFacturacion = $.trim($('#trunckSD_renegociacion_solicitud_cuentaFacturacion_'+i).val()); 
							if(trunckSD_renegociacion_solicitud_cuentaFacturacion.length==0){
								error_formulario = true;
								$('#menuPrincipal li:eq(3) a').tab('show');
								alertFail('Trunck Sin Datos/Renegociación/Solicitud', 'Debe ingresar el código de servicio.');
								$('#trunckSD_renegociacion_solicitud_cuentaFacturacion_'+i).addClass('panel-bordered-danger');
								return false;
							}
						*/


					/* Telefonia */
						var trunckSD_renegociacion_Telefonia_cantCanales = $.trim($('#trunckSD_renegociacion_Telefonia_cantCanales_'+i).val());
						if(trunckSD_renegociacion_Telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckSD_renegociacion_Telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_Telefonia_cantCanales_'+i).select();
							$('#trunckSD_renegociacion_Telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_Telefonia_minSLM = $.trim($('#trunckSD_renegociacion_Telefonia_minSLM_'+i).val());
						if(trunckSD_renegociacion_Telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckSD_renegociacion_Telefonia_minSLM_'+i).addClass('panel-bordered-danger');}
							$('#trunckSD_renegociacion_Telefonia_minSLM_'+i).select();
							$('#trunckSD_renegociacion_Telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_Telefonia_minMovil = $.trim($('#trunckSD_renegociacion_Telefonia_minMovil_'+i).val());
						if(trunckSD_renegociacion_Telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckSD_renegociacion_Telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_Telefonia_minMovil_'+i).select();
							$('#trunckSD_renegociacion_Telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_Telefonia_contratoVigente = $.trim($('#trunckSD_renegociacion_Telefonia_contratoVigente_'+i).val());
						if(trunckSD_renegociacion_Telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Telefonía', 'Debe ingresar el USV Contrato Vigente.');
							$('#trunckSD_renegociacion_Telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_Telefonia_contratoVigente_'+i).select();
							$('#trunckSD_renegociacion_Telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_Telefonia_contratoTelefonia = $.trim($('#trunckSD_renegociacion_Telefonia_contratoTelefonia_'+i).val());
						if(trunckSD_renegociacion_Telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckSD_renegociacion_Telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_Telefonia_contratoTelefonia_'+i).select();
							$('#trunckSD_renegociacion_Telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckSD_renegociacion_InternetDedicado_velNacional = $.trim($('#trunckSD_renegociacion_InternetDedicado_velNacional_'+i).val());
						if(trunckSD_renegociacion_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckSD_renegociacion_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_InternetDedicado_velNacional_'+i).select();
							$('#trunckSD_renegociacion_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_InternetDedicado_velInternacional = $.trim($('#trunckSD_renegociacion_InternetDedicado_velInternacional_'+i).val());
						if(trunckSD_renegociacion_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckSD_renegociacion_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_InternetDedicado_velInternacional_'+i).select();
							$('#trunckSD_renegociacion_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_InternetDedicado_precioObjetivo = $.trim($('#trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckSD_renegociacion_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckSD_renegociacion_InternetDedicado_plazo = $.trim($('#trunckSD_renegociacion_InternetDedicado_plazo_'+i).val());
						if(trunckSD_renegociacion_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Renegociación/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckSD_renegociacion_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_renegociacion_InternetDedicado_plazo_'+i).select();
							$('#trunckSD_renegociacion_InternetDedicado_plazo_'+i).focus();
							return false;
						}

				}
			}

			if(cantidad21.length>0 && cantidad21>0){
				for(var i=1;i<=cantidad21;i++){
					
					/* Solicitud */
						var trunckSD_cambioProducto_Solicitud_numOft = $.trim($('#trunckSD_cambioProducto_Solicitud_numOft_'+i).val());
						if(trunckSD_cambioProducto_Solicitud_numOft.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Solicitud', 'Debe el número de OFT.');
							$('#trunckSD_cambioProducto_Solicitud_numOft_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_Solicitud_numOft_'+i).select();
							$('#trunckSD_cambioProducto_Solicitud_numOft_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_Solicitud_codServicio = $.trim($('#trunckSD_cambioProducto_Solicitud_codServicio_'+i).val());
						if(trunckSD_cambioProducto_Solicitud_codServicio.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Solicitud', 'Debe el código de servicio.');
							$('#trunckSD_cambioProducto_Solicitud_codServicio_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_Solicitud_codServicio_'+i).select();
							$('#trunckSD_cambioProducto_Solicitud_codServicio_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_Solicitud_contratoVigente = $.trim($('#trunckSD_cambioProducto_Solicitud_contratoVigente_'+i).val()); 
						if(trunckSD_cambioProducto_Solicitud_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Solicitud', 'Debe el USV Contrato Vigente.');
							$('#trunckSD_cambioProducto_Solicitud_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_Solicitud_contratoVigente_'+i).select();
							$('#trunckSD_cambioProducto_Solicitud_contratoVigente_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_Solicitud_cuentaFacturacion = $.trim($('#trunckSD_cambioProducto_Solicitud_cuentaFacturacion_'+i).val()); 

					/* Telefonia */
						var trunckSD_cambioProducto_telefonia_cantCanales = $.trim($('#trunckSD_cambioProducto_telefonia_cantCanales_'+i).val());
						if(trunckSD_cambioProducto_telefonia_cantCanales.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Telefonía', 'Debe ingresar la cantidad de canales.');
							$('#trunckSD_cambioProducto_telefonia_cantCanales_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_telefonia_cantCanales_'+i).select();
							$('#trunckSD_cambioProducto_telefonia_cantCanales_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_telefonia_minSLM = $.trim($('#trunckSD_cambioProducto_telefonia_minSLM_'+i).val());
						if(trunckSD_cambioProducto_telefonia_minSLM.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Telefonía', 'Debe ingresar los minutos SLM.');
							$('#trunckSD_cambioProducto_telefonia_minSLM_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_telefonia_minSLM_'+i).select();
							$('#trunckSD_cambioProducto_telefonia_minSLM_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_telefonia_minMovil = $.trim($('#trunckSD_cambioProducto_telefonia_minMovil_'+i).val());
						if(trunckSD_cambioProducto_telefonia_minMovil.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Telefonía', 'Debe ingresar los minutos móviles.');
							$('#trunckSD_cambioProducto_telefonia_minMovil_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_telefonia_minMovil_'+i).select();
							$('#trunckSD_cambioProducto_telefonia_minMovil_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_telefonia_contratoVigente = $.trim($('#trunckSD_cambioProducto_telefonia_contratoVigente_'+i).val());
						if(trunckSD_cambioProducto_telefonia_contratoVigente.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Telefonía', 'Debe ingresar el USV Contrato Central.');
							$('#trunckSD_cambioProducto_telefonia_contratoVigente_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_telefonia_contratoVigente_'+i).select();
							$('#trunckSD_cambioProducto_telefonia_contratoVigente_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_telefonia_contratoTelefonia = $.trim($('#trunckSD_cambioProducto_telefonia_contratoTelefonia_'+i).val());
						if(trunckSD_cambioProducto_telefonia_contratoTelefonia.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Telefonía', 'Debe ingresar el USV Contrato Telefonía.');
							$('#trunckSD_cambioProducto_telefonia_contratoTelefonia_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_telefonia_contratoTelefonia_'+i).select();
							$('#trunckSD_cambioProducto_telefonia_contratoTelefonia_'+i).focus();
							return false;
						}

					/* Internet Dedicado */
						var trunckSD_cambioProducto_InternetDedicado_velNacional = $.trim($('#trunckSD_cambioProducto_InternetDedicado_velNacional_'+i).val());
						if(trunckSD_cambioProducto_InternetDedicado_velNacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar la velocidad nacional.');
							$('#trunckSD_cambioProducto_InternetDedicado_velNacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_InternetDedicado_velNacional_'+i).select();
							$('#trunckSD_cambioProducto_InternetDedicado_velNacional_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_InternetDedicado_velInternacional = $.trim($('#trunckSD_cambioProducto_InternetDedicado_velInternacional_'+i).val());
						if(trunckSD_cambioProducto_InternetDedicado_velInternacional.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar la velocidad internacional.');
							$('#trunckSD_cambioProducto_InternetDedicado_velInternacional_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_InternetDedicado_velInternacional_'+i).select();
							$('#trunckSD_cambioProducto_InternetDedicado_velInternacional_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_InternetDedicado_precioObjetivo = $.trim($('#trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+i).val());
						if(trunckSD_cambioProducto_InternetDedicado_precioObjetivo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar el precio objetivo.');
							$('#trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+i).select();
							$('#trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+i).focus();
							return false;
						}

						var trunckSD_cambioProducto_InternetDedicado_plazo = $.trim($('#trunckSD_cambioProducto_InternetDedicado_plazo_'+i).val());
						if(trunckSD_cambioProducto_InternetDedicado_plazo.length==0){
							error_formulario = true;
							$('#menuPrincipal li:eq(3) a').tab('show');
							alertFail('Trunck Sin Datos/Cambio Producto/Internet Dedicado', 'Debe ingresar el plazo.');
							$('#trunckSD_cambioProducto_InternetDedicado_plazo_'+i).addClass('panel-bordered-danger');
							$('#trunckSD_cambioProducto_InternetDedicado_plazo_'+i).select();
							$('#trunckSD_cambioProducto_InternetDedicado_plazo_'+i).focus();
							return false;
						}
				}
			}	
		}		

		if(servicio_seleccionado==5){
		    if(cantidad22.length>0 && cantidad22>0){
				for(var i=1;i<=cantidad22;i++){
					
					var itemtypeTelefoniaMinutosSlm = $.trim($('#itemtypeTelefoniaMinutosSlm_'+i).val());
					if(itemtypeTelefoniaMinutosSlm.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar los Minutos SLM.');
						$('#itemtypeTelefoniaMinutosSlm_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaMinutosSlm_'+i).select();
						$('#itemtypeTelefoniaMinutosSlm_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaMinutosMovil = $.trim($('#itemtypeTelefoniaMinutosMovil_'+i).val());
					if(itemtypeTelefoniaMinutosMovil.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar los Minutos Móviles.');
						$('#itemtypeTelefoniaMinutosMovil_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaMinutosMovil_'+i).select();
						$('#itemtypeTelefoniaMinutosMovil_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaPrecioObjetivo = $.trim($('#itemtypeTelefoniaPrecioObjetivo_'+i).val());
					if(itemtypeTelefoniaPrecioObjetivo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar el Precio Objetivo.');
						$('#itemtypeTelefoniaPrecioObjetivo_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaPrecioObjetivo_'+i).select();
						$('#itemtypeTelefoniaPrecioObjetivo_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaPlazo = $.trim($('#itemtypeTelefoniaPlazo_'+i).val());
					if(itemtypeTelefoniaPlazo.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar los plazo.');
						$('#itemtypeTelefoniaPlazo_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaPlazo_'+i).select();
						$('#itemtypeTelefoniaPlazo_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaCodContrato = $.trim($('#itemtypeTelefoniaCodContrato_'+i).val());
					if(itemtypeTelefoniaCodContrato.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar el Código de servicio.');
						$('#itemtypeTelefoniaCodContrato_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaCodContrato_'+i).select();
						$('#itemtypeTelefoniaCodContrato_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaUsvVigente = $.trim($('#itemtypeTelefoniaUsvVigente_'+i).val());
					if(itemtypeTelefoniaUsvVigente.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar el USV Contrato Vigente.');
						$('#itemtypeTelefoniaUsvVigente_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaUsvVigente_'+i).select();
						$('#itemtypeTelefoniaUsvVigente_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaMinSlmUsv = $.trim($('#itemtypeTelefoniaMinSlmUsv_'+i).val());
					if(itemtypeTelefoniaMinSlmUsv.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar los Minutos SLM asociados a USV Contrato.');
						$('#itemtypeTelefoniaMinSlmUsv_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaMinSlmUsv_'+i).select();
						$('#itemtypeTelefoniaMinSlmUsv_'+i).focus();
						return false;
					}

					var itemtypeTelefoniaMinMovilUsv = $.trim($('#itemtypeTelefoniaMinMovilUsv_'+i).val());
					if(itemtypeTelefoniaMinMovilUsv.length==0){
						error_formulario = true;
						$('#menuPrincipal li:eq(4) a').tab('show');
						alertFail('Telefonía', 'Debe ingresar los Minutos Móvil asociados a USV Contrato.');
						$('#itemtypeTelefoniaMinMovilUsv_'+i).addClass('panel-bordered-danger');
						$('#itemtypeTelefoniaMinMovilUsv_'+i).select();
						$('#itemtypeTelefoniaMinMovilUsv_'+i).focus();
						return false;
					}
				}
			}
		}

		if(!error_formulario){
			$('#formSend').attr('action', "resultado_formulario_fijo.php");
			$('#cotizacion').submit(); 	
		}

	}else{
		alertFail('Formulario', 'Debe ingresar a lo menos 1 item en el formulario.');
		return false;
	}
}
