/*
function cargarCargoFijo(valor,numero) {

    console.log('Entra');

    var i = parseInt(valor);
    var j = numero;
    $("#cant_linea"+j).val(1);
    $("#cargo_fijo"+j).val(i);
    $("#plan"+j).val(i);
    $("#total"+j).attr("readonly" ,true);
}
*/
       
function setcantEquipos(num){
    
    var disabled = $('#cant_equipos'+num).attr('disabled');
    var readonly = $('#cant_equipos'+num).attr('readonly');

    var disabledModelo = $('#equipos'+num).attr('disabled');
    //console.log(disabledModelo);

    if((readonly=='readonly' || disabled=='disabled') && disabledModelo!='disabled') {
        var cant_lineas = $('#cant_linea'+num).val();
        $('#cant_equipos'+num).val(cant_lineas);
    }
}

function total_linea(num){
    //console.log('Entra');
    var cantidad_lineas;
    var costo_fijo;
    var totales = 0;
    var totales_temp = 0;
    var total_lineas = 0;
    var totalExtra = 0;
    
    //console.log(logInput);

    for(i=0;i<logInput.length;i++){
       
       cantidad_lineas = $("#cant_linea"+logInput[i]).val();
       //console.log('id= #cant_linea'+logInput[i]+' Cantidad de lienas : '+cantidad_lineas);

       cantidad_lineas = cantidad_lineas.replace(".", "");
       cantidad_lineas = cantidad_lineas.replace(".", "");
       cantidad_lineas = cantidad_lineas.replace(".", "");
       
       costo_fijo = $("#cargo_fijo"+logInput[i]).val(); 
       //console.log('Cargo fijo : '+costo_fijo); 

       costo_fijo = costo_fijo.replace(".", "");
       costo_fijo = costo_fijo.replace("$", "");
       costo_fijo = costo_fijo.replace(".", "");


       totales_temp = parseInt(costo_fijo)*parseInt(cantidad_lineas);
       


       var descuento = $("#descExtra"+logInput[i]).val();
       if(descuento==null){
         descuento = 0;
       }else{
         descuento = descuento.replace("%", ""); 
       }
       
       newValTemp = totales_temp - (((totales_temp)*descuento)/100);
       var decount = (parseInt(totales_temp/1.19)*descuento)/100;
       totalExtra+=parseInt(decount);

       $("#total"+logInput[i]).val('$'+number_format(totales_temp, 0));
       
       total_lineas+=parseInt(cantidad_lineas);
       totales+= parseInt(costo_fijo)*parseInt(cantidad_lineas);
    }

    $('#extra_total').val('$'+number_format(totalExtra, 0));
    $("#lineas_totales").val(total_lineas);
    $("#lineas_totales").attr("readonly" ,true);
}

function replacePuntos(val){
    var val1 = val;
    if(val1.length>0){  
        val1 = val1.replace(" ", "");
        val1 = val1.replace(" ", "");
        if(val1.length>0){    
            val1 = val1.replace(".", "");
            val1 = val1.replace(".", "");
            val1 = val1.replace("$", "");
            val1 = val1.replace("$", "");
            val1 = val1.replace(".", "");
            val1 = val1.replace(".", "");
        }
    }

    return val1;
}
              
function recalcula(){
    var totalSB = replacePuntos($('#fact_total').val());
    var descuentoExtra = replacePuntos($('#extra_total').val());
    var descuentoFide = replacePuntos($('#fidelizacion_total').val());
    var descuentoPorta = replacePuntos($('#portabilidad_total').val());

    var total = parseInt(totalSB) - (parseInt(descuentoExtra)+parseInt(descuentoFide)+parseInt(descuentoPorta));
    $('#fact_total_fidelizacion').val('$'+number_format(total, 0));
}

function cantidad_equipos(num){

    var p ="cant_equipos"+num;
    
    //Cantidad de equipos
    var x = $("#"+p).val();

    //Cantidad de lineas
    var y = $("#cant_linea"+num).val();
    
    if(y.length==0){
        y=0;
    }    

    if(x.length==0){
        x=0;
    }     
    
    x = parseInt(x);
    y = parseInt(y);

    if(x>y){
        $("#"+p).val(y);
        alert("Excede el máximo ("+y+") de equipos");
        return false;	
    }
}

/*
function total_facturacion(num){
    var x;
    var total = 0;
    var i;
    var y = 1;                   
    for(i=0;i<num;i++){
        x = $("#total"+y).val();
        if(x>0){
            total = parseInt(total) + parseInt(x);
        }else{
            total = parseInt(total);
        }                               
        y++;
    }
    
    //console.log(total);

    $("#fact_total").val(total);
    $("#fact_total").attr("readonly" ,true);
}
*/