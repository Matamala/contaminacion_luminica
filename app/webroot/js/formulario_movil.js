var filas_planes_multimedia = 0;
var filas_planes_mmf_bolsas = 0;
var filas_planes_bam = 0;
var filas_planes_mdm = 0;

function listaEquiposCotizador(input, index, ){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=10',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaEquiposMultimedia(input, index, ){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=11',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaEquiposBAM(input, index, ){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=12',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaEquiposMDM(input, index, ){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=13',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaEquiposMMFF(input, index, ){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=14',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function quitaFormato(num){
     num = replaceAll(num, ".", "");
     num = replaceAll(num, "$", "");
     num = replaceAll(num, ",", "");
     return num;
}

function listaPlanesCotizador(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=6',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaPlanesMultimedia(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=7',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaPlanesBAM(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=8',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaPlanesMDM(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=9',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaPlanesMMFF(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=5',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

function listaTipoLineas(input, index){
    $.ajax({
        url: 'includes/listadoEquipos.php',
        type: 'POST',
        data: 'modulo=3',
        async: true,
        success: function(listado){
            $('#'+input+'_'+index).html(listado);
        }
    })
}

/* -------------------------------------------------------------------------> Calculo Lineas MMF y Bolsas <----------------------------------------------------------------------- */

function calculaLineasMMFF(){

    var qcantidad = $.trim($('#mmf_bolsas_q').val());
    var tarifa_por_minuto = $.trim($('#mmf_bolsas_tarifa_minuto').val());
    var total_minutos = $.trim($('#mmf_bolsas_total_minutos').val());

    if(qcantidad.length==0){qcantidad = 0;}
    if(tarifa_por_minuto.length==0){tarifa_por_minuto = 0;}
    if(total_minutos.length==0){total_minutos = 0;}

    var valor1 = parseInt(total_minutos)/parseInt(qcantidad);
    var valor2 = parseInt(valor1)*parseInt(tarifa_por_minuto);
    var valor3 = parseInt(valor2)*parseInt(qcantidad);

    valor1 = Math.round(valor1);
    valor2 = Math.round(valor2);
    valor3 = Math.round(valor3);

    $('#mmf_bolsas_total_minutos_linea').val(number_format(valor1, 0));
    $('#mmf_bolsas_mmf').val(number_format(valor2, 0));
    $('#mmf_bolsas_total_mmf').val(number_format(valor3, 0));

}


/* -------------------------------------------------------------------------> Calculos <----------------------------------------------------------------------- */

function calculoGeneral(){

    var planesLineas = 0;
    var planesValor = 0;

    var qcantidad = $.trim($('#mmf_bolsas_q').val());
    var tarifa_por_minuto = $.trim($('#mmf_bolsas_tarifa_minuto').val());
    var total_minutos = $.trim($('#mmf_bolsas_total_minutos').val());

    for ( var i=1; i<=filas_planes_multimedia; i++ ) {
        var multimedia_lineas = $('#multimedia_lineas_'+i).val(); 
        planesLineas+=parseInt(multimedia_lineas);
        var multimedia_total = quitaFormato($('#multimedia_total_'+i).val());
        planesValor+=parseInt(multimedia_total);
    }

    for ( var i=1; i<=filas_planes_mmf_bolsas; i++ ) {
        var mmf_bolsas_lineas = $('#mmf_bolsas_lineas_'+i).val();
        planesLineas+=parseInt(mmf_bolsas_lineas);
        var mmf_bolsas_total = quitaFormato($('#mmf_bolsas_total_'+i).val());
        planesValor+=parseInt(mmf_bolsas_total);
    }

    $('#plan_voz_lineas').val(planesLineas);
    planesValor = number_format(planesValor, 0);
    $('#plan_voz_facturacion').val(planesValor);

    planesLineas = 0;
    planesValor = 0;

    for ( var i=1; i<=filas_planes_bam; i++ ) {
        var mmf_bolsas_lineas = $('#bam_lineas_'+i).val();
        planesLineas+=parseInt(mmf_bolsas_lineas);
        var mmf_bolsas_total = quitaFormato($('#bam_total_'+i).val());
        planesValor+=parseInt(mmf_bolsas_total);
    }

    $('#bam_lineas').val(planesLineas);
    planesValor = number_format(planesValor, 0);
    $('#bam_facturacion').val(planesValor);

    for ( var i=1; i<=filas_planes_mdm; i++ ) {
        var mmf_bolsas_lineas = $('#mdm_lineas_'+i).val();
        planesLineas+=parseInt(mmf_bolsas_lineas);
        var mmf_bolsas_total = quitaFormato($('#mdm_total_'+i).val());
        planesValor+=parseInt(mmf_bolsas_total);
    }

    $('#mdm_lineas').val(planesLineas);
    planesValor = number_format(planesValor, 0);
    $('#mdm_facturacion').val(planesValor);

    var facturacion_plan_voz = quitaFormato($('#plan_voz_facturacion').val());
    var descuento_fidelizacion = quitaFormato($('#descuento_fidelizacion_facturacion').val());
    var descuento_portabilidad = quitaFormato($('#descuento_portabilidad_facturacion').val());
    var facturacion_bam = quitaFormato($('#bam_facturacion').val());
    var facturacion_mdm = quitaFormato($('#mdm_facturacion').val());

    var facturacion_total = (parseInt(facturacion_plan_voz)+parseInt(facturacion_bam)+parseInt(facturacion_mdm))-(parseInt(descuento_fidelizacion)+parseInt(descuento_portabilidad));
    facturacion_total = number_format(facturacion_total, 0);
    $('#facturacion_total_facturacion').val(facturacion_total);

}

/* -------------------------------------------------------------------------> Formato Numeros <----------------------------------------------------------------------- */
function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join(',');
}


/* -------------------------------------------------------------------------> CArgo Fijo Plan <----------------------------------------------------------------------- */
function cargoFijoPlanMultimedia(index){
    var plan_id = $.trim($('#multimedia_plan_'+index).val());
    var lineasQ = $.trim($('#multimedia_lineas_'+index).val());
    if(plan_id.length>0 && plan_id>0){
        $.ajax({
            url: 'includes/listadoEquipos.php',
            type: 'POST',
            data: 'modulo=4&plan_id='+plan_id,
            async: true,
            success: function(valor){
                var totalCF = parseInt(valor)*parseInt(lineasQ);
                totalCF = number_format(totalCF, 0);
                var total = number_format(valor, 0);
                $('#multimedia_cargo_fijo_'+index).val(total);
                $('#multimedia_total_'+index).val(totalCF);
                calculoGeneral();
            }
        })
    }else{
        $('#multimedia_cargo_fijo_'+index).val('0');
        $('#multimedia_total_'+index).val('0');
        calculoGeneral();
    }
}

function cargoFijoPlanMMF(index){
    var plan_id = $('#mmf_bolsas_plan_'+index).val();
    var lineasQ = $.trim($('#mmf_bolsas_lineas_'+index).val());
    if(plan_id.length>0 && plan_id>0){
        $.ajax({
            url: 'includes/listadoEquipos.php',
            type: 'POST',
            data: 'modulo=4&plan_id='+plan_id,
            async: true,
            success: function(valor){
                var totalCF = parseInt(valor)*parseInt(lineasQ);
                totalCF = number_format(totalCF, 0);
                var total = number_format(valor, 0);
                $('#mmf_bolsas_cargo_fijo_'+index).val(total);
                $('#mmf_bolsas_total_'+index).val(totalCF);
                calculoGeneral();
            }
        })
    }else{
        $('#mmf_bolsas_cargo_fijo_'+index).val('0');
        $('#mmf_bolsas_total_'+index).val('0');
        calculoGeneral();
    }
}

function cargoFijoPlanBAM(index){
    var plan_id = $('#bam_plan_'+index).val();
    var lineasQ = $.trim($('#bam_lineas_'+index).val());
    if(plan_id.length>0 && plan_id>0){
        $.ajax({
            url: 'includes/listadoEquipos.php',
            type: 'POST',
            data: 'modulo=4&plan_id='+plan_id,
            async: true,
            success: function(valor){
                var totalCF = parseInt(valor)*parseInt(lineasQ);
                totalCF = number_format(totalCF, 0);
                var total = number_format(valor, 0);
                $('#bam_cargo_fijo_'+index).val(total);
                $('#bam_total_'+index).val(totalCF);
                calculoGeneral();
            }
        })
    }else{
        $('#bam_cargo_fijo_'+index).val('0');
        $('#bam_total_'+index).val('0');
        calculoGeneral();
    }
}

function cargoFijoPlanMDM(index){
    var plan_id = $('#mdm_plan_'+index).val();
    var lineasQ = $.trim($('#mdm_lineas_'+index).val());
    if(plan_id.length>0 && plan_id>0){
        $.ajax({
            url: 'includes/listadoEquipos.php',
            type: 'POST',
            data: 'modulo=4&plan_id='+plan_id,
            async: true,
            success: function(valor){
                var totalCF = parseInt(valor)*parseInt(lineasQ);
                totalCF = number_format(totalCF, 0);
                var total = number_format(valor, 0);
                $('#mdm_cargo_fijo_'+index).val(total);
                $('#mdm_total_'+index).val(totalCF);
                calculoGeneral();
            }
        })
    }else{
        $('#mdm_cargo_fijo_'+index).val('0');
        $('#mdm_total_'+index).val('0');
        calculoGeneral();
    }
}




//Planes Multimedia
function addPlanMultimedia(){
    filas_planes_multimedia++;
    var fila = '<tr>'+
        '<td><select onchange="cargoFijoPlanMultimedia('+filas_planes_multimedia+');" id="multimedia_plan_'+filas_planes_multimedia+'" name="multimedia_plan_'+filas_planes_multimedia+'" class="form-control input-sm"></select></td>'+
        '<td><select id="multimedia_tipo_linea_'+filas_planes_multimedia+'" name="multimedia_tipo_linea_'+filas_planes_multimedia+'" class="form-control input-sm"></select></td>'+
        '<td><input oninput="cargoFijoPlanMultimedia('+filas_planes_multimedia+');" id="multimedia_lineas_'+filas_planes_multimedia+'" name="multimedia_lineas_'+filas_planes_multimedia+'" type="number" min="1" value="1" max="100" class="form-control input-sm text-center" /></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="multimedia_cargo_fijo_'+filas_planes_multimedia+'" name="multimedia_cargo_fijo_'+filas_planes_multimedia+'" readonly="readonly" type="text" value="0" class="form-control input-sm text-center" /></div></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="multimedia_total_'+filas_planes_multimedia+'" name="multimedia_total_'+filas_planes_multimedia+'" readonly="readonly" type="text" value="0" class="form-control input-sm text-center" /></div></td>'+
        '<td><input id="multimedia_cantidad_equipos_'+filas_planes_multimedia+'" name="multimedia_cantidad_equipos_'+filas_planes_multimedia+'" type="number" min="0" value="0" max="100" class="form-control input-sm text-center" /></td>'+
        '<td><select id="multimedia_equipo_'+filas_planes_multimedia+'" name="multimedia_equipo_'+filas_planes_multimedia+'" class="form-control input-sm"></select></td>'+
        '<td><div class="input-group"><input id="multimedia_descuento_'+filas_planes_multimedia+'" name="multimedia_descuento_'+filas_planes_multimedia+'" type="text" value="0" class="form-control input-sm text-center" /><span class="input-group-addon">%</span></div></td>'+
    '</tr>';
    $('#contenido_planes_multimedia').append(fila);
    listaPlanesMultimedia('multimedia_plan', filas_planes_multimedia);
    listaTipoLineas('multimedia_tipo_linea', filas_planes_multimedia);
    listaEquiposMultimedia('multimedia_equipo', filas_planes_multimedia);

    var lineas = $('#plan_voz_lineas').val();
    lineas = parseInt(lineas)+1;
    $('#plan_voz_lineas').val(lineas);
    
    $('#numMultimedia').val(filas_planes_multimedia);
}

function quitPlanMultimedia(){
    filas_planes_multimedia--;
    $("#contenido_planes_multimedia tr:last").remove();
    $('#numMultimedia').val(filas_planes_multimedia);
    
    var lineas = $('#plan_voz_lineas').val();
    lineas = parseInt(lineas)-1;
    $('#plan_voz_lineas').val(lineas);
    
    $('#numMultimedia').val(filas_planes_multimedia);
    calculoGeneral();
}

//Planes Planes MMF Bolsas
function addPlanMmfBolsas(){

    var notError = true;
    var qcantidad = $.trim($('#mmf_bolsas_q').val());
    var tarifa_por_minuto = $.trim($('#mmf_bolsas_tarifa_minuto').val());
    var total_minutos = $.trim($('#mmf_bolsas_total_minutos').val());

    if(qcantidad.length==0 || qcantidad==0){
        notError = false;
        showError('Formulario Movil / Plan Multimedia', 'Debe ingresar la cantidad de lineas.');
        $('#mmf_bolsas_q').select();
        $('#mmf_bolsas_q').focus();
        return false;
    }

    if(tarifa_por_minuto.length==0 || tarifa_por_minuto==0){
        notError = false;
        showError('Formulario Movil / Plan Multimedia', 'Debe ingresar la tarifa por minuto.');
        $('#mmf_bolsas_tarifa_minuto').select();
        $('#mmf_bolsas_tarifa_minuto').focus();
        return false;
    }

    if(total_minutos.length==0 || total_minutos==0){
        notError = false;
        showError('Formulario Movil / Plan Multimedia', 'Debe ingresar el total de minutos.');
        $('#mmf_bolsas_total_minutos').select();
        $('#mmf_bolsas_total_minutos').focus();
        return false;
    }

    if(notError){
        filas_planes_mmf_bolsas++;
        var fila = '<tr>'+
            '<td><select onchange="cargoFijoPlanMMF('+filas_planes_mmf_bolsas+');" id="mmf_bolsas_plan_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_plan_'+filas_planes_mmf_bolsas+'" class="form-control input-sm"></select></td>'+
            '<td><select id="mmf_bolsas_tipo_linea_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_tipo_linea_'+filas_planes_mmf_bolsas+'" class="form-control input-sm"></select></td>'+
            '<td><input oninput="cargoFijoPlanMMF('+filas_planes_mmf_bolsas+');" id="mmf_bolsas_lineas_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_lineas_'+filas_planes_mmf_bolsas+'" type="number" value="1" min="1" max="100" class="form-control input-sm text-center" /></td>'+
            '<td><div class="input-group"><span class="input-group-addon">$</span><input id="mmf_bolsas_cargo_fijo_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_cargo_fijo_'+filas_planes_mmf_bolsas+'" readonly="readonly" type="text"  value="0" class="form-control input-sm text-center" /></div></td>'+
            '<td><div class="input-group"><span class="input-group-addon">$</span><input id="mmf_bolsas_total_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_total_'+filas_planes_mmf_bolsas+'" readonly="readonly" type="text"  value="0" class="form-control input-sm text-center" /></div></td>'+
            '<td><input id="mmf_bolsas_cantidad_equipos_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_cantidad_equipos_'+filas_planes_mmf_bolsas+'" type="number" min="0" value="0" max="100" class="form-control input-sm text-center" /></td>'+
            '<td><select id="mmf_bolsas_equipo_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_equipo_'+filas_planes_mmf_bolsas+'" class="form-control input-sm"></select></td>'+
            '<td><div class="input-group"><input id="mmf_bolsas_descuento_'+filas_planes_mmf_bolsas+'" name="mmf_bolsas_descuento_'+filas_planes_mmf_bolsas+'" type="text" value="0" class="form-control input-sm text-center" /><span class="input-group-addon">%</span></div></td>'+
        '</tr>';
        $('#contenido_planes_mmf_bolsas').append(fila);
        listaPlanesMMFF('mmf_bolsas_plan', filas_planes_mmf_bolsas);
        listaTipoLineas('mmf_bolsas_tipo_linea', filas_planes_mmf_bolsas);
        listaEquiposMMFF('mmf_bolsas_equipo', filas_planes_mmf_bolsas);

        var lineas = $('#plan_voz_lineas').val();
        lineas = parseInt(lineas)+1;
        $('#plan_voz_lineas').val(lineas);

        $('#numMBolsas').val(filas_planes_mmf_bolsas);
    }
}

function quitPlanMmfBolsas(){
    filas_planes_mmf_bolsas--;
    $("#contenido_planes_mmf_bolsas tr:last").remove();

    var lineas = $('#plan_voz_lineas').val();
    lineas = parseInt(lineas)-1;
    $('#plan_voz_lineas').val(lineas);

    $('#numMBolsas').val(filas_planes_mmf_bolsas);
    calculoGeneral();
}

////Planes Planes BAM
function addPlanBam(){
    filas_planes_bam++;

     var fila = '<tr>'+
        '<td><select onchange="cargoFijoPlanBAM('+filas_planes_bam+');" id="bam_plan_'+filas_planes_bam+'" name="bam_plan_'+filas_planes_bam+'" class="form-control input-sm"></select></td>'+
        '<td><select id="bam_tipo_linea_'+filas_planes_bam+'" name="bam_tipo_linea_'+filas_planes_bam+'" class="form-control input-sm"></select></td>'+
        '<td><input oninput="cargoFijoPlanBAM('+filas_planes_bam+');" id="bam_lineas_'+filas_planes_bam+'" name="bam_lineas_'+filas_planes_bam+'" type="number" min="1" max="100" value="1" class="form-control input-sm text-center" /></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="bam_cargo_fijo_'+filas_planes_bam+'" name="bam_cargo_fijo_'+filas_planes_bam+'" readonly="readonly" type="text" value="0" class="form-control input-sm text-center" /></div></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="bam_total_'+filas_planes_bam+'" name="bam_total_'+filas_planes_bam+'" readonly="readonly" type="text"  value="0" class="form-control input-sm text-center" /></div></td>'+
        '<td><input id="bam_cantidad_equipos_'+filas_planes_bam+'" name="bam_cantidad_equipos_'+filas_planes_bam+'" type="number"  min="0" value="0" max="100" class="form-control input-sm text-center" /></td>'+
        '<td><select id="bam_equipo_'+filas_planes_bam+'" name="bam_equipo_'+filas_planes_bam+'" class="form-control input-sm"></select></td>'+
        '<td><div class="input-group"><input id="bam_descuento_'+filas_planes_bam+'" name="bam_descuento_'+filas_planes_bam+'" type="text"  value="0" class="form-control input-sm text-center" /><span class="input-group-addon">%</span></div></td>'+
    '</tr>';

    $('#contenido_planes_bam').append(fila);
    listaPlanesBAM('bam_plan', filas_planes_bam);
    listaTipoLineas('bam_tipo_linea', filas_planes_bam);
    listaEquiposBAM('bam_equipo', filas_planes_bam);

    var lineas = $('#bam_lineas').val();
    lineas = parseInt(lineas)+1;
    $('#bam_lineas').val(lineas);

    $('#numBan').val(filas_planes_bam);
}

function quitPlanBam(){
    filas_planes_bam--;
    $("#contenido_planes_bam tr:last").remove();

    var lineas = $('#bam_lineas').val();
    lineas = parseInt(lineas)-1;
    $('#bam_lineas').val(lineas);

    $('#numBan').val(filas_planes_bam);
    calculoGeneral();
}


//Planes Planes MDM
function addPlanMdm(){
    filas_planes_mdm++;

     var fila = '<tr>'+
        '<td><select onchange="cargoFijoPlanMDM('+filas_planes_mdm+');" id="mdm_plan_'+filas_planes_mdm+'" name="mdm_plan_'+filas_planes_mdm+'" class="form-control input-sm"></select></td>'+
        '<td><select id="mdm_tipo_linea_'+filas_planes_mdm+'" name="mdm_tipo_linea_'+filas_planes_mdm+'" class="form-control input-sm"></select></td>'+
        '<td><input oninput="cargoFijoPlanMDM('+filas_planes_mdm+');" id="mdm_lineas_'+filas_planes_mdm+'" name="mdm_lineas_'+filas_planes_mdm+'" type="number" min="1" max="100" value="1" class="form-control input-sm text-center" /></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="mdm_cargo_fijo_'+filas_planes_mdm+'" name="mdm_cargo_fijo_'+filas_planes_mdm+'" readonly="readonly"  value="0" type="text" class="form-control input-sm text-center" /></div></td>'+
        '<td><div class="input-group"><span class="input-group-addon">$</span><input id="mdm_total_'+filas_planes_mdm+'" name="mdm_total_'+filas_planes_mdm+'" readonly="readonly" type="text"  value="0" class="form-control input-sm text-center" /></div></td>'+
    '</tr>';

    $('#contenido_planes_mdm').append(fila);
    listaPlanesMDM('mdm_plan', filas_planes_mdm);
    listaTipoLineas('mdm_tipo_linea', filas_planes_mdm);

    var lineas = $('#mdm_lineas').val();
    lineas = parseInt(lineas)+1;
    $('#mdm_lineas').val(lineas);

    $('#numMdm').val(filas_planes_mdm);
}

function quitPlanMdm(){
    filas_planes_mdm--;
    $("#contenido_planes_mdm tr:last").remove();

    var lineas = $('#mdm_lineas').val();
    lineas = parseInt(lineas)-1;
    $('#mdm_lineas').val(lineas);

    $('#numMdm').val(filas_planes_mdm);
    calculoGeneral();
}

function formateaRut(rut) {

    var actual = rut;
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }else{
        rutPuntos = rut;                    
    }

    return rutPuntos;
}

function replaceAll( text, busca, reemplaza ){
    while (text.toString().indexOf(busca) != -1){
      text = text.toString().replace(busca,reemplaza);
    }
  return text;
}

var max_chars = 300;
$(document).ready(function(){
    
    $('#rut_cliente').keyup(function(){
        var rut = $.trim($('#rut_cliente').val());
        rut = replaceAll(rut, ".", "");
        rut = replaceAll(rut, ",", "");
        rut = replaceAll(rut, "/", "");
        rut = replaceAll(rut, "*", "");
        rut = replaceAll(rut, "{", "");
        rut = replaceAll(rut, "}", "");
        rut = replaceAll(rut, "[", "");
        rut = replaceAll(rut, "]", "");
        rut = replaceAll(rut, ",", "");
        rut = replaceAll(rut, "-", "");
        rut = replaceAll(rut, "+", "");
        rut = replaceAll(rut, "'", "");
        rut = replaceAll(rut, "`", "");
        rut = replaceAll(rut, "^", "");
        var newRut = formateaRut(rut);
        $('#rut_cliente').val(newRut);
    });

    $('#comentario').keyup(function() {
        var chars = $(this).val().length;
        var diff = max_chars - chars;
        $('#contadorLetters').html('Te quedan '+diff+' caracteres.');   
    });

    $('#isHolding').bind('change', function(){
        var state = $(this).val();
        if(state==1){
            $('#isHoldingInfo').hide();
        }else{
            $('#isHoldingInfo').show();
        }
    });

    $('#isLicitacion').bind('change', function(){
        var state = $(this).val();
        if(state==1){
            $('.isLicitacion').hide();
        }else{
            $('.isLicitacion').show();
        }
    });

    $('#isAsoc').bind('change', function(){
        var state = $(this).val();
        if(state==1){
            $('.isAsoc').hide();
        }else{
            $('.isAsoc').show();
        }
    });



})

function validaFormulario(){

    $('#realizaEzaluacion').attr('disabled', true);

    var FindError = false;

    var rut_cliente = $.trim($('#rut_cliente').val());
    var nombre_cliente = $.trim($('#nombre_cliente').val());
    var canal = $.trim($('#canal').val());
    var cuenta = $.trim($('#cuenta').val());
    var prioridad_cliente = $.trim($('#prioridad_cliente').val());
    var prioridad_equipos = $.trim($('#prioridad_equipos').val());
    var isHolding = $.trim($('#isHolding').val());
    var isLicitacion = $.trim($('#isLicitacion').val());
    var isAsoc = $.trim($('#isAsoc').val());

    if(rut_cliente.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe ingresar el rut del cliente.');
        $('#rut_cliente').select();
        $('#rut_cliente').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(nombre_cliente.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe ingresar el nombre del cliente.');
        $('#nombre_cliente').select();
        $('#nombre_cliente').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(canal.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar el canal al cual pertenece.');
        $('#canal').select();
        $('#canal').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(cuenta.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe ingresar el número de cuenta.');
        $('#cuenta').select();
        $('#cuenta').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(prioridad_cliente.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar la prioridad del cliente.');
        $('#prioridad_cliente').select();
        $('#prioridad_cliente').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(prioridad_equipos.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar la prioridad de equipos.');
        $('#prioridad_equipos').select();
        $('#prioridad_equipos').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(isHolding.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar si es Holding o no.');
        $('#isHolding').select();
        $('#isHolding').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    if(isLicitacion.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar si es licitación o no.');
        $('#isLicitacion').select();
        $('#isLicitacion').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }else{
        if(isLicitacion==2){

            var plazo_renovacion = $.trim($('#plazo_renovacion').val());
            var plazo_licitacion = $.trim($('#plazo_licitacion').val());    

            if(plazo_renovacion.length==0){
                FindError = true;
                showError('Formulario Movil', 'Debe seleccionar si es Holding o no.');
                $('#plazo_renovacion').select();
                $('#plazo_renovacion').focus();
                $('#realizaEzaluacion').attr('disabled', false);
                return false;
            }

            if(plazo_licitacion.length==0){
                FindError = true;
                showError('Formulario Movil', 'Debe ingresar el plazo de licitación');
                $('#plazo_licitacion').select();
                $('#plazo_licitacion').focus();
                $('#realizaEzaluacion').attr('disabled', false);
                return false;
            }

        }
    }

    if(isAsoc.length==0){
        FindError = true;
        showError('Formulario Movil', 'Debe seleccionar aceleración de ACOC.');
        $('#isAsoc').select();
        $('#isAsoc').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }else{
        if(isAsoc==2){
            var total_solicitado = $.trim($('#total_solicitado').val());
            if(total_solicitado.length==0){
                FindError = true;
                showError('Formulario Movil', 'Debe ingresar el total solicitado.');
                $('#total_solicitado').select();
                $('#total_solicitado').focus();
                $('#realizaEzaluacion').attr('disabled', false);
                return false;
            }
        }
    }

    if(filas_planes_multimedia==0 && filas_planes_mmf_bolsas==0 && filas_planes_bam==0 && filas_planes_mdm==0){
        FindError = true;
        showError('Formulario Movil', 'Debe ingresar a lo menos 1 item.');
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    //--------------------------------------------------------------------------> Validación plan multimedia
    for ( var i=1; i<=filas_planes_multimedia; i++ ) {
        
        //plan
        var multimedia_plan = $('#multimedia_plan_'+i).val();
        if(multimedia_plan.length==0){
            FindError = true;
            showError('Formulario Movil / Plan Multimedia', 'Debe seleccionar el plan.');
            $('#multimedia_plan_'+i).select();
            $('#multimedia_plan_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var multimedia_tipo_linea = $('#multimedia_tipo_linea_'+i).val();
        if(multimedia_tipo_linea.length==0){
            FindError = true;
            showError('Formulario Movil / Plan Multimedia', 'Debe seleccionar el tipo de linea.');
            $('#multimedia_tipo_linea_'+i).select();
            $('#multimedia_tipo_linea_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var multimedia_lineas = $('#multimedia_lineas_'+i).val();
        if(multimedia_lineas.length==0){
            FindError = true;
            showError('Formulario Movil / Plan Multimedia', 'Debe ingresar la cantidad de lineas.');
            $('#multimedia_lineas_'+i).select();
            $('#multimedia_lineas_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var multimedia_cargo_fijo = $('#multimedia_cargo_fijo_'+i).val();
        var multimedia_total = $('#multimedia_total_'+i).val();

        var multimedia_cantidad_equipos = $('#multimedia_cantidad_equipos_'+i).val();
        if(multimedia_cantidad_equipos.length==0){
            FindError = true;
            showError('Formulario Movil / Plan Multimedia', 'Debe ingresar la cantidad de equipos.');
            $('#multimedia_cantidad_equipos_'+i).select();
            $('#multimedia_cantidad_equipos_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var multimedia_equipo = $('#multimedia_equipo_'+i).val();
         if(multimedia_cantidad_equipos.length==0){
            FindError = true;
            showError('Formulario Movil / Plan Multimedia', 'Debe ingresar el equipo.');
            $('#multimedia_equipo_'+i).select();
            $('#multimedia_equipo_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var multimedia_descuento = $('#multimedia_descuento_'+i).val();
        
    }

    //-------------------------------------------------------------------------------------> Validación MMF y Bolsas
    var count = 0;
    for ( var i=1; i<=filas_planes_mmf_bolsas; i++ ) {
        
        //plan
        var mmf_bolsas_plan = $('#mmf_bolsas_plan_'+i).val();
        if(mmf_bolsas_plan.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MMMF y Bolsas', 'Debe seleccionar el plan.');
            $('#mmf_bolsas_plan_'+i).select();
            $('#mmf_bolsas_plan_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mmf_bolsas_tipo_linea = $('#mmf_bolsas_tipo_linea_'+i).val();
        if(mmf_bolsas_tipo_linea.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MMMF y Bolsas', 'Debe seleccionar el tipo de linea.');
            $('#mmf_bolsas_tipo_linea_'+i).select();
            $('#mmf_bolsas_tipo_linea_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mmf_bolsas_lineas = $('#mmf_bolsas_lineas_'+i).val();
        if(mmf_bolsas_lineas.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MMMF y Bolsas', 'Debe ingresar la cantidad de lineas.');
            $('#mmf_bolsas_lineas_'+i).select();
            $('#mmf_bolsas_lineas_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        count+=parseInt(mmf_bolsas_lineas);

        var mmf_bolsas_cargo_fijo = $('#mmf_bolsas_cargo_fijo_'+i).val();
        var mmf_bolsas_total = $('#mmf_bolsas_total_'+i).val();

        var mmf_bolsas_cantidad_equipos = $('#mmf_bolsas_cantidad_equipos_'+i).val();
        if(mmf_bolsas_cantidad_equipos.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MMMF y Bolsas', 'Debe ingresar la cantidad de equipos.');
            $('#mmf_bolsas_cantidad_equipos_'+i).select();
            $('#mmf_bolsas_cantidad_equipos_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mmf_bolsas_equipo = $('#mmf_bolsas_equipo_'+i).val();
         if(mmf_bolsas_equipo.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MMMF y Bolsas', 'Debe ingresar el equipo.');
            $('#mmf_bolsas_equipo_'+i).select();
            $('#mmf_bolsas_equipo_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mmf_bolsas_descuento = $('#mmf_bolsas_descuento_'+i).val();

    }

    //console.log('Cantidas de lineas : '+count);
    var mmf_bolsas_q = parseInt($('#mmf_bolsas_q').val());
    if(count<mmf_bolsas_q || count>mmf_bolsas_q){
        FindError = true;
        showError('Formulario Movil / Plan MMMF y Bolsas / Q', 'La cantidad de lineas no coinciden con el valor Q ingresado.');
        $('#mmf_bolsas_q').select();
        $('#mmf_bolsas_q').focus();
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }

    //-------------------------------------------------------------------------------------> PROPUESTA BAM
    for ( var i=1; i<=filas_planes_bam; i++ ) {
        
        //plan
        var bam_plan = $('#bam_plan_'+i).val();
        if(bam_plan.length==0){
            FindError = true;
            showError('Formulario Movil / Plan BAM', 'Debe seleccionar el plan.');
            $('#bam_plan_'+i).select();
            $('#bam_plan_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var bam_tipo_linea = $('#bam_tipo_linea_'+i).val();
        if(bam_tipo_linea.length==0){
            FindError = true;
            showError('Formulario Movil / Plan BAM', 'Debe seleccionar el tipo de linea.');
            $('#bam_tipo_linea_'+i).select();
            $('#bam_tipo_linea_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var bam_lineas = $('#bam_lineas_'+i).val();
        if(bam_lineas.length==0){
            FindError = true;
            showError('Formulario Movil / Plan BAM', 'Debe ingresar la cantidad de lineas.');
            $('#bam_lineas_'+i).select();
            $('#bam_lineas_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var bam_cargo_fijo = $('#bam_cargo_fijo_'+i).val();
        var bam_total = $('#bam_total_'+i).val();

        var bam_cantidad_equipos = $('#bam_cantidad_equipos_'+i).val();
        if(bam_cantidad_equipos.length==0){
            FindError = true;
            showError('Formulario Movil / Plan BAM', 'Debe ingresar la cantidad de equipos.');
            $('#bam_cantidad_equipos_'+i).select();
            $('#bam_cantidad_equipos_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mmf_bolsas_equipo = $('#bam_equipo_'+i).val();
         if(mmf_bolsas_equipo.length==0){
            FindError = true;
            showError('Formulario Movil / Plan BAM', 'Debe ingresar el equipo.');
            $('#bam_equipo_'+i).select();
            $('#bam_equipo_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var bam_descuento = $('#bam_descuento_'+i).val();

        

    }

    //-------------------------------------------------------------------------------------> PROPUESTA MDM
    for ( var i=1; i<=filas_planes_mdm; i++ ) {
        
        //plan
        var bam_plan = $('#mdm_plan_'+i).val();
        if(bam_plan.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MDM', 'Debe seleccionar el plan.');
            $('#bam_plan_'+i).select();
            $('#bam_plan_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mdm_tipo_linea = $('#mdm_tipo_linea_'+i).val();
        if(mdm_tipo_linea.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MDM', 'Debe seleccionar el tipo de linea.');
            $('#bam_tipo_linea_'+i).select();
            $('#bam_tipo_linea_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mdm_lineas = $('#mdm_lineas_'+i).val();
        if(mdm_lineas.length==0){
            FindError = true;
            showError('Formulario Movil / Plan MDM', 'Debe ingresar la cantidad de lineas.');
            $('#bam_lineas_'+i).select();
            $('#bam_lineas_'+i).focus();
            $('#realizaEzaluacion').attr('disabled', false);
            return false;
        }

        var mdm_cargo_fijo = $('#mdm_cargo_fijo_'+i).val();
        var mdm_total = $('#mdm_total_'+i).val();

    }

    if(FindError){
        $('#realizaEzaluacion').attr('disabled', false);
        return false;
    }else{
        $('#formSend').attr('action', "resultado_formulario_movil.php");
        $('#formSend').submit(); 
        return true;
    }

}