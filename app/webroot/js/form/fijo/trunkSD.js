function monitorTrunckSd(){
    var solicitud = $('#solicitudTrunkSd').val();
    if(solicitud>0){

            if(solicitud==1){
                $('#addItem20').attr('disabled', false);
                if(TrunkSdInstalacion>0){
                    $('#quitItem20').attr('disabled', false);
                }else{
                    $('#quitItem20').attr('disabled', true);
                }
            }

            if(solicitud==2){
                if(TrunkSdUpgradePanelActivity==1){
                    $('#addItem20').attr('disabled', true);
                    $('#quitItem20').attr('disabled', false);
                    return false;
                }else{
                    $('#addItem20').attr('disabled', false);
                    $('#quitItem20').attr('disabled', true);
                    return false;
                }
            }

            if(solicitud==3){
                $('#addItem20').attr('disabled', false);
                if(TrunckSdTraslado>0){
                    $('#quitItem20').attr('disabled', false);
                }else{
                    $('#quitItem20').attr('disabled', true);
                }
            }

            if(solicitud==4){
                $('#addItem20').attr('disabled', false);
                if(TrunckSdAcceso>0){
                    $('#quitItem20').attr('disabled', false);
                }else{
                    $('#quitItem20').attr('disabled', true);
                }
            }

            if(solicitud==5){
                $('#addItem20').attr('disabled', false);
                if(TrunckSdRenegociacion>0){
                    $('#quitItem20').attr('disabled', false);
                }else{
                    $('#quitItem20').attr('disabled', true);
                }
            }

            if(solicitud==6){
                if(TrunkSdDowngradePanelActivity==1){
                    $('#addItem20').attr('disabled', true);
                    $('#quitItem20').attr('disabled', false);
                    return false;
                }else{
                    $('#addItem20').attr('disabled', false);
                    $('#quitItem20').attr('disabled', true);
                    return false;
                }
            }

            if(solicitud==7){
                $('#addItem20').attr('disabled', false);
                if(TrunckSdCambioProducto>0){
                    $('#quitItem20').attr('disabled', false);
                }else{
                    $('#quitItem20').attr('disabled', true);
                }
            }

            $('#addItem20').attr('disabled', false);
    
    }else{
        $('#addItem20').attr('disabled', true);
        $('#quitItem20').attr('disabled', true);

    }
}


function addItemTrunckSd(){
    var solicitud = $('#solicitudTrunkSd').val();
    if(solicitud==1){
        addItemTruncksdInstalacion();
    }

    if(solicitud==2){
        if(TrunkSdUpgradePanelActivity==0){
            $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_upgrade" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Upgrade</h3></div><div class="panel-body"><div class="row"><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"><table style="" class="table"><tbody><tr><td>Tipo Solicitud</td><td><select id="solicitud14" onchange="monitortruncksdupgrade();" class="form-control input-sm"><option value="">Seleccione</option><option value="1">Telefonia</option><option value="2">Internet Dedicado</option></select></td><td class="text-right"><button  style="margin-right:3px;" type="button" onclick="addItemTrunckSdUpgrade();" disabled="true" id="addItem14" class="btn btn-primary">Agregar</button><button type="button" onclick="quitItemTrunckSdUpgrade();" disabled="true" id="quitItem14" class="btn btn-danger">Eliminar</button></td></tr></tbody></table></div></div><div id="bodyUpgradeTrunsd" class="tab-base tab-stacked-left"><ul id="tabs_truncksd_upgrade" class="nav nav-tabs"></ul></div></div></div>');
             $('#addItem20').attr('disabled', true);
             $('#quitItem20').attr('disabled', false);   
             TrunkSdUpgradePanelActivity = 1;
        }else{ 
            $('#addItem20').attr('disabled', true);
            $('#quitItem20').attr('disabled', false);
            if(TrunkSdUpgradePanelActivity==1){    
                addItemTrunckSdUpgrade();
            }else{
                alertFail('Trunck Con Datos: Upgrade', 'Debe seleccionar Upgrade en la lista desplegable');
                $('#solicitudTrunkSd').select();
                $('#solicitudTrunkSd').focus();
                return false;
            }
        }
    }

    if(solicitud==3){
        addItemTrunckSdTraslado();
    }

    if(solicitud==4){
        addItemTrunckSdAcceso();
    }

    if(solicitud==5){
        addItemTrunckSdRenegociacion();
    }

    if(solicitud==6){
        if(TrunkSdDowngradePanelActivity==0){
            
            var item = '<div id="contenedor_truncksd_boxs_downgrade" class="panel panel-dark">'+
                            '<div class="panel-heading"><h3 class="panel-title">Downgrade</h3></div>'+
                            '<div class="panel-body">'+
                                '<div class="row">'+
                                    '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>'+
                                    '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
                                        '<table style="" class="table">'+
                                            '<tbody>'+
                                                '<tr>'+
                                                    '<td>Tipo Solicitud</td>'+
                                                    '<td>'+
                                                        '<select id="solicitud21" onchange="monitortruncksddowngrade();" class="form-control input-sm">'+
                                                            '<option value="">Seleccione</option>'+
                                                            '<option value="1">Telefonia</option>'+
                                                            '<option value="2">Internet Dedicado</option>'+
                                                        '</select>'+
                                                    '</td>'+
                                                    '<td class="text-right">'+
                                                        '<button type="button" style="margin-right: 3px;" onclick="addItemTrunckSdDowngrade();" disabled="true" id="addItem21" class="btn btn-primary">Agregar</button>'+
                                                        '<button type="button" onclick="quitItemTrunckSdDowngrade();" disabled="true" id="quitItem21" class="btn btn-danger">Eliminar</button>'+
                                                    '</td>'+
                                                '</tr>'+
                                            '</tbody>'+
                                        '</table>'+
                                    '</div>'+
                                '</div>'+
                                '<div id="bodyDowngradeTrunsd" class="tab-base tab-stacked-left">'+
                                    '<ul id="tabs_truncksd_downgrade" class="nav nav-tabs">'+
                                    '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

            $('#contenedor_truncksd_boxs').append(item);
            $('#addItem20').attr('disabled', true);
            $('#quitItem20').attr('disabled', false);   
            TrunkSdDowngradePanelActivity = 1;
        }else{ 
            $('#addItem20').attr('disabled', true);
            $('#quitItem20').attr('disabled', false);
            if(TrunkSdDowngradePanelActivity==1){    
                addItemTrunckSdUpgrade();
            }else{
                alertFail('Trunck Con Datos: Downgrade', 'Debe seleccionar Upgrade en la lista desplegable');
                $('#solicitudTrunkSd').select();
                $('#solicitudTrunkSd').focus();
                return false;
            }
        }
    }

    if(solicitud==7){
        addItemTrunckSdCambioProducto();
    }
}

function quitItemTrunckSd(){
    var solicitud = $('#solicitudTrunkSd').val();

    if(solicitud==1){
       quitItemTruncksdInstalacion();
    }

    if(solicitud==2){
        $('#contenedor_truncksd_boxs_upgrade').remove();
        TrunkSdUpgradeTelefonia = 0;
        TrunkSdUpgradeInternetDedicado = 0;
        TrunkSdUpgradeAccesoMPLS = 0;
        TrunkSdUpgradeGeneral = 0;
        TrunkSdUpgradePanelActivity = 0;
        $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
        $('#cantidad_trunckSD_upgrade_telefonia_filas').val(TrunkSdUpgradeTelefonia);
        $('#cantidad_trunckSD_upgrade_internetDedicado_filas').val(TrunkSdUpgradeInternetDedicado);
        $('#cantidad_trunckSD_upgrade_accesoMpls_filas').val(TrunkSdUpgradeAccesoMPLS);

        $('#quitItem20').attr('disabled', true);
        $('#addItem20').attr('disabled', false);
    }

    if(solicitud==3){
        quitItemTrunckSdTraslado();
    }

    if(solicitud==4){
        quitItemTrunckSdAcceso();
    }

    if(solicitud==5){
        quitItemTrunckSdRenegociacion();
    }

    if(solicitud==6){
        $('#contenedor_truncksd_boxs_downgrade').remove();
        TrunkSdDowngradeTelefonia = 0;
        TrunkSdDowngradeInternetDedicado = 0;
        TrunkSdDowngradeAccesoMPLS = 0;
        TrunkSdDowngradeGeneral = 0;
        TrunkSdDowngradePanelActivity = 0;

        $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
        $('#cantidad_trunckSD_downgrade_telefonia_filas').val(TrunkSdDowngradeTelefonia);
        $('#cantidad_trunckSD_downgrade_accesoMpls_filas').val(TrunkSdDowngradeAccesoMPLS);
        $('#cantidad_trunckSD_downgrade_internetDedicado_filas').val(TrunkSdDowngradeInternetDedicado);

        $('#quitItem20').attr('disabled', true);
        $('#addItem20').attr('disabled', false);
    }

    if(solicitud==7){
        quitItemTrunckSdCambioProducto();
    }

}


/*********************************************************************** Instalacion ************************************************************************/
var TrunkSdInstalacion = 0;
var TrunkSdTraslado = 0;
var TrunkSdCambioAcceso = 0;
var TrunkSdRenegociación = 0;


function addItemTruncksdInstalacion(){
    
    if(TrunkSdInstalacion==0){
       $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_instalador" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Instalación</h3></div><div class="panel-body"><div id="contenedor_truncksd_instalacion" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunkSdInstalacion++;
    $('#cantidad_trunckSD_instalacion_filas').val(TrunkSdInstalacion);

    var item = '<div style="margin-bottom:15px;" id="row_truncksd_instalacion_'+TrunkSdInstalacion+'" class="row">'+
                '<ul id="tabs_truncksd_instalacion_'+TrunkSdInstalacion+'" class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#truncksd-tab-1-'+TrunkSdInstalacion+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#truncksd-tab-2-'+TrunkSdInstalacion+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#truncksd-tab-3-'+TrunkSdInstalacion+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                '</ul>'+
                '<div id="content_truncksd_instalacion_'+TrunkSdInstalacion+'" class="tab-content">'+
                    '<div id="truncksd-tab-1-'+TrunkSdInstalacion+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td width="50%">Número de OFT</td>'+
                                    '<td width="50%"><input type="number" id="tsd_inst_solicitud_oft_'+TrunkSdInstalacion+'" name="tsd_inst_solicitud_oft_'+TrunkSdInstalacion+'" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>'+
                                        '<div class="radio"><input id="tsd_inst_requisito_'+TrunkSdInstalacion+'_1" data="'+TrunkSdInstalacion+'" data-col="1" class="magic-radio tsdradioRequerimientos" type="radio" value="Requiere Central Combo" name="tsd_inst_requisito_'+TrunkSdInstalacion+'" checked=""><label for="tsd_inst_requisito_'+TrunkSdInstalacion+'_1">Requiere Central Combo</label></div>'+
                                        '<div class="radio"><input id="tsd_inst_requisito_'+TrunkSdInstalacion+'_2" data="'+TrunkSdInstalacion+'" data-col="2" class="magic-radio tsdradioRequerimientos" type="radio" value="Cliente posee central propia" name="tsd_inst_requisito_'+TrunkSdInstalacion+'"><label for="tsd_inst_requisito_'+TrunkSdInstalacion+'_2">Cliente posee central propia</label></div>'+
                                        '<div class="radio"><input id="tsd_inst_requisito_'+TrunkSdInstalacion+'_3" data="'+TrunkSdInstalacion+'" data-col="3" class="magic-radio tsdradioRequerimientos" type="radio" value="Requiere central a medida" name="tsd_inst_requisito_'+TrunkSdInstalacion+'"><label for="tsd_inst_requisito_'+TrunkSdInstalacion+'_3">Requiere central a medida</label></div>'+
                                    '</td>'+
                                    '<td>'+
                                        '<div class="panel-group accordion" id="tsd-avisos-requerimientos-'+TrunkSdInstalacion+'" style="margin-bottom:0px;">'+
                                            
                                            '<div class="panel panel-info">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Requiere central combo</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse in tsdcollapsesRequerimientos-'+TrunkSdInstalacion+'" id="collapse-SD-'+TrunkSdInstalacion+'-1" aria-expanded="true">'+
                                                    '<div class="panel-body">Considerar los valores de cartilla vigente.'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="panel panel-info">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Requiere central a medida</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse tsdcollapsesRequerimientos-'+TrunkSdInstalacion+'" id="collapse-SD-'+TrunkSdInstalacion+'-2" aria-expanded="false" style="">'+
                                                    '<div class="panel-body">Debe ingresar ingresar formulario AD Equipos Telco mediante USV.</div>'+
                                                '</div>'+
                                            '</div>'+
                                            
                                            '<div class="panel panel-info" style="margin-bottom:0px;">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Cliente posee central a propia.</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse tsdcollapsesRequerimientos-'+TrunkSdInstalacion+'" id="collapse-SD-'+TrunkSdInstalacion+'-3" aria-expanded="false">'+
                                                    '<div class="panel-body">Se debe asegurar que la central del cliente este homologada con el servicio de Entel.</div>'+
                                                '</div>'+
                                            '</div>'+

                                        '</div>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="truncksd-tab-2-'+TrunkSdInstalacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="tsd_inst_telefonia_cantCanales_'+TrunkSdInstalacion+'" name="tsd_inst_telefonia_cantCanales_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="tsd_inst_telefonia_minSlm_'+TrunkSdInstalacion+'" name="tsd_inst_telefonia_minSlm_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos móvil</td>'+
                                    '<td><input id="tsd_inst_telefonia_minMovil_'+TrunkSdInstalacion+'" name="tsd_inst_telefonia_minMovil_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos adicionales</td>'+
                                    '<td><input id="tsd_inst_telefonia_minAdicional_'+TrunkSdInstalacion+'" name="tsd_inst_telefonia_minAdicional_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="truncksd-tab-3-'+TrunkSdInstalacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="tsd_inst_InternDedicado_minNacional_'+TrunkSdInstalacion+'" name="tsd_inst_InternDedicado_minNacional_'+TrunkSdInstalacion+'" type="number" class="form-control input-sml" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="tsd_inst_InternDedicado_minInteracional_'+TrunkSdInstalacion+'" name="tsd_inst_InternDedicado_minInteracional_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="tsd_inst_InternDedicado_precioObjetivoNacional_'+TrunkSdInstalacion+'" name="tsd_inst_InternDedicado_precioObjetivoNacional_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="tsd_inst_InternDedicado_plazo_'+TrunkSdInstalacion+'" name="tsd_inst_InternDedicado_plazo_'+TrunkSdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div>'+
                '</div>'+
            '</div>';

    $('#contenedor_truncksd_instalacion').append(item);

    $('.tsdradioRequerimientos').bind('click', function(){
        var data = $(this).attr('data');
        var dataCol = $(this).attr('data-col');
        $('.tsdcollapsesRequerimientos-'+data).collapse('hide');
        $('#collapse-SD-'+data+'-'+dataCol).collapse('show');
    });

    $('#quitItem20').attr('disabled', false);
    setInputNumber();
}   

function quitItemTruncksdInstalacion(){
    $('#row_truncksd_instalacion_'+TrunkSdInstalacion).remove();
    TrunkSdInstalacion--;
    $('#cantidad_trunckSD_instalacion_filas').val(TrunkSdInstalacion);
    if(TrunkSdInstalacion==0){
        $('#contenedor_truncksd_boxs_instalador').remove();
        $('#quitItem20').attr('disabled', true);
    }
}

/*********************************************************************** Upgrade ************************************************************************/

var TrunkSdUpgrade = 0;
var TrunkSdUpgradeTelefonia = 0;
var TrunkSdUpgradeInternetDedicado = 0;
var TrunkSdUpgradeAccesoMPLS = 0;
var TrunkSdUpgradeGeneral = 0;
var TrunkSdUpgradePanelActivity = 0;

function quitItemTrunckSdUpgrade(){
   var solicitud14 = $('#solicitud14').val();
    
   if(solicitud14==1){
        $("#trunksd-upgradeT-li-"+TrunkSdUpgradeTelefonia).remove();
        $("#trunksd-upgradeT-contain-"+TrunkSdUpgradeTelefonia).remove();
        TrunkSdUpgradeTelefonia--;
        TrunkSdUpgradeGeneral--;

        $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
        $('#cantidad_trunckSD_upgrade_telefonia_filas').val(TrunkSdUpgradeTelefonia);

        if(TrunkSdUpgradeTelefonia==0){
            $('#quitItem14').attr('disabled', true);
        }

       if(TrunkSdUpgradeGeneral==0){
            $('#contenedor_truncksd_boxs_upgrade').remove();
            TrunkSdUpgradePanelActivity = 0;
            $('#quitItem14').attr('disabled', true);
            $('#addItem14').attr('disabled', false);
       }
   }

   if(solicitud14==2){
       $("#trunksd-upgradeId-li-"+TrunkSdUpgradeInternetDedicado).remove(); 
       $("#trunksd-upgradeId-contain-"+TrunkSdUpgradeInternetDedicado).remove();
       TrunkSdUpgradeInternetDedicado--;
       TrunkSdUpgradeGeneral--;

       $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
       $('#cantidad_trunckSD_upgrade_internetDedicado_filas').val(TrunkSdUpgradeInternetDedicado);

       if(TrunkSdUpgradeInternetDedicado==0){
            $('#quitItem14').attr('disabled', true);
       }

       if(TrunkSdUpgradeGeneral==0){
            $('#contenedor_truncksd_boxs_upgrade').remove();
            TrunkSdUpgradePanelActivity = 0;
            $('#quitItem14').attr('disabled', true);
            $('#addItem14').attr('disabled', false);
       }
   }

   if(solicitud14==3){
       $("#trunksd-upgradeAMPLS-li-"+TrunkSdUpgradeAccesoMPLS).remove(); 
       $("#trunksd-upgradeAMPLS-contain-"+TrunkSdUpgradeAccesoMPLS).remove();
       TrunkSdUpgradeAccesoMPLS--;
       TrunkSdUpgradeGeneral--;

       $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
       $('#cantidad_trunckSD_upgrade_accesoMpls_filas').val(TrunkSdUpgradeAccesoMPLS);

       if(TrunkSdUpgradeAccesoMPLS==0){
            $('#quitItem14').attr('disabled', true);
       }

       if(TrunkSdUpgradeGeneral==0){
            $('#contenedor_truncksd_boxs_upgrade').remove();
            TrunkSdUpgradePanelActivity = 0;
            $('#quitItem14').attr('disabled', true);
            $('#addItem14').attr('disabled', false);
       }

   }

   if(TrunkSdUpgradeGeneral==0){
        $('#contenedor_truncksd_boxs_upgrade').remove();
        TrunkSdUpgradePanelActivity = 0;
        $('#quitItem14').attr('disabled', true);
        $('#addItem14').attr('disabled', false);
   }

   monitorTrunckSd();

}

function addItemTrunckSdUpgrade(){

    var solicitud14 = $('#solicitud14').val();
    var item = "";
    var contain = "";
    
    var activeTab = "";
    var activeContain = "";
    var activetrue = "";

    if(TrunkSdUpgradeGeneral==0){
        $('#bodyUpgradeTrunsd').append('<div id="content_truncksd_upgrade" class="tab-content"></div>');
        activeTab = "active";    
        activeContain = "active in";
        activetrue = "true";
    }else{
        activetrue = "false";
    }

    if(solicitud14==1){
        TrunkSdUpgradeTelefonia++;
        TrunkSdUpgradeGeneral++;

        $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
        $('#cantidad_trunckSD_upgrade_telefonia_filas').val(TrunkSdUpgradeTelefonia);

        item = '<li id="trunksd-upgradeT-li-'+TrunkSdUpgradeTelefonia+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-upgradeT-contain-'+TrunkSdUpgradeTelefonia+'" aria-expanded="'+activetrue+'">Telefonia</a>'+
        '</li>';

        contain = '<div id="trunksd-upgradeT-contain-'+TrunkSdUpgradeTelefonia+'" class="tab-pane fade '+activeContain+'">'+  
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input type="number" id="trunckSD_upgrade_telefonia_numOft_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_numOft_'+TrunkSdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input type="number" id="trunckSD_upgrade_telefonia_CodServicio_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_CodServicio_'+TrunkSdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input type="number" id="trunckSD_upgrade_telefonia_contVigente_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_contVigente_'+TrunkSdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input type="number" id="trunckSD_upgrade_telefonia_ctaFacturacion_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_ctaFacturacion_'+TrunkSdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Actual</th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Cant. de canales</td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_cantCanales_actual_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_cantCanales_actual_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_cantCanales_solicitada_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_cantCanales_solicitada_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos SLM</td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_minSLM_actual_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_minSLM_actual_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_minSLM_solicitada_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_minSLM_solicitada_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos a Móvil</td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_minMovil_actual_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_minMovil_actual_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_upgrade_telefonia_minMovil_solicitada_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_minMovil_solicitada_'+TrunkSdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentario</td>'+
                                        '<td colspan="2"><textarea id="trunckSD_upgrade_telefonia_comentario_'+TrunkSdUpgradeTelefonia+'" name="trunckSD_upgrade_telefonia_comentario_'+TrunkSdUpgradeTelefonia+'" placeholder="Message" rows="5" class="form-control" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_upgrade").append(item);
        $("#content_truncksd_upgrade").append(contain);
        $('#quitItem14').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud14==2){
        TrunkSdUpgradeInternetDedicado++;
        TrunkSdUpgradeGeneral++;

        $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
       $('#cantidad_trunckSD_upgrade_internetDedicado_filas').val(TrunkSdUpgradeInternetDedicado);

        item = '<li id="trunksd-upgradeId-li-'+TrunkSdUpgradeInternetDedicado+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-upgradeId-contain-'+TrunkSdUpgradeInternetDedicado+'" aria-expanded="'+activetrue+'">Internet Dedicado</a>'+
        '</li>';

        contain = '<div id="trunksd-upgradeId-contain-'+TrunkSdUpgradeInternetDedicado+'" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_numOft_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_numOft_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_CodServicio_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_CodServicio_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_contVigente_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_contVigente_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_ctaFacturacion_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_ctaFacturacion_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Vel. Nacional</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_velNacional_solicitada_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Vel. Internacional</td>'+
                                        '<td><input id="trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_velInternacional_solicitada_'+TrunkSdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckSD_upgrade_InternetDedicado_comentario_solicitada_'+TrunkSdUpgradeInternetDedicado+'" name="trunckSD_upgrade_InternetDedicado_comentario_solicitada_'+TrunkSdUpgradeInternetDedicado+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_upgrade").append(item);
        $("#content_truncksd_upgrade").append(contain);
        $('#quitItem14').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud14==3){
        TrunkSdUpgradeAccesoMPLS++;
        TrunkSdUpgradeGeneral++;

        $('#cantidad_trunckSD_upgrade_filas').val(TrunkSdUpgradeGeneral);
        $('#cantidad_trunckSD_upgrade_accesoMpls_filas').val(TrunkSdUpgradeAccesoMPLS);

        item = '<li id="trunksd-upgradeAMPLS-li-'+TrunkSdUpgradeAccesoMPLS+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-upgradeAMPLS-contain-'+TrunkSdUpgradeAccesoMPLS+'" aria-expanded="'+activetrue+'">Acceso MPLS</a>'+
        '</li>';
        
        contain = '<div id="trunksd-upgradeAMPLS-contain-'+TrunkSdUpgradeAccesoMPLS+'"" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckSD_upgrade_accesoMPLS_numOft_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_numOft_'+TrunkSdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckSD_upgrade_accesoMPLS_codServicio_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_codServicio_'+TrunkSdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckSD_upgrade_accesoMPLS_contratoVigente_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_contratoVigente_'+TrunkSdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckSD_upgrade_accesoMPLS_cuentaFacturacion_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_cuentaFacturacion_'+TrunkSdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                           '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Velocidad</td>'+
                                        '<td><input id="trunckSD_upgrade_accesoMPLS_velocidad_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_velocidad_'+TrunkSdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckSD_upgrade_accesoMPLS_comentario_'+TrunkSdUpgradeAccesoMPLS+'" name="trunckSD_upgrade_accesoMPLS_comentario_'+TrunkSdUpgradeAccesoMPLS+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_upgrade").append(item);
        $("#content_truncksd_upgrade").append(contain);  
        $('#quitItem14').attr('disabled', false); 
        setInputNumber();     
    }
}

function monitortruncksdupgrade(){
    var solicitud14 = $('#solicitud14').val();
    if(solicitud14>0){
        
        $('#addItem14').attr('disabled', false);

        if(solicitud14==1){
            if(TrunkSdUpgradeTelefonia>0){
                $('#quitItem14').attr('disabled', false);
            }else{
                $('#quitItem14').attr('disabled', true);
            }    
        }

        if(solicitud14==2){
            if(TrunkSdUpgradeInternetDedicado>0){
                $('#quitItem14').attr('disabled', false);
            }else{
                $('#quitItem14').attr('disabled', true);
            }                        
        }

        if(solicitud14==3){
            if(TrunkSdUpgradeAccesoMPLS>0){
                $('#quitItem14').attr('disabled', false);
            }else{
                $('#quitItem14').attr('disabled', true);
            }    
        }

    }else{
        $('#addItem14').attr('disabled', true);
        $('#quitItem14').attr('disabled', true);
    }
}


/*********************************************************************** Traslado ************************************************************************/
var TrunckSdTraslado = 0;
function addItemTrunckSdTraslado(){

    if(TrunckSdTraslado==0){
        $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_traslado" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Traslado</h3></div><div class="panel-body"><div id="TrunksdContenedorTraslado" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunckSdTraslado++;

    $('#cantidad_trunckSD_traslado_filas').val(TrunckSdTraslado);

    var item = '<div style="margin-bottom:15px;" id="fila_TrunckSdTraslado_'+TrunckSdTraslado+'" class="row">'+
            '<ul class="nav nav-tabs">'+
                        '<li class="active">'+
                            '<a data-toggle="tab" href="#contenedor_TrunckSdTraslado_solicitud_'+TrunckSdTraslado+'" aria-expanded="true">Solicitud</a>'+
                        '</li>'+
                        '<li>'+
                            '<a data-toggle="tab" href="#contenedor_TrunckSdTraslado_telefonia_'+TrunckSdTraslado+'" aria-expanded="false">Telefonia</a>'+
                        '</li>'+
                        '<li class="">'+
                            '<a data-toggle="tab" href="#contenedor_TrunckSdTraslado_internetdedicado_'+TrunckSdTraslado+'" aria-expanded="false">Internet Dedicado</a>'+
                        '</li>'+
            '</ul>'+
            '<div class="tab-content">'+
                '<div id="contenedor_TrunckSdTraslado_solicitud_'+TrunckSdTraslado+'" class="tab-pane fade active in">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Numero de OFT</td>'+
                                '<td><input id="trunckSD_Traslado_Solicitud_numOft_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Solicitud_numOft_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckSD_Traslado_Solicitud_codServicio_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Solicitud_codServicio_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cuenta Facturación</td>'+
                                '<td><input id="trunckSD_Traslado_Solicitud_cuentaFacturacion_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Solicitud_cuentaFacturacion_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckSdTraslado_telefonia_'+TrunckSdTraslado+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Cant. de canales</td>'+
                                '<td><input id="trunckSD_Traslado_Telefonia_cantCanales_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Telefonia_cantCanales_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos SLM</td>'+
                                '<td><input id="trunckSD_Traslado_Telefonia_minSLM_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Telefonia_minSLM_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos móvil</td>'+
                                '<td><input id="trunckSD_Traslado_Telefonia_minMovil_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Telefonia_minMovil_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos adicionales</td>'+
                                '<td><input id="trunckSD_Traslado_Telefonia_minAdicional_'+TrunckSdTraslado+'" name="trunckSD_Traslado_Telefonia_minAdicional_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckSdTraslado_internetdedicado_'+TrunckSdTraslado+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                               '<td>Vel. Nacional</td>'+
                               '<td><input id="trunckSD_Traslado_InternetDedicado_velNacional_'+TrunckSdTraslado+'" name="trunckSD_Traslado_InternetDedicado_velNacional_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                           '</tr>'+
                            '<tr>'+
                                '<td>Vel. Internacional</td>'+
                                '<td><input id="trunckSD_Traslado_InternetDedicado_velInternacional_'+TrunckSdTraslado+'" name="trunckSD_Traslado_InternetDedicado_velInternacional_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckSD_Traslado_InternetDedicado_precioObjetivo_'+TrunckSdTraslado+'" name="trunckSD_Traslado_InternetDedicado_precioObjetivo_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckSD_Traslado_InternetDedicado_plazo_'+TrunckSdTraslado+'" name="trunckSD_Traslado_InternetDedicado_plazo_'+TrunckSdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div>'+
        '</div>';


    $('#TrunksdContenedorTraslado').append(item);        
    $('#quitItem20').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckSdTraslado(){
    $("#fila_TrunckSdTraslado_"+TrunckSdTraslado).remove();
    TrunckSdTraslado--;
    $('#cantidad_trunckSD_traslado_filas').val(TrunckSdTraslado);
    if(TrunckSdTraslado==0){
        $('#contenedor_truncksd_boxs_traslado').remove();
        $('#quitItem20').attr('disabled', true);
    }
}



/*********************************************************************** DownGrade ************************************************************************/
var TrunkSdDowngrade = 0;
var TrunkSdDowngradeTelefonia = 0;
var TrunkSdDowngradeInternetDedicado = 0;
var TrunkSdDowngradeAccesoMPLS = 0;
var TrunkSdDowngradeGeneral = 0;
var TrunkSdDowngradePanelActivity = 0;
    
function quitItemTrunckSdDowngrade(){
   var solicitud21 = $('#solicitud21').val();
    
   if(solicitud21==1){
        $("#trunksd-downgradeT-li-"+TrunkSdDowngradeTelefonia).remove();
        $("#trunksd-downgradeT-contain-"+TrunkSdDowngradeTelefonia).remove();
        TrunkSdDowngradeTelefonia--;
        TrunkSdDowngradeGeneral--;

        $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
        $('#cantidad_trunckSD_downgrade_telefonia_filas').val(TrunkSdDowngradeTelefonia);

        if(TrunkSdDowngradeTelefonia==0){
            $('#quitItem21').attr('disabled', true);
        }

       if(TrunkSdDowngradeGeneral==0){
            $('#contenedor_truncksd_boxs_downgrade').remove();
            TrunkSdDowngradePanelActivity = 0;
            $('#quitItem21').attr('disabled', true);
            $('#addItem21').attr('disabled', false);
       }
   }

   if(solicitud21==2){
       $("#trunksd-downgradeId-li-"+TrunkSdDowngradeInternetDedicado).remove(); 
       $("#trunksd-downgradeId-contain-"+TrunkSdDowngradeInternetDedicado).remove();
       TrunkSdDowngradeInternetDedicado--;
       TrunkSdDowngradeGeneral--;

       $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
       $('#cantidad_trunckSD_downgrade_internetDedicado_filas').val(TrunkSdDowngradeInternetDedicado);

       if(TrunkSdDowngradeInternetDedicado==0){
            $('#quitItem21').attr('disabled', true);
            $('#addItem21').attr('disabled', false);
       }

       if(TrunkSdDowngradeGeneral==0){
            $('#contenedor_truncksd_boxs_downgrade').remove();
            TrunkSdDowngradePanelActivity = 0;
            $('#quitItem21').attr('disabled', true);
            $('#addItem21').attr('disabled', false);
       }
   }

   if(solicitud21==3){
       $("#trunksd-downgradeAMPLS-li-"+TrunkSdDowngradeAccesoMPLS).remove(); 
       $("#trunksd-downgradeAMPLS-contain-"+TrunkSdDowngradeAccesoMPLS).remove();
       TrunkSdDowngradeAccesoMPLS--;
       TrunkSdDowngradeGeneral--;

       $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
       $('#cantidad_trunckSD_downgrade_accesoMpls_filas').val(TrunkSdDowngradeAccesoMPLS);

       if(TrunkSdDowngradeAccesoMPLS==0){
            $('#quitItem21').attr('disabled', true);
       }

       if(TrunkSdDowngradeGeneral==0){
            $('#contenedor_truncksd_boxs_downgrade').remove();
            TrunkSdDowngradePanelActivity = 0;
            $('#quitItem21').attr('disabled', true);
            $('#addItem21').attr('disabled', false);
       }

   }

   if(TrunkSdDowngradeGeneral==0){
        $('#contenedor_truncksd_boxs_downgrade').remove();
        TrunkSdDowngradePanelActivity = 0;
        $('#quitItem21').attr('disabled', true);
        $('#addItem21').attr('disabled', false);
   }

   monitorTrunckSd();

}

function addItemTrunckSdDowngrade(){

    var solicitud21 = $('#solicitud21').val();
    var item = "";
    var contain = "";
    
    var activeTab = "";
    var activeContain = "";
    var activetrue = "";

    if(TrunkSdDowngradeGeneral==0){
        $('#bodyDowngradeTrunsd').append('<div id="content_truncksd_downgrade" class="tab-content"></div>');
        activeTab = "active";    
        activeContain = "active in";
        activetrue = "true";
    }else{
        activetrue = "false";
    }

    if(solicitud21==1){
        TrunkSdDowngradeTelefonia++;
        TrunkSdDowngradeGeneral++;

        $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
        $('#cantidad_trunckSD_downgrade_telefonia_filas').val(TrunkSdDowngradeTelefonia);

        item = '<li id="trunksd-downgradeT-li-'+TrunkSdDowngradeTelefonia+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-downgradeT-contain-'+TrunkSdDowngradeTelefonia+'" aria-expanded="'+activetrue+'">Telefonia</a>'+
        '</li>';
        
        contain = '<div id="trunksd-downgradeT-contain-'+TrunkSdDowngradeTelefonia+'" class="tab-pane fade '+activeContain+'">'+  
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_numOft_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_numOft_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_codServicio_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_codServicio_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_contratoVigente_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_contratoVigente_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_cuentaFacturacion_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_cuentaFacturacion_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Actual</th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Cant. de canales</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_cantCanales_actual_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_cantCanales_actual_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_cantCanales_solicitada_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_cantCanales_solicitada_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos SLM</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_minSLM_actual_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_minSLM_actual_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_minSLM_solicitada_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_minSLM_solicitada_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos a Móvil</td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_minMovil_actual_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_minMovil_actual_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckSD_downgrade_telefonia_minMovil_solicitada_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_minMovil_solicitada_'+TrunkSdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentario</td>'+
                                        '<td colspan="2"><textarea id="trunckSD_downgrade_telefonia_comentario_'+TrunkSdDowngradeTelefonia+'" name="trunckSD_downgrade_telefonia_comentario_'+TrunkSdDowngradeTelefonia+'" placeholder="Message" rows="5" class="form-control" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_downgrade").append(item);
        $("#content_truncksd_downgrade").append(contain);
        $('#quitItem21').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud21==2){
        TrunkSdDowngradeInternetDedicado++;
        TrunkSdDowngradeGeneral++;

        $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
        $('#cantidad_trunckSD_downgrade_internetDedicado_filas').val(TrunkSdDowngradeInternetDedicado);

        item = '<li id="trunksd-downgradeId-li-'+TrunkSdDowngradeInternetDedicado+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-downgradeId-contain-'+TrunkSdDowngradeInternetDedicado+'" aria-expanded="'+activetrue+'">Internet Dedicado</a>'+
        '</li>';

        contain = '<div id="trunksd-downgradeId-contain-'+TrunkSdDowngradeInternetDedicado+'" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_numOft_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_numOft_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_codServicio_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_codServicio_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_contratoVigente_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_contratoVigente_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_cuentaFacturacion_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_cuentaFacturacion_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Vel. Nacional</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_velNacional_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_velNacional_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Vel. Internacional</td>'+
                                        '<td><input id="trunckSD_downgrade_internetDedicado_velInternacional_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_velInternacional_'+TrunkSdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckSD_downgrade_internetDedicado_comentario_'+TrunkSdDowngradeInternetDedicado+'" name="trunckSD_downgrade_internetDedicado_comentario_'+TrunkSdDowngradeInternetDedicado+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_downgrade").append(item);
        $("#content_truncksd_downgrade").append(contain);
        $('#quitItem21').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud21==3){
        TrunkSdDowngradeAccesoMPLS++;
        TrunkSdDowngradeGeneral++;

        $('#cantidad_trunckSD_downgrade_filas').val(TrunkSdDowngradeGeneral);
        $('#cantidad_trunckSD_downgrade_accesoMpls_filas').val(TrunkSdDowngradeAccesoMPLS);

        item = '<li id="trunksd-downgradeAMPLS-li-'+TrunkSdDowngradeAccesoMPLS+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunksd-downgradeAMPLS-contain-'+TrunkSdDowngradeAccesoMPLS+'" aria-expanded="'+activetrue+'">Acceso MPLS</a>'+
        '</li>';
       
       contain = '<div id="trunksd-downgradeAMPLS-contain-'+TrunkSdDowngradeAccesoMPLS+'"" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckSD_downgrade_accesoMpls_numOft_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_numOft_'+TrunkSdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckSD_downgrade_accesoMpls_codServicio_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_codServicio_'+TrunkSdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckSD_downgrade_accesoMpls_contratoVigente_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_contratoVigente_'+TrunkSdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckSD_downgrade_accesoMpls_cuentaFacturacion_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_cuentaFacturacion_'+TrunkSdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                           '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Velocidad</td>'+
                                        '<td><input id="trunckSD_downgrade_accesoMpls_velocidad_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_velocidad_'+TrunkSdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckSD_downgrade_accesoMpls_comentario_'+TrunkSdDowngradeAccesoMPLS+'" name="trunckSD_downgrade_accesoMpls_comentario_'+TrunkSdDowngradeAccesoMPLS+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_truncksd_downgrade").append(item);
        $("#content_truncksd_downgrade").append(contain);  
        $('#quitItem21').attr('disabled', false);   
        setInputNumber();   
    }
}

function monitortruncksddowngrade(){
    var solicitud21 = $('#solicitud21').val();
    if(solicitud21>0){
        
        $('#addItem21').attr('disabled', false);

        if(solicitud21==1){
            if(TrunkSdDowngradeTelefonia>0){
                $('#quitItem21').attr('disabled', false);
            }else{
                $('#quitItem21').attr('disabled', true);
            }    
        }

        if(solicitud21==2){
            if(TrunkSdDowngradeInternetDedicado>0){
                $('#quitItem21').attr('disabled', false);
            }else{
                $('#quitItem21').attr('disabled', true);
            }                        
        }

        if(solicitud21==3){
            if(TrunkSdDowngradeAccesoMPLS>0){
                $('#quitItem21').attr('disabled', false);
            }else{
                $('#quitItem21').attr('disabled', true);
            }    
        }

    }else{
        $('#addItem21').attr('disabled', true);
        $('#quitItem21').attr('disabled', true);
    }
}

/*********************************************************************** Renegociacion ************************************************************************/
var TrunckSdRenegociacion = 0;
function addItemTrunckSdRenegociacion(){

    if(TrunckSdRenegociacion==0){
        $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_renegociacion" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Renegociación</h3></div><div class="panel-body"><div id="contenedor_truncksd_renegociacion" class="tab-base tab-stacked-left"></div></div></div>'); 
    }

    TrunckSdRenegociacion++;
    $('#cantidad_trunckSD_renegociacion_filas').val(TrunckSdRenegociacion);

    var item = '<div style="margin-bottom:15px;" id="fila_TrunckSdRenegociacion_'+TrunckSdRenegociacion+'" class="row">'+
                '<ul class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdRenegociacion_solicitud_'+TrunckSdRenegociacion+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdRenegociacion_telefonia_'+TrunckSdRenegociacion+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdRenegociacion_InternetDedicado_'+TrunckSdRenegociacion+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                '</ul>'+
                '<div class="tab-content">'+
                    '<div id="contenedor_TrunckSdRenegociacion_solicitud_'+TrunckSdRenegociacion+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Numero de OFT</td>'+
                                    '<td><input id="trunckSD_renegociacion_solicitud_numOft_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_solicitud_numOft_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckSD_renegociacion_solicitud_codServicio_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_solicitud_codServicio_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckSD_renegociacion_solicitud_contratoVigente_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_solicitud_contratoVigente_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cuenta Facturación</td>'+
                                    '<td><input id="trunckSD_renegociacion_solicitud_cuentaFacturacion_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_solicitud_cuentaFacturacion_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                               '</tr>'+
                            '</tbody>'+
                       '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckSdRenegociacion_telefonia_'+TrunckSdRenegociacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+       
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="trunckSD_renegociacion_Telefonia_cantCanales_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_Telefonia_cantCanales_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="trunckSD_renegociacion_Telefonia_minSLM_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_Telefonia_minSLM_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos Móvil</td>'+
                                    '<td><input id="trunckSD_renegociacion_Telefonia_minMovil_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_Telefonia_minMovil_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckSD_renegociacion_Telefonia_contratoVigente_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_Telefonia_contratoVigente_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Telefonía</td>'+
                                    '<td><input id="trunckSD_renegociacion_Telefonia_contratoTelefonia_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_Telefonia_contratoTelefonia_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckSdRenegociacion_InternetDedicado_'+TrunckSdRenegociacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="trunckSD_renegociacion_InternetDedicado_velNacional_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_InternetDedicado_velNacional_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="trunckSD_renegociacion_InternetDedicado_velInternacional_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_InternetDedicado_velInternacional_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_InternetDedicado_precioObjetivo_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckSD_renegociacion_InternetDedicado_plazo_'+TrunckSdRenegociacion+'" name="trunckSD_renegociacion_InternetDedicado_plazo_'+TrunckSdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                               '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div></div>';

    $('#contenedor_truncksd_renegociacion').append(item);
    $('#quitItem20').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckSdRenegociacion(){
    $('#fila_TrunckSdRenegociacion_'+TrunckSdRenegociacion).remove();
    TrunckSdRenegociacion--;
    $('#cantidad_trunckSD_renegociacion_filas').val(TrunckSdRenegociacion);
    if(TrunckSdRenegociacion==0){
        $('#contenedor_truncksd_boxs_renegociacion').remove();
        $('#quitItem20').attr('disabled', true);
    }
}


/*********************************************************************** Acceso ************************************************************************/
var TrunckSdAcceso = 0;
function addItemTrunckSdAcceso(){

    if(TrunckSdAcceso==0){
         $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_cambioacceso" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Cambio Acceso</h3></div><div class="panel-body"><div id="contenedor_truncksd_acceso" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunckSdAcceso++;

    $('#cantidad_trunckSD_cambioAcceso_filas').val(TrunckSdAcceso);

    var item = '<div style="margin-bottom:15px;" id="fila_TrunckSdAcceso_'+TrunckSdAcceso+'" class="row">'+
                '<ul class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdAcceso_solicitud_'+TrunckSdAcceso+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdAcceso_telefonia_'+TrunckSdAcceso+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckSdAcceso_internetDedicado_'+TrunckSdAcceso+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                '</ul>'+
                '<div  class="tab-content">'+
                    '<div id="contenedor_TrunckSdAcceso_solicitud_'+TrunckSdAcceso+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Numero de OFT</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_solicitud_numOft_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_solicitud_numOft_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_solicitud_codServicio_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_solicitud_codServicio_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_solicitud_contratoVigente_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_solicitud_contratoVigente_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cuenta Facturación</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_solicitud_cuentaFacturacion_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_solicitud_cuentaFacturacion_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckSdAcceso_telefonia_'+TrunckSdAcceso+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_Telefonia_cantCanales_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_Telefonia_cantCanales_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                               '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_Telefonia_minSLM_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_Telefonia_minSLM_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos móvil</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_Telefonia_minMovil_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_Telefonia_minMovil_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_Telefonia_contratoVigente_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_Telefonia_contratoVigente_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Telefonía</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_Telefonia_contratoTelefonia_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckSdAcceso_internetDedicado_'+TrunckSdAcceso+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_internetDedicado_velNacional_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_internetDedicado_velNacional_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_internetDedicado_velInternacional_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_internetDedicado_velInternacional_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_internetDedicado_precioObjetivo_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckSD_cambio_acceso_internetDedicado_plazo_'+TrunckSdAcceso+'" name="trunckSD_cambio_acceso_internetDedicado_plazo_'+TrunckSdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div></div>';

    $('#contenedor_truncksd_acceso').append(item);            
    $('#quitItem20').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckSdAcceso(){
    $('#fila_TrunckSdAcceso_'+TrunckSdAcceso).remove();
    TrunckSdAcceso--;
    $('#cantidad_trunckSD_cambioAcceso_filas').val(TrunckSdAcceso);

    if(TrunckSdAcceso==0){
        $('#contenedor_truncksd_boxs_cambioacceso').remove();
        $('#quitItem20').attr('disabled', true);
    }
}

/*********************************************************************** Cambio Producto ************************************************************************/
var TrunckSdCambioProducto = 0;
function addItemTrunckSdCambioProducto(){

    if(TrunckSdCambioProducto==0){
        $('#contenedor_truncksd_boxs').append('<div id="contenedor_truncksd_boxs_cambioproducto" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Cambio de Producto</h3></div><div class="panel-body"><div id="contenedor_truncksd_cambio_producto" class="tab-base tab-stacked-left"> </div></div></div>');
    }

    TrunckSdCambioProducto++;
    $('#cantidad_trunckSD_cambioProducto_filas').val(TrunckSdCambioProducto);

    var item = '<div style="margin-bottom:15px;" id="fila_TrunckSdCambioProducto_'+TrunckSdCambioProducto+'" class="row">'+
            '<ul class="nav nav-tabs">'+
                '<li class="active">'+
                    '<a data-toggle="tab" href="#contenedor_TrunckSdCambioProducto_solicitud_'+TrunckSdCambioProducto+'" aria-expanded="true">Solicitud</a>'+
                '</li>'+
                '<li>'+
                    '<a data-toggle="tab" href="#contenedor_TrunckSdCambioProducto_telefonia_'+TrunckSdCambioProducto+'" aria-expanded="false">Telefonia</a>'+
                '</li>'+
                '<li class="">'+
                    '<a data-toggle="tab" href="#contenedor_TrunckSdCambioProducto_InternetDedicado_'+TrunckSdCambioProducto+'" aria-expanded="false">Internet Dedicado</a>'+
                '</li>'+
            '</ul>'+
            '<div class="tab-content">'+
                '<div id="contenedor_TrunckSdCambioProducto_solicitud_'+TrunckSdCambioProducto+'" class="tab-pane fade active in">'+
                    '<table style="" class="table table-striped">'+
                        '<tbody>'+
                            '<tr>'+
                                '<td>Numero de OFT</td>'+
                                '<td><input id="trunckSD_cambioProducto_Solicitud_numOft_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_Solicitud_numOft_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckSD_cambioProducto_Solicitud_codServicio_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_Solicitud_codServicio_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Vigente</td>'+
                                '<td><input id="trunckSD_cambioProducto_Solicitud_contratoVigente_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_Solicitud_contratoVigente_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cuenta Facturación</td>'+
                                '<td><input id="trunckSD_cambioProducto_Solicitud_cuentaFacturacion_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_Solicitud_cuentaFacturacion_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckSdCambioProducto_telefonia_'+TrunckSdCambioProducto+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Cant. de canales</td>'+
                                '<td><input id="trunckSD_cambioProducto_telefonia_cantCanales_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_telefonia_cantCanales_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos SLM</td>'+
                                '<td><input id="trunckSD_cambioProducto_telefonia_minSLM_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_telefonia_minSLM_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos móvil</td>'+
                                '<td><input id="trunckSD_cambioProducto_telefonia_minMovil_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_telefonia_minMovil_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Central</td>'+
                                '<td><input id="trunckSD_cambioProducto_telefonia_contratoVigente_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_telefonia_contratoVigente_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Telefonía</td>'+
                                '<td><input id="trunckSD_cambioProducto_telefonia_contratoTelefonia_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_telefonia_contratoTelefonia_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckSdCambioProducto_InternetDedicado_'+TrunckSdCambioProducto+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Vel. Nacional</td>'+
                                '<td><input id="trunckSD_cambioProducto_InternetDedicado_velNacional_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_InternetDedicado_velNacional_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Vel. Internacional</td>'+
                                '<td><input id="trunckSD_cambioProducto_InternetDedicado_velInternacional_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_InternetDedicado_velInternacional_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_InternetDedicado_precioObjetivo_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckSD_cambioProducto_InternetDedicado_plazo_'+TrunckSdCambioProducto+'" name="trunckSD_cambioProducto_InternetDedicado_plazo_'+TrunckSdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div></div>';

    $('#contenedor_truncksd_cambio_producto').append(item);
    $('#quitItem20').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckSdCambioProducto(){
    $('#fila_TrunckSdCambioProducto_'+TrunckSdCambioProducto).remove();
    TrunckSdCambioProducto--;
    $('#cantidad_trunckSD_cambioProducto_filas').val(TrunckSdCambioProducto);
    
    if(TrunckSdCambioProducto==0){
        $('#contenedor_truncksd_boxs_cambioproducto').remove();
        $('#quitItem20').attr('disabled', true);
    }
}