//MPLS
var count_mpls_1 = 0;
var count_mpls_2 = 0;
var count_mpls_3 = 0;
var count_mpls_4 = 0;
var count_mpls_5 = 0;
var count_mpls_6 = 0;

var input_mpls_1 = 0;
var input_mpls_2 = 0;
var input_mpls_3 = 0;
var input_mpls_4 = 0;
var input_mpls_5 = 0;
var input_mpls_6 = 0;

function addMpls(){

    var solicitudmpls = $('#solicitudmpls').val();
    if(solicitudmpls>0){

        var item = "";
        if(solicitudmpls==1){

            //'<th>Cód. servicio Casa Matriz</th>'+
            if(count_mpls_1==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-instalacion" class="panel panel-dark">'+
                    '<div class="panel-heading">'+
                        '<h3 class="panel-title">Instalación</h3>'+
                      '</div>'+
                    '<div class="panel-body">'+
                        '<div class="table-responsive">'+
                          '<table style="" class="table table-striped">'+
                              '<thead>'+
                                    '<tr>'+
                                        '<th colspan="2">'+
                                            '<label>Checkear e ingresar código de servicio de la casa matriz en caso de existir una.</label>'+
                                            '<div style="width:100%;" class="input-group mar-btm">'+
                                                '<div class="input-group-addon">'+
                                                    '<input id="mpls_instalacion_check_codigo_casa_matriz" name="mpls_instalacion_check_codigo_casa_matriz" class="magic-checkbox" type="checkbox">'+
                                                    '<label id="mpls_instalacion_label_codigo_casa_matriz" for="mpls_instalacion_check_codigo_casa_matriz"></label>'+
                                                '</div>'+
                                                '<input type="number" class="form-control input-sm" name="mpls_instalacion_codigo_casa_matriz" id="mpls_instalacion_codigo_casa_matriz" readonly="readonly">'+ 
                                            '</div>'+
                                        '</th>'+
                                    '</tr>'+
                                    '<tr>'+
                                      '<th width="10%" class="isCasaMatriz"></th>'+
                                      '<th>Número OFT</th>'+
                                      '<th>Velocidad</th>'+
                                      '<th>Precio Objetivo</th>'+
                                      '<th>Plazo </th>'+
                                    '</tr>'+
                              '</thead>'+
                              '<tbody id="mpls-instalacion">'+
                              '</tbody>'+
                            '</table></div></div></div>');  

                  $('#mpls_instalacion_label_codigo_casa_matriz').click(function(){
                      var checkedIntslacionMPLS = $('#mpls_instalacion_check_codigo_casa_matriz').is(':checked');
                      if(!checkedIntslacionMPLS){
                          $('#mpls_instalacion_codigo_casa_matriz').attr('readonly', false);
                          $('.isCasaMatriz').fadeOut('fast');
                      }else{
                          $('#mpls_instalacion_codigo_casa_matriz').val('');
                          $('#mpls_instalacion_codigo_casa_matriz').attr('readonly', true);
                          $('.isCasaMatriz').fadeIn('fast');
                      }
                  });
            }

            count_mpls_1++;
            input_mpls_1++;

            $('#cantidad_mpls_instalacion_filas').val(count_mpls_1);

            var checked; 
            if(count_mpls_1==1){
              checked = "checked";
            }else{
              checked = "";
            }


            var isItemCasaMatruiz = $('#mpls_instalacion_check_codigo_casa_matriz').is(':checked');
            var itemShow = '';
            if(isItemCasaMatruiz){
              itemShow = 'style="display:none;"';
            }

            item = '<tr id="fila_1_'+input_mpls_1+'">'
                        +'<td '+itemShow+' class="isCasaMatriz">'
                            +'<div class="radio">'
                               +'<input id="mpls_instalacion_radio_codigo_casa_matriz_'+count_mpls_1+'" name="mpls_instalacion_radio_codigo_casa_matriz" value="'+count_mpls_1+'" class="magic-radio" type="radio" '+checked+'>'
                               +'<label for="mpls_instalacion_radio_codigo_casa_matriz_'+count_mpls_1+'">Casa Matriz</label>'
                            +'</div>'
                        +'</td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_numero_oft_1_'+input_mpls_1+'" id="mpls_numero_oft_1_'+input_mpls_1+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_1_'+input_mpls_1+'" id="mpls_velocidad_1_'+input_mpls_1+'" ></td>'
                        /*+'<td><input type="number" class="form-control input-sm" name="mpls_casamatriz_1_'+input_mpls_1+'" id="mpls_casamatriz_1_'+input_mpls_1+'" ></td>'*/
                        +'<td><input type="number" class="form-control input-sm" name="mpls_precioobjetivo_1_'+input_mpls_1+'" id="mpls_precioobjetivo_1_'+input_mpls_1+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_plazo_1_'+input_mpls_1+'" id="mpls_plazo_1_'+input_mpls_1+'" ></td>'
                    +'</tr>';                                                        
            
            $('#mpls-instalacion').append(item);
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        if(solicitudmpls==2){
            
            if(count_mpls_2==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-upgrade" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Upgrade</h3></div><div class="panel-body"><div class="table-responsive"><table style="" class="table table-striped"><thead><tr><th>Código de servicio</th><th>USV contrato vigente</th><th>Número OFT</th><th>Velocidad</th><th>Cód. servicio Casa Matriz</th><th>Cuenta Facturación</th></tr></thead><tbody id="mpls-upgrade"></tbody></table></div></div></div>');  
            }

            count_mpls_2++;
            input_mpls_2++;  

            $('#cantidad_mpls_upgrade_filas').val(input_mpls_2);

            item = '<tr id="fila_2_'+input_mpls_2+'">'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_codigo_servicio_2_'+input_mpls_2+'" id="mpls_codigo_servicio_2_'+input_mpls_2+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_usv_vigente_2_'+input_mpls_2+'" id="mpls_usv_vigente_2_'+input_mpls_2+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_numero_oft_2_'+input_mpls_2+'" id="mpls_numero_oft_2_'+input_mpls_2+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_2_'+input_mpls_2+'" id="mpls_velocidad_2_'+input_mpls_2+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_casamatriz_2_'+input_mpls_2+'" id="mpls_casamatriz_2_'+input_mpls_2+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_cta_factura_2_'+input_mpls_2+'" id="mpls_cta_factura_2_'+input_mpls_2+'" ></td>'
                    +'</tr>';      
            $('#mpls-upgrade').append(item); 
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        if(solicitudmpls==3){
            
            if(count_mpls_3==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-traslado" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Traslado</h3></div><div class="panel-body"><div class="table-responsive"><table style="" class="table table-striped"><thead><tr><th>Código de servicio</th><th>USV contrato vigente</th><th>Número OFT</th><th>Velocidad</th><th>Cód. servicio Casa Matriz</th><th>Cuenta Facturación</th></tr></thead><tbody id="mpls-traslado"></tbody></table></div></div></div>');  
            }

            count_mpls_3++;
            input_mpls_3++;

            $('#cantidad_mpls_traslado_filas').val(input_mpls_3);

            item = '<tr id="fila_3_'+input_mpls_3+'">'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_codigo_servicio_3_'+input_mpls_3+'" id="mpls_codigo_servicio_3_'+input_mpls_3+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_usv_vigente_3_'+input_mpls_3+'" id="mpls_usv_vigente_3_'+input_mpls_3+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_numero_oft_3_'+input_mpls_3+'" id="mpls_numero_oft_3_'+input_mpls_3+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_3_'+input_mpls_3+'" id="mpls_velocidad_3_'+input_mpls_3+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_casamatriz_3_'+input_mpls_3+'" id="mpls_casamatriz_3_'+input_mpls_3+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_cta_factura_3_'+input_mpls_3+'" id="mpls_cta_factura_3_'+input_mpls_3+'" ></td>'
                    +'</tr>';      
            $('#mpls-traslado').append(item);
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        if(solicitudmpls==4){

            if(count_mpls_4==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-cambioacceso" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Cambio de Acceso</h3></div><div class="panel-body"><div class="table-responsive"><table style="" class="table table-striped"><thead><tr><th>Código de servicio</th><th>USV contrato vigente</th><th>Número OFT</th><th>Velocidad</th><th>Cód. servicio Casa Matriz</th><th>Cuenta Facturación</th></tr></thead><tbody id="mpls-acceso"></tbody></table></div></div></div>');  
            }

            count_mpls_4++;
            input_mpls_4++;

            $('#cantidad_mpls_cambioAcceso_filas').val(input_mpls_4);

            item = '<tr id="fila_4_'+input_mpls_4+'">'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_codigo_servicio_4_'+input_mpls_4+'" id="mpls_codigo_servicio_4_'+input_mpls_4+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_usv_vigente_4_'+input_mpls_4+'" id="mpls_usv_vigente_4_'+input_mpls_4+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_numero_oft_4_'+input_mpls_4+'" id="mpls_numero_oft_4_'+input_mpls_4+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_4_'+input_mpls_4+'" id="mpls_velocidad_4_'+input_mpls_4+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_casamatriz_4_'+input_mpls_4+'" id="mpls_casamatriz_4_'+input_mpls_4+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_cta_factura_4_'+input_mpls_4+'" id="mpls_cta_factura_4_'+input_mpls_4+'" ></td>'
                    +'</tr>';      
            $('#mpls-acceso').append(item);
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        if(solicitudmpls==5){

            if(count_mpls_5==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-renegociacion" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Renegociación</h3></div><div class="panel-body"><div class="table-responsive"><table style="" class="table table-striped"><thead><tr><th>Código de servicio</th><th>USV contrato vigente</th><th>Velocidad</th><th>Precio Objetivo</th><th>Plazo</th><th>Cuenta Facturación</th></tr></thead><tbody id="mpls-renegociacion"></tbody></table></div></div></div>');  
            }

            count_mpls_5++;
            input_mpls_5++;

            $('#cantidad_mpls_renegociacion_filas').val(input_mpls_5);

            item = '<tr id="fila_5_'+input_mpls_5+'">'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_codigo_servicio_5_'+input_mpls_5+'" id="mpls_codigo_servicio_5_'+input_mpls_5+'" ></td>'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_usv_vigente_5_'+input_mpls_5+'" id="mpls_usv_vigente_5_'+input_mpls_5+'" ></td>'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_5_'+input_mpls_5+'" id="mpls_velocidad_5_'+input_mpls_5+'" ></td>'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_precioobjetivo_5_'+input_mpls_5+'" id="mpls_precioobjetivo_5_'+input_mpls_5+'" ></td>'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_plazo_5_'+input_mpls_5+'" id="mpls_plazo_5_'+input_mpls_5+'" ></td>'
                       +'<td><input type="number" class="form-control input-sm" name="mpls_cta_factura_5_'+input_mpls_5+'" id="mpls_cta_factura_5_'+input_mpls_5+'" ></td>'
                    +'</tr>';      
            $('#mpls-renegociacion').append(item);
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        if(solicitudmpls==6){

            if(count_mpls_6==0){
                  $('#contenedor_tablas_mpls').append('<div id="panel-mpls-downgrade" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Downgrade</h3></div><div class="panel-body"><div class="table-responsive"><table style="" class="table table-striped"><thead><tr><th>Código de servicio</th><th>USV contrato vigente</th><th>Número OFT</th><th>Velocidad</th><th>Cód. servicio Casa Matriz</th><th>Cuenta Facturación</th></tr></thead><tbody id="mpls-downgrade"></tbody></table></div></div></div>');  
            }

            count_mpls_6++;
            input_mpls_6++;  

            $('#cantidad_mpls_downgrade_filas').val(input_mpls_6);

            item = '<tr id="fila_6_'+input_mpls_6+'">'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_codigo_servicio_6_'+input_mpls_6+'" id="mpls_codigo_servicio_6_'+input_mpls_6+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_usv_vigente_6_'+input_mpls_6+'" id="mpls_usv_vigente_6_'+input_mpls_6+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_numero_oft_6_'+input_mpls_6+'" id="mpls_numero_oft_6_'+input_mpls_6+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_velocidad_6_'+input_mpls_6+'" id="mpls_velocidad_6_'+input_mpls_6+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_casamatriz_6_'+input_mpls_6+'" id="mpls_casamatriz_6_'+input_mpls_6+'" ></td>'
                        +'<td><input type="number" class="form-control input-sm" name="mpls_cta_factura_6_'+input_mpls_6+'" id="mpls_cta_factura_6_'+input_mpls_6+'" ></td>'
                    +'</tr>';      
            $('#mpls-downgrade').append(item); 
            $('#addItem12').attr('disabled', false);
            $('#quitItem12').attr('disabled', false);
        }

        setInputNumber();

    }
}


function monitorMpls(){
    var solicitudmpls = $('#solicitudmpls').val();
    if(solicitudmpls>0){
        
        if(solicitudmpls==1){
           if(count_mpls_1>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }   

        if(solicitudmpls==2){
            if(count_mpls_2>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }

        if(solicitudmpls==3){
            if(count_mpls_3>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }

        if(solicitudmpls==4){
            if(count_mpls_4>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }

        if(solicitudmpls==5){
            if(count_mpls_5>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }

         if(solicitudmpls==6){
            if(count_mpls_6>0){
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', false);
           }else{
              $('#addItem12').attr('disabled', false);
              $('#quitItem12').attr('disabled', true);
           } 
        }

    
    }else{
       $('#addItem12').attr('disabled', true);
       $('#quitItem12').attr('disabled', true);
    }
}

function quitItemMpls(){
    var solicitudmpls = $('#solicitudmpls').val();
    if(solicitudmpls>0){

         if(solicitudmpls==1){
            $('#fila_1_'+input_mpls_1).remove();
            count_mpls_1--;
            input_mpls_1--;
            $('#cantidad_mpls_instalacion_filas').val(count_mpls_1);
            if(input_mpls_1==0){
                $('#panel-mpls-instalacion').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

         if(solicitudmpls==2){
            $('#fila_2_'+input_mpls_2).remove();
            count_mpls_2--;
            input_mpls_2--;
            $('#cantidad_mpls_upgrade_filas').val(input_mpls_2);
            if(input_mpls_2==0){
                $('#panel-mpls-upgrade').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

         if(solicitudmpls==3){
            $('#fila_3_'+input_mpls_3).remove();
            count_mpls_3--;
            input_mpls_3--;
            $('#cantidad_mpls_traslado_filas').val(input_mpls_3);
            if(input_mpls_3==0){
                $('#panel-mpls-traslado').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

         if(solicitudmpls==4){
            $('#fila_4_'+input_mpls_4).remove();
            count_mpls_4--;
            input_mpls_4--;
            $('#cantidad_mpls_cambioAcceso_filas').val(input_mpls_4);
            if(input_mpls_4==0){
                $('#panel-mpls-cambioacceso').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

         if(solicitudmpls==5){
            $('#fila_5_'+input_mpls_5).remove();
            count_mpls_5--;
            input_mpls_5--;
            $('#cantidad_mpls_renegociacion_filas').val(input_mpls_5);
            if(input_mpls_5==0){
                $('#panel-mpls-renegociacion').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

         if(solicitudmpls==6){
            $('#fila_6_'+input_mpls_6).remove();
            count_mpls_6--;
            input_mpls_6--;
            $('#cantidad_mpls_downgrade_filas').val(input_mpls_6);
            if(input_mpls_6==0){
                $('#panel-mpls-downgrade').remove();
                $('#quitItem12').attr('disabled', true);
            }
         }

    }
}