/* Telefonia */
var contador51 = 0;
function FunctionaddItem51(){
    contador51++;
    $('#cantidad_telefonia_filas').val(contador51);
    
    var active = "";
    var tab = "";
    var activeLi = "";
    if(contador51==1){
        active = "active in";
        tab = "true";
        activeLi = "active";
    }

    var listType = $("#solicitud51").val();

    var Text = $("#solicitud51 option:selected").text();

    var itemMenu = '<li id="li-tab-51-'+contador51+'" class="'+activeLi+'">'
                    +'<a data-toggle="tab" href="#demo-stk-lft-tab-51-'+contador51+'" aria-expanded="false">'+Text+'</a>'
                +'</li>';
    
    var content51 = '<div id="demo-stk-lft-tab-51-'+contador51+'" class="tab-pane fade '+active+'">'
                    +'<input type="hidden" name="itemtypeTelefonia_'+contador51+'" id="itemtypeTelefonia_'+contador51+'" value="'+listType+'" class="form-control input-sm" />'
                    +'<table style="" class="table table-striped">'             
                        +'<tbody>'
                            +'<tr>'
                                +'<td>Minutos SLM</td>'
                                +'<td><input name="itemtypeTelefoniaMinutosSlm_'+contador51+'" id="itemtypeTelefoniaMinutosSlm_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Minutos Móvil</td>'
                                +'<td><input name="itemtypeTelefoniaMinutosMovil_'+contador51+'" id="itemtypeTelefoniaMinutosMovil_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Precio Objetivo</td>'
                                +'<td><input name="itemtypeTelefoniaPrecioObjetivo_'+contador51+'" id="itemtypeTelefoniaPrecioObjetivo_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Plazo</td>'
                                +'<td><input name="itemtypeTelefoniaPlazo_'+contador51+'" id="itemtypeTelefoniaPlazo_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Codigo de servicio</td>'
                                +'<td><input name="itemtypeTelefoniaCodContrato_'+contador51+'" id="itemtypeTelefoniaCodContrato_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>USV Contrato Vigente</td>'
                                +'<td><input name="itemtypeTelefoniaUsvVigente_'+contador51+'" id="itemtypeTelefoniaUsvVigente_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Minutos SLM asociados a USV Contrato</td>'
                                +'<td><input name="itemtypeTelefoniaMinSlmUsv_'+contador51+'" id="itemtypeTelefoniaMinSlmUsv_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                            +'<tr>'
                                +'<td>Minutos Móvil asociados a USV Contrato</td>'
                                +'<td><input name="itemtypeTelefoniaMinMovilUsv_'+contador51+'" id="itemtypeTelefoniaMinMovilUsv_'+contador51+'" type="number" class="form-control input-sm" /></td>'
                            +'</tr>'
                        +'</tbody>'
                    +'</table>'
                    +'<input type="hidden" id="tipo-51-'+contador51+'" />'
                +'</div>';

    $('#menu51').append(itemMenu);
    $('#content51').append(content51);
    

    if(contador51>0){
         $('#quitItem51').attr('disabled', false);
    }else{
        $('#quitItem51').attr('disabled', true);
    }

    setInputNumber();
}

function FunctionquitItem51(){

    $('#li-tab-51-'+contador51).remove();
    $('#demo-stk-lft-tab-51-'+contador51).remove();
    contador51--;

    $('#cantidad_telefonia_filas').val(contador51);

    if(contador51>0){
        $('#quitItem51').attr('disabled', false);
    }else{
        $('#quitItem51').attr('disabled', true);
    }
}

function monitor51(){
    var solicitud51 = $('#solicitud51').val();
    if(solicitud51>0){
        $('#addItem51').attr('disabled', false);
    }else{
        $('#addItem51').attr('disabled', true);
    }
}