function monitorTrunckCd(){
    var solicitud = $('#solicitudTrunkCd').val();
    if(solicitud>0){

            if(solicitud==1){
                $('#addItem19').attr('disabled', false);
                if(TrunkCdInstalacion>0){
                    $('#quitItem19').attr('disabled', false);
                }else{
                    $('#quitItem19').attr('disabled', true);
                }
            }

            if(solicitud==2){
                if(TrunkCdUpgradePanelActivity==1){
                    $('#addItem19').attr('disabled', true);
                    $('#quitItem19').attr('disabled', false);
                    return false;
                }else{
                    $('#addItem19').attr('disabled', false);
                    $('#quitItem19').attr('disabled', true);
                    return false;
                }
            }

            if(solicitud==3){
                $('#addItem19').attr('disabled', false);
                if(TrunckCdTraslado>0){
                    $('#quitItem19').attr('disabled', false);
                }else{
                    $('#quitItem19').attr('disabled', true);
                }
            }

            if(solicitud==4){
                $('#addItem19').attr('disabled', false);
                if(TrunckCdAcceso>0){
                    $('#quitItem19').attr('disabled', false);
                }else{
                    $('#quitItem19').attr('disabled', true);
                }
            }

            if(solicitud==5){
                $('#addItem19').attr('disabled', false);
                if(TrunckCdRenegociacion>0){
                    $('#quitItem19').attr('disabled', false);
                }else{
                    $('#quitItem19').attr('disabled', true);
                }
            }

            if(solicitud==6){
                if(TrunkCdDowngradePanelActivity==1){
                    $('#addItem19').attr('disabled', true);
                    $('#quitItem19').attr('disabled', false);
                    return false;
                }else{
                    $('#addItem19').attr('disabled', false);
                    $('#quitItem19').attr('disabled', true);
                    return false;
                }
            }

            if(solicitud==7){
                $('#addItem19').attr('disabled', false);
                if(TrunckCdCambioProducto>0){
                    $('#quitItem19').attr('disabled', false);
                }else{
                    $('#quitItem19').attr('disabled', true);
                }
            }

            $('#addItem19').attr('disabled', false);
    
    }else{
        $('#addItem19').attr('disabled', true);
        $('#quitItem19').attr('disabled', true);

    }
}


function addItemTrunckCd(){
    var solicitud = $('#solicitudTrunkCd').val();
    if(solicitud==1){
        addItemTrunckcdInstalacion();
    }

    if(solicitud==2){
        if(TrunkCdUpgradePanelActivity==0){
             $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_upgrade" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Upgrade</h3></div><div class="panel-body"><div class="row"><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"><table style="" class="table"><tbody><tr><td>Tipo Solicitud</td><td><select id="solicitud15" onchange="monitortrunckcdupgrade();" class="form-control input-sm"><option value="">Seleccione</option><option value="1">Telefonia</option><option value="2">Internet Dedicado</option><option value="3">Acceso MPLS</option></select></td><td class="text-right"><button type="button" onclick="addItemTrunckCdUpgrade();"  style="margin-right: 3px;" disabled="true" id="addItem15" class="btn btn-primary">Agregar</button><button type="button" onclick="quitItemTrunckCdUpgrade();" disabled="true" id="quitItem15" class="btn btn-danger">Eliminar</button></td></tr></tbody></table></div></div><div id="bodyUpgradeTruncd" class="tab-base tab-stacked-left"><ul id="tabs_trunckcd_upgrade" class="nav nav-tabs"></ul></div></div></div>');
             $('#addItem19').attr('disabled', true);
             $('#quitItem19').attr('disabled', false);   
             TrunkCdUpgradePanelActivity = 1;
        }else{ 
            $('#addItem19').attr('disabled', true);
            $('#quitItem19').attr('disabled', false);
            if(TrunkCdUpgradePanelActivity==1){    
                addItemTrunckCdUpgrade();
            }else{
                alertFail('Trunck Con Datos: Upgrade', 'Debe seleccionar Upgrade en la lista desplegable');
                $('#solicitudTrunkCd').select();
                $('#solicitudTrunkCd').focus();
                return false;
            }
        }
    }

    if(solicitud==3){
        addItemTrunckCdTraslado();
    }

    if(solicitud==4){
        addItemTrunckCdAcceso();
    }

    if(solicitud==5){
        addItemTrunckCdRenegociacion();
    }

    if(solicitud==6){
        if(TrunkCdDowngradePanelActivity==0){
            
            var item = '<div id="contenedor_trunckcd_boxs_downgrade" class="panel panel-dark">'+
                            '<div class="panel-heading"><h3 class="panel-title">Downgrade</h3></div>'+
                            '<div class="panel-body">'+
                                '<div class="row">'+
                                    '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>'+
                                    '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
                                        '<table style="" class="table">'+
                                            '<tbody>'+
                                                '<tr>'+
                                                    '<td>Tipo Solicitud</td>'+
                                                    '<td>'+
                                                        '<select id="solicitud19" onchange="monitortrunckcddowngrade();" class="form-control input-sm">'+
                                                            '<option value="">Seleccione</option>'+
                                                            '<option value="1">Telefonia</option>'+
                                                            '<option value="2">Internet Dedicado</option>'+
                                                            '<option value="3">Acceso MPLS</option>'+
                                                        '</select>'+
                                                    '</td>'+
                                                    '<td class="text-right">'+
                                                        '<button type="button" style="margin-right: 3px;" onclick="addItemTrunckCdDowngrade();" disabled="true" id="addItem22" class="btn btn-primary">Agregar</button>'+
                                                        '<button type="button" onclick="quitItemTrunckCdDowngrade();" disabled="true" id="quitItem22" class="btn btn-danger">Eliminar</button>'+
                                                    '</td>'+
                                                '</tr>'+
                                            '</tbody>'+
                                        '</table>'+
                                    '</div>'+
                                '</div>'+
                                '<div id="bodyDowngradeTruncd" class="tab-base tab-stacked-left">'+
                                    '<ul id="tabs_trunckcd_downgrade" class="nav nav-tabs">'+
                                    '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

            $('#contenedor_trunckcd_boxs').append(item);
            $('#addItem19').attr('disabled', true);
            $('#quitItem19').attr('disabled', false);   
            $('#addItem22').attr('disabled', true);
            $('#quitItem22').attr('disabled', false);   
            TrunkCdDowngradePanelActivity = 1;
        }else{ 
            $('#addItem22').attr('disabled', true);
            $('#quitItem22').attr('disabled', false);
            if(TrunkCdDowngradePanelActivity==1){    
                addItemTrunckCdUpgrade();
            }else{
                alertFail('Trunck Con Datos: Downgrade', 'Debe seleccionar Upgrade en la lista desplegable');
                $('#solicitudTrunkCd').select();
                $('#solicitudTrunkCd').focus();
                return false;
            }
        }
    }

    if(solicitud==7){
        addItemTrunckCdCambioProducto();
    }
}

function quitItemTrunckCd(){
    var solicitud = $('#solicitudTrunkCd').val();

    if(solicitud==1){
       quitItemTrunckcdInstalacion();
    }

    if(solicitud==2){
        $('#contenedor_trunckcd_boxs_upgrade').remove();
        TrunkCdUpgradeTelefonia = 0;
        TrunkCdUpgradeInternetDedicado = 0;
        TrunkCdUpgradeAccesoMPLS = 0;
        TrunkCdUpgradeGeneral = 0;
        TrunkCdUpgradePanelActivity = 0;

        $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_upgrade_telefonia_filas').val(TrunkCdDowngradeTelefonia);
        $('#cantidad_trunckCD_upgrade_internetDedicado_filas').val(TrunkCdDowngradeInternetDedicado);
        $('#cantidad_trunckCD_upgrade_accesoMpls_filas').val(TrunkCdDowngradeAccesoMPLS);


        $('#quitItem19').attr('disabled', true);
        $('#addItem19').attr('disabled', false);
    }

    if(solicitud==3){
        quitItemTrunckCdTraslado();
    }

    if(solicitud==4){
        quitItemTrunckCdAcceso();
    }

    if(solicitud==5){
        quitItemTrunckCdRenegociacion();
    }

    if(solicitud==6){
        $('#contenedor_trunckcd_boxs_downgrade').remove();
        TrunkCdDowngradeTelefonia = 0;
        TrunkCdDowngradeInternetDedicado = 0;
        TrunkCdDowngradeAccesoMPLS = 0;
        TrunkCdDowngradeGeneral = 0;
        TrunkCdDowngradePanelActivity = 0;

        $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_downgrade_telefonia_filas').val(TrunkCdDowngradeTelefonia);
        $('#cantidad_trunckCD_downgrade_internetDedicado_filas').val(TrunkCdDowngradeInternetDedicado);
        $('#cantidad_trunckCD_downgrade_accesoMpls_filas').val(TrunkCdDowngradeAccesoMPLS);

        $('#quitItem19').attr('disabled', true);
        $('#addItem19').attr('disabled', false);   
        $('#quitItem22').attr('disabled', true);
        $('#addItem22').attr('disabled', false);   
    }

    if(solicitud==7){
        quitItemTrunckCdCambioProducto();
    }

}


/*********************************************************************** Instalacion ************************************************************************/
var TrunkCdInstalacion = 0;
var TrunkCdTraslado = 0;
var TrunkCdCambioAcceso = 0;
var TrunkCdRenegociación = 0;


function addItemTrunckcdInstalacion(){
    
    if(TrunkCdInstalacion==0){
       $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_instalador" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Instalación</h3></div><div class="panel-body"><div id="contenedor_trunckcd_instalacion" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunkCdInstalacion++;
    $('#cantidad_trunckCD_instalacion_filas').val(TrunkCdInstalacion);

    var item = '<div style="margin-bottom:15px;" id="row_trunckcd_instalacion_'+TrunkCdInstalacion+'" class="row">'+
                '<ul id="tabs_trunckcd_instalacion_'+TrunkCdInstalacion+'" class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#trunckcd-tab-1-'+TrunkCdInstalacion+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#trunckcd-tab-2-'+TrunkCdInstalacion+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#trunckcd-tab-3-'+TrunkCdInstalacion+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#trunckcd-tab-4-'+TrunkCdInstalacion+'" aria-expanded="false">Datos</a>'+
                    '</li>'+
                '</ul>'+
                '<div id="content_trunckcd_instalacion_'+TrunkCdInstalacion+'" class="tab-content">'+
                    '<div id="trunckcd-tab-1-'+TrunkCdInstalacion+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td width="50%">Número de OFT</td>'+
                                    '<td width="50%"><input type="number" id="tcd_inst_solicitud_oft_'+TrunkCdInstalacion+'" name="tcd_inst_solicitud_oft_'+TrunkCdInstalacion+'" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td><div class="checkbox"><input id="isTcdCentralHomecheck_'+TrunkCdInstalacion+'" data="'+TrunkCdInstalacion+'" class="magic-checkbox isTcdCentralHomecheckClass" data-number="'+TrunkCdInstalacion+'" type="checkbox" name="isTcdCentralHomecheck_'+TrunkCdInstalacion+'"><label for="isTcdCentralHomecheck_'+TrunkCdInstalacion+'">Chequear en caso de que la OFT ingresada corresponda a la casa matriz.</label></div></td>'+
                                    '<td><input type="number" id="isTcdCentralHomeNumber_'+TrunkCdInstalacion+'" name="isTcdCentralHomeNumber_'+TrunkCdInstalacion+'" class="form-control input-sm" placeholder="Código Servicio casa Matriz" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>'+
                                        '<div class="radio"><input id="tcd_inst_requisito_'+TrunkCdInstalacion+'_1" data="'+TrunkCdInstalacion+'" data-col="1" class="magic-radio radioRequerimientos" type="radio" value="Requiere Central Combo" name="tcd_inst_requisito_'+TrunkCdInstalacion+'" checked=""><label for="tcd_inst_requisito_'+TrunkCdInstalacion+'_1">Requiere Central Combo</label></div>'+
                                        '<div class="radio"><input id="tcd_inst_requisito_'+TrunkCdInstalacion+'_2" data="'+TrunkCdInstalacion+'" data-col="2" class="magic-radio radioRequerimientos" type="radio" value="Cliente posee central propia" name="tcd_inst_requisito_'+TrunkCdInstalacion+'"><label for="tcd_inst_requisito_'+TrunkCdInstalacion+'_2">Cliente posee central propia</label></div>'+
                                        '<div class="radio"><input id="tcd_inst_requisito_'+TrunkCdInstalacion+'_3" data="'+TrunkCdInstalacion+'" data-col="3" class="magic-radio radioRequerimientos" type="radio" value="Requiere central a medida" name="tcd_inst_requisito_'+TrunkCdInstalacion+'"><label for="tcd_inst_requisito_'+TrunkCdInstalacion+'_3">Requiere central a medida</label></div>'+
                                    '</td>'+
                                    '<td>'+
                                        '<div class="panel-group accordion" id="avisos-requerimientos-'+TrunkCdInstalacion+'" style="margin-bottom:0px;">'+
                                            
                                            '<div class="panel panel-info">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Requiere central combo</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse in collapsesRequerimientos-'+TrunkCdInstalacion+'" id="collapse-CD-'+TrunkCdInstalacion+'-1" aria-expanded="true">'+
                                                    '<div class="panel-body">Considerar los valores de cartilla vigente.'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="panel panel-info">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Requiere central a medida</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse collapsesRequerimientos-'+TrunkCdInstalacion+'" id="collapse-CD-'+TrunkCdInstalacion+'-2" aria-expanded="false" style="">'+
                                                    '<div class="panel-body">Debe ingresar ingresar formulario AD Equipos Telco mediante USV.</div>'+
                                                '</div>'+
                                            '</div>'+
                                            
                                            '<div class="panel panel-info" style="margin-bottom:0px;">'+
                                                '<div class="panel-heading">'+
                                                    '<h4 class="panel-title">'+
                                                        '<a class="collapsed">Cliente posee central a propia.</a>'+
                                                    '</h4>'+
                                                '</div>'+
                                                '<div class="panel-collapse collapse collapsesRequerimientos-'+TrunkCdInstalacion+'" id="collapse-CD-'+TrunkCdInstalacion+'-3" aria-expanded="false">'+
                                                    '<div class="panel-body">Se debe asegurar que la central del cliente este homologada con el servicio de Entel.</div>'+
                                                '</div>'+
                                            '</div>'+

                                        '</div>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="trunckcd-tab-2-'+TrunkCdInstalacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="tcd_inst_telefonia_cantCanales_'+TrunkCdInstalacion+'" name="tcd_inst_telefonia_cantCanales_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="tcd_inst_telefonia_minSlm_'+TrunkCdInstalacion+'" name="tcd_inst_telefonia_minSlm_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos móvil</td>'+
                                    '<td><input id="tcd_inst_telefonia_minMovil_'+TrunkCdInstalacion+'" name="tcd_inst_telefonia_minMovil_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos adicionales</td>'+
                                    '<td><input id="tcd_inst_telefonia_minAdicional_'+TrunkCdInstalacion+'" name="tcd_inst_telefonia_minAdicional_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="trunckcd-tab-3-'+TrunkCdInstalacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="tcd_inst_InternDedicado_minNacional_'+TrunkCdInstalacion+'" name="tcd_inst_InternDedicado_minNacional_'+TrunkCdInstalacion+'" type="number" class="form-control input-sml" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="tcd_inst_InternDedicado_minInteracional_'+TrunkCdInstalacion+'" name="tcd_inst_InternDedicado_minInteracional_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="tcd_inst_InternDedicado_precioObjetivoNacional_'+TrunkCdInstalacion+'" name="tcd_inst_InternDedicado_precioObjetivoNacional_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="tcd_inst_InternDedicado_plazo_'+TrunkCdInstalacion+'" name="tcd_inst_InternDedicado_plazo_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="trunckcd-tab-4-'+TrunkCdInstalacion+'" class="tab-pane fade">'+
                       '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                   '<td>Velocidad</td>'+
                                   '<td><input id="tcd_inst_datos_velocidad_'+TrunkCdInstalacion+'" name="tcd_inst_datos_velocidad_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="tcd_inst_datos_codServicio_'+TrunkCdInstalacion+'" name="tcd_inst_datos_codServicio_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="tcd_inst_datos_precioObjetivo_'+TrunkCdInstalacion+'" name="tcd_inst_datos_precioObjetivo_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="tcd_inst_datos_plazo_'+TrunkCdInstalacion+'" name="tcd_inst_datos_plazo_'+TrunkCdInstalacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div>'+
                '</div>'+
            '</div>';

    $('#contenedor_trunckcd_instalacion').append(item);
    

    $('.radioRequerimientos').bind('click', function(){
        var data = $(this).attr('data');
        var dataCol = $(this).attr('data-col');
        $('.collapsesRequerimientos-'+data).collapse('hide');
        $('#collapse-CD-'+data+'-'+dataCol).collapse('show');
    });
 
    $('.isTcdCentralHomecheckClass').bind('click', function(){
        var isChhecccked = $(this).is(':checked');
        var data = $(this).attr('data');
        if(!isChhecccked){
            $('#isTcdCentralHomeNumber_'+data).attr('readonly', false);
            $('#isTcdCentralHomeNumber_'+data).show();
        }else{
            $('#isTcdCentralHomeNumber_'+data).val('');
            $('#isTcdCentralHomeNumber_'+data).attr('readonly', true);
            $('#isTcdCentralHomeNumber_'+data).hide();
        }
    });

    $('#quitItem19').attr('disabled', false);
    setInputNumber();
}   

function quitItemTrunckcdInstalacion(){
    $('#row_trunckcd_instalacion_'+TrunkCdInstalacion).remove();
    TrunkCdInstalacion--;
    $('#cantidad_trunckCD_instalacion_filas').val(TrunkCdInstalacion);
    if(TrunkCdInstalacion==0){
        $('#contenedor_trunckcd_boxs_instalador').remove();
        $('#quitItem19').attr('disabled', true);
    }
}

/*********************************************************************** Upgrade ************************************************************************/

var TrunkCdUpgrade = 0;
var TrunkCdUpgradeTelefonia = 0;
var TrunkCdUpgradeInternetDedicado = 0;
var TrunkCdUpgradeAccesoMPLS = 0;
var TrunkCdUpgradeGeneral = 0;
var TrunkCdUpgradePanelActivity = 0;

function quitItemTrunckCdUpgrade(){
   var solicitud15 = $('#solicitud15').val();
    
   if(solicitud15==1){
        $("#trunkcd-upgradeT-li-"+TrunkCdUpgradeTelefonia).remove();
        $("#trunkcd-upgradeT-contain-"+TrunkCdUpgradeTelefonia).remove();
        TrunkCdUpgradeTelefonia--;
        TrunkCdUpgradeGeneral--;

        $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
        $('#cantidad_trunckCD_upgrade_telefonia_filas').val(TrunkCdUpgradeTelefonia);

        if(TrunkCdUpgradeTelefonia==0){
            $('#quitItem15').attr('disabled', true);
        }

       if(TrunkCdUpgradeGeneral==0){
            $('#quitItem19').attr('disabled', true);
            $('#addItem19').attr('disabled', false);
            $('#content_trunckcd_upgrade').remove();
            TrunkCdUpgradePanelActivity = 0;
       }
   }

   if(solicitud15==2){
       $("#trunkcd-upgradeId-li-"+TrunkCdUpgradeInternetDedicado).remove(); 
       $("#trunkcd-upgradeId-contain-"+TrunkCdUpgradeInternetDedicado).remove();
       TrunkCdUpgradeInternetDedicado--;
       TrunkCdUpgradeGeneral--;

       $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
       $('#cantidad_trunckCD_upgrade_internetDedicado_filas').val(TrunkCdUpgradeInternetDedicado);

       if(TrunkCdUpgradeInternetDedicado==0){
            $('#quitItem15').attr('disabled', true);
       }

       if(TrunkCdUpgradeGeneral==0){
            $('#quitItem15').attr('disabled', true);
            $('#addItem15').attr('disabled', false);
            $('#content_trunckcd_upgrade').remove();
            TrunkCdUpgradePanelActivity = 0;
       }
   }

   if(solicitud15==3){
       $("#trunkcd-upgradeAMPLS-li-"+TrunkCdUpgradeAccesoMPLS).remove(); 
       $("#trunkcd-upgradeAMPLS-contain-"+TrunkCdUpgradeAccesoMPLS).remove();
       TrunkCdUpgradeAccesoMPLS--;
       TrunkCdUpgradeGeneral--;

       $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
       $('#cantidad_trunckCD_upgrade_accesoMpls_filas').val(TrunkCdUpgradeAccesoMPLS);


       if(TrunkCdUpgradeAccesoMPLS==0){
            $('#quitItem15').attr('disabled', true);
       }

       if(TrunkCdUpgradeGeneral==0){
            $('#quitItem15').attr('disabled', true);
            $('#addItem15').attr('disabled', false);
            $('#content_trunckcd_upgrade').remove();
            TrunkCdUpgradePanelActivity = 0;
       }

   }

   if(TrunkCdUpgradeGeneral==0){
        $('#contenedor_trunckcd_boxs_upgrade').remove();
   }

   monitorTrunckCd();

}

function addItemTrunckCdUpgrade(){

    var solicitud15 = $('#solicitud15').val();
    var item = "";
    var contain = "";
    
    var activeTab = "";
    var activeContain = "";
    var activetrue = "";

    if(TrunkCdUpgradeGeneral==0){
        $('#bodyUpgradeTruncd').append('<div id="content_trunckcd_upgrade" class="tab-content"></div>');
        activeTab = "active";    
        activeContain = "active in";
        activetrue = "true";
    }else{
        activetrue = "false";
    }

    if(solicitud15==1){
        TrunkCdUpgradeTelefonia++;
        TrunkCdUpgradeGeneral++;
        
        $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
        $('#cantidad_trunckCD_upgrade_telefonia_filas').val(TrunkCdUpgradeTelefonia);

        item = '<li id="trunkcd-upgradeT-li-'+TrunkCdUpgradeTelefonia+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-upgradeT-contain-'+TrunkCdUpgradeTelefonia+'" aria-expanded="'+activetrue+'">Telefonia</a>'+
        '</li>';
        contain = '<div id="trunkcd-upgradeT-contain-'+TrunkCdUpgradeTelefonia+'" class="tab-pane fade '+activeContain+'">'+  
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input type="number" id="trunckCD_upgrade_telefonia_numOft_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_numOft_'+TrunkCdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input type="number" id="trunckCD_upgrade_telefonia_CodServicio_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_CodServicio_'+TrunkCdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input type="number" id="trunckCD_upgrade_telefonia_contVigente_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_contVigente_'+TrunkCdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input type="number" id="trunckCD_upgrade_telefonia_ctaFacturacion_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_ctaFacturacion_'+TrunkCdUpgradeTelefonia+'" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Actual</th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Cant. de canales</td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_cantCanales_actual_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_cantCanales_actual_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_cantCanales_solicitada_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_cantCanales_solicitada_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos SLM</td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_minSLM_actual_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_minSLM_actual_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_minSLM_solicitada_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_minSLM_solicitada_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos a Móvil</td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_minMovil_actual_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_minMovil_actual_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_upgrade_telefonia_minMovil_solicitada_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_minMovil_solicitada_'+TrunkCdUpgradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentario</td>'+
                                        '<td colspan="2"><textarea id="trunckCD_upgrade_telefonia_comentario_'+TrunkCdUpgradeTelefonia+'" name="trunckCD_upgrade_telefonia_comentario_'+TrunkCdUpgradeTelefonia+'" placeholder="Message" rows="5" class="form-control" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_trunckcd_upgrade").append(item);
        $("#content_trunckcd_upgrade").append(contain);
        $('#quitItem15').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud15==2){
        TrunkCdUpgradeInternetDedicado++;
        TrunkCdUpgradeGeneral++;

        $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
        $('#cantidad_trunckCD_upgrade_internetDedicado_filas').val(TrunkCdUpgradeInternetDedicado);

        item = '<li id="trunkcd-upgradeId-li-'+TrunkCdUpgradeInternetDedicado+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-upgradeId-contain-'+TrunkCdUpgradeInternetDedicado+'" aria-expanded="'+activetrue+'">Internet Dedicado</a>'+
        '</li>';
        contain = '<div id="trunkcd-upgradeId-contain-'+TrunkCdUpgradeInternetDedicado+'" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_numOft_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_numOft_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_CodServicio_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_CodServicio_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_contVigente_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_contVigente_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_ctaFacturacion_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Vel. Nacional</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_velNacional_solicitada_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Vel. Internacional</td>'+
                                        '<td><input id="trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_velInternacional_solicitada_'+TrunkCdUpgradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckCD_upgrade_InternetDedicado_comentario_solicitada_'+TrunkCdUpgradeInternetDedicado+'" name="trunckCD_upgrade_InternetDedicado_comentario_solicitada_'+TrunkCdUpgradeInternetDedicado+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        $("#tabs_trunckcd_upgrade").append(item);
        $("#content_trunckcd_upgrade").append(contain);
        $('#quitItem15').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud15==3){
        TrunkCdUpgradeAccesoMPLS++;
        TrunkCdUpgradeGeneral++;

        $('#cantidad_trunckCD_upgrade_filas').val(TrunkCdUpgradeGeneral);
        $('#cantidad_trunckCD_upgrade_accesoMpls_filas').val(TrunkCdUpgradeAccesoMPLS);

        item = '<li id="trunkcd-upgradeAMPLS-li-'+TrunkCdUpgradeAccesoMPLS+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-upgradeAMPLS-contain-'+TrunkCdUpgradeAccesoMPLS+'" aria-expanded="'+activetrue+'">Acceso MPLS</a>'+
        '</li>';
        contain = '<div id="trunkcd-upgradeAMPLS-contain-'+TrunkCdUpgradeAccesoMPLS+'"" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckCD_upgrade_accesoMPLS_numOft_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_numOft_'+TrunkCdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckCD_upgrade_accesoMPLS_codServicio_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_codServicio_'+TrunkCdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckCD_upgrade_accesoMPLS_contratoVigente_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_contratoVigente_'+TrunkCdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_cuentaFacturacion_'+TrunkCdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                           '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Velocidad</td>'+
                                        '<td><input id="trunckCD_upgrade_accesoMPLS_velocidad_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_velocidad_'+TrunkCdUpgradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckCD_upgrade_accesoMPLS_comentario_'+TrunkCdUpgradeAccesoMPLS+'" name="trunckCD_upgrade_accesoMPLS_comentario_'+TrunkCdUpgradeAccesoMPLS+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_trunckcd_upgrade").append(item);
        $("#content_trunckcd_upgrade").append(contain);  
        $('#quitItem15').attr('disabled', false); 
        setInputNumber();     
    }
}

function monitortrunckcdupgrade(){
    var solicitud15 = $('#solicitud15').val();
    if(solicitud15>0){
        
        $('#addItem15').attr('disabled', false);

        if(solicitud15==1){
            if(TrunkCdUpgradeTelefonia>0){
                $('#quitItem15').attr('disabled', false);
            }else{
                $('#quitItem15').attr('disabled', true);
            }    
        }

        if(solicitud15==2){
            if(TrunkCdUpgradeInternetDedicado>0){
                $('#quitItem15').attr('disabled', false);
            }else{
                $('#quitItem15').attr('disabled', true);
            }                        
        }

        if(solicitud15==3){
            if(TrunkCdUpgradeAccesoMPLS>0){
                $('#quitItem15').attr('disabled', false);
            }else{
                $('#quitItem15').attr('disabled', true);
            }    
        }

    }else{
        $('#addItem15').attr('disabled', true);
        $('#quitItem15').attr('disabled', true);
    }
}


/*********************************************************************** Traslado ************************************************************************/
var TrunckCdTraslado = 0;
function addItemTrunckCdTraslado(){

    if(TrunckCdTraslado==0){
        $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_traslado" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Traslado</h3></div><div class="panel-body"><div id="TrunkcdContenedorTraslado" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunckCdTraslado++;
    $('#cantidad_trunckCD_traslado_filas').val(TrunckCdTraslado);
    
    var item = '<div style="margin-bottom:15px;" id="fila_TrunckCdTraslado_'+TrunckCdTraslado+'" class="row">'+
            '<ul class="nav nav-tabs">'+
                        '<li class="active">'+
                            '<a data-toggle="tab" href="#contenedor_TrunckCdTraslado_solicitud_'+TrunckCdTraslado+'" aria-expanded="true">Solicitud</a>'+
                        '</li>'+
                        '<li>'+
                            '<a data-toggle="tab" href="#contenedor_TrunckCdTraslado_telefonia_'+TrunckCdTraslado+'" aria-expanded="false">Telefonia</a>'+
                        '</li>'+
                        '<li class="">'+
                            '<a data-toggle="tab" href="#contenedor_TrunckCdTraslado_internetdedicado_'+TrunckCdTraslado+'" aria-expanded="false">Internet Dedicado</a>'+
                        '</li>'+
                        '<li class="">'+
                            '<a data-toggle="tab" href="#contenedor_TrunckCdTraslado_datos_'+TrunckCdTraslado+'" aria-expanded="false">Datos</a>'+
                        '</li>'+
            '</ul>'+
            '<div class="tab-content">'+
                '<div id="contenedor_TrunckCdTraslado_solicitud_'+TrunckCdTraslado+'" class="tab-pane fade active in">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Numero de OFT</td>'+
                                '<td><input id="trunckCD_Traslado_Solicitud_numOft_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Solicitud_numOft_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckCD_Traslado_Solicitud_codServicio_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Solicitud_codServicio_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cuenta Facturación</td>'+
                                '<td><input id="trunckCD_Traslado_Solicitud_cuentaFacturacion_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Solicitud_cuentaFacturacion_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdTraslado_telefonia_'+TrunckCdTraslado+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Cant. de canales</td>'+
                                '<td><input id="trunckCD_Traslado_Telefonia_cantCanales_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Telefonia_cantCanales_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos SLM</td>'+
                                '<td><input id="trunckCD_Traslado_Telefonia_minSLM_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Telefonia_minSLM_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos móvil</td>'+
                                '<td><input id="trunckCD_Traslado_Telefonia_minMovil_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Telefonia_minMovil_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos adicionales</td>'+
                                '<td><input id="trunckCD_Traslado_Telefonia_minAdicional_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Telefonia_minAdicional_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdTraslado_internetdedicado_'+TrunckCdTraslado+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                               '<td>Vel. Nacional</td>'+
                               '<td><input id="trunckCD_Traslado_InternetDedicado_velNacional_'+TrunckCdTraslado+'" name="trunckCD_Traslado_InternetDedicado_velNacional_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                           '</tr>'+
                            '<tr>'+
                                '<td>Vel. Internacional</td>'+
                                '<td><input id="trunckCD_Traslado_InternetDedicado_velInternacional_'+TrunckCdTraslado+'" name="trunckCD_Traslado_InternetDedicado_velInternacional_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckCD_Traslado_InternetDedicado_precioObjetivo_'+TrunckCdTraslado+'" name="trunckCD_Traslado_InternetDedicado_precioObjetivo_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckCD_Traslado_InternetDedicado_plazo_'+TrunckCdTraslado+'" name="trunckCD_Traslado_InternetDedicado_plazo_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdTraslado_datos_'+TrunckCdTraslado+'" class="tab-pane fade">'+
                   '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Velocidad</td>'+
                                '<td><input id="trunckCD_Traslado_Datos_velocidad_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Datos_velocidad_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckCD_Traslado_Datos_codServicio_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Datos_codServicio_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckCD_Traslado_Datos_precioObjetivo_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Datos_precioObjetivo_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckCD_Traslado_Datos_plazo_'+TrunckCdTraslado+'" name="trunckCD_Traslado_Datos_plazo_'+TrunckCdTraslado+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div>'+
            '</div>';

    $('#TrunkcdContenedorTraslado').append(item);        
    $('#quitItem19').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckCdTraslado(){
    $("#fila_TrunckCdTraslado_"+TrunckCdTraslado).remove();
    TrunckCdTraslado--;
    $('#cantidad_trunckCD_traslado_filas').val(TrunckCdTraslado);
    if(TrunckCdTraslado==0){
        $('#contenedor_trunckcd_boxs_traslado').remove();
        $('#quitItem19').attr('disabled', true);
    }
}

/*********************************************************************** Renegociacion ************************************************************************/
var TrunckCdRenegociacion = 0;
function addItemTrunckCdRenegociacion(){

    if(TrunckCdRenegociacion==0){
        $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_renegociacion" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Renegociación</h3></div><div class="panel-body"><div id="contenedor_trunckcd_renegociacion" class="tab-base tab-stacked-left"></div></div></div>'); 
    }

    TrunckCdRenegociacion++;
    $('#cantidad_trunckCD_renegociacion_filas').val(TrunckCdRenegociacion);
    var item = '<div style="margin-bottom:15px;" id="fila_TrunckCdRenegociacion_'+TrunckCdRenegociacion+'" class="row">'+
                '<ul class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdRenegociacion_solicitud_'+TrunckCdRenegociacion+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdRenegociacion_telefonia_'+TrunckCdRenegociacion+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdRenegociacion_InternetDedicado_'+TrunckCdRenegociacion+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdRenegociacion_datos_'+TrunckCdRenegociacion+'" aria-expanded="false">Datos</a>'+
                    '</li>'+
                '</ul>'+
                '<div class="tab-content">'+
                    '<div id="contenedor_TrunckCdRenegociacion_solicitud_'+TrunckCdRenegociacion+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Numero de OFT</td>'+
                                    '<td><input id="trunckCD_renegociacion_solicitud_numOft_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_solicitud_numOft_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckCD_renegociacion_solicitud_codServicio_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_solicitud_codServicio_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckCD_renegociacion_solicitud_contratoVigente_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_solicitud_contratoVigente_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cuenta Facturación</td>'+
                                    '<td><input id="trunckCD_renegociacion_solicitud_cuentaFacturacion_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_solicitud_cuentaFacturacion_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                               '</tr>'+
                            '</tbody>'+
                       '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdRenegociacion_telefonia_'+TrunckCdRenegociacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+       
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="trunckCD_renegociacion_Telefonia_cantCanales_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_Telefonia_cantCanales_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="trunckCD_renegociacion_Telefonia_minSLM_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_Telefonia_minSLM_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos Móvil</td>'+
                                    '<td><input id="trunckCD_renegociacion_Telefonia_minMovil_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_Telefonia_minMovil_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckCD_renegociacion_Telefonia_contratoVigente_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_Telefonia_contratoVigente_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Telefonía</td>'+
                                    '<td><input id="trunckCD_renegociacion_Telefonia_contratoTelefonia_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_Telefonia_contratoTelefonia_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdRenegociacion_InternetDedicado_'+TrunckCdRenegociacion+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="trunckCD_renegociacion_InternetDedicado_velNacional_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_InternetDedicado_velNacional_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="trunckCD_renegociacion_InternetDedicado_velInternacional_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_InternetDedicado_velInternacional_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_InternetDedicado_precioObjetivo_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckCD_renegociacion_InternetDedicado_plazo_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_InternetDedicado_plazo_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                               '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdRenegociacion_datos_'+TrunckCdRenegociacion+'" class="tab-pane fade">'+
                       '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Velocidad</td>'+
                                    '<td><input id="trunckCD_renegociacion_datos_velocidad_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_datos_velocidad_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckCD_renegociacion_datos_codServicio_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_datos_codServicio_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckCD_renegociacion_datos_precioObjetivo_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_datos_precioObjetivo_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckCD_renegociacion_datos_plazo_'+TrunckCdRenegociacion+'" name="trunckCD_renegociacion_datos_plazo_'+TrunckCdRenegociacion+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div></div>';

    $('#contenedor_trunckcd_renegociacion').append(item);
    $('#quitItem19').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckCdRenegociacion(){
    $('#fila_TrunckCdRenegociacion_'+TrunckCdRenegociacion).remove();
    TrunckCdRenegociacion--;
    $('#cantidad_trunckCD_renegociacion_filas').val(TrunckCdRenegociacion);
    if(TrunckCdRenegociacion==0){
        $('#contenedor_trunckcd_boxs_renegociacion').remove();
        $('#quitItem17').attr('disabled', true);
    }
}


/*********************************************************************** Acceso ************************************************************************/
var TrunckCdAcceso = 0;
function addItemTrunckCdAcceso(){

    if(TrunckCdAcceso==0){
         $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_cambioacceso" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Cambio Acceso</h3></div><div class="panel-body"><div id="contenedor_trunckcd_acceso" class="tab-base tab-stacked-left"></div></div></div>');
    }

    TrunckCdAcceso++;
    $('#cantidad_trunckCD_cambioAcceso_filas').val(TrunckCdAcceso);
    var item = '<div style="margin-bottom:15px;" id="fila_TrunckCdAcceso_'+TrunckCdAcceso+'" class="row">'+
                '<ul class="nav nav-tabs">'+
                    '<li class="active">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdAcceso_solicitud_'+TrunckCdAcceso+'" aria-expanded="true">Solicitud</a>'+
                    '</li>'+
                    '<li>'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdAcceso_telefonia_'+TrunckCdAcceso+'" aria-expanded="false">Telefonia</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdAcceso_internetDedicado_'+TrunckCdAcceso+'" aria-expanded="false">Internet Dedicado</a>'+
                    '</li>'+
                    '<li class="">'+
                        '<a data-toggle="tab" href="#contenedor_TrunckCdAcceso_datos_'+TrunckCdAcceso+'" aria-expanded="false">Datos</a>'+
                    '</li>'+
                '</ul>'+
                '<div  class="tab-content">'+
                    '<div id="contenedor_TrunckCdAcceso_solicitud_'+TrunckCdAcceso+'" class="tab-pane fade active in">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Numero de OFT</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_solicitud_numOft_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_solicitud_numOft_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_solicitud_codServicio_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_solicitud_codServicio_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_solicitud_contratoVigente_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_solicitud_contratoVigente_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cuenta Facturación</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_solicitud_cuentaFacturacion_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_solicitud_cuentaFacturacion_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdAcceso_telefonia_'+TrunckCdAcceso+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Cant. de canales</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Telefonia_cantCanales_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Telefonia_cantCanales_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                               '<tr>'+
                                    '<td>Minutos SLM</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Telefonia_minSLM_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Telefonia_minSLM_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Minutos móvil</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Telefonia_minMovil_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Telefonia_minMovil_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Vigente</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Telefonia_contratoVigente_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Telefonia_contratoVigente_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>USV Contrato Telefonía</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Telefonia_contratoTelefonia_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdAcceso_internetDedicado_'+TrunckCdAcceso+'" class="tab-pane fade">'+
                        '<table style="" class="table table-striped">'+             
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Vel. Nacional</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_internetDedicado_velNacional_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_internetDedicado_velNacional_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Vel. Internacional</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_internetDedicado_velInternacional_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_internetDedicado_velInternacional_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_internetDedicado_precioObjetivo_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_internetDedicado_plazo_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_internetDedicado_plazo_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                    '<div id="contenedor_TrunckCdAcceso_datos_'+TrunckCdAcceso+'" class="tab-pane fade">'+
                       '<table style="" class="table table-striped">'+           
                            '<tbody>'+
                                '<tr>'+
                                    '<td>Velocidad</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Datos_velocidad_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Datos_velocidad_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Cód. Servicio</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Datos_codServicio_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Datos_codServicio_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Precio Objetivo</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Datos_precioObjetivo_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Datos_precioObjetivo_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Plazo</td>'+
                                    '<td><input id="trunckCD_cambio_acceso_Datos_plazo_'+TrunckCdAcceso+'" name="trunckCD_cambio_acceso_Datos_plazo_'+TrunckCdAcceso+'" type="number" class="form-control input-sm" /></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'+
                '</div></div>';

    $('#contenedor_trunckcd_acceso').append(item);            
    $('#quitItem19').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckCdAcceso(){
    $('#fila_TrunckCdAcceso_'+TrunckCdAcceso).remove();
    TrunckCdAcceso--;
    $('#cantidad_trunckCD_cambioAcceso_filas').val(TrunckCdAcceso);

    if(TrunckCdAcceso==0){
        $('#contenedor_trunckcd_boxs_cambioacceso').remove();
        $('#quitItem16').attr('disabled', true);
    }
}

/*********************************************************************** Cambio Producto ************************************************************************/
var TrunckCdCambioProducto = 0;
function addItemTrunckCdCambioProducto(){

    if(TrunckCdCambioProducto==0){
        $('#contenedor_trunckcd_boxs').append('<div id="contenedor_trunckcd_boxs_cambioproducto" class="panel panel-dark"><div class="panel-heading"><h3 class="panel-title">Cambio de Producto</h3></div><div class="panel-body"><div id="contenedor_trunckcd_cambio_producto" class="tab-base tab-stacked-left"> </div></div></div>');
    }

    TrunckCdCambioProducto++;
    $('#cantidad_trunckCD_cambioProducto_filas').val(TrunckCdCambioProducto);

    var item = '<div style="margin-bottom:15px;" id="fila_TrunckCdCambioProducto_'+TrunckCdCambioProducto+'" class="row">'+
            '<ul class="nav nav-tabs">'+
                '<li class="active">'+
                    '<a data-toggle="tab" href="#contenedor_TrunckCdCambioProducto_solicitud_'+TrunckCdCambioProducto+'" aria-expanded="true">Solicitud</a>'+
                '</li>'+
                '<li>'+
                    '<a data-toggle="tab" href="#contenedor_TrunckCdCambioProducto_telefonia_'+TrunckCdCambioProducto+'" aria-expanded="false">Telefonia</a>'+
                '</li>'+
                '<li class="">'+
                    '<a data-toggle="tab" href="#contenedor_TrunckCdCambioProducto_InternetDedicado_'+TrunckCdCambioProducto+'" aria-expanded="false">Internet Dedicado</a>'+
                '</li>'+
                '<li class="">'+
                    '<a data-toggle="tab" href="#contenedor_TrunckCdCambioProducto_datos_'+TrunckCdCambioProducto+'" aria-expanded="false">Datos</a>'+
                '</li>'+
            '</ul>'+
            '<div class="tab-content">'+
                '<div id="contenedor_TrunckCdCambioProducto_solicitud_'+TrunckCdCambioProducto+'" class="tab-pane fade active in">'+
                    '<table style="" class="table table-striped">'+
                        '<tbody>'+
                            '<tr>'+
                                '<td>Numero de OFT</td>'+
                                '<td><input id="trunckCD_cambioProducto_Solicitud_numOft_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_Solicitud_numOft_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckCD_cambioProducto_Solicitud_codServicio_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_Solicitud_codServicio_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Vigente</td>'+
                                '<td><input id="trunckCD_cambioProducto_Solicitud_contratoVigente_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_Solicitud_contratoVigente_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cuenta Facturación</td>'+
                                '<td><input id="trunckCD_cambioProducto_Solicitud_cuentaFacturacion_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_Solicitud_cuentaFacturacion_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdCambioProducto_telefonia_'+TrunckCdCambioProducto+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Cant. de canales</td>'+
                                '<td><input id="trunckCD_cambioProducto_telefonia_cantCanales_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_telefonia_cantCanales_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos SLM</td>'+
                                '<td><input id="trunckCD_cambioProducto_telefonia_minSLM_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_telefonia_minSLM_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Minutos móvil</td>'+
                                '<td><input id="trunckCD_cambioProducto_telefonia_minMovil_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_telefonia_minMovil_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Central</td>'+
                                '<td><input id="trunckCD_cambioProducto_telefonia_contratoVigente_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_telefonia_contratoVigente_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>USV Contrato Telefonía</td>'+
                                '<td><input id="trunckCD_cambioProducto_telefonia_contratoTelefonia_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_telefonia_contratoTelefonia_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdCambioProducto_InternetDedicado_'+TrunckCdCambioProducto+'" class="tab-pane fade">'+
                    '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Vel. Nacional</td>'+
                                '<td><input id="trunckCD_cambioProducto_InternetDedicado_velNacional_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_InternetDedicado_velNacional_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Vel. Internacional</td>'+
                                '<td><input id="trunckCD_cambioProducto_InternetDedicado_velInternacional_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_InternetDedicado_velInternacional_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_InternetDedicado_precioObjetivo_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckCD_cambioProducto_InternetDedicado_plazo_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_InternetDedicado_plazo_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
                '<div id="contenedor_TrunckCdCambioProducto_datos_'+TrunckCdCambioProducto+'" class="tab-pane fade">'+
                   '<table style="" class="table table-striped">'+             
                        '<tbody>'+
                            '<tr>'+
                                '<td>Velocidad</td>'+
                                '<td><input id="trunckCD_cambioProducto_datos_velocidad_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_datos_velocidad_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Cód. Servicio</td>'+
                                '<td><input id="trunckCD_cambioProducto_datos_codServicio_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_datos_codServicio_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Precio Objetivo</td>'+
                                '<td><input id="trunckCD_cambioProducto_datos_precioObjetivo_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_datos_precioObjetivo_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td>Plazo</td>'+
                                '<td><input id="trunckCD_cambioProducto_datos_plazo_'+TrunckCdCambioProducto+'" name="trunckCD_cambioProducto_datos_plazo_'+TrunckCdCambioProducto+'" type="number" class="form-control input-sm" /></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div></div>';

    $('#contenedor_trunckcd_cambio_producto').append(item);
    $('#quitItem19').attr('disabled', false);
    setInputNumber();
}

function quitItemTrunckCdCambioProducto(){
    $('#fila_TrunckCdCambioProducto_'+TrunckCdCambioProducto).remove();
    TrunckCdCambioProducto--;
    $('#cantidad_trunckCD_cambioProducto_filas').val(TrunckCdCambioProducto);
    if(TrunckCdCambioProducto==0){
        $('#contenedor_trunckcd_boxs_cambioproducto').remove();
        $('#quitItem19').attr('disabled', true);
    }
}

/*********************************************************************** DownGrade ************************************************************************/
var TrunkCdDowngrade = 0;
var TrunkCdDowngradeTelefonia = 0;
var TrunkCdDowngradeInternetDedicado = 0;
var TrunkCdDowngradeAccesoMPLS = 0;
var TrunkCdDowngradeGeneral = 0;
var TrunkCdDowngradePanelActivity = 0;
    
function quitItemTrunckCdDowngrade(){
   var solicitud19 = $('#solicitud19').val();
    
   if(solicitud19==1){
        $("#trunkcd-downgradeT-li-"+TrunkCdDowngradeTelefonia).remove();
        $("#trunkcd-downgradeT-contain-"+TrunkCdDowngradeTelefonia).remove();
        TrunkCdDowngradeTelefonia--;
        TrunkCdDowngradeGeneral--;

        $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_downgrade_telefonia_filas').val(TrunkCdDowngradeTelefonia);

        if(TrunkCdDowngradeTelefonia==0){
            $('#quitItem22').attr('disabled', true);
        }

       if(TrunkCdDowngradeGeneral==0){
            $('#contenedor_trunckcd_boxs_downgrade').remove();
            TrunkCdDowngradePanelActivity = 0;
            $('#quitItem22').attr('disabled', true);
            $('#addItem22').attr('disabled', false);
       }
   }

   if(solicitud19==2){
       $("#trunkcd-downgradeId-li-"+TrunkCdDowngradeInternetDedicado).remove(); 
       $("#trunkcd-downgradeId-contain-"+TrunkCdDowngradeInternetDedicado).remove();
       TrunkCdDowngradeInternetDedicado--;
       TrunkCdDowngradeGeneral--;

       $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
       $('#cantidad_trunckCD_downgrade_internetDedicado_filas').val(TrunkCdDowngradeInternetDedicado);

       if(TrunkCdDowngradeInternetDedicado==0){
            $('#quitItem22').attr('disabled', true);
            $('#addItem22').attr('disabled', false);
       }

       if(TrunkCdDowngradeGeneral==0){
            $('#contenedor_trunckcd_boxs_downgrade').remove();
            TrunkCdDowngradePanelActivity = 0;
            $('#quitItem22').attr('disabled', true);
            $('#addItem22').attr('disabled', false);
       }
   }

   if(solicitud19==3){
       $("#trunkcd-downgradeAMPLS-li-"+TrunkCdDowngradeAccesoMPLS).remove(); 
       $("#trunkcd-downgradeAMPLS-contain-"+TrunkCdDowngradeAccesoMPLS).remove();
       TrunkCdDowngradeAccesoMPLS--;
       TrunkCdDowngradeGeneral--;

       $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
       $('#cantidad_trunckCD_downgrade_accesoMpls_filas').val(TrunkCdDowngradeAccesoMPLS);


       if(TrunkCdDowngradeAccesoMPLS==0){
            $('#quitItem22').attr('disabled', true);
       }

       if(TrunkCdDowngradeGeneral==0){
            $('#contenedor_trunckcd_boxs_downgrade').remove();
            TrunkCdDowngradePanelActivity = 0;
            $('#quitItem22').attr('disabled', true);
            $('#addItem22').attr('disabled', false);
       }

   }

   if(TrunkCdDowngradeGeneral==0){
        $('#contenedor_trunckcd_boxs_downgrade').remove();
        TrunkCdDowngradePanelActivity = 0;
        $('#quitItem22').attr('disabled', true);
        $('#addItem22').attr('disabled', false);
   }

   monitorTrunckCd();

}

function addItemTrunckCdDowngrade(){

    var solicitud19 = $('#solicitud19').val();
    var item = "";
    var contain = "";
    
    var activeTab = "";
    var activeContain = "";
    var activetrue = "";

    if(TrunkCdDowngradeGeneral==0){
        $('#bodyDowngradeTruncd').append('<div id="content_trunckcd_downgrade" class="tab-content"></div>');
        activeTab = "active";    
        activeContain = "active in";
        activetrue = "true";
    }else{
        activetrue = "false";
    }

    if(solicitud19==1){
        TrunkCdDowngradeTelefonia++;
        TrunkCdDowngradeGeneral++;

        $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_downgrade_telefonia_filas').val(TrunkCdDowngradeTelefonia);

        item = '<li id="trunkcd-downgradeT-li-'+TrunkCdDowngradeTelefonia+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-downgradeT-contain-'+TrunkCdDowngradeTelefonia+'" aria-expanded="'+activetrue+'">Telefonia</a>'+
        '</li>';
        contain = '<div id="trunkcd-downgradeT-contain-'+TrunkCdDowngradeTelefonia+'" class="tab-pane fade '+activeContain+'">'+  
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_numOft_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_numOft_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_codServicio_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_codServicio_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_contratoVigente_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_contratoVigente_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_cuentaFacturacion_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_cuentaFacturacion_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Actual</th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Cant. de canales</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_cantCanales_actual_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_cantCanales_actual_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_cantCanales_solicitada_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_cantCanales_solicitada_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos SLM</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_minSLM_actual_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_minSLM_actual_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_minSLM_solicitada_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_minSLM_solicitada_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Minutos a Móvil</td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_minMovil_actual_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_minMovil_actual_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                        '<td><input id="trunckCD_downgrade_telefonia_minMovil_solicitada_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_minMovil_solicitada_'+TrunkCdDowngradeTelefonia+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentario</td>'+
                                        '<td colspan="2"><textarea id="trunckCD_downgrade_telefonia_comentario_'+TrunkCdDowngradeTelefonia+'" name="trunckCD_downgrade_telefonia_comentario_'+TrunkCdDowngradeTelefonia+'" placeholder="Message" rows="5" class="form-control" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_trunckcd_downgrade").append(item);
        $("#content_trunckcd_downgrade").append(contain);
        $('#quitItem22').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud19==2){
        TrunkCdDowngradeInternetDedicado++;
        TrunkCdDowngradeGeneral++;

        $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_downgrade_internetDedicado_filas').val(TrunkCdDowngradeInternetDedicado);

        item = '<li id="trunkcd-downgradeId-li-'+TrunkCdDowngradeInternetDedicado+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-downgradeId-contain-'+TrunkCdDowngradeInternetDedicado+'" aria-expanded="'+activetrue+'">Internet Dedicado</a>'+
        '</li>';
        contain = '<div id="trunkcd-downgradeId-contain-'+TrunkCdDowngradeInternetDedicado+'" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_numOft_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_numOft_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_codServicio_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_codServicio_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_contratoVigente_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_contratoVigente_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_cuentaFacturacion_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_cuentaFacturacion_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Vel. Nacional</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_velNacional_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_velNacional_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Vel. Internacional</td>'+
                                        '<td><input id="trunckCD_downgrade_internetDedicado_velInternacional_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_velInternacional_'+TrunkCdDowngradeInternetDedicado+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckCD_downgrade_internetDedicado_comentario_'+TrunkCdDowngradeInternetDedicado+'" name="trunckCD_downgrade_internetDedicado_comentario_'+TrunkCdDowngradeInternetDedicado+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        $("#tabs_trunckcd_downgrade").append(item);
        $("#content_trunckcd_downgrade").append(contain);
        $('#quitItem22').attr('disabled', false);
        setInputNumber();
    }

    if(solicitud19==3){
        TrunkCdDowngradeAccesoMPLS++;
        TrunkCdDowngradeGeneral++;

        $('#cantidad_trunckCD_downgrade_filas').val(TrunkCdDowngradeGeneral);
        $('#cantidad_trunckCD_downgrade_accesoMpls_filas').val(TrunkCdDowngradeAccesoMPLS);

        item = '<li id="trunkcd-downgradeAMPLS-li-'+TrunkCdDowngradeAccesoMPLS+'" class="'+activeTab+'">'+
            '<a data-toggle="tab" href="#trunkcd-downgradeAMPLS-contain-'+TrunkCdDowngradeAccesoMPLS+'" aria-expanded="'+activetrue+'">Acceso MPLS</a>'+
        '</li>';
        contain = '<div id="trunkcd-downgradeAMPLS-contain-'+TrunkCdDowngradeAccesoMPLS+'"" class="tab-pane fade '+activeContain+'">'+
                    '<div class="row">'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Número OFT</td>'+
                                        '<td><input id="trunckCD_downgrade_accesoMpls_numOft_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_numOft_'+TrunkCdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cód. Servicio</td>'+
                                        '<td><input id="trunckCD_downgrade_accesoMpls_codServicio_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_codServicio_'+TrunkCdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>USV Contrato Vigente</td>'+
                                        '<td><input id="trunckCD_downgrade_accesoMpls_contratoVigente_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_contratoVigente_'+TrunkCdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Cuenta Facturación</td>'+
                                        '<td><input id="trunckCD_downgrade_accesoMpls_cuentaFacturacion_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_cuentaFacturacion_'+TrunkCdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                '</tbody>'+
                           '</table>'+
                        '</div>'+
                        '<div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">'+
                            '<table style="" class="table table-striped">'+             
                                '<thead>'+
                                    '<tr>'+
                                        '<th></th>'+
                                        '<th class="text-center">Solicitada</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr>'+
                                        '<td>Velocidad</td>'+
                                        '<td><input id="trunckCD_downgrade_accesoMpls_velocidad_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_velocidad_'+TrunkCdDowngradeAccesoMPLS+'" type="number" class="form-control input-sm" /></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td>Comentarios</td>'+
                                        '<td><textarea id="trunckCD_downgrade_accesoMpls_comentario_'+TrunkCdDowngradeAccesoMPLS+'" name="trunckCD_downgrade_accesoMpls_comentario_'+TrunkCdDowngradeAccesoMPLS+'" placeholder="Message" rows="5" class="form-control input-sm" style=""></textarea></td>'+
                                    '</tr>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>';

        $("#tabs_trunckcd_downgrade").append(item);
        $("#content_trunckcd_downgrade").append(contain);  
        $('#quitItem22').attr('disabled', false);
        setInputNumber();      
    }
}

function monitortrunckcddowngrade(){
    var solicitud19 = $('#solicitud19').val();

    if(solicitud19>0){
        
        $('#addItem22').attr('disabled', false);

        if(solicitud19==1){
            if(TrunkCdDowngradeTelefonia>0){
                $('#quitItem22').attr('disabled', false);
            }else{
                $('#quitItem22').attr('disabled', true);
            }    
        }

        if(solicitud19==2){
            if(TrunkCdDowngradeInternetDedicado>0){
                $('#quitItem22').attr('disabled', false);
            }else{
                $('#quitItem22').attr('disabled', true);
            }                        
        }

        if(solicitud19==3){
            if(TrunkCdDowngradeAccesoMPLS>0){
                $('#quitItem22').attr('disabled', false);
            }else{
                $('#quitItem22').attr('disabled', true);
            }    
        }

    }else{
        $('#addItem22').attr('disabled', true);
        $('#quitItem22').attr('disabled', true);
    }
}