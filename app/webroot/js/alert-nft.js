function alertFail(titulo, text){
     $.niftyNoty({
        type: 'danger',
        container : 'floating',
        title : titulo,
        message : text,
        closeBtn : false,
        icon : 'fa fa-info-circle fa-2x',
        closeOnSelfClick : true,
        timer : 3000
    });
}

function alertSuccess(titulo, text){
    $.niftyNoty({
        type: 'success',
        container : 'floating',
        icon : 'fa fa-thumbs-up fa-2x',
        title : titulo,
        message : text,
        closeBtn : false,
        closeOnSelfClick : true,
        timer : 3000
    });
}