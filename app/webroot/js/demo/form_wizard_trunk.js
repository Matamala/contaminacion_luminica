
// Form-Wizard.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -


$(document).ready(function() {

    
    $('#demo-cls-wz').bootstrapWizard({
        tabClass		: 'wz-classic',
        nextSelector	: '.next',
        previousSelector	: '.previous',
        onTabClick: function(tab, navigation, index) {
            return false;
        },
        onInit : function(){
            $('#demo-cls-wz').find('.finish').hide().prop('disabled', true);
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            var wdt = 100/$total;
            var lft = wdt*index;
            $('#demo-cls-wz').find('.progress-bar').css({width:$percent+'%'});

            if($current >= $total) {
                $('#demo-cls-wz').find('.next').hide();
                $('#demo-cls-wz').find('.finish').show();
                $('#demo-cls-wz').find('.finish').prop('disabled', false);
            } else {
                $('#demo-cls-wz').find('.next').show();
                $('#demo-cls-wz').find('.finish').hide().prop('disabled', true);
            }
        },
        'onNext': function(tab, navigation, index) {

            //console.log(index);

            //Telefonía
            if(index==1){
                
                var oft_estado = $('#oft_estado').val();
                var oftId = $('#oft_id').val();
                var conInternet = $('#con_internet').val();
                var propuestaModulo1 = $('#propuesta_id').val();

                var CanalConInternet = $('#CanalConInternet').is(':checked');
                var oftNumber = $('#oft').val();
                var segmento_id = $('#segmento_id').val();
                var rut_cliente = $('#rcliente').val();
                var nom_cliente = $('#ncliente').val();
                var macceso = $('#macceso').val();
                var ctoft = $('#ctoft').val();
                var region_id = $('#region_id').val();
                var comuna_id = $('#comuna_id').val();
                var cargo_inicial = $('#cargo_inicial').val();
                var bw_nacional = $('#bw_nacional').val();
                var bw_internacional = $('#bw_internacional').val();
                var descuento = $('#descuento').val();
                
                
                if(oftNumber==0 || oftNumber.length==0){
                    showError('Internet Dedicado', 'Debe buscar la OFT a evaluar.');
                    $('#oft').focus();
                    $('#oft').select();
                    return false;
                }

                if(segmento_id.length==0){
                    showError('Internet Dedicado', 'Debe seleccionar el segmento del cliente.');
                    $('#segmento_id').focus();
                    $('#segmento_id').select();
                    return false;
                }

                if(rut_cliente.length==0){
                    showError('Internet Dedicado', 'Debe ingresar el rut del cliente.');
                    $('#rcliente').focus();
                    $('#rcliente').select();
                    return false;
                }

                if(nom_cliente.length==0){
                    showError('Internet Dedicado', 'Debe ingresar el nombre del cliente.');
                    $('#ncliente').focus();
                    $('#ncliente').select();
                    return false;
                }

                if(macceso.length==0){
                    showError('Internet Dedicado', 'Debe ingresar el medio acceso.');
                    $('#macceso').focus();
                    $('#macceso').select();
                    return false;
                }

                if(ctoft.length==0){
                    showError('Internet Dedicado', 'Debe ingresar el costo total OFT.');
                    $('#ctoft').focus();
                    $('#ctoft').select();
                    return false;
                }

                if(conInternet==1){    
                    if(region_id.length==0){
                        showError('Internet Dedicado', 'Debe seleccionar la región donde se realizará el servicio.');
                        $('#region_id').focus();
                        $('#region_id').select();
                        return false;
                    }

                    if(comuna_id.length==0){
                        showError('Internet Dedicado', 'Debe seleccionar la comuna donde se realizará el servicio.');
                        $('#comuna_id').focus();
                        $('#comuna_id').select();
                        return false;
                    }

                    if(bw_nacional.length==0){
                        showError('Internet Dedicado', 'Debe seleccionar el BW Nacional.');
                        $('#bw_nacional').focus();
                        $('#bw_nacional').select();
                        return false;
                    }

                    if(bw_internacional.length==0){
                        showError('Internet Dedicado', 'Debe seleccionar el BW Internacional.');
                        $('#bw_internacional').focus();
                        $('#bw_internacional').select();
                        return false;
                    }
                    
                    if(propuestaModulo1>0 && oft_estado==13){
                        $('#demo-cls-wz').bootstrapWizard('show', index);
                    }else{
                        showError('Internet Dedicado', 'Debe realizar el cálculo correspondiente para pasar al módulo de Canales IP.');
                    }
                }else{
                    if(oftId>0 && oft_estado==13){    
                        
                        $('#demo-cls-wz').bootstrapWizard('show', index);
                   
                    }else{
                        showError('Internet Dedicado', 'Debe ingresar una OFt válida.');
                    }
                }

            }

            //Canales IP
            if(index==2){

                var val_canal = $('input[name=canal_check]:checked').val();
                if(val_canal>0){
                     $('#demo-cls-wz').bootstrapWizard('show', index);
                }else{
                    showError('Internet Dedicado', 'Debe seleccionar los canales.');
                }
            }

            //Minutos Adicionales
            if(index==3){
                
                $('#demo-cls-wz').bootstrapWizard('show', index);
            
            }

            //Central Telefónica
            if(index==4){
                var central_control = $('#centralita').is(':checked');
                
                //console.log(central_control);

                if(central_control){
                    var val_canalcontrol = $('input[name=canal]:checked').val();
                    
                    //console.log(val_canalcontrol);

                    if(val_canalcontrol=='basico' || val_canalcontrol=='normal' || val_canalcontrol=='pro'){
                        $('#demo-cls-wz').bootstrapWizard('show', index);        
                    }else{
                        showError('Central Telefónica', 'Debe seleccionar un tipo de central.'); 
                    }
                }else{
                    $('#demo-cls-wz').bootstrapWizard('show', index);
                }      
            }

            //Pack
            if(index==5){
                $('#demo-cls-wz').bootstrapWizard('show', index);
            }

            //Totales
            if(index==6){
                
                //Propuesta Internet
                
            }   

            return false;
        
        }
    });


     $('#saveAll').bind('click', function(){
            var save_propuesta_id = $('#propuesta_id').val();
            var costo_oft = $('#ctoft').val();
            costo_oft = costo_oft.replace(".", "");
            costo_oft = costo_oft.replace(".", "");
            costo_oft = costo_oft.replace(".", "");
            costo_oft = costo_oft.replace("$", "");
            costo_oft = costo_oft.replace(" ", "");
            var save_oft = $('#oft').val();

            var rut_cliente = $('#rcliente').val();
            var nom_cliente = $('#ncliente').val();

            var trunk_internet_val = 0;
            var trunk_internet = $('#CanalConInternet').is(':checked');
            if(trunk_internet){
                trunk_internet_val = 1;
            }

            //Canales_ip
            var save_canales = $( ".central_sel:checked" ).val();

            //Minutos Móbiles Adicionales y Moviles
            var adicionales_fijos = array_adicionales_fijos;
            var adicionales_moviles = array_adicionales_moviles;

            //instalacion canales
            var con_central = $('#centralita').is(':checked');
            var central = 0;
            if(con_central){
                
                central = 1;
                var central_selecccionada = $( ".central_seleccionada:checked" ).attr('data-chanel');
                var central_tipo = $( ".central_seleccionada:checked" ).attr('data-type');
                var instalacion = $('#instala').is(':checked');
                var instalacion_val = 0;
                if(instalacion){
                    instalacion_val = 1;
                }

            }else{
                
                central = 0;
                var instalacion_val = 0;
                var central_selecccionada = 0;
                var instalacion = 0;
            
            }
           
            //Packs
            var packs = array_packs; 

            rut_cliente = rut_cliente.replace(".", "");
            rut_cliente = rut_cliente.replace(".", "");
            rut_cliente = rut_cliente.replace(",", "");
            rut_cliente = rut_cliente.replace(",", "");
                
            var numero_oft = $('#oft').val();
            var segmento_id = $('#segmento_id').val();
            $('#saveAll').attr('disabled', true);

            $.ajax({
                url: 'includes/trunkid.php',
                type: 'POST',
                data: {
                    'modulo' : 5,
                    'cliente' : nom_cliente,
                    'rut_cliente' : rut_cliente,
                    'packs' : JSON.stringify(array_packs),
                    'costo_oft' : costo_oft,
                    'numero_oft' : numero_oft,
                    'segmento_id' : segmento_id,
                    'moviles_adicionales' : JSON.stringify(adicionales_moviles),
                    'fijos_adicionales' : JSON.stringify(adicionales_fijos),
                    'trunk_internet' : trunk_internet_val,
                    'con_central' : central,
                    'central_tipo' : central_tipo,
                    'central_id' : central_selecccionada,
                    'propuesta_id' : save_propuesta_id, 
                    'canal_id' : save_canales,
                    'central_instalacion' : instalacion_val
                },
                success: function(resp){                

                    console.log(resp);
                    if(resp==1){

                        top.location.href = "cotizaciones_trunk.php";

                    }else{
                        
                        showError('Cotización Combos Trunk IP', 'Error, intentelo nuevamente.'); 
                        $('#saveAll').attr('disabled', false);
                    
                    }

                }
            });
     })

});

