
	function alertFail(text){
         $.niftyNoty({
            type: 'danger',
            container : 'floating',
            title : 'Atención',
            message : text,
            closeBtn : true,
            timer : 1500,
            onShown:function(){
                //alert("onShown Callback");
            }
        });
    }

    function alertSuccess(text){
        $.niftyNoty({
            type: 'success',
            container : 'floating',
            title : 'Atención',
            message : text,
            closeBtn : true,
            timer : 1500,
            onShown:function(){
                //alert("onShown Callback");
            }
        });
    }
