
function showError(titulo, text){
     $.niftyNoty({
        type: 'danger',
        container : 'floating',
        title : titulo,
        message : text,
        closeBtn : true,
        icon : 'fa fa-info-circle fa-2x',
        closeOnSelfClick : true,
        timer : 5000
    });
}

function showSuccess(titulo, text){
    $.niftyNoty({
        type: 'success',
        container : 'floating',
        icon : 'fa fa-thumbs-up fa-2x',
        title : titulo,
        message : text,
        closeBtn : true,
        closeOnSelfClick : true,
        timer : 5000
    });
}

function alertFail(text){
     $.niftyNoty({
        type: 'danger',
        container : 'floating',
        title : 'Atención',
        message : text,
        closeBtn : true,
        icon : 'fa fa-info-circle fa-2x',
        closeOnSelfClick : true,
        timer : 5000
    });
}

function alertSuccess(text){
    $.niftyNoty({
        type: 'success',
        container : 'floating',
        icon : 'fa fa-thumbs-up fa-2x',
        title : 'Atención',
        message : text,
        closeBtn : true,
        closeOnSelfClick : true,
        timer : 5000
    });
}

function alertMessage(mnsg, clase){
    $.niftyNoty({
        type: clase,
        container : 'floating',
        title : 'Atención',
        message : mnsg,
        closeBtn : false,
        timer : 3000
    });
}