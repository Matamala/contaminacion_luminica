

    function alertFail(text){
         $.niftyNoty({
            type: 'danger',
            container : 'floating',
            title : 'Atención',
            message : text,
            closeBtn : false,
            timer : 2000,
            onShown:function(){
                //alert("onShown Callback");
            }
        });
    }

    function alertSuccess(text){
        $.niftyNoty({
            type: 'success',
            container : 'floating',
            title : 'Atención',
            message : text,
            closeBtn : false,
            timer : 2000,
            onShown:function(){
                //alert("onShown Callback");
            }
        });
    }

        function number_format(amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0) 
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

            return amount_parts.join(',');
        }

        

        function aplicar_descuento_equipo(num){

            descuento = parseInt($("#desc_equipos"+num).val());
            descuento = replacePuntos(descuento);
            valor_total_equipos = parseInt($("#total"+num).val());
            valor_total_equipos = replacePuntos(valor_total_equipos);

            resultado           = valor_total_equipos * descuento;
            valor_a_descontar   = resultado / 100;

            nuevo_total_equipos = valor_total_equipos - valor_a_descontar;
            //$("#total"+num).val(parseFloat(nuevo_total_equipos));

            total_facturacion(num);

        }

        function total_facturacion(num){
            var x;
            var total = 0;
            var i;
            var y     = 1;

            var lineas_totales = "";
            var planes = "";
            var esPortabilidad = "";

            for(i=0;i<logInput.length;i++){

                var plantot = $("#planes"+logInput[i]).val();
                var ltot = $("#cant_linea"+logInput[i]).val();
                var portatot = $("#tipo_lineas"+logInput[i]).val();

                if(i==0){
                    lineas_totales = ltot;
                    planes = plantot;
                    esPortabilidad = portatot;
                }else{
                    lineas_totales+= '-'+ltot;
                    planes+= '-'+plantot;
                    esPortabilidad+= '-'+portatot;
                }
                            

                x = $("#total"+logInput[i]).val();
                //console.log('x = '+x+' #total'+logInput[i])
                x = x.replace(".", "");
                x = x.replace("$", "");
                x = x.replace(".", "");

                if(x>0){
                    total = parseInt(total) + parseInt(x);         
                }else{
                    total = parseInt(total);
                }   
            }

            //console.log('Antes :'+total);
            total = Math.round(total/1.19);
            //console.log('Despues :'+total);

            //console.log(esPortabilidad);

            if(total>0){
                var totval = total;
                $.ajax({
                    url     : "includes/test.php",
                    type    : "POST",
                    data    : { "planes_array" : planes , 'lineas_array' : lineas_totales }, 
                    success : function(result){
                        $('#fidelizacion_total').val('$'+number_format(result, 0));
                        totval = totval - result;
                        $('#fact_total_fidelizacion').val('$'+number_format(totval, 0));
                        recalcula();
                    }
                })

                $.ajax({
                    url     : "includes/descuentos.php",
                    type    : "POST",
                    data    : { "modulo": 1, "planes_array" : planes, "esPortabilidad" : esPortabilidad, 'lineas_array' : lineas_totales}, 
                    success : function(result){
                        //console.log(result);
                        $('#portabilidad_total').val('$'+number_format(result, 0));
                        totval = totval - result;
                        $('#fact_total_fidelizacion').val('$'+number_format(totval, 0));
                        recalcula();
                    }
                })
                
               

            }else{
                $('#fidelizacion_total').val('$'+0);
                $('#fact_total_fidelizacion').val('$'+0);
            }

            //console.log(total);
            //console.log(total);
            $("#fact_total").val('$'+number_format(total, 0));
            $("#fact_total").attr("readonly" ,true);
            
        }

        function estaAlineado(num){
            var id_equipo = $('#equipos'+num).val();
            var id_plan = $('#planes'+num).val();

            if(id_equipo>0 && id_plan>0){
                //console.log('Id Equipo : '+id_equipo+' Id Plan: '+id_plan);
                $.ajax({
                    url     : "includes/descuentos.php",
                    type    : "POST",
                    data    : { "modulo" : 2 , 'id_equipo' : id_equipo, 'id_plan': id_plan}, 
                    success : function(result){
                        if(result==1){
                            $("#desc_equipos"+num).attr("readonly" ,true);
                            $('#desc_equipos'+num).val('100%');
                        }else{
                            $('#desc_equipos'+num).attr('readonly', false);
                            $('#desc_equipos'+num).val('0%');
                            
                        }
                    }
                })
            }else{
                $('#desc_equipos'+num).val('0%');
                $('#desc_equipos'+num).attr('readonly', false);
            }

           
        }

        function cantidad_descuento(num){
            var desc = $('#desc_equipos'+num).val();

            desc = desc.replace("%", "");
            desc = desc.replace("$", "");
            desc = desc.replace(".", "");
            desc = desc.replace("%", "");
            desc = desc.replace("$", "");
            desc = desc.replace(".", "");
            desc = desc.replace("%", "");
            desc = desc.replace("$", "");
            desc = desc.replace(".", "");

            $('#desc_equipos'+num).val(desc+'%');

            if(isNaN(desc)){
               $("#desc_equipos"+num).val(0);
               desc = 0; 
            }else{

                var id_plan = $('#planes'+num).val();
                var id_equipo = $('#equipos'+num).val();

                if(isNaN(id_plan) && isNaN(id_equipo)){
                    $("#desc_equipos"+num).val('0%');
                    alert('Debe seleccionar el plan y el equipo a contratar.');
                    return false;
                }else{
                    if(id_plan>0 && id_equipo>0){
                        $.ajax({
                            url     : "includes/funciones.php",
                            type    : "POST",
                            data    : { "ValidaDescuentoEquipo" : 1 , 'descuento' : desc, "id_plan" : id_plan, "id_equipo" : id_equipo }, 
                            success : function(result){
                                //console.log(result);    

                                var resp = result.split('-');
                                if(resp[1]=='fail'){
                                    var nm_equipo = $("#equipos"+num+" option:selected").text();
                                    var nm_plan = $("#planes"+num+" option:selected").text();
                                    $("#desc_equipos"+num).val('0%');
                                    alert(resp[0]+' para el equipo '+nm_equipo+' y el plan '+nm_plan+'.');
                                    return false;
                                }

                                if(resp[1]=='ok'){
                                    
                                }

                                if(resp[1]=='empty'){
                                    //alert(resp[0]);
                                    //return false;   
                                }
                            }
                        })
                    }else{
                       $("#desc_equipos"+num).val('0%');
                       alert('Debe seleccionar el plan y el equipo a contratar.');
                       return false; 
                    }
                }
            }  
        }
        
        function cargarCargoFijo(id_plan,numero) {
            //console.log('Id Plan : '+id_plan+' / numero : '+numero);
            var j = numero; // Fila en la que se realizo la selección
            var vatLineas = $("#cant_linea"+j).val();

            $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : { "get_dec_extra" : '1' , 'id_plan' : id_plan }, 
                success : function(result){
                    $('#descExtra'+j).html(result);
                }
            })

            $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : { "get_cargo_con_iva" : '1' , 'id_plan' : id_plan }, 
                success : function(result){
                   
                    var resultVar = result.split('-');
                    $("#cargo_fijo"+j).val('$'+number_format(Math.round(resultVar[0]), 0));
                    
                    $("#cant_linea"+j).val(1);
                    total_linea(j);
                    total_facturacion(j);
                    //console.log(resultVar[1]);
                    if(resultVar[1]==0){
                        $("#cant_equipos"+j).val(0);
                        $("#equipos"+j).val(0);
                        $("#desc_equipos"+j).val('0%');
                        $("#cant_equipos"+j).attr("readonly", true);
                        $("#equipos"+j).attr("readonly", true);
                        $("#equipos"+j).attr("disabled", true);
                        $("#desc_equipos"+j).attr("readonly", true);
                        $("#cant_linea"+j).val(1);
                    }else{
                        $("#cant_equipos"+j).val(0);
                        $("#cant_equipos"+j).attr("readonly" ,true);
                        $("#cant_equipos"+j).val(1);
                        $("#equipos"+j).attr("readonly" ,false);
                        $("#equipos"+j).attr("disabled", false);
                        $("#desc_equipos"+j).attr("readonly" ,false);
                        $("#desc_equipos"+j).val('0%');
                    }

                }
            });

           
        
        }

        function enviar_cotizacion(){
            
            total_siniva = $("#fact_total").val();

            $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : { "get_fidelizacion_plan" : '1' , 'total_siniva' : total_siniva }, 
                success : function(result){
                    $("#porcentaje_fidelizacion").val(result);
                    valida_campos();

                    //$("#cotizacion").submit();
                }
            });

        }

        var bandera = 0;

        function valida_campos(){

                $('#contenedorAlertas').html('');
                $('.form-control').removeClass('panel-bordered-danger');

                var maximoLineas = $('#maximoLineas').val();
                var minimoLineas = $('#minimoLineas').val();

                if(maximoLineas.length==0 || maximoLineas==0){
                    maximoLineas = 0;
                }

                if(minimoLineas.length==0 || minimoLineas==0){
                    minimoLineas = 1;
                }                

                minimoLineas = parseInt(minimoLineas);
                maximoLineas = parseInt(maximoLineas);

                var DgRazonSocial = $.trim($('#DGnombre_cliente').val());   
                var DgRut = $.trim($('#DGrut_cliente').val());   
                validaCliente(); 
                var DgNdeBPO = $.trim($('#DGnumero_bpo').val());  
                var DgCanal = $.trim($('#GDcanales').val()); 


                if(DgRut.length==0){
                    alertFail('Debe ingresar el rut de la empresa');
                    $('#DGrut_cliente').addClass('panel-bordered-danger');
                    $('#DGrut_cliente').select();
                    $('#DGrut_cliente') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false;
                } 

                if(DgRazonSocial.length==0){
                    alertFail('Debe ingresar la razon social de la empresa');
                    $('#DGnombre_cliente').addClass('panel-bordered-danger');
                    $('#DGnombre_cliente').select();
                    $('#DGnombre_cliente') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false;
                } 

                //console.log(DgCanal);
                if(DgCanal.length==0 || DgCanal==0 || DgCanal=='0'){
                    alertFail('Debe seleccionar el canal');
                    $('#GDcanales').addClass('panel-bordered-danger');
                    $('#GDcanales').select();
                    $('#GDcanales') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false;
                } 

            //Resumen Propuesta
                var RpTipoSolicitud = $.trim($('#tipo_solicitud').val());   
                var RpLineasTotalesVozPropuesta = $.trim($('#lineas_totales').val());    
                var RpFacturacionTotalSinIva = $.trim($('#fact_total').val());
                RpFacturacionTotalSinIva = replacePuntos(RpFacturacionTotalSinIva);  
                var RpFacturacionObjetivoSinIva = $.trim($('#fact_obj').val()); 
                RpFacturacionObjetivoSinIva = replacePuntos(RpFacturacionObjetivoSinIva);

                if(RpTipoSolicitud.length==0 || RpTipoSolicitud.length=='0' || RpTipoSolicitud==0){
                    alertFail('Debe seleccionar el tipo de solicitud');
                    $('#tipo_solicitud').addClass('panel-bordered-danger');
                    $('#tipo_solicitud').select();
                    $('#tipo_solicitud') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false; 
                }

                if(RpLineasTotalesVozPropuesta.length==0){
                    alertFail('Debe ingresar las lineas totales de voz propuesta');
                    $('#lineas_totales').addClass('panel-bordered-danger');
                    $('#lineas_totales').select();
                    $('#lineas_totales') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false; 
                }

                if(RpLineasTotalesVozPropuesta<minimoLineas){
                    alertFail('A lo menos deben ser '+minimoLineas+' lineas contratadas.');
                    $('#lineas_totales').addClass('panel-bordered-danger');
                    $('#lineas_totales').select();
                    $('#lineas_totales') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false; 
                }

                if(maximoLineas>0){
                    if(RpLineasTotalesVozPropuesta>maximoLineas){
                        alertFail('Maximo deben ser '+maximoLineas+' lineas contratadas.');
                        $('#lineas_totales').addClass('panel-bordered-danger');
                        $('#lineas_totales').select();
                        $('#lineas_totales') .focus();
                        $('#realizaEzaluacion').attr('disabled', false);
                        return false; 
                    }
                }

                if(RpFacturacionTotalSinIva.length==0){
                    alertFail('Debe ingresar la Facturación Total (sin IVA)');
                    $('#fact_total').addClass('panel-bordered-danger');
                    $('#fact_total').select();
                    $('#fact_total') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false;
                }

                if(isNaN(RpFacturacionTotalSinIva)){
                    alertFail('Debe ingresar solo números en la Facturación Total (sin IVA)');
                    $('#fact_total').addClass('panel-bordered-danger');
                    $('#fact_total').select();
                    $('#fact_total') .focus();
                    $('#realizaEzaluacion').attr('disabled', false);
                    return false;
                }   

                var pp_id_plan;
                var pp_tipo_linea;
                var pp_cantidad_linea;
                var pp_cargo_fijo;
                var pp_total;
                var pp_cant_equipos;
                var pp_equipos;
                var pp_desc_equipos;

                for(i=0;i<logInput.length;i++){
                    pp_id_plan = $('#planes'+logInput[i]).val();
                    pp_id_plan = replacePuntos(pp_id_plan);
                    pp_tipo_linea = $('#tipo_lineas'+logInput[i]).val();
                    pp_tipo_linea = replacePuntos(pp_tipo_linea);
                    pp_cantidad_linea = $('#cant_linea'+logInput[i]).val();
                    pp_cantidad_linea = replacePuntos(pp_cantidad_linea);
                    pp_cargo_fijo = $('#cargo_fijo'+logInput[i]).val();
                    pp_cargo_fijo = replacePuntos(pp_cargo_fijo);
                    pp_total = $('#total'+logInput[i]).val();
                    pp_total = replacePuntos(pp_total);
                    pp_cant_equipos = $('#cant_equipos'+logInput[i]).val();
                    pp_cant_equipos = replacePuntos(pp_cant_equipos);
                    pp_equipos = $('#equipos'+logInput[i]).val();
                    pp_equipos = replacePuntos(pp_equipos);
                    pp_desc_equipos = $('#desc_equipos'+logInput[i]).val();
                    pp_desc_equipos = replacePuntos(pp_desc_equipos);    

                    if(pp_id_plan.length==0 || pp_id_plan==0){
                        alertFail('Debe seleccionar el Plan.');
                        $('#planes'+logInput[i]).addClass('panel-bordered-danger');
                        $('#planes'+logInput[i]).select();
                        $('#planes'+logInput[i]).focus();
                        $('#realizaEzaluacion').attr('disabled', false);
                        return false;
                    }else{
                         $('#planes'+logInput[i]).removeClass('panel-bordered-danger');
                    }

                    if(pp_tipo_linea.length==0 || pp_tipo_linea==0){
                        alertFail('Debe seleccionar el tipo de linea.');
                        $('#tipo_lineas'+logInput[i]).addClass('panel-bordered-danger');
                        $('#tipo_lineas'+logInput[i]).select();
                        $('#tipo_lineas'+logInput[i]).focus();
                        $('#realizaEzaluacion').attr('disabled', false);
                        return false;
                    }else{
                       $('#tipo_lineas'+logInput[i]).removeClass('panel-bordered-danger'); 
                    }

                    if(pp_cantidad_linea.length==0 || pp_cantidad_linea==0){
                        alertFail('Debe ingresar el número de lineas a contratar.');
                        $('#cant_linea'+logInput[i]).addClass('panel-bordered-danger');
                        $('#cant_linea'+logInput[i]).select();
                        $('#cant_linea'+logInput[i]).focus();
                        $('#realizaEzaluacion').attr('disabled', false);
                        return false;
                    }else{
                       $('#cant_linea'+logInput[i]).removeClass('panel-bordered-danger'); 
                    }

                    var disabledModelo = $('#equipos'+logInput[i]).attr('disabled');
                    if(disabledModelo!='disabled'){
                        if(pp_cant_equipos>0){
                            if(pp_equipos==0){     
                                alertFail('Debe seleccionar el modelo del equipo.');
                                $('#equipos'+logInput[i]).addClass('panel-bordered-danger');
                                $('#equipos'+logInput[i]).select();
                                $('#equipos'+logInput[i]).focus();
                                $('#realizaEzaluacion').attr('disabled', false);
                                return false;
                            }
                        }
                    }else{
                       $('#equipos'+logInput[i]).removeClass('panel-bordered-danger'); 
                    }
                }

                $("#facturacion_total").val($('#fact_total').val());
                //$("#realizaEzaluacion").hide();

                //console.log($("#cotizacion").serialize());
                
                $('#realizaEzaluacion').attr('disabled', true);
                
                //console.log($("#cotizacion").serialize());
              
                $.ajax({
                   type: "POST",
                   url: '<?php echo $url_pre_evaluacion;?>',
                   data: $("#cotizacion").serialize(), // Adjuntar los campos del formulario enviado.
                   success: function(data){
               
                       data = $.trim(data);
                       //console.log(data);
                       if(data=='ok'){
                        alertSuccess('Redirigiendo a la evaluación...')
                        $('#cotizacion').attr('action', "evaluacion.php");
                        $('#cotizacion').submit(); 
                    
                       }else{
                            alertFail('No es posible dar respuesta con los servicios y descuentos solicitados, favor disminuir descuentos en cargos fijos y/o equipos.');
                            $('#realizaEzaluacion').attr('disabled', false);
                       } 
                   }
                 });
                
        }

       

        function agregaFila() {
            filas++;
            logInput.push(filas);
         
            var nuevaFila = '<tr class="filas_portabilidad">'
                                +'<td>'+
                                    '<div class="num'+filas+'"></div>'+
                                    '<input class="form-control" type="hidden" name="plan'+filas+'" id="plan'+filas+'">'+ 
                                '</td>'+
                                '<td>'+'<div class="tipo_linea'+filas+'"></div>'+  
                                    '<input class="form-control" type="hidden" name="tipo_linea'+filas+'" id="tipo_linea'+filas+'">'+
                                '</td>'+
                                '<td>'+ 
                                    '<input required min="1" max="100" value="1" class="form-control cant_linea_" name="cant_linea'+filas+'" id="cant_linea'+filas+'" type="number" oninput="setcantEquipos('+filas+');total_linea('+filas+');total_facturacion('+filas+');">'+
                                '</td>'+    
                                '<td>'+  
                                    '<input required class="form-control" name="cargo_fijo'+filas+'" value="$0" id="cargo_fijo'+filas+'" type="text" readonly>'+ 
                                '</td>'+ 
                                '<td>'+  
                                    '<select class="form-control equipos_ dropBXY" id="descExtra'+filas+'" name="descExtra'+filas+'" onchange="total_linea('+filas+');total_facturacion('+filas+')"></select>'+ 
                                '</td>'+ 
                                '<td>'+  
                                    '<input required class="form-control" value="$0" name="total'+filas+'" id="total'+filas+'" type="text" readonly>'+ 
                                '</td>'+ 
                                '<td>'+ 
                                    '<input class="form-control cant_equipos_" name="cant_equipos'+filas+'" value="0" id="cant_equipos'+filas+'" oninput="cantidad_equipos('+filas+');" type="number">'+ 
                                '</td>'+ 
                                '<td>'+
                                    '<div class="llenarEquipos'+filas+'"></div>'+
                                    '<input type="hidden" name="equipo'+filas+'" id="equipo'+filas+'">'+
                                '</td>'+ 
                                '<td> '+ 
                                    '<input class="form-control" name="desc_equipos'+filas+'" value="0%" id="desc_equipos'+filas+'" oninput="cantidad_descuento('+filas+');aplicar_descuento_equipo('+filas+');" type="text">'+ 
                                '</td>'+ 
                            '</tr>';

            $("#tabla_detail").append(nuevaFila);

            $('#desc_equipos'+filas).keyup(function(){
                var descuento = $(this).val();
                descuento = descuento.replace("%", "");
                descuento = descuento.replace("$", "");
                descuento = descuento.replace(".", "");
                descuento = descuento.replace("%", "");
                descuento = descuento.replace("$", "");
                descuento = descuento.replace(".", "");
                descuento = descuento.replace("%", "");
                descuento = descuento.replace("$", "");
                descuento = descuento.replace(".", "");

                if(descuento>100){
                    $(this).val('100%');
                }else{
                    $(this).val(descuento+'%');
                }
            })

            //total_linea(filas);
            //total_facturacion(filas);
            
            var busca = parseInt(filas);

            
            //AJAX PARA LLENAR PLAN DINAMICAMENTE NUEVAS FILAS
            $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : {"llenarPlan" : busca}, 
                success : function(result){
                $(".num"+busca).html(result);
            }});

            
            //AJAX PARA LLENAR TIPO LINEA DINAMICAMENTE NUEVAS FILAS
            $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : {"llenarTipoLinea" : busca}, 
                success : function(result){
                $(".tipo_linea"+busca).html(result);
            }});

            //AJAX PARA LLENAR EQUIPOS DINAMICAMENTE NUEVAS FILAS
             $.ajax({
                url     : "includes/funciones.php",
                type    : "POST",
                data    : {"llenarEquipos" : busca}, 
                success : function(result){
                $(".llenarEquipos"+busca).html(result);
            }});
            
            $("#total_filas_tabla").val(filas);
        }

        function eliminarFila() {
            var trs = $("#tabla_detail tr").length;
            if(trs>1){
                // Eliminamos la ultima columna
                var last = $("#tabla_detail tr:last").index()+1;
                var pos = logInput.indexOf(last);
                if(pos){
                     logInput.splice(pos, 1);
                }

                $("#tabla_detail tr:last").remove();
                cargarCargoFijo(1);
                filas--;
            }
        
            $("#total_filas_tabla").val(filas);
        }

        function validaCliente(){
            $('#alertRtu').html('');
            var DgRut = $.trim($('#DGrut_cliente').val());   
            $.ajax({
                url     : "includes/descuentos.php",
                type    : "POST",
                data    : { "modulo": 3, "rutCliente" : DgRut}, 
                success : function(result){
                    if(result==1){
                        alertFail('No es posible realizar evaluación a este Cliente ya que posee cotizaciones vigentes con otro ejecutivo(a).');
                        $('#realizaEzaluacion').attr('disabled', true);
                    }else{
                        $('#realizaEzaluacion').attr('disabled', false);
                    }   
                }
            }) 
        }

        function giveRazonSocial(){
            var DgRut = $.trim($('#DGrut_cliente').val());   
            $.ajax({
                url     : "includes/descuentos.php",
                type    : "POST",
                data    : { "modulo": 4, "rutCliente" : DgRut}, 
                success : function(result){
                     //console.log('Retorno : '+result)
                    $('#DGnombre_cliente').val(result);  
                }
            })
        }

function formateaRut(rut) {

    var actual = rut;
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }else{
        rutPuntos = rut;                    
    }

    return rutPuntos;
}

$(document).ready(function(){

    $('#DGrut_cliente').keyup(function(){
        var rut = $('#DGrut_cliente').val();
        var newRut = formateaRut(rut);
        $('#DGrut_cliente').val(newRut);
        validaCliente();
        giveRazonSocial();
    });

    $('#realizaEzaluacion').bind('click', function(){
        valida_campos();
    })

     Pace.on("done", function(){
        $(".preload").fadeOut("fast");
    });

})