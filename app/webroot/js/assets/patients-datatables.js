
// Tables-DataTables.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -



$(document).on('nifty.ready', function() {


    // DATA TABLES
    // =================================================================
    // Require Data Tables
    // -----------------------------------------------------------------
    // http://www.datatables.net/
    // =================================================================
    /*
    $('#demo-dt-basic').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "Patients/findList",
        "aoColumns": [
            {mData:"nombres",   className: "text-center", bSearchable: true, bSortable: true, className: "text-left"},
            {mData:"apellidos", className: "text-center", bSearchable: true, bSortable: true, className: "text-left"},
            {mData:"16288294-5",         className: "text-center", bSearchable: true, bSortable: true, className: "text-left"},
            {mData:"nombre",             className: "text-center", bSearchable: true, bSortable: true, className: "text-left"},
        ],
        "fnCreatedRow": function(nRow, aData, iDataIndex){
            console.log(aData);
        },
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se encontro nada",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay datos disponibles",
            "search": "Buscar: ",
            "processing": "Procesando información",
            "loadingRecords": "Cargando información",
            "infoFiltered": "(filtrado de _MAX_ registros en total)",
            "paginate": {
              "previous": '<i class="demo-psi-arrow-left"></i>',
              "next": '<i class="demo-psi-arrow-right"></i>'
            }
        }
    });
    */

});
