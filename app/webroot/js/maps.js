
var map;
var markers = [];
var chart;

var temperaturas = Array();
temperaturas['value'] = Array();
temperaturas['date'] = Array();
temperaturas['time'] = Array();

var contaminacion = Array();
contaminacion['value'] = Array();
contaminacion['date'] = Array();
contaminacion['time'] = Array();

var humedad = [];
humedad['value'] = Array();
humedad['date'] = Array();
humedad['time'] = Array();

var ubicaciones = [];

var info = [];

var config = {
  apiKey: "AIzaSyADq2eJCb88WMqv1Vi4s8jm8c9VDpu_9zg",
  authDomain: "recirculador-1190f.firebaseapp.com",
  databaseURL: "https://recirculador-1190f.firebaseio.com",
  projectId: "recirculador-1190f",
  storageBucket: "recirculador-1190f.appspot.com",
  messagingSenderId: "405439143098"
};
firebase.initializeApp(config);
var database = firebase.database();
var refZero1 = database.ref('zero1');
var refZero2 = database.ref('zero2');

refZero1.on("value", function(snapshot, prevChildKey) {
  var data = snapshot.val();
  
  if(info[data.name]==undefined){
    info[data.name] = Array();
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: new google.maps.LatLng(data['lat'], data['lng']),
      mapTypeId: 'terrain',
      zoomControl: false,
      scaleControl: true,
      fullscreenControl: false
    });   
  }else{
    DeleteMarker(data.name);
  }

  info[data.name]['id'] = data.name;
  info[data.name]['description'] = 'Nodo: '+data.name+' <br /><a class="point" href="#">Monitoreo ATTR1: '+data.attr1+'</a>';
  info[data.name]['lat'] = data.lat;
  info[data.name]['lng'] = data.lng;
  //console.log('Lat:'+data.lat+' --- Long:'+data.lng);
  addMarker(info[data.name], map);
});

refZero2.on("value", function(snapshot, prevChildKey) {
  var data = snapshot.val();
  
  if(info[data.name]==undefined){
    info[data.name] = Array();
  }else{
    DeleteMarker(data.name);
  }

  info[data.name]['id'] = data.name;
  info[data.name]['description'] = 'Nodo: '+data.name+' <br /><a id="'+data.name+'" class="point" href="#">Monitoreo ATTR1: '+data.attr1+'</a>';
  info[data.name]['lat'] = data.lat;
  info[data.name]['lng'] = data.lng;
  //console.log('Lat:'+data.lat+' --- Long:'+data.lng);
  addMarker(info[data.name], map);

});

function addMarker(data, map) {

  var icon = {
      url: "img/point.png", // url
      size: new google.maps.Size(64, 64),
      scaledSize: new google.maps.Size(64, 64)
  };

  var marker = new google.maps.Marker({
    id: data['id'],
    position: data,
    map: map,
    icon: icon
  });
  markers.push(marker);

  var infowindow = new google.maps.InfoWindow({});
  
  marker.addListener('click', function() {
    infowindow.open(map, marker);
        VerDatos(data['id']);
  });
  
}

function DeleteMarker(id) {
  for (var i = 0; i < markers.length; i++) {
    if (markers[i].id == id) {                 
      markers[i].setMap(null);
      markers.splice(i, 1);
      return;
    }
  }
};

function VerDatos(data){
  var refData = database.ref(data);
  refData.on("value", function(snapshot, prevChildKey) {
    var data = snapshot.val();

    var date = data.date;
    var fecha = date.split("-");
    var dia = fecha[0];
    var mes = fecha[1];
    var ano = fecha[2];

    var time = data.time;
    var tiempo = time.split(":");
    var horas = tiempo[0];
    var minutos = tiempo[1];
    var segundos = tiempo[2];

    chart.series[0].addPoint([Date.UTC(ano, mes, dia, horas, minutos, segundos), parseFloat(data.attr3)]);
    
    $('#humidity').html(data.attr2);
    $('#pollution').html(data.attr3);
    $('#temperature').html(data.attr4);
    //$('#containInfo').addClass('openInfo');

    $('#Modal').modal('show');
  });
}


$(document).ready(function(){
  Highcharts.theme = {
      colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', 
               '#FF9655', '#FFF263', '#6AF9C4'],
      chart: {
          backgroundColor: {
              linearGradient: [0, 0, 500, 500],
              stops: [
                  [0, 'rgb(49, 49, 49)'],
                  [1, 'rgb(48, 48, 48)']
              ]
          },
      },
      title: {
          style: {
              color: '#000',
              font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
          }
      },
      subtitle: {
          style: {
              color: '#666666',
              font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
          }
      },
      legend: {
          itemStyle: {
              font: '9pt Trebuchet MS, Verdana, sans-serif',
              color: 'black'
          },
          itemHoverStyle:{
              color: 'gray'
          }   
      },
      lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ]
      },
  };
    
  Highcharts.setOptions(Highcharts.theme);

  // Create the chart
  chart = Highcharts.stockChart('container', { 
      chart: {
          events: {},
          type: 'spline',
          animation: Highcharts.svg
      },
     lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miercoles',
            'Jueves', 'Viernes', 'Sabado'
        ]
      },
      tooltip: {
          backgroundColor: {
              linearGradient: [0, 0, 0, 60],
              stops: [
                  [0, '#FFFFFF'],
                  [1, '#E0E0E0']
              ]
          },
          borderWidth: 1,
          borderColor: '#AAA'
      },
      rangeSelector: {
          buttons: [{
              count: 1,
              type: 'minute',
              text: '1M'
          }, {
              count: 5,
              type: 'minute',
              text: '5M'
          }, {
              type: 'all',
              text: 'All'
          }],
          inputEnabled: false,
          selected: 0
      },

      title: {
          text: 'Monitoreo en tiempo real'
      },

      exporting: {
          enabled: false
      },
      series: [{
          name: 'Medida',
          data: (function () {
          }())
      }]
  });

})





