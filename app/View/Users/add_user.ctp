<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Nuevo Usuario</h3>
    </div>
    <div class="panel-body">
    	<?php echo $this->Form->create('User'); ?>
	    	<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <?php
							echo $this->Form->input('nombres', [
								'label' => 'Nombres',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'text',
							 	'autocomplete' => 'off',
							 	'required' => true
							]);
						?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('apellidos', [
						 		'label' => 'Apellidos',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'text',
						 		'autocomplete' => 'off',
						 		'required' => true
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <?php
							echo $this->Form->input('email', [
								'label' => 'Email',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'mail',
							 	'autocomplete' => 'off',
							 	'required' => true
							]);
						?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('telefono', [
						 		'label' => 'Teléfono',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'number',
						 		'autocomplete' => 'off',
						 		'required' => true
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <?php
							echo $this->Form->input('pass1', [
								'label' => 'Nueva Contraseña',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'password',
							 	'autocomplete' => 'off',
							 	'required' => false
							]);
						?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('pass2', [
						 		'label' => 'Repita Contraseña',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'password',
						 		'autocomplete' => 'off',
						 		'required' => false
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
	        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
	        		<?php echo $this->Form->button('Guardar', ['class' => ['btn', 'btn-primary', 'btn-active-success', 'btn-hover-success']]); ?>
	        	</div>
	        </div>
	    </form>

    </div>
</div>

