<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Proyecto : ');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css([
			'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700',
			'bootstrap.min',
			'style',
			'animate',
			'https://fonts.googleapis.com/css?family=Roboto',
			'magic-check',
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css'
			//'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		]);
		
		echo $this->Html->script([
			'https://code.jquery.com/jquery-1.12.4.js',
			'bootstrap.min',
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js'
			//'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
			//'jquery.mtz.monthpicker'
		]);

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

<?php if(empty($dataUser)){ ?>
	<!-- Registro -->
	<div class="modal fade" id="Registrarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
			<?php echo $this->Form->create('User', ['url' => ['controller' => 'Users', 'action' => 'add']]); ?>
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Registro de usuario</h4>
			      </div>
			      <div class="modal-body" style="padding-bottom: 0;">
				      	<div class="row">
				      		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					      		<div class="form-group">
				                    <?php
										echo $this->Form->input('nombres', [
											'label' => false,
										 	'class' => ['form-control', 'input-sm'],
										 	'placeholder' => 'Nombres',
										 	'type' => 'text',
										 	'autocomplete' => 'off',
										 	'required' => true
										]);
				    				?>
				                </div>
				            </div>
			                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                <div class="form-group">
				                    <?php
										echo $this->Form->input('apellidos', [
									 		'label' => false,
									 		'class' => ['form-control', 'input-sm'],
									 		'placeholder' => 'Apellidos',
									 		'type' => 'text',
									 		'autocomplete' => 'off',
									 		'required' => true
										]);
				    				?>
				                </div>
				            </div>
				        </div>
				        <div class="row">
				      		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					      		<div class="form-group">
				                    <?php
										echo $this->Form->input('email', [
											'label' => false,
										 	'class' => ['form-control', 'input-sm'],
										 	'placeholder' => 'Email',
										 	'type' => 'mail',
										 	'autocomplete' => 'off',
										 	'required' => true
										]);
				    				?>
				                </div>
				            </div>
			                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                <div class="form-group">
				                    <?php
										echo $this->Form->input('telefono', [
									 		'label' => false,
									 		'class' => ['form-control', 'input-sm'],
									 		'placeholder' => 'Teléfono',
									 		'type' => 'number',
									 		'autocomplete' => 'off',
									 		'required' => true
										]);
				    				?>
				                </div>
				            </div>

				       
				        </div>	
			      </div>
			      <div class="modal-footer">
			        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
			        <button type="submit" class="btn btn-warning">Registrarme</button>
			      </div>
			</form>
	    </div>
	  </div>
	</div>
<?php } ?>

<?php if(!empty($dataUser)){ ?>
	<!-- Edicion Registro -->
	<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalEdit">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
			<?php echo $this->Form->create('User', ['url' => ['controller' => 'Users', 'action' => 'edit']]); ?>
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Edición de usuario</h4>
			      </div>
			      <div class="modal-body" style="padding-bottom: 0;">
				      	
				      	<div class="row">
				      		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					      		<div class="form-group">
				                    <?php
										echo $this->Form->input('nombres', [
											'label' => false,
										 	'class' => ['form-control', 'input-sm'],
										 	'placeholder' => 'Nombres',
										 	'type' => 'text',
										 	'autocomplete' => 'off',
										 	'required' => true
										]);
				    				?>
				                </div>
				            </div>
			                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                <div class="form-group">
				                    <?php
										echo $this->Form->input('apellidos', [
									 		'label' => false,
									 		'class' => ['form-control', 'input-sm'],
									 		'placeholder' => 'Apellidos',
									 		'type' => 'text',
									 		'autocomplete' => 'off',
									 		'required' => true
										]);
				    				?>
				                </div>
				            </div>
				        </div>

				        <div class="row">
				      		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					      		<div class="form-group">
				                    <?php
										echo $this->Form->input('email', [
											'label' => false,
										 	'class' => ['form-control', 'input-sm'],
										 	'placeholder' => 'Email',
										 	'type' => 'mail',
										 	'autocomplete' => 'off',
										 	'required' => true
										]);
				    				?>
				                </div>
				            </div>
			                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                <div class="form-group">
				                    <?php
										echo $this->Form->input('telefono', [
									 		'label' => false,
									 		'class' => ['form-control', 'input-sm'],
									 		'placeholder' => 'Teléfono',
									 		'type' => 'number',
									 		'autocomplete' => 'off',
									 		'required' => true
										]);
				    				?>
				                </div>
				            </div>
				        </div>

				        <div class="row">
				      		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					      		<div class="form-group">
				                    <?php
										echo $this->Form->input('password', [
											'label' => false,
										 	'class' => ['form-control', 'input-sm'],
										 	'placeholder' => 'Contraseña',
										 	'type' => 'password',
										 	'autocomplete' => 'off',
										 	'required' => false
										]);
				    				?>
				                </div>
				            </div>
			                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				                <div class="form-group">
				                    <?php
										echo $this->Form->input('password2', [
									 		'label' => false,
									 		'class' => ['form-control', 'input-sm'],
									 		'placeholder' => 'Repita contraseña',
									 		'type' => 'password',
									 		'autocomplete' => 'off',
									 		'required' => false
										]);
				    				?>
				                </div>
				            </div>
				        </div>	
			      </div>
			      <div class="modal-footer">
			        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
			        <button type="submit" class="btn btn-warning">Registrarme</button>
			      </div>
			</form>
	    </div>
	  </div>
	</div>
<?php } ?>

<?php if(empty($dataUser)){ ?>
	<!-- Inicio sesion -->
	<div class="modal fade" id="IniciarSesion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
		    <?php echo $this->Form->create('User', ['url' => ['controller' => 'Users', 'action' => 'login']]); ?>
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Iniciar Sesion</h4>
			      </div>
			      <div class="modal-body">
		      			<div class="form-group">
		                    <?php
								echo $this->Form->input('username', [
									'label' => false,
								 	'class' => ['form-control', 'input-sm'],
								 	'placeholder' => 'Usuario',
								 	'type' => 'text',
								 	'autocomplete' => 'off',
								 	'required' => true
								]);
		    				?>
		                </div>
		                <div class="form-group">
		                    <?php
								echo $this->Form->input('password', [
							 		'label' => false,
							 		'class' => ['form-control', 'input-sm'],
							 		'placeholder' => 'Ingresa tu contraseña',
							 		'type' => 'password',
							 		'autocomplete' => 'off',
							 		'required' => true
								]);
		    				?>
		                </div>	
			      </div>
			      <div class="modal-footer">
			        <!--<button class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
			        <button type="submit" class="btn btn-warning">Ingresar</button>
			      </div>
		    </form>
	    </div>
	  </div>
	</div>
<?php } ?>

	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo Router::url('/', true); ?>">Inicio</a></li>
            <li><a href="<?php echo Router::url('/Sobre-Nosotros', true); ?>">Sobre Nosotros</a></li>
            <li><a href="<?php echo Router::url('/Contacto', true); ?>">Contacto</a></li> 
          </ul>
          <ul class="nav navbar-nav navbar-right">
	            
	            <?php if(empty($dataUser)){ ?>
          			<li><a data-toggle="modal" data-target="#IniciarSesion" href="#about">Iniciar Sesión</a></li>
            		<li><a data-toggle="modal" data-target="#Registrarme" href="#contact">Registrarme</a></li>
            	<?php }else{ ?>
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		              	<?php echo $GlobalDataUser['User']['nombres'].' '.$GlobalDataUser['User']['apellidos'];?> <span class="caret"></span>
		              </a>
		              <ul class="dropdown-menu">
		                <!--<li><a href="#" data-toggle="modal" data-target="#myModalEdit">Mi perfil</a></li>-->
		                <li><a href="<?php echo Router::url('/Dashboard', true); ?>">Administración</a></li>
		                <li><a href="<?php echo Router::url('/Users/logout', true); ?>">Cerrar Sesión</a></li>
		              </ul>
		            </li>
	            <?php } ?>

          </ul>
        </div>
      </div>
    </nav>
    <?php echo $this->Flash->render(); ?>
	<?php echo $this->fetch('content'); ?>
</body>
<script>
	$(document).ready(function(){

		$('.magic-checkbox').bind('click', function(){
			var status = $(this).is(':checked');
			var status_id = $(this).attr('id');
			if(status){
				$('#range_start_'+status_id).attr('readonly', false);
				$('#range_finished_'+status_id).attr('readonly', false);
			}else{
				$('#range_start_'+status_id).val('');
				$('#range_finished_'+status_id).val('');

				$('#range_start_'+status_id).attr('readonly', true);
				$('#range_finished_'+status_id).attr('readonly', true);
			}
		});

	})
</script>
</html>
