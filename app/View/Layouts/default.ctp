<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Proyecto : ');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css([
	        'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700',
	        'bootstrap.min',
	        'nifty.min',
	        'demo/nifty-demo-icons.min',
            'plugins/animate-css/animate.min',
	        'plugins/pace/pace.min',
	        'demo/nifty-demo.min',
	        'plugins/datatables/media/css/dataTables.bootstrap',
            'plugins/datatables/extensions/Responsive/css/responsive.dataTables.min',
	        'premium/icon-sets/icons/line-icons/premium-line-icons.min',
            'premium/icon-sets/icons/solid-icons/premium-solid-icons',
	        'plugins/bootstrap-select/bootstrap-select.min',
            'plugins/select2/css/select2.min',
            'themes/type-c/theme-dark.min',
            'plugins/morris-js/morris.min',
            'plugins/noUiSlider/nouislider.min',
            'plugins/bootstrap-timepicker/bootstrap-timepicker.min',
            'plugins/unitegallery/css/unitegallery.min',
            'main'
	    ]);

		echo $this->Html->script([
	        'jquery.min',
	        'bootstrap.min',
	        'plugins/pace/pace.min',
	        'demo/nifty-demo.min',
	        'nifty.min',
	        'icons',
            'alerts',
	        'plugins/bootstrap-select/bootstrap-select.min',
            'plugins/select2/js/select2',
            'plugins/datatables/media/js/jquery.dataTables',
            'plugins/datatables/media/js/dataTables.bootstrap',
            'plugins/datatables/extensions/Responsive/js/dataTables.responsive.min',
            'plugins/bootstrap-wizard/jquery.bootstrap.wizard.min',
            'plugins/bootstrap-validator/bootstrapValidator.min',
            'plugins/bootbox/bootbox.min',
            'plugins/noUiSlider/nouislider.min',
            'plugins/bootstrap-timepicker/bootstrap-timepicker.min',
            'http://maps.google.com/maps/api/js?sensor=true',
            'plugins/gmaps/gmaps',
            'plugins/unitegallery/js/unitegallery.min',
            'plugins/unitegallery/themes/tilesgrid/ug-theme-tilesgrid',
            'jquery-ui.min'
	    ]);

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
        <!-- Header -->
        <header id="navbar">
            <div id="navbar-container" class="boxed">

                <div class="navbar-header">
                    <a href="<?php echo Router::url('/Dashboard', true); ?>" class="navbar-brand">
                        <?php echo $this->Html->image('logo2.png', ['class' => ['brand-icon'], 'alt' => 'CakePHP']);?>
                        <div class="brand-title">
                            <span class="brand-text">Proyecto</span>
                        </div>
                    </a>
                </div>
                <div class="navbar-content">
                    <ul class="nav navbar-top-links">
                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#">
                                <i class="demo-pli-list-view"></i>
                            </a>
                        </li>
                        <li>
                            <div class="custom-search-form">
                                <!--
                                <label class="btn btn-trans" for="search-input" data-toggle="collapse" data-target="#nav-searchbox">
                                    <i class="demo-pli-magnifi-glass"></i>
                                </label>
                                <form>
                                    <div class="search-container collapse" id="nav-searchbox">
                                        <input id="search-input" type="text" class="form-control" placeholder="Type for search...">
                                    </div>
                                </form>
                                -->
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links">
                        
                        <!--
                        <li id="dropdown-user" class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="ic-user pull-right">
                                    <i class="demo-pli-male"></i>
                                </span>
                            </a>


                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right panel-default">
                                <ul class="head-list">
                                    <li>
                                        <a href="<?php echo Router::url('/Users/logout', true); ?>"><i class="demo-pli-unlock icon-lg icon-fw"></i> Cerrar Sesión</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        -->
                    </ul>
                </div>
            </div>
        </header>

        <div class="boxed">
            <div id="content-container">
                <div id="page-head">
                    
                    <!--
                    <div class="pad-all text-center">
                        <h3>Welcome back to the Dashboard.</h3>
                        <p1>Scroll down to see quick links and overviews of your Server, To do list, Order status or get some Help using Nifty.<p></p>
                        </p1>
                    </div>
                    -->

                    <div id="page-title">
                        <?php if(!empty($Title)){ ?>
                        <h1 class="page-header text-overflow"><?php echo $Title; ?></h1>
                        <?php } ?>
                    </div>
                    <ol class="breadcrumb">
                        
                        <li><a href="<?php echo Router::url('/Dashboard', true); ?>"><i class="demo-pli-home"></i></a></li>
                        
                        <?php if(!empty($Title)){ ?>
                        <li><a href="<?php echo $Url;?>"><?php echo $Title;?></a></li>
                        <?php } ?>

                        <?php if(!empty($Subtitle)){ ?>
                            <li class="active"><?php echo $Subtitle;?></li>
                        <?php } ?>
                    </ol>
                </div>
                <div id="page-content">
                    <?= $this->Flash->render() ?>
                    <!--<hr class="new-section-sm bord-no">-->
                    <?= $this->fetch('content') ?>
                </div>
            </div>
            <aside id="aside-container">
                <div id="aside">
                    <div class="nano">
                        <div class="nano-content">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#demo-asd-tab-1" data-toggle="tab">
                                        <i class="demo-pli-speech-bubble-7 icon-lg"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#demo-asd-tab-2" data-toggle="tab">
                                        <i class="demo-pli-information icon-lg icon-fw"></i> Report
                                    </a>
                                </li>
                                <li>
                                    <a href="#demo-asd-tab-3" data-toggle="tab">
                                        <i class="demo-pli-wrench icon-lg icon-fw"></i> Settings
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="demo-asd-tab-1">
                                    <p class="pad-all text-main text-sm text-uppercase text-bold">
                                        <span class="pull-right badge badge-warning">3</span> Family
                                    </p>
                                    <div class="list-group bg-trans">
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/2.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                                <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Stephen Tran</p>
                                                <small class="text-muteds">Availabe</small>
                                            </div>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/7.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Brittany Meyer</p>
                                                <small class="text-muteds">I think so</small>
                                            </div>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/1.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                                <i class="badge badge-info badge-stat badge-icon pull-left"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Jack George</p>
                                                <small class="text-muteds">Last Seen 2 hours ago</small>
                                            </div>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/4.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Donald Brown</p>
                                                <small class="text-muteds">Lorem ipsum dolor sit amet.</small>
                                            </div>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/8.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                                <i class="badge badge-warning badge-stat badge-icon pull-left"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Betty Murphy</p>
                                                <small class="text-muteds">Idle</small>
                                            </div>
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <div class="media-left pos-rel">
                                                <?php echo $this->Html->image('profile-photos/9.png', ['class' => ['img-circle img-xs'], 'alt' => '']);?>
                                                <i class="badge badge-danger badge-stat badge-icon pull-left"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="mar-no text-main">Samantha Reid</p>
                                                <small class="text-muteds">Offline</small>
                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                    <p class="pad-all text-main text-sm text-uppercase text-bold">
                                        <span class="pull-right badge badge-success">Offline</span> Friends
                                    </p>
                                    <div class="list-group bg-trans">
                                        <a href="#" class="list-group-item">
                                            <span class="badge badge-purple badge-icon badge-fw pull-left"></span> Joey K. Greyson
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <span class="badge badge-info badge-icon badge-fw pull-left"></span> Andrea Branden
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <span class="badge badge-success badge-icon badge-fw pull-left"></span> Johny Juan
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <span class="badge badge-danger badge-icon badge-fw pull-left"></span> Susan Sun
                                        </a>
                                    </div>
                                    <hr>
                                    <p class="pad-all text-main text-sm text-uppercase text-bold">News</p>
                                    <div class="pad-hor">
                                        <p>Lorem ipsum dolor sit amet, consectetuer
                                            <a data-title="45%" class="add-tooltip text-semibold text-main" href="#">adipiscing elit</a>, sed diam nonummy nibh. Lorem ipsum dolor sit amet.
                                        </p>
                                        <small><em>Last Update : Des 12, 2014</em></small>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="demo-asd-tab-2">
                                    <div class="pad-all">
                                        <p class="pad-ver text-main text-sm text-uppercase text-bold">Billing &amp; reports</p>
                                        <p>Get <strong class="text-main">$5.00</strong> off your next bill by making sure your full payment reaches us before August 5, 2018.</p>
                                    </div>
                                    <hr class="new-section-xs">
                                    <div class="pad-all">
                                        <span class="pad-ver text-main text-sm text-uppercase text-bold">Amount Due On</span>
                                        <p class="text-sm">August 17, 2018</p>
                                        <p class="text-2x text-thin text-main">$83.09</p>
                                        <button class="btn btn-block btn-success mar-top">Pay Now</button>
                                    </div>
                                    <hr>
                                    <p class="pad-all text-main text-sm text-uppercase text-bold">Additional Actions</p>
                                    <div class="list-group bg-trans">
                                        <a href="#" class="list-group-item"><i class="demo-pli-information icon-lg icon-fw"></i> Service Information</a>
                                        <a href="#" class="list-group-item"><i class="demo-pli-mine icon-lg icon-fw"></i> Usage Profile</a>
                                        <a href="#" class="list-group-item"><span class="label label-info pull-right">New</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Payment Options</a>
                                        <a href="#" class="list-group-item"><i class="demo-pli-support icon-lg icon-fw"></i> Message Center</a>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <div><i class="demo-pli-old-telephone icon-3x"></i></div>
                                        Questions?
                                        <p class="text-lg text-semibold text-main"> (415) 234-53454 </p>
                                        <small><em>We are here 24/7</em></small>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="demo-asd-tab-3">
                                    <ul class="list-group bg-trans">
                                        <li class="pad-top list-header">
                                            <p class="text-main text-sm text-uppercase text-bold mar-no">Account Settings</p>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-1" type="checkbox" checked>
                                                <label for="demo-switch-1"></label>
                                            </div>
                                            <p class="mar-no text-main">Show my personal status</p>
                                            <small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-2" type="checkbox" checked>
                                                <label for="demo-switch-2"></label>
                                            </div>
                                            <p class="mar-no text-main">Show offline contact</p>
                                            <small class="text-muted">Aenean commodo ligula eget dolor. Aenean massa.</small>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-3" type="checkbox">
                                                <label for="demo-switch-3"></label>
                                            </div>
                                            <p class="mar-no text-main">Invisible mode </p>
                                            <small class="text-muted">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </small>
                                        </li>
                                    </ul>
                                    <hr>
                                    <ul class="list-group pad-btm bg-trans">
                                        <li class="list-header"><p class="text-main text-sm text-uppercase text-bold mar-no">Public Settings</p></li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-4" type="checkbox" checked>
                                                <label for="demo-switch-4"></label>
                                            </div>
                                            Online status
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-5" type="checkbox" checked>
                                                <label for="demo-switch-5"></label>
                                            </div>
                                            Show offline contact
                                        </li>
                                        <li class="list-group-item">
                                            <div class="pull-right">
                                                <input class="toggle-switch" id="demo-switch-6" type="checkbox" checked>
                                                <label for="demo-switch-6"></label>
                                            </div>
                                            Show my device icon
                                        </li>
                                    </ul>
                                    <hr>
                                    <p class="pad-hor text-main text-sm text-uppercase text-bold mar-no">Task Progress</p>
                                    <div class="pad-all">
                                        <p class="text-main">Upgrade Progress</p>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>
                                        </div>
                                        <small>15% Completed</small>
                                    </div>
                                    <div class="pad-hor">
                                        <p class="text-main">Database</p>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>
                                        </div>
                                        <small>17/23 Database</small>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <nav id="mainnav-container">
                <div id="mainnav">

                    <div class="mainnav-brand">
                        <a href="index.html" class="brand">
                            <?php echo $this->Html->image('logo.png', ['class' => ['brand-icon'], 'alt' => '']);?>
                            <span class="brand-text">Nifty</span>
                        </a>
                        <a href="#" class="mainnav-toggle"><i class="pci-cross pci-circle icon-lg"></i></a>
                    </div>
                   
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap text-center">
                                        <div class="pad-btm">
                                            <?php echo $this->Html->image('profile-photos/1.png', ['class' => ['img-circle img-md'], 'alt' => '']);?>
                                        </div>
                                        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                            <p class="mnp-name"><?php echo $GlobalDataUser['User']['nombres'].' '.$GlobalDataUser['User']['apellidos'];?></p>
                                            <span class="mnp-desc"><?php echo $GlobalDataUser['User']['email'];?></span>
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                        <a href="<?php echo Router::url('/Mi-Perfil/'.$GlobalDataUser['User']['id'], true); ?>" class="list-group-item">
                                            <i class="demo-pli-male icon-lg icon-fw"></i> Mi Perfil
                                        </a>
                                        <a href="#" class="list-group-item">
                                            <i class="demo-pli-information icon-lg icon-fw"></i> Ayuda
                                        </a>
                                        <a href="<?php echo Router::url('/Users/logout', true); ?>" class="list-group-item">
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Cerrar Sesión
                                        </a>
                                    </div>
                                </div>
                                <div id="mainnav-shortcut" class="hidden">
                                    <ul class="list-unstyled shortcut-wrap">
                                        <li class="col-xs-3" data-content="My Profile">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                                <i class="demo-pli-male"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Messages">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                                <i class="demo-pli-speech-bubble-3"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Activity">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                                <i class="demo-pli-thunder"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Lock Screen">
                                            <a class="shortcut-grid" href="#">
                                                <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                                <i class="demo-pli-lock-2"></i>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <ul id="mainnav-menu" class="list-group">
                                
                                
                                    
                                    <li>
                                        <a href="<?php echo Router::url('/', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Mapa</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Home</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Mis-Nodos', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Mis Nodos</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Nodos-Monitoreadas', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Nodos Monitoreadas</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Galeria/Imagenes', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Imagenes</span>
                                        </a>
                                    </li>

                                    <li class="list-header">Administración</li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Nodos', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Nodos</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Regiones', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Regiones</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Ciudades', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Ciudades</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Comunas', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Comunas</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo Router::url('/Dashboard/Usuarios', true); ?>">
                                            <i class="pli-male-female"></i>
                                            <span class="menu-title">Usuarios</span>
                                        </a>
                                    </li>

                                </ul>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <footer id="footer">
            <div class="show-fixed pad-rgt pull-right">
                You have <a href="#" class="text-main"><span class="badge badge-danger">3</span> pending action.</a>
            </div>
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>
            <p class="pad-lft">&#0169; 2018 Your Company</p>
        </footer>
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>

    </div>
</body>
</html>
