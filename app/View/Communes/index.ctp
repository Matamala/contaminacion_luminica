<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Comunas</h3>
    </div>
    <div class="panel-body">
    	
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-btm text-right">
            <a href="<?php echo Router::url( '/Dashboard/Comunas/Nuevo',false);?>" type="button" class="btn btn-primary">
                <i class="pli-add-user" aria-hidden="true"></i> Nuevo
            </a>
        </div>
    </div>

        <table id="comunas" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Comuna</th>
                    <th>Ciudad</th>
                   	<th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script>
    var dataTable="";
    $(document).ready(function(){
        dataTable = $('#comunas').DataTable({   
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "sAjaxSource": "<?php echo Router::url('/Communes/getCommunes', true);?>",
            "aoColumns": [
                { mData: 'Comuna', "bSearchable": true, "className":"text-left" },
                { mData: 'Ciudad', "bSearchable": true, "className":"text-left" },
                { mData: 'Button', "bSearchable": false, "className":"text-center" }
            ], 
            "rowCallback": function( row, data ) {
               
            },
            "fnDrawCallback": function (oSettings) {
                $('.deleteItem').on('click', function(){
                    var id = $(this).attr('data-item');
                    bootbox.confirm({
                        message: "Se eliminará la comuna seleccionada, ¿Desea Continuar?",
                        buttons: {
                            confirm: {
                                label: 'Aceptar',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $.ajax({
                                    url     : "<?php echo Router::url('/Communes/DeleteItem', true);?>",
                                    type    : "POST",
                                    data    : { id : id }, 
                                    success : function(result){
                                        var obj = JSON.parse(result);
                                        alertMessage(obj.message, obj.clase);
                                        if(obj.state==1){dataTable.ajax.reload();}
                                    }
                                });
                            }else{
                                alertMessage('Operación cancelada.', 'danger');
                            }
                        }
                    });  
                });
            },
            "autoWidth": true,
            "language" : {"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"}
        });
    });
</script>