<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Nuevo Nodo</h3>
    </div>
    <div class="panel-body">
        <?php echo $this->Form->create('UserRange'); ?>
	        	
	        <div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <label for="regions_id">Region</label>
	                    <?php
							echo $this->Form->select('regions_id', $regions, [
								'label' => false,
								'id' => 'regions_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione región',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <label for="citys_id">Ciudad</label>
	                    <?php
							echo $this->Form->select('citys_id', $citys, [
								'label' => false,
								'id' => 'citys_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione ciudad',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <label for="communes_id">Comuna</label>
	                    <?php
							echo $this->Form->select('communes_id', $communes, [
								'label' => false,
							 	'id' => 'communes_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione comuna',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-left">
			        <div class="form-group">	
			        	<label>&nbsp;</label>
			        	<button id="searchNodes" class="btn btn-primary btn-active-primary btn-hover-primary" style="margin-top:22px;" type="button">Buscar Nodos disponibles</button>
			        </div>
			    </div>
			</div>
			<div id="items" class="row">
				
			</div>
	        <div class="row">
	        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
	        		<?php echo $this->Form->button('Guardar', ['class' => ['btn', 'btn-primary', 'btn-active-success', 'btn-hover-success']]); ?>
	        	</div>
	        </div>
        </form>

    </div>
</div>
<script>
	function alertMessage(mnsg, clase){
        $.niftyNoty({
            type: clase,
            container : 'floating',
            title : 'Atención',
            message : mnsg,
            closeBtn : false,
            timer : 3000
        });
    }

	$(document).ready(function(){

		$('#regions_id').select2();
		$('#citys_id').select2();
		$('#communes_id').select2();	
	
		$('#searchNodes').bind('click', function(){
			var region = $('#regions_id').val();
			var city = $('#citys_id').val();
			var commune = $('#communes_id').val();

			if(region==0 || region.length==0){
				$('#regions_id').select2('open');
				alertMessage('debe seleccionar la región.', 'danger');
				return false;
			}

			if(city==0 || city.length==0){
				$('#citys_id').select2('open');
				alertMessage('debe seleccionar la ciudad.', 'danger');
				return false;
			}

			if(commune==0 || commune.length==0){
				$('#communes_id').select2('open');
				alertMessage('debe seleccionar la comuna.', 'danger');
				return false;
			}

			$.ajax({
                url     : "<?php echo Router::url('/UserRanges/getNodes', true);?>",
                type    : "POST",
                data    : { 
                	regions_id : region,
                	citys_id : city,
                	communes_id : commune 
                }, 
                success : function(result){
                	$('#items').html('');

                	var obj = JSON.parse(result);

                	$.each(obj, function( index, value ) {

  						$('#items').append(value.div);
  						var markerMap = new GMaps({
                            el: value.div_id,
                            lat: value.latitud,
                            lng: value.longitud
                        });
                        markerMap.addMarker({
                            lat: value.latitud,
                            lng: value.longitud,
                            title: value.zone,
                            click: function(e) {
                                $.niftyNoty({
                                    type: "info",
                                    icon: "fa fa-info",
                                    message: "You clicked in the marker",
                                    container: 'floating',
                                    timer: 3500
                                });
                            },
                            infoWindow: {
                                 content: '<div>Texto</div>'
                            }
                        });

                        var rs_def = document.getElementById(value.barra);
                        var rs_def_value = document.getElementById(value.barraVal);
                        //var rs_def_hidden = document.getElementById(value.barraHidden);
                        noUiSlider.create(rs_def,{
                            start   : [ 0 ],
                            connect : 'lower',
                            range   : {
                                'min': [  0 ],
                                'max': [ 100 ]
                            }
                        });

                        rs_def.noUiSlider.on('update', function( values, handle ) {
                            rs_def_value.innerHTML = values[handle];
                            $('#'+value.barraHidden).val(values[handle]);
                        });

                        $('#time-start-'+value.id).timepicker({
                            minuteStep: 1,
                            showInputs: false,
                            disableFocus: true,
                            showSeconds: false,
                            showMeridian: false,
                        });
                        $('#time-finished-'+value.id).timepicker({
                            minuteStep: 1,
                            showInputs: false,
                            disableFocus: true,
                            showSeconds: false,
                            showMeridian: false,
                        });

					});
                }
            });

		});

		$('#regions_id').on("select2:select", function (e) { 
			var regions_id = $(this).val();
			if(regions_id.length>0 || regions_id>0){
				$.ajax({
	                url     : "<?php echo Router::url('/Dashboards/getCitys', true);?>",
	                type    : "POST",
	                data    : { regions_id : regions_id }, 
	                success : function(result){
	                	//console.log(result);
	                	var obj = JSON.parse(result);
	                	//console.log(obj.response);
	                	if(obj.status==1){
	                		$('#citys_id').html(obj.response);
	                		$('#citys_id').trigger('change');
	                	}else{
	                		$('#citys_id').html('<option value="">Seleccione ciudad</option>');
				            $('#citys_id').trigger('change');
				            $('#communes_id').html('<option value="">Seleccione comuna</option>');
				            $('#communes_id').trigger('change');
	                	}
	                }
	            })
			}else{
				$('#citys_id').html('<option value="">Seleccione ciudad</option>');
	            $('#citys_id').trigger('change');
	            $('#communes_id').html('<option value="">Seleccione comuna</option>');
	            $('#communes_id').trigger('change');
			} 
		});

		$('#citys_id').on("select2:select", function (e) { 
			var citys_id = $(this).val();
			if(citys_id.length>0 || citys_id>0){
				$.ajax({
	                url     : "<?php echo Router::url('/Dashboards/getCommunes', true);?>",
	                type    : "POST",
	                data    : { citys_id : citys_id }, 
	                success : function(result){
	                	//console.log(result);
	                	var obj = JSON.parse(result);
	                	if(obj.status==1){
	                		$('#communes_id').html(obj.response);
	                		$('#communes_id').trigger('change');
	                	}else{
	                		$('#communes_id').html('<option value="">Seleccione comuna</option>');
	            			$('#communes_id').trigger('change');
	                	}
	                }
	            })
			}else{
				$('#communes_id').html('<option value="">Seleccione comuna</option>');
	            $('#communes_id').trigger('change');
			} 
		});

	});

</script>
<?php
    echo $this->Html->script(['plugins/gmaps/gmaps']);
    echo $this->fetch('script');
?>