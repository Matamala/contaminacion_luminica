
<?php
	echo $this->Html->css([
			'plugins/morris-js/morris.min',
	]);
	echo $this->fetch('css');	

	echo $this->Html->script([
		'https://maps.googleapis.com/maps/api/js?key=AIzaSyD6hH3QuPG2iX8GAxiwY4o8tuCIEtXignI',
		'https://code.highcharts.com/stock/highstock.js',
		'https://code.highcharts.com/stock/modules/exporting.js',
		'https://code.highcharts.com/stock/modules/export-data.js',
		'https://www.gstatic.com/firebasejs/5.4.0/firebase.js'
		//'maps',
		//'date'
	]);
	echo $this->fetch('script');
?>
	<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 id="nodo" class="modal-title"></h4>
			    </div>
			    <div class="modal-body">
					
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-btm">
					    	<div class="form-group">
							  <label for="sel1">Seleccione filtro</label>
							  <select class="form-control" id="sel1">
							    <option selected="selected" value="ahora">Ahora</option>
							    <option value="dia">Día</option>
							    <option value="semana">Semana</option>
							    <option value="mes">Mes</option>
							    <option value="ano">Año</option>
							  </select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-btm">
							<div class="form-group">
								<label id="nmSel" for="sel1">Ahora</label>
								<?php
									echo $this->Form->input('date', [
										'label' => false,
									 	'class' => ['form-control'],
									 	'id' => 'date',
									 	'placeholder' => '',
									 	'type' => '',
									 	'autocomplete' => 'off',
									 	'required' => true,
									 	'escape' => true,
									 	'disabled' => true
									]);
			    				?>
			    				
		    				</div>
		    			</div>	
		    			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-btm">
		    				<button type="button" style="margin-top:25px;" class="btn btn-success" id="aplicaFiltro">Actualizar</button>
		    				<?php 
		    					echo $this->Html->image('ajax-loader.gif', [
		    						'class' => ['cargando'], 
		    						'alt' => 'Cargando',
		    						'id' => 'cargando'
		    					]);
		    				?>
		    			</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-btm">
	                        <div id="legend-cont" class="text-center"></div>
	                        <div id="cont" style="height:350px"></div>
	                    </div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-btm">
	                        <div id="legend-temp" class="text-center"></div>
	                        <div id="temp" style="height:350px"></div>
	                    </div>
	                </div>
	                <div class="row">
	                	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-btm">
	                        <div id="legend-presion" class="text-center"></div>
	                        <div id="presion" style="height:350px"></div>
	                    </div>
	                </div>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			    </div>
			</div>
	    </div>
	</div>
	<div id="map"></div>

	<?php 
	$zones = "[";
	$i = 0;
	foreach ($Zonas as $zona) {
		
		if($i==0){
			$zones.= '["'.$zona['Zone']['zone'].'",'.$zona['Zone']['latitud'].','.$zona['Zone']['longitud'].','.$zona['Zone']['id'].', "'.$zona['Zone']['firebase_name'].'"]';
		}else{
			$zones.= ',["'.$zona['Zone']['zone'].'",'.$zona['Zone']['latitud'].','.$zona['Zone']['longitud'].','.$zona['Zone']['id'].', "'.$zona['Zone']['firebase_name'].'"]';
		}
		$i++;
	}
	$zones.= "]";
	?>
	
	<script>
	
	var id_firebase = 0;
	var id_nodo = 0;
	var refData;
	var chartLum;
	var chartTemp;
	var chartPresion;
	var database; 
	var config = {
	    apiKey: "AIzaSyCBRyNRTRbSq8zkfHpo-gfHO3aB7OiZ4as",
	    authDomain: "sqmunab.firebaseapp.com",
	    databaseURL: "https://sqmunab.firebaseio.com",
	    projectId: "sqmunab",
	    storageBucket: "sqmunab.appspot.com",
	    messagingSenderId: "826870606716"
	};
	firebase.initializeApp(config);

function verMediciones(id, id_fb){
	$('#cargando').show();
	id_firebase = id_fb;
	id_nodo = id;
	
	var tipo = $('#sel1').val();
	var date = $('#date').val();
	
	database = firebase.database().ref('Nodos');


	$.ajax({
        url     : "<?php echo Router::url('/Monitoreo/Mediciones', true);?>",
        type    : "POST",
        data    : { itemId : id, idFb: id_fb, tipo: tipo, date: date }, 
        success : function(result){

        	//console.log(result);

            var obj = JSON.parse(result);
            $('#nodo').html(obj.zonaname);   

            //console.log(obj.categorias);

            chartLum = Highcharts.chart('cont', {
			    chart: {
			        type: 'areaspline'
			    },
			    title: {
			        text: 'Luminosidad'
			    },
			    
			    legend: {
			        layout: 'vertical',
			        align: 'left',
			        verticalAlign: 'top',
			        x: 150,
			        y: 100,
			        floating: true,
			        borderWidth: 1,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			    },
			    xAxis: {
			        categories: obj.categorias,
			        plotBands: [{ // visualize the weekend
			            from: 4.5,
			            to: 6.5,
			            color: 'rgba(68, 170, 213, .2)'
			        }]
			    },
			    yAxis: {
			        title: {
			            text: 'Hz'
			        }
			    },
			    tooltip: {
			        shared: true,
			        valueSuffix: ''
			    },
			    credits: {
			        enabled: false
			    },
			    plotOptions: {
			        areaspline: {
			            fillOpacity: 0.5
			        },
			        series: {
			            fillColor: {
			                linearGradient: [0, 0, 0, 300],
			                stops: [
			                    [0, '#9bd184'],
			                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			                ]
			            },
			            turboThreshold: 100
			        }
			    },
			    series: [{
			        name: 'Luminosidad',
			        data: (function () {
  					}())
			    }]
			});

			chartTemp = Highcharts.chart('temp', {
			    chart: {
			        type: 'areaspline'
			    },
			    title: {
			        text: 'Temperatura del Nodo'
			    },
			    legend: {
			        layout: 'vertical',
			        align: 'left',
			        verticalAlign: 'top',
			        x: 150,
			        y: 100,
			        floating: true,
			        borderWidth: 1,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			    },
			    xAxis: {
			        categories: obj.categorias,
			        plotBands: [{ // visualize the weekend
			            from: 4.5,
			            to: 6.5,
			            color: 'rgba(68, 170, 213, .2)'
			        }]
			    },
			    yAxis: {
			        title: {
			            text: 'Grados'
			        }
			    },
			    tooltip: {
			        shared: true,
			        valueSuffix: ''
			    },
			    credits: {
			        enabled: false
			    },
			    plotOptions: {
			        areaspline: {
			            fillOpacity: 0.5
			        },
			        series: {
			            fillColor: {
			                linearGradient: [0, 0, 0, 300],
			                stops: [
			                    [0, '#ffb300'],
			                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			                ]
			            },
			            turboThreshold: 100
			        }
			    },
			    series: [{
			        name: 'Temperatura',
			        data: (function () {
  					}())
			    }]
			});

			chartPresion = Highcharts.chart('presion', {
			    chart: {
			        type: 'areaspline'
			    },
			    title: {
			        text: 'Presión Atmosférica'
			    },
			    legend: {
			        layout: 'vertical',
			        align: 'left',
			        verticalAlign: 'top',
			        x: 150,
			        y: 100,
			        floating: true,
			        borderWidth: 1,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			    },
			    xAxis: {
			        categories: obj.categorias,
			        plotBands: [{ // visualize the weekend
			            from: 4.5,
			            to: 6.5,
			            color: 'rgba(10, 177, 252, .2)'
			        }]
			    },
			    yAxis: {
			        title: {
			            text: 'Presión'
			        }
			    },
			    tooltip: {
			        shared: true,
			        valueSuffix: ''
			    },
			    credits: {
			        enabled: false
			    },
			    plotOptions: {
			        areaspline: {
			            fillOpacity: 0.5
			        },
			        series: {
			            fillColor: {
			                linearGradient: [0, 0, 0, 300],
			                stops: [
			                    [0, '#0ab1fc'],
			                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			                ]
			            },
			            turboThreshold: 100
			        }			        
			    },
			    series: [{
			        name: 'Presión Atmosférica',
			        data: (function () {
  					}())
			    }]
			});
			
			$.each(obj.contaminacion, function( index, value ) {
					chartLum.series[0].addPoint([parseFloat(value.contaminacion)]);
			});
			$.each(obj.temperatura, function( index, value ) {
					chartTemp.series[0].addPoint([parseFloat(value.temperatura)]);
			});
			$.each(obj.presion, function( index, value ) {
					chartPresion.series[0].addPoint([parseFloat(value.presion)]);
			});

			if(tipo=='ahora'){
				refData = database.child(id_fb);
				refData.on("value", function(snapshot, prevChildKey) {
					var tipoDate = $('#sel1').val();
					if(tipoDate=='ahora'){
					    var data = snapshot.val();
					    var date = data.FECHA;
					    date = date.replace("/", "-");
					    date = date.replace("/", "-");
					    date = date.split(" ");
					    var fecha = date[0].split("-");
					    var dia = fecha[2];
					    var mes = fecha[1];
					    var ano = fecha[0];
					    
					    var time = date[1];
					    var tiempo = time.split(":");
					    var horas = tiempo[0];
					    var minutos = tiempo[1];
					    var segundosTemp = tiempo[2].split(".");
					    var segundos = segundosTemp[0];
					    var dt = dia+'-'+mes+'-'+ano+' '+horas+':'+minutos+':'+segundos;
					    
					    var categories = chartLum.xAxis[0].categories;
			            categories.push(dt);
			            chartLum.xAxis[0].setCategories(categories);
			            chartTemp.xAxis[0].setCategories(categories);
			            chartPresion.xAxis[0].setCategories(categories);

					    //obj.categorias.push(dt);

					    chartLum.series[0].data[0].remove();
					    chartTemp.series[0].data[0].remove();
					    chartPresion.series[0].data[0].remove();
					    chartLum.series[0].addPoint([parseFloat(data.Msqm.toFixed(2))]);			  
					    chartTemp.series[0].addPoint([parseFloat(data.TEMP.toFixed(2))]);
					    chartPresion.series[0].addPoint([parseFloat(data.hPa.toFixed(2))]);			 
					}   
				});
			}else{
				database.remove();
				refData.remove();
			}
			
			$('#aplicaFiltro').attr('disabled', false);
			if($('#Modal').hasClass('in')){
				$('#cargando').hide();
			}else{
				$('#cargando').hide();
				$('#Modal').modal('show');	
			}
			
        }
    });
}

//No tocar
$(document).ready(function(){
	$('#cargando').hide();
    var locations = <?php echo $zones;?>;
    var icon = {
    	url: "img/point2.png", // url
      	size: new google.maps.Size(64, 64),
      	scaledSize: new google.maps.Size(64, 64)
    };
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(-35.0447936, -69.4862045),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        id: locations[i][3],
        fb_id: locations[i][4],
        map: map,
        icon: icon
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
          verMediciones(marker.id, marker.fb_id);
        }
      })(marker, i));
    }

    var weekpicker, start_date, end_date;

	function set_week_picker(date) {
	  var type = $('#sel1').val();
	  if(type=='semana'){
	    start_date = new Date(date.getFullYear(), date.getMonth(), (date.getDate()+1) - date.getDay());
	    end_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
	    
	    $('#date').datepicker('update', start_date);

	    $('#date').val(start_date.getDate() + '-' + (start_date.getMonth() + 1) + '-' + start_date.getFullYear() + ' al ' + end_date.getDate() + '-' + (end_date.getMonth() + 1) + '-' + end_date.getFullYear());
		}
	}

    $('#sel1').bind('change', function(){
    	var type = $(this).val();
    	if(type=='ahora'){
    		$('#nmSel').html('Ahora');
    		//verMediciones(id_nodo, id_firebase);
    		$('#date').val('');
    		$('#date').attr('disabled', true);
    		//$('#aplicaFiltro').attr('disabled', true);
    	}	

    	if(type=='dia'){
    		$('#nmSel').html('Seleccione el día');
    		//verMediciones(id_nodo, id_firebase);
    		$('#date').datepicker('destroy');
    		$('#date').datepicker('remove');
    		$("#date").datepicker( {
			    format: "dd-mm-yyyy",
			}).on("changeDate", function(e) {
		       //$('#date').val(e.date);
		    });
			$('#date').val("").datepicker("update");
			$('#date').attr('disabled', false);
			$('#date').focus();
			//$('#aplicaFiltro').attr('disabled', false);
    	}	

    	if(type=='semana'){
    		$('#date').datepicker('destroy');
    		$('#date').datepicker('remove');
		    $('#date').datepicker({
		        format: "dd-mm-yyyy",
		        autoclose: true,
		        forceParse: false,
        		//container: '#week-picker-wrapper',
		    }).on("changeDate", function(e) {
		        set_week_picker(e.date);
		    });
			$('#date').attr('disabled', false);
			$('#date').focus();
			//$('#aplicaFiltro').attr('disabled', false);
    	}

    	if(type=='mes'){
    		$('#nmSel').html('Seleccione el mes');
    		$('#date').datepicker('destroy');
    		$('#date').datepicker('remove');
    		$("#date").datepicker( {
			    format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months"
			}).on("changeDate", function(e) {
		        //$('#date').val(e.date);
		    });
			$('#date').val("").datepicker("update");
			$('#date').attr('disabled', false);
			$('#date').focus();
			//$('#aplicaFiltro').attr('disabled', false);
    	}

    	if(type=='ano'){
    		$('#nmSel').html('Seleccione el año');
    		$('#date').datepicker('destroy');
    		$('#date').datepicker('remove');
    		$("#date").datepicker( {
			    format: "yyyy",
			    viewMode: "years", 
			    minViewMode: "years"
			}).on("changeDate", function(e) {
		       //$('#date').val(e.date);
		    });
		    $('#date').val("").datepicker("update");
		    $('#date').attr('disabled', false);
		    $('#date').focus();
		    //$('#aplicaFiltro').attr('disabled', false);
    	}

    	//verMediciones(id_nodo, id_firebase);
    
    });

	$('#date').on('changeDate', function(ev){
	    $(this).datepicker('hide');
	});

    $('#aplicaFiltro').bind('click', function(){
    	$('#aplicaFiltro').attr('disabled', true);
    	verMediciones(id_nodo, id_firebase);
    });

})
</script>