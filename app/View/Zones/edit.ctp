<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Editar Nodo</h3>
    </div>
    <div class="panel-body">
    	<?php echo $this->Form->create('Zone'); ?>
	    	<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <?php
							echo $this->Form->input('zone', [
								'label' => 'Nombre',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'text',
							 	'autocomplete' => 'off',
							 	'required' => true
							]);
						?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('address', [
						 		'label' => 'Dirección',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'text',
						 		'autocomplete' => 'off',
						 		'required' => true
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <label for="regions_id">Region</label>
	                    <?php
							echo $this->Form->select('regions_id', $regions, [
								'label' => false,
								'id' => 'regions_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione región',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <label for="citys_id">Ciudad</label>
	                    <?php
							echo $this->Form->select('citys_id', $citys, [
								'label' => false,
								'id' => 'citys_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione ciudad',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <label for="communes_id">Comuna</label>
	                    <?php
							echo $this->Form->select('communes_id', $communes, [
								'label' => false,
							 	'id' => 'communes_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione comuna',
                            	'required' => true
							]);
	    				?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('latitud', [
						 		'label' => 'Latitud',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'number',
						 		'autocomplete' => 'off',
						 		'required' => false
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  		<div class="form-group">
			            <?php
							echo $this->Form->input('longitud', [
						 		'label' => 'Longitud',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'number',
						 		'autocomplete' => 'off',
						 		'required' => false
							]);
						?>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			        <div class="form-group">
			            <?php
							echo $this->Form->input('firebase_name', [
						 		'label' => 'id Firebase',
						 		'class' => ['form-control'],
						 		'placeholder' => '',
						 		'type' => 'text',
						 		'autocomplete' => 'off',
						 		'required' => true
							]);
						?>
			        </div>
			    </div>
			</div>
			<div class="row">
	        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
	        		<?php echo $this->Form->button('Guardar', ['class' => ['btn', 'btn-primary', 'btn-active-success', 'btn-hover-success']]); ?>
	        	</div>
	        </div>
	    </form>

    </div>
</div>
<script>
	
	$(document).ready(function(){

		$('#regions_id').select2();
		$('#citys_id').select2();
		$('#communes_id').select2();	

		$('#regions_id').on("select2:select", function (e) { 
			var regions_id = $(this).val();
			if(regions_id.length>0 || regions_id>0){
				$.ajax({
	                url     : "<?php echo Router::url('/Dashboards/getCitys', true);?>",
	                type    : "POST",
	                data    : { regions_id : regions_id }, 
	                success : function(result){
	                	//console.log(result);
	                	var obj = JSON.parse(result);
	                	//console.log(obj.response);
	                	if(obj.status==1){
	                		$('#citys_id').html(obj.response);
	                		$('#citys_id').trigger('change');
	                	}else{
	                		$('#citys_id').html('<option value="">Seleccione ciudad</option>');
				            $('#citys_id').trigger('change');
				            $('#communes_id').html('<option value="">Seleccione comuna</option>');
				            $('#communes_id').trigger('change');
	                	}
	                }
	            })
			}else{
				$('#citys_id').html('<option value="">Seleccione ciudad</option>');
	            $('#citys_id').trigger('change');
	            $('#communes_id').html('<option value="">Seleccione comuna</option>');
	            $('#communes_id').trigger('change');
			} 
		});

		$('#citys_id').on("select2:select", function (e) { 
			var citys_id = $(this).val();
			if(citys_id.length>0 || citys_id>0){
				$.ajax({
	                url     : "<?php echo Router::url('/Dashboards/getCommunes', true);?>",
	                type    : "POST",
	                data    : { citys_id : citys_id }, 
	                success : function(result){
	                	//console.log(result);
	                	var obj = JSON.parse(result);
	                	if(obj.status==1){
	                		$('#communes_id').html(obj.response);
	                		$('#communes_id').trigger('change');
	                	}else{
	                		$('#communes_id').html('<option value="">Seleccione comuna</option>');
	            			$('#communes_id').trigger('change');
	                	}
	                }
	            })
			}else{
				$('#communes_id').html('<option value="">Seleccione comuna</option>');
	            $('#communes_id').trigger('change');
			} 
		});
		
	})

</script>
