<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Editar Región</h3>
    </div>
    <div class="panel-body">
        <?php echo $this->Form->create('Region'); ?>
	        	
	        <div class="row">
	        	
		        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		        	<div class="form-group">
	                    <?php
							echo $this->Form->input('name', [
								'label' => 'Nombre',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'text',
							 	'autocomplete' => 'off',
							 	'required' => true,
							 	'disabled' => true
							]);
	    				?>
	                </div>
	            </div>

	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		        	<div class="form-group">
	                    <?php
							echo $this->Form->input('code', [
								'label' => 'Código',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'text',
							 	'autocomplete' => 'off',
							 	'required' => true,
							 	'disabled' => true
							]);
	    				?>
	                </div>
	            </div>

	            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		        	<div class="form-group">
	                    <?php
							echo $this->Form->input('order', [
								'label' => 'Orden',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'number',
							 	'autocomplete' => 'off',
							 	'required' => true,
							 	'disabled' => true
							]);
	    				?>
	                </div>
	            </div>

	            

	        </div>
	        
        </form>

    </div>
</div>
<script>
	
	$(document).ready(function(){

		
		
	})

</script>