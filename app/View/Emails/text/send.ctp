<tr>
    <td></td>
    <td class="container" width="600">
        <div class="content">
            <table class="main" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="alert alert-warning">
                        Título
                    </td>
                </tr>
                <tr>
                    <td class="content-wrap">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-block">
                                    You have <strong>1 free report</strong> remaining.
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block">
                                    Texto
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block">
                                    <a href="#" class="btn-primary">Upgrade my account</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block">
                                    Thanks for choosing Company Inc.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div class="footer">
                <table width="100%">
                    <tr>
                        <td class="aligncenter content-block"><a href="#">Unsubscribe</a> from these alerts.</td>
                    </tr>
                </table>
            </div></div>
    </td>
    <td></td>
</tr>

