<tr>
    <td></td>
    <td class="container" width="600">
        <div class="content">
            <table class="main" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="alert alert-warning">
                        Notificación
                    </td>
                </tr>
                <tr>
                    <td class="content-wrap">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2" class="content-block">
                                    Estimado  <strong><?php echo $nombre_completo; ?></strong>, el umbral del nodo monitoreado por ud, ha sido superado bajo los siguientes datos:
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Nodo</strong></td>
                                <td class="content-block"><?php echo $nodo_nombre; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Fecha/Hora</strong></td>
                                <td class="content-block"><?php echo $hora; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Longitud</strong></td>
                                <td class="content-block"><?php echo $longitud; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Latitud</strong></td>
                                <td class="content-block"><?php echo $latitud; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Umbral Definido</strong></td>
                                <td class="content-block"><?php echo $umbral_definido; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>MSQM</strong></td>
                                <td class="content-block"><?php echo $msqm; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>Temperatura del nodo</strong></td>
                                <td class="content-block"><?php echo $temperatura; ?></td>
                            </tr>
                            <tr>
                                <td class="content-block"><strong>HPA</strong></td>
                                <td class="content-block"><?php echo $hpa; ?></td>
                            </tr>
                            <!--
                            <tr>
                                <td class="content-block">
                                    Se puede olo poner el <a href="http://www.google.cl">Link</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block">
                                    <a href="http://www.google.cl" class="btn-primary">Se puede porner un boton con un link</a>
                                </td>
                            </tr>
                            -->
                        </table>
                    </td>
                </tr>
            </table>
            <div class="footer">
                <table width="100%">
                    <tr>
                        <td class="aligncenter content-block"><a href="http://www.google.cl">No más alertas.</a></td>
                    </tr>
                </table>
            </div></div>
    </td>
    <td></td>
</tr>

