<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Ver Ciudad</h3>
    </div>
    <div class="panel-body">
        <?php echo $this->Form->create('City'); ?>
	        	
	        <div class="row">
	        	
	        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		        	<div class="form-group">
	                    <label for="categories_id">Región</label>
	                    <?php
							echo $this->Form->select('regions_id', $Regiones, [
								'label' => false,
								'id' => 'regions_id',
							 	'class' => ['form-control'],
							 	'empty' => 'Seleccione región',
                            	'required' => true,
                            	'disabled' => true
							]);
	    				?>
	                </div>
	            </div>

		        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		        	<div class="form-group">
	                    <?php
							echo $this->Form->input('name', [
								'label' => 'Nombre',
							 	'class' => ['form-control'],
							 	'placeholder' => '',
							 	'type' => 'text',
							 	'autocomplete' => 'off',
							 	'required' => true,
							 	'disabled' => true
							]);
	    				?>
	                </div>
	            </div>

	        </div>
        </form>

    </div>
</div>
<script>
	
	$(document).ready(function(){

		$('#regions_id').select2();
		
	})

</script>