
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-btm text-right">
        <a href="<?php echo Router::url( '/Dashboard/Mis-Nodos/Nuevo',false);?>" type="button" class="btn btn-primary">
            <i class="pli-add-user" aria-hidden="true"></i> Nuevo
        </a>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Mis Nodos Monitoreadas</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <?php if(count($Nodos)==0){ ?>
            <div class="alert alert-warning">
                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                <strong>Estimad@</strong> Debes elegir un nodo para su monitoreo.
            </div>
            <?php } ?>
            <?php $i=0;?>
            <?php foreach ($Nodos as $Nodo) { ?>
                <div id="item-block-<?php echo $Nodo['UserRange']['id'];?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 <?php if($i==0){ echo 'well';}?>">
                    
                    <div class="mar-all">
                        <div class="media">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4  col-lg-4">
                                    <div class="media-left">
                                        <i class="icon-lg icon-fw demo-pli-map-marker-2"></i>
                                    </div>
                                    <div class="media-body">
                                        <address>
                                            <strong class="text-main"> <?php echo $Nodo['Zone']['zone'];?></strong><br>
                                            Dirección: <?php echo $Nodo['Zone']['address'];?><br>
                                            Comuna: <?php echo $Nodo['Commune']['Commune']['name'];?><br>
                                            Ciudad: <?php echo $Nodo['City']['City']['name'];?><br>
                                            Región: <?php echo $Nodo['Region']['Region']['name'];?><br>
                                            <button class="delete-item btn btn-danger mar-top" data-item="<?php echo $Nodo['UserRange']['id'];?>" class="btn btn-danger mar-top">
                                                <i class="demo-pli-cross"></i> Eliminar de mi lista
                                            </button>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4  col-lg-4">
                                    <div id="lum-legend-<?php echo $Nodo['Zone']['id'];?>" class="text-center"></div>
                                    <div id="luminosidad-<?php echo $Nodo['Zone']['id'];?>" style="height:150px"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4  col-lg-4">
                                    <div id="temp-legend-<?php echo $Nodo['Zone']['id'];?>" class="text-center"></div>
                                    <div id="temperatura-<?php echo $Nodo['Zone']['id'];?>" style="height:150px"></div>
                                </div>
                            </div>
                        </div>
                        <div id="marker-map-<?php echo $Nodo['Zone']['id'];?>" style="height:300px"></div>
                        <div class="row mar-top">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <p class="text-main mar-btm">Medida de alerta</p>
                                <small class="help-block">texto descriptivo</small>
                                <div id="range-def-<?php echo $Nodo['Zone']['id'];?>"></div>
                                <div class="row mar-top">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <strong>Valor : </strong>
                                            <span id="range-def-val-<?php echo $Nodo['Zone']['id'];?>"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                                        <?php echo $this->Form->create('UserRange', ['url' => ['controller' => 'UserRanges', 'action' => 'editRange']]); ?>
                                            <input type="hidden" id="range-send-val-<?php echo $Nodo['Zone']['id'];?>" name="data[UserRange][range_finished]" value="<?php echo $Nodo['UserRange']['range_finished'];?>">
                                            <input type="hidden" name="data[UserRange][id]" value="<?php echo $Nodo['UserRange']['id'];?>">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </form>
                                    </div>
                                </div>

                                <script>
                                    var rs_def = document.getElementById('range-def-<?php echo $Nodo['Zone']['id'];?>');
                                    var rs_def_value = document.getElementById('range-def-val-<?php echo $Nodo['Zone']['id'];?>');
                                    var rs_range_value = document.getElementById('range-send-val-<?php echo $Nodo['Zone']['id'];?>');

                                    noUiSlider.create(rs_def,{
                                        start   : [ <?php echo $Nodo['UserRange']['range_finished'];?> ],
                                        connect : 'lower',
                                        range   : {
                                            'min': [  0 ],
                                            'max': [ 100 ]
                                        }
                                    });

                                    rs_def.noUiSlider.on('update', function( values, handle ) {
                                        //rs_def_value.innerHTML = values[handle];
                                        $('#range-def-val-<?php echo $Nodo['Zone']['id'];?>').html(values[handle]);
                                        $('#range-send-val-<?php echo $Nodo['Zone']['id'];?>').val(values[handle]);
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <?php
                        $dataChartsLum = '';
                        $dataChartsTemp = '';
                        $i=1;
                        foreach ($Nodo['Medidas'] as $Meidida) {
                            if($i<=12){
                                if($i==1){
                                    $dataChartsLum .= "{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']))."', up:".$Meidida['Medidas']['hpa']." }";

                                    $dataChartsTemp .= "{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']))."', up:".$Meidida['Medidas']['temp']." }";
                                }else{
                                    $dataChartsLum .= ",{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']))."', up:".$Meidida['Medidas']['hpa']." }";

                                    $dataChartsTemp .= ",{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']))."', up:".$Meidida['Medidas']['temp']." }";
                                }

                                $i++;
                            }
                        }
                    ?>
                <script>
                    $(document).ready(function(){

                        var chartLum = Morris.Area({
                            element: 'luminosidad-<?php echo $Nodo['Zone']['id'];?>',
                            data: [<?php echo $dataChartsLum;?>],
                            gridEnabled: true,
                            gridLineColor: 'rgba(0,0,0,.1)',
                            gridTextColor: '#8f9ea6',
                            gridTextSize: '11px',
                            behaveLikeLine: true,
                            smooth: true,
                            xkey: 'period',
                            ykeys: ['up'],
                            labels: ['Luminosidad'],
                            lineColors: ['#8bc34a'],
                            pointSize: 0,
                            pointStrokeColors : ['#045d97'],
                            lineWidth: 0,
                            resize:true,
                            hideHover: 'auto',
                            fillOpacity: 0.9,
                            parseTime:false
                        });

                        chartLum.options.labels.forEach(function(label, i){
                            var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
                            $('<span></span>').css('background-color', chartLum.options.lineColors[i]).prependTo(legendItem);
                            $('#lum-legend-<?php echo $Nodo['Zone']['id'];?>').append(legendItem);
                        });

                        var charTemp = Morris.Area({
                            element: 'temperatura-<?php echo $Nodo['Zone']['id'];?>',
                            data: [<?php echo $dataChartsTemp;?>],
                            gridEnabled: true,
                            gridLineColor: 'rgba(0,0,0,.1)',
                            gridTextColor: '#8f9ea6',
                            gridTextSize: '11px',
                            behaveLikeLine: true,
                            smooth: true,
                            xkey: 'period',
                            ykeys: ['up'],
                            labels: ['Temperatura Nodo'],
                            lineColors: ['#ffb300'],
                            pointSize: 0,
                            pointStrokeColors : ['#045d97'],
                            lineWidth: 0,
                            resize:true,
                            hideHover: 'auto',
                            fillOpacity: 0.9,
                            parseTime:false
                        });

                        charTemp.options.labels.forEach(function(label, i){
                            var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
                            $('<span></span>').css('background-color', charTemp.options.lineColors[i]).prependTo(legendItem);
                            $('#temp-legend-<?php echo $Nodo['Zone']['id'];?>').append(legendItem);
                        });

                        var markerMap = new GMaps({
                            el: '#marker-map-<?php echo $Nodo['Zone']['id'];?>',
                            lat: <?php echo $Nodo['Zone']['latitud'];?>,
                            lng: <?php echo $Nodo['Zone']['longitud'];?>
                        });
                        markerMap.addMarker({
                            lat: <?php echo $Nodo['Zone']['latitud'];?>,
                            lng: <?php echo $Nodo['Zone']['longitud'];?>,
                            title: 'Location',
                            click: function(e) {
                                $.niftyNoty({
                                    type: "info",
                                    icon: "fa fa-info",
                                    message: "You clicked in the marker",
                                    container: 'floating',
                                    timer: 3500
                                });
                            },
                            infoWindow: {
                                 content: '<div>HTML Content</div>'
                            }
                        });

                    });
                </script>
                    
        </div>
        
            <?php 
                if($i==0){
                    $i=1;
                }else{
                    $i=0;
                }
            ?>
            
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
            <div class="pagination pagination-large">
                <ul class="pagination">
                    <?php
                        echo $this->Paginator->prev(__('Anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next(__('Siguiente'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function(){
    
        $('.delete-item').bind('click', function(){
            var item = $(this).attr('data-item');
            //console.log(item);
            bootbox.confirm({
                message: "Se eliminará el nodo seleccionado, ¿Desea Continuar?",
                buttons: {
                    confirm: {
                        label: 'Aceptar',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url     : "<?php echo Router::url('/UserRanges/DeleteItem', true);?>",
                            type    : "POST",
                            data    : { zones_id : item }, 
                            success : function(result){
                                console.log(result);
                                var obj = JSON.parse(result);
                                alertMessage(obj.message, obj.clase);
                                if(obj.state==1){
                                    $('#item-block-'+item).remove();
                                }
                            }
                        });
                    }else{
                        alertMessage('Operación cancelada.', 'danger');
                    }
                }
            });  
        })

    });
</script>
<?php
    echo $this->Html->script([
        'plugins/morris-js/morris.min',
        'plugins/morris-js/raphael-js/raphael.min',
        'plugins/gmaps/gmaps'
    ]);
  
    echo $this->fetch('script');
?>