<div class="row">
    <div class="col-md-3">
        <div class="panel panel-warning panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="pli-wifi-2 icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?php echo $NumeroNodos;?></p>
                <p class="mar-no">Nodos</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-info panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-file-zip icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Zip Files</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-mint panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-camera-2 icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Photos</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-danger panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-video icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Videos</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
					    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel">
            <!--<div class="panel-heading">
                <h3 class="panel-title">Sensor: Sensor 1</h3>
            </div>-->
            <div class="panel-body pad-no">
	            <div class="row">
		            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                <div class="mar-btm" style="height:500px">
		                	
							<iframe width="100%" height="100%" src="https://www.youtube.com/embed/FOXslemU4Hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

		                </div>
		                <p>El vídeo ha sido realizado por Sriram Murali, un fanático estadounidense de la astronomía, que impulsado por su afición se ha dado la labor de recorrer el planeta en busca de los cielos más claros y preciosos que una persona pueda comprender. De momento, el mejor cielo, conforme Murali, se halla en las Dunas de Eureka, en el parque natural Valle de la Muerte situado en California.</p>
            		</div>
            	</div>
            </div>
        </div>
    </div>

    
</div>

<script>
	$(document).ready(function(){


	});
</script>