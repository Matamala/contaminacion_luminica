
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Nodos Monitoreados</h3>
    </div>
    <div class="panel-body">

		<div class="row">
		    <?php $i=0;?>
		    <?php foreach ($Nodos as $Nodo) { ?>
		    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 <?php if($i==0){ echo 'well';}?>">
		            
	                <div class="mar-all">
	                    <div class="media">
		                    <div class="row">
			                    <div class="col-xs-12 col-sm-12 col-md-3  col-lg-3">
			                        <div class="media-left">
			                            <i class="icon-lg icon-fw demo-pli-map-marker-2"></i>
			                        </div>
			                        <div class="media-body">
			                            <address>
			                                <strong class="text-main"> <?php echo $Nodo['Zone']['zone'];?></strong><br>
			                                Dirección: <?php echo $Nodo['Zone']['address'];?><br>
			                                Comuna: <?php echo $Nodo['Commune']['name'];?><br>
			                                Ciudad: <?php echo $Nodo['City']['name'];?><br>
			                                Región: <?php echo $Nodo['Region']['name'];?><br>
			                            </address>
			                        </div>
			                    </div>
			                    <div class="col-xs-12 col-sm-12 col-md-3  col-lg-3">
			                    	<div id="lum-legend-<?php echo $Nodo['Zone']['id'];?>" class="text-center"></div>
                    				<div id="luminosidad-<?php echo $Nodo['Zone']['id'];?>" style="height:150px"></div>
			                    </div>
			                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			                    	<div id="temp-legend-<?php echo $Nodo['Zone']['id'];?>" class="text-center"></div>
                    				<div id="temperatura-<?php echo $Nodo['Zone']['id'];?>" style="height:150px"></div>
			                    </div>
			                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <div id="hum-legend-<?php echo $Nodo['Zone']['id'];?>" class="text-center"></div>
                                    <div id="humedad-<?php echo $Nodo['Zone']['id'];?>" style="height:150px"></div>
                                </div>
		                    </div>
	                    </div>
	                    <div>
	                        <div id="marker-map-<?php echo $Nodo['Zone']['id'];?>" style="height:300px"></div>
	                    </div>
	                </div>

	                <?php
	                	$dataChartsLum = '';
	                	$dataChartsTemp = '';
	                	$dataChartsHum = '';
	                	$i=1;
	                	foreach ($Nodo['Medidas'] as $Meidida) {
	                		if($i<=12){
	                			if($i==1){
		                			$dataChartsLum .= "{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['hz']." }";

		                			$dataChartsTemp .= "{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['temp']." }";

		                			$dataChartsHum .= "{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['hpa']." }";
		                		}else{
		                			$dataChartsLum .= ",{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['hz']." }";

		                			$dataChartsTemp .= ",{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['temp']." }";

		                			$dataChartsHum .= ",{ period:'".date('d-m-Y H:i:s', strtotime($Meidida['fecha']))."', up:".$Meidida['hpa']." }";
		                		}

		                		$i++;
	                		}
	                	}
	                ?>

	            <script>
    				$(document).ready(function(){

    					var chartLum = Morris.Area({
        					element: 'luminosidad-<?php echo $Nodo['Zone']['id'];?>',
            				data: [<?php echo $dataChartsLum;?>],
            				gridEnabled: true,
				            gridLineColor: 'rgba(0,0,0,.1)',
				            gridTextColor: '#8f9ea6',
				            gridTextSize: '11px',
				            behaveLikeLine: true,
				            smooth: true,
				            xkey: 'period',
				            ykeys: ['up'],
				            labels: ['Luminosidad'],
				            lineColors: ['#8bc34a'],
				            pointSize: 0,
				            pointStrokeColors : ['#045d97'],
				            lineWidth: 0,
				            resize:true,
				            hideHover: 'auto',
				            fillOpacity: 0.9,
				            parseTime:false
				        });

				        chartLum.options.labels.forEach(function(label, i){
				            var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
				            $('<span></span>').css('background-color', chartLum.options.lineColors[i]).prependTo(legendItem);
				            $('#lum-legend-<?php echo $Nodo['Zone']['id'];?>').append(legendItem);
				        });

				        var charTemp = Morris.Area({
        					element: 'temperatura-<?php echo $Nodo['Zone']['id'];?>',
            				data: [<?php echo $dataChartsTemp;?>],
            				gridEnabled: true,
				            gridLineColor: 'rgba(0,0,0,.1)',
				            gridTextColor: '#8f9ea6',
				            gridTextSize: '11px',
				            behaveLikeLine: true,
				            smooth: true,
				            xkey: 'period',
				            ykeys: ['up'],
				            labels: ['Temperatura Nodo'],
				            lineColors: ['#ffb300'],
				            pointSize: 0,
				            pointStrokeColors : ['#045d97'],
				            lineWidth: 0,
				            resize:true,
				            hideHover: 'auto',
				            fillOpacity: 0.9,
				            parseTime:false
				        });

				        charTemp.options.labels.forEach(function(label, i){
				            var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
				            $('<span></span>').css('background-color', charTemp.options.lineColors[i]).prependTo(legendItem);
				            $('#temp-legend-<?php echo $Nodo['Zone']['id'];?>').append(legendItem);
				        });

				        var charHum = Morris.Area({
	                        element: 'humedad-<?php echo $Nodo['Zone']['id'];?>',
	                        data: [<?php echo $dataChartsHum;?>],
	                        gridEnabled: true,
	                        gridLineColor: 'rgba(0,0,0,.1)',
	                        gridTextColor: '#8f9ea6',
	                        gridTextSize: '11px',
	                        behaveLikeLine: true,
	                        smooth: true,
	                        xkey: 'period',
	                        ykeys: ['up'],
	                        labels: ['Presión Atmosférica'],
	                        lineColors: ['#0ab1fc'],
	                        pointSize: 0,
	                        pointStrokeColors : ['#045d97'],
	                        lineWidth: 0,
	                        resize:true,
	                        hideHover: 'auto',
	                        fillOpacity: 0.9,
	                        parseTime:false
	                    });

	                    charHum.options.labels.forEach(function(label, i){
	                        var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
	                        $('<span></span>').css('background-color', charHum.options.lineColors[i]).prependTo(legendItem);
	                        $('#hum-legend-<?php echo $Nodo['Zone']['id'];?>').append(legendItem);
	                    });

				        var markerMap = new GMaps({
					        el: '#marker-map-<?php echo $Nodo['Zone']['id'];?>',
					        lat: <?php echo $Nodo['Zone']['latitud'];?>,
					        lng: <?php echo $Nodo['Zone']['longitud'];?>
					    });
					    markerMap.addMarker({
					        lat: <?php echo $Nodo['Zone']['latitud'];?>,
					        lng: <?php echo $Nodo['Zone']['longitud'];?>,
					        title: 'Location',
					        click: function(e) {
					            $.niftyNoty({
					                type: "info",
					                icon: "fa fa-info",
					                message: "You clicked in the marker",
					                container: 'floating',
					                timer: 3500
					            });
					        },
					        infoWindow: {
					             content: '<div>HTML Content</div>'
					        }
					    });

    				});
    			</script>
		         	
		</div>
		    <?php 
		    	if($i==0){
		    		$i=1;
		    	}else{
		    		$i=0;
		    	}
		    
		} ?>
    </div>
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
	    	<div class="pagination pagination-large">
			    <ul class="pagination">
			        <?php
			            echo $this->Paginator->prev(__('Anterior'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
			            echo $this->Paginator->next(__('Siguiente'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			        ?>
			    </ul>
			</div>
    	</div>
    </div>
</div>
<?php
    echo $this->Html->script([
        'plugins/morris-js/morris.min',
        'plugins/morris-js/raphael-js/raphael.min',
        'plugins/gmaps/gmaps'
    ]);
  
    echo $this->fetch('script');
?>
