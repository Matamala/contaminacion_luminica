<div id="<?php echo $key; ?>Message" class="alert alert-success">
    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
    <strong>Info:</strong> <?php echo $message; ?>
</div>