<!--<div id="<?php echo $key; ?>Message" class="alert alert-success"><?php echo $message; ?></div>-->
<div class="modal fade bs-example-modal-sm" id="successModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">En hora buena!</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
        		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        			<div id="<?php echo $key; ?>Message" class="alert alert-success"><?php echo $message; ?></div>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<script>
	$(document).ready(function(){
		$('#successModal').modal('show');
	
	});
</script>