<?php
App::uses('AppModel', 'Model');

class Zone extends AppModel {

    public $useTable = 'zones';  
    public $hasAndBelongsToMany = [
	    'User' => [
	    	'className' => 'User',
	    	'joinTable' => 'users_ranges',
	    	'foreignKey' => 'zones_id',
	    	'associationForeignKey' => 'users_id'
  		]
  	];

  	
    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'Commune' => [
            'className' => 'Commune',
            'foreignKey' => 'communes_id',
        ],
        'City' => [
            'className' => 'City',
            'foreignKey' => 'citys_id',
        ],
        'Region' => [
            'className' => 'Region',
            'foreignKey' => 'regions_id',
        ],
    ];

    public $hasMany = [
        'UserRange' => [
            'className' => 'UserRange',
            'foreignKey' => 'zones_id',
        ],
        'Medidas' => [
            'className' => 'Medidas',
            'foreignKey' => 'zones_id',
        ]
    ];
    

}
?>