<?php
App::uses('AppModel', 'Model');

class UserRange extends AppModel {

    public $useTable = 'users_ranges';  

    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'zones_id',
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'users_id',
        ]
    ];

    public $hasMany = [
        'SentAlert' => [
            'className' => 'SentAlert',
            'foreignKey' => 'users_ranges_id',
        ]
    ];

}
?>