<?php
App::uses('AppModel', 'Model');

class Commune extends AppModel {
    public $useTable = 'communes';  
    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'City' => [
            'className' => 'City',
            'foreignKey' => 'citys_id',
        ],
    ];

    public $hasMany = [
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'communes_id',
        ]
    ];
}
?>