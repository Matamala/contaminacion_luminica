<?php
App::uses('AppModel', 'Model');

class State extends AppModel {
    public $useTable = 'states';  
    public $hasMany = [
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'states_id',
        ],
        'UserRange' => [
            'className' => 'UserRange',
            'foreignKey' => 'states_id',
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'states_id',
        ],
        'City' => [
            'className' => 'City',
            'foreignKey' => 'states_id',
        ],
        'Commune' => [
            'className' => 'Commune',
            'foreignKey' => 'states_id',
        ],
        'Region' => [
            'className' => 'Region',
            'foreignKey' => 'states_id',
        ],
        'SentAlert' => [
            'className' => 'SentAlert',
            'foreignKey' => 'states_id',
        ],
        'Medidas' => [
            'className' => 'Medidas',
            'foreignKey' => 'states_id',
        ]
    ];
}
?>