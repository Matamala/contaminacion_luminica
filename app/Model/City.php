<?php
App::uses('AppModel', 'Model');

class City extends AppModel {
    public $useTable = 'citys';  
    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'Region' => [
        	'className' => 'Region',
            'foreignKey' => 'regions_id',
        ]
    ]; 

    public $hasMany = [
        'Commune' => [
            'className' => 'Commune',
            'foreignKey' => 'citys_id',
        ],
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'citys_id',
        ]
    ];
}
?>