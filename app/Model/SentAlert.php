<?php
App::uses('AppModel', 'Model');

class SentAlert extends AppModel {

    public $useTable = 'sent_alerts';  

  	public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'UserRange' => [
            'className' => 'UserRange',
            'foreignKey' => 'users_ranges_id',
        ]
    ];

    public $hasMany = [
        'UserRange' => [
            'className' => 'UserRange',
            'foreignKey' => 'zones_id',
        ]
    ];

}
?>