<?php
App::uses('AppModel', 'Model');

class Region extends AppModel {
    public $useTable = 'regions';  
    
    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
    ];

    public $hasMany = [
        'City' => [
            'className' => 'City',
            'foreignKey' => 'regions_id',
        ],
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'regions_id',
        ]
    ];
}
?>