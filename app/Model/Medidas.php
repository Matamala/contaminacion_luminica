<?php
App::uses('AppModel', 'Model');

class Medidas extends AppModel {
    public $useTable = 'mediciones_zonas';  
    
    public $belongsTo = [
        'State' => [
            'className' => 'State',
            'foreignKey' => 'states_id',
        ],
        'Zone' => [
            'className' => 'Zone',
            'foreignKey' => 'zones_id',
        ],
    ];
}
?>