<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

	public $hasAndBelongsToMany = [
	    'Zone' => [
	    	'className' => 'Zone',
	    	'joinTable' => 'users_ranges',
	    	'foreignKey' => 'users_id',
	    	'associationForeignKey' => 'zones_id'
  		]
  	];

    public $hasMany = [
        'UserRange' => [
            'className' => 'UserRange',
            'foreignKey' => 'users_id',
        ]
    ];

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

}
?>