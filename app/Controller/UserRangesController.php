<?php
App::uses('AppController', 'Controller');
class UserRangesController extends AppController {

   public function index(){

   }

   public function getNodes(){

   		if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('Zone');

            $zonas = $this->Zone->find('all', [
            	'conditions' => [
            		'Zone.regions_id' => $this->request->data('regions_id'),
            		'Zone.citys_id' => $this->request->data('citys_id'),
            		'Zone.communes_id' => $this->request->data('communes_id'),
            		'Zone.states_id' => 1
            	],
            	'order' => ['Zone.zone ASC']
            ]);

            $i=0;
            foreach ($zonas as $zona) {

            	$div = '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 mar-top">
		            <div class="panel pos-rel mar-top">
		                <div class="text-center">
	                        <div id="marker-map-'.$zona['Zone']['id'].'" style="height:150px"></div>
		                    <div class="table-responsive">
		                        <table class="table table-bordered">
		                        	<tr>
		                        		<td colspan="2" class="text-lg text-semibold mar-top text-main text-center">
		                        			'.$zona['Zone']['zone'].'
		                        		</td>
		                        	</tr>
		                        	<tr>
		                        		<td class="text-lg text-semibold text-sm text-left">Dirección</td>
		                        		<td class="text-sm text-left">'.$zona['Zone']['address'].'</td>
		                        	</tr>
		                        	<tr>
		                        		<td class="text-lg text-semibold text-sm text-left">Región</td>
		                        		<td class="text-sm text-left">'.$zona['Region']['name'].'</td>
		                        	</tr>
		                        	<tr>
		                        		<td class="text-lg text-semibold text-sm text-left">Ciudad</td>
		                        		<td class="text-sm text-left">'.$zona['City']['name'].'</td>
		                        	</tr>
		                        	<tr>
		                        		<td class="text-lg text-semibold text-sm text-left">Comuna</td>
		                        		<td class="text-sm text-left">'.$zona['Commune']['name'].'</td>
		                        	</tr>
		                        	<tr>
		                        		<td colspan="2" class="text-lg text-semibold mar-top text-main text-center">
		                        			<div class="mar-top mar-btm" id="barra-'.$zona['Zone']['id'].'"></div>
		                        			<span class="text-sm text-center">Valor:</span>
                                  <span class="text-sm text-center" id="rangeVal-'.$zona['Zone']['id'].'"></span>
                                  <input type="hidden" id="barraVal-'.$zona['Zone']['id'].'" name="data[UserRange][Nodo]['.$zona['Zone']['id'].'][valor]">
                                  <input type="hidden" name="data[UserRange][Nodo]['.$zona['Zone']['id'].'][id]" value="'.$zona['Zone']['id'].'">
		                        		</td>
		                        	</tr>
                              <tr>
                                <td>
                                  <small class="help-block text-center">Desde</small>
                                  <input id="time-start-'.$zona['Zone']['id'].'" name="data[UserRange][Nodo]['.$zona['Zone']['id'].'][time_start]" value="" type="text" class="form-control text-center">
                                </td>
                                <td>
                                  <small class="help-block text-center">Hasta</small>
                                  <input id="time-finished-'.$zona['Zone']['id'].'" name="data[UserRange][Nodo]['.$zona['Zone']['id'].'][time_finished]" value="" type="text" class="form-control text-center">
                                </td>
                              </tr>
		                        	<tr>
		                        		<td colspan="2" class="text-center text-normal">
		                        			<div class="checkbox">
					                            <input id="zone-'.$zona['Zone']['id'].'" name="data[UserRange][Nodo]['.$zona['Zone']['id'].'][Monitorear]" class="magic-checkbox" type="checkbox">
					                            <label for="zone-'.$zona['Zone']['id'].'">Monitorear</label>
					                        </div>
		                        		</td>
		                        	</tr>
		                        </table>	
		                    </div>
		                </div>
		            </div>	
				</div>';

              $i++;
            	$dataTemp = [
	                'id' => $zona['Zone']['id'],
	                'zone' => $zona['Zone']['zone'],
	                'div_id' => '#marker-map-'.$zona['Zone']['id'],
	                'latitud' => $zona['Zone']['latitud'],
	                'longitud' => $zona['Zone']['longitud'],
	                'div' => $div,
	                'barra' => 'barra-'.$zona['Zone']['id'],
	                'barraVal' => 'rangeVal-'.$zona['Zone']['id'],
	                'barraHidden' => 'barraVal-'.$zona['Zone']['id']
	            ];
	            array_push($data, $dataTemp);
            }
	            
            echo json_encode($data);
        }

   }

   public function add(){
   		if($this->request->is('post')){

        $monit = false;
   			$this->loadModel('UserRange');
   			foreach ($this->request->data['UserRange']['Nodo'] as $value) {

            if(!empty($value['Monitorear'])){	
       					
                $monit = true;
                $valid = $this->UserRange->find('all', [
       						'conditions' => [
       							'UserRange.zones_id' => $value['id'],
       							'UserRange.users_id' => $this->Auth->user('id'),
       							'UserRange.states_id' => 1
       						]
       					]);

       					if(!$valid){
    	   					$dataRange = [];	
    	   					$dataRange['regions_id'] = $this->request->data['UserRange']['regions_id'];
    	   					$dataRange['citys_id'] = $this->request->data['UserRange']['citys_id'];
    	   					$dataRange['communes_id'] = $this->request->data['UserRange']['communes_id'];
    	   					$dataRange['users_id']= $this->Auth->user('id');
    	   					$dataRange['zones_id']= $value['id'];
    	   					$dataRange['range_finished']= $value['valor'];
                  $dataRange['time_start']= $value['time_start'];
                  $dataRange['time_finished']= $value['time_finished'];
    	   					$this->UserRange->create();
    	   					$this->UserRange->save($dataRange);
                }else{
                  $dataRange = [];	
    	   					$dataRange['id'] = $valid[0]['UserRange']['id'];
    	   					$dataRange['range_finished'] = $value['valor'];
                  $dataRange['time_start'] = $value['time_start'];
                  $dataRange['time_finished'] = $value['time_finished'];    						
                  $this->UserRange->save($dataRange);
       					}	
     				}
   			}
       
        if(!$monit){
          $this->Flash->error('Debe seleccionar a lo menos 1 nodo a monitorear.');
          return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos/Nuevo', 'action' => '']);
        }else{
          $this->Flash->ok('Datos guardados con exito.');
          return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
        }

    }

   		  $this->loadModel('Region');
        $this->loadModel('City');
        $this->loadModel('Commune');

        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);

        $citys = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);

        $communes = $this->Commune->find('list', [
            'fields' => [ 'Commune.id', 'Commune.name' ], 
            'conditions' => [ 'Commune.states_id' => 1 ],
            'order' => [ 'Commune.name ASC' ]
        ]);

        $this->set('communes', $communes);
        $this->set('regions', $regions);
        $this->set('citys', $citys);

        $this->setTitles('Nodos', 'Nuevo Nodo', Router::url('/Dashboard/Mis-Nodos', true));
   }

   public function editRange(){
   		if($this->request->is('post')){
   			$this->loadModel('UserRange');
   			$id = $this->request->data['UserRange']['id'];
   			$minimum = $this->request->data['UserRange']['range_finished'];

   			$validate = $this->UserRange->find('all', [
   				'conditions' => [
   					'UserRange.id' => $this->request->data['UserRange']['id'], 
   					'UserRange.users_id' => $this->Auth->user('id')
   				]
   			]);

   			if($validate){
	   			if($this->UserRange->save($this->request->data)){
	   				$this->Flash->ok('Datos guardados con exito.');
	                return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
	   			}else{
	   				$this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
	                return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
	   			}
	   		}else{
	   			$this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
	            return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
	   		}
   		}else{
   			$this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
	        return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
   		}
   }


   public function DeleteItem($id=null){

	   	/*
      $data = [];
	   	$data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
	   	$data['clase'] = 'danger';
	   	$data['state'] = 2;

	   	if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];
      */
      $this->autoRender = false;
      if(!$id){
        $this->Flash->error('Error, intentelo nuevamente.');
        return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);
      }

      $this->loadModel('UserRange');
      $zone = $this->UserRange->find('all', [
      	'conditions' => [
      		'UserRange.states_id' => 1,
      		'UserRange.id' => $id,
      		'UserRange.users_id' => $this->Auth->user('id')
      	]
      ]);

      

      if(!$zone){
        $this->Flash->error('Error, intentelo nuevamente.');
        return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);   	
      }

      $this->UserRange->updateAll(
        ['UserRange.states_id' => 2], 
        [
          'UserRange.states_id' => 1,
          'UserRange.id' => $id,
          'UserRange.users_id' => $this->Auth->user('id')
        ]
      );

      $this->Flash->ok('Datos guardados con éxito.');
      return $this->redirect(['controller'=> '/Dashboard/Mis-Nodos', 'action' => '']);  
              //$data['message'] = 'Datos guardados con éxito.';
              //$data['clase'] = 'success';
              //$data['state'] = 1;

        //}	
   		//echo json_encode($data);
   }


}

?>