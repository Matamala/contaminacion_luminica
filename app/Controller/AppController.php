<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {  
    
    public $components = array(
        'DebugKit.Toolbar',
        'Paginator', 
        'Flash',
        'Cookie',
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'Monitoreo',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'Monitoreo',
                'action' => 'index'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );

    public function beforeFilter() {
        $this->disableCache();
        $this->response->disableCache();
        $this->Auth->allow('contact', 'about', 'login', 'add');

        $this->loadModel('Zone');
        $this->loadModel('User');
        $dataZones = $this->Zone->find('all',[
            'fields' => ['Zone.id', 'Zone.zone'],
            'conditions' => [ 'Zone.states_id'=> 1 ]
        ]);
        $this->set('ZonasActivas', $dataZones);
        
        if(!empty($this->Auth->user('id'))){
            $dataUser = $this->User->findById($this->Auth->user('id'));
            $this->set('GlobalDataUser', $dataUser);
        }else{
            $dataUser = [];
            $this->set('GlobalDataUser', $dataUser);
        }

    }

    public function setTitles($title, $subtitle, $url){

        $this->set('Title', $title);
        $this->set('Subtitle', $subtitle);
        $this->set('Url', $url);

    }
    

}
