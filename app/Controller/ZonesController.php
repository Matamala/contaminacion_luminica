<?php
App::uses('AppController', 'Controller');
class ZonesController extends AppController {

   public function index(){
   		$this->setTitles('Nodos', 'Listado', Router::url('/Dashboard/Nodos', true));
   }

   public function getZones() {

      if($this->request->is('ajax')){
         $this->autoRender = false;
         $data = [];

         $this->loadModel('Zone');

         $conditions = [];
         if(!empty($this->params['url']['sSearch'])){
             
             $conditions = [
                 'Zone.states_id' => 1,
                 [   
                     'OR' => [ 
                         'Zone.zone LIKE' => '%'.$this->params['url']['sSearch'].'%',
                         'Zone.firebase_name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                         'Zone.address LIKE' => '%'.$this->params['url']['sSearch'].'%',
                         'Zone.latitud LIKE' => '%'.$this->params['url']['sSearch'].'%',
                         'Zone.longitud LIKE' => '%'.$this->params['url']['sSearch'].'%', 
                         'Region.name LIKE' => '%'.$this->params['url']['sSearch'].'%', 
                         'City.name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                         'Commune.name LIKE' => '%'.$this->params['url']['sSearch'].'%' 
                     ]
                 ]
             ];

         }else{
             $conditions = [ 
                 'Zone.states_id' => 1,
             ];
         }

         $eEcho = 1;
         if( !empty($this->params['url']['sEcho']) ){
             $eEcho = $this->params['url']['sEcho'];
         }

         $limit = 10;
         if( !empty($this->params['url']['iDisplayLength']) ){
             $limit = $this->params['url']['iDisplayLength'];
         }

         $dataZones = $this->Zone->find('all', [
             'fields' => [
                 'Zone.id',
                 'Zone.zone',
                 'Zone.firebase_name',
                 'Zone.address',
                 'Zone.latitud',
                 'Zone.longitud',
                 'Region.name',
                 'City.name',
                 'Commune.name'
             ],
             'conditions' => $conditions,
             'offset' => intval($this->params['url']['iDisplayStart']),
             'limit' => intval($this->params['url']['iDisplayLength']),
             'recursive' => 1
         ]);

         $dataZonesCount = $this->Zone->find('all', [
             'fields' => [
                 'Zone.id',
                 'Zone.zone',
                 'Zone.firebase_name',
                 'Zone.address',
                 'Zone.latitud',
                 'Zone.longitud',
                 'Region.name',
                 'City.name',
                 'Commune.name'
             ],
             'conditions' => $conditions
         ]);

         foreach ($dataZones as $dataZone) {
             $option = '';
             $option.='<div class="btn-group">
                     <div class="dropdown">
                         <button class="btn btn-md btn-primary btn-active-primary dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                             Opciones <i class="dropdown-caret"></i>
                         </button>
                         <ul class="dropdown-menu" style="">
                             <li class="dropdown-header">Seleccione</li>';                                    
                             //$option.='<li><a href="'.Router::url( '/Dashboard/Nodos/Ver/'.$dataUser['User']['id'], false).'">Ver</a></li>';
                             $option.='<li><a href="'.Router::url( '/Dashboard/Nodos/Editar/'.$dataZone['Zone']['id'], false).'">Editar</a></li>';
                             $option.='<li><a class="deleteItem" data-item="'.$dataZone['Zone']['id'].'" href="#">Eliminar</a></li>';
                         $option.='</ul>
                     </div>
                 </div>';

             $dataTemp = [
                 'Zona' => $dataZone['Zone']['zone'],
                 'Firebase' => $dataZone['Zone']['firebase_name'],
                 'Direccion' => $dataZone['Zone']['address'],
                 'Region' => $dataZone['Region']['name'],
                 'Ciudad' => $dataZone['City']['name'],
                 'Comuna' => $dataZone['Commune']['name'],
                 'Latitud' => $dataZone['Zone']['latitud'],
                 'Longitud' => $dataZone['Zone']['longitud'],
                 'Button' => $option
             ];
             array_push($data, $dataTemp);
         }

         $results = [
             "sEcho" => $eEcho,
             "iTotalRecords" => count($dataZonesCount),
             "iTotalDisplayRecords" => count($dataZonesCount),
             "aaData" => $data
         ];
   
         echo json_encode($results);
      }
   }

   public function add(){

        if ($this->request->is('post')) {
            $this->loadModel('Zone');
            $this->Zone->create();
            $this->request->data['Zone']['states_id'] = 1;
            if ($this->Zone->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Nodos', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        $this->loadModel('Region');
        $this->loadModel('City');
        $this->loadModel('Commune');

        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);

        $citys = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);

        $communes = $this->Commune->find('list', [
            'fields' => [ 'Commune.id', 'Commune.name' ], 
            'conditions' => [ 'Commune.states_id' => 1 ],
            'order' => [ 'Commune.name ASC' ]
        ]);

        $this->set('communes', $communes);
        $this->set('regions', $regions);
        $this->set('citys', $citys);
        $this->setTitles('Nodos', 'Nuevo Nodo', Router::url('/Dashboard/Nodos', true));
   }

   public function edit($id=null){
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->loadModel('Zone');
            $this->Zone->id = $id;
            if ($this->Zone->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Nodos', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Nodos', 'action' => '']);
        }

        $this->loadModel('Zone');
        $nodo = $this->Zone->findById($id);

        if(!$nodo){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Nodos', 'action' => '']);
        }

        $this->request->data = $nodo;

        $this->loadModel('Region');
        $this->loadModel('City');
        $this->loadModel('Commune');

        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);

        $citys = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);

        $communes = $this->Commune->find('list', [
            'fields' => [ 'Commune.id', 'Commune.name' ], 
            'conditions' => [ 'Commune.states_id' => 1 ],
            'order' => [ 'Commune.name ASC' ]
        ]);

        $this->set('communes', $communes);
        $this->set('regions', $regions);
        $this->set('citys', $citys);

        $this->setTitles('Nodos', 'Editar Nodo', Router::url('/Dashboard/Nodos', true));
   }

   public function DeleteItem(){
        $data = [];
        $data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
        $data['clase'] = 'danger';
        $data['state'] = 2;

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('Zone');
            $this->loadModel('UserRange');

            $item = $this->Zone->find('all', [
                'conditions' => [
                    'Zone.states_id' => 1,
                    'Zone.id' => $this->request->data('id')
                ]
            ]);

            if($item){

                $this->Zone->updateAll(
                    ['Zone.states_id' => 2], 
                    [
                        'Zone.states_id' => 1,
                        'Zone.id' => $this->request->data('id')
                    ]
                );

                $this->UserRange->updateAll(
                    ['UserRange.states_id' => 2], 
                    [
                        'UserRange.states_id' => 1,
                        'UserRange.zones_id' => $this->request->data('id')
                    ]
                );

                $data['message'] = 'Datos guardados con éxito.';
                $data['clase'] = 'success';
                $data['state'] = 1;
            }

        }   
        echo json_encode($data);
    }

}

?>