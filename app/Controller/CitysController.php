<?php
App::uses('AppController', 'Controller');
class CitysController extends AppController {

    public function index(){
        $this->setTitles('Ciudades', 'Listado', Router::url('/Dashboard/Ciudades', true));
    }

    public function getCitys(){
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('City');

            $conditions = [];
            if(!empty($this->params['url']['sSearch'])){ 
                $conditions = [
                    'City.states_id' => 1,
                    [   
                        'OR' => [ 
                            'City.name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                            'Region.name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                            'Region.code LIKE' => '%'.$this->params['url']['sSearch'].'%'
                        ]
                    ]

                ];
            }else{
                $conditions = [ 
                    'City.states_id' => 1
                ];
            }

            $eEcho = 1;
            if( !empty($this->params['url']['sEcho']) ){
                $eEcho = $this->params['url']['sEcho'];
            }

            $limit = 10;
            if( !empty($this->params['url']['iDisplayLength']) ){
                $limit = $this->params['url']['iDisplayLength'];
            }

            $dataCiudades = $this->City->find('all', [
                'fields' => [
                    'City.id',
                    'City.name',
                    'Region.name',
                    'Region.code'
                ],
                'conditions' => $conditions,
                'offset' => intval($this->params['url']['iDisplayStart']),
                'limit' => intval($this->params['url']['iDisplayLength']),
                'recursive' => 1
            ]);

            $dataCiudadesCount = $this->City->find('all', [
                'fields' => [
                    'City.id',
                    'City.name',
                    'Region.name',
                    'Region.code'
                ],
                'conditions' => $conditions
            ]);

            foreach ($dataCiudades as $dataCiudad) {

                $option = '';
                $option.='<div class="btn-group">
                        <div class="dropdown">
                            <button class="btn btn-md btn-primary btn-active-primary dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                Opciones <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu" style="">
                                <li class="dropdown-header">Seleccione</li>';                                    
                                //$option.='<li><a href="'.Router::url( '/Ciudades/Ver/'.$dataCiudad['City']['id'], false).'">Ver</a></li>';
                                $option.='<li><a href="'.Router::url( '/Dashboard/Ciudades/Editar/'.$dataCiudad['City']['id'], false).'">Editar</a></li>';
                                $option.='<li><a class="deleteItem" data-item="'.$dataCiudad['City']['id'].'" href="#">Eliminar</a></li>';
                            $option.='</ul>
                        </div>
                    </div>';

                $dataTemp = [
                    'Ciudad' => $dataCiudad['City']['name'],
                    'Region' => $dataCiudad['Region']['name'].' ( '.$dataCiudad['Region']['code'].' )',
                    'Button' => $option
                ];
                array_push($data, $dataTemp);
            }

            $results = [
                "sEcho" => $eEcho,
                "iTotalRecords" => count($dataCiudadesCount),
                "iTotalDisplayRecords" => count($dataCiudadesCount),
                "aaData" => $data
            ];
      
            echo json_encode($results);
       }
    }

    public function add(){
        if ($this->request->is('post')) {
            $this->loadModel('City');
            $this->City->create();
            $this->request->data['City']['states_id'] = 1;
            $this->request->data['City']['register_by'] = $this->Auth->User('id');
            if ($this->City->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        $this->loadModel('Region');
        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);
        $this->set('Regiones', $regions);
        $this->setTitles('Ciudades', 'Nueva Ciudad', Router::url('/Dashboard/Ciudades', true));
    }

    public function edit($id = null){
        $this->loadModel('City');
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['City']['register_by'] = $this->Auth->User('id');
            $this->City->id = $id;
            if ($this->City->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
        }

        $Ciudad = $this->City->findById($id);

        if(!$Ciudad){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
        }

        $this->request->data = $Ciudad;

        $this->loadModel('Region');
        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);
        $this->set('Regiones', $regions);
        $this->setTitles('Ciudades', 'Editar Ciudad', Router::url('/Dashboard/Ciudades', true));

    }

    public function view($id = null){
        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
        }

        $Ciudad = $this->City->findById($id);

        if(!$Ciudad){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Ciudades', 'action' => '']);
        }

        $this->request->data = $Ciudad;

        $this->loadModel('Region');
        $regions = $this->Region->find('list', [
            'fields' => [ 'Region.id', 'Region.name' ], 
            'conditions' => [ 'Region.states_id' => 1 ],
            'order' => [ 'Region.order ASC' ]
        ]);
        $this->set('Regiones', $regions);

    }

    public function DeleteItem(){
        $data = [];
        $data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
        $data['clase'] = 'danger';
        $data['state'] = 2;

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('City');

            $item = $this->City->find('all', [
                'conditions' => [
                    'City.states_id' => 1,
                    'City.id' => $this->request->data('id')
                ]
            ]);

            if($item){

                $this->City->updateAll(
                    ['City.states_id' => 2], 
                    [
                        'City.states_id' => 1,
                        'City.id' => $this->request->data('id')
                    ]
                );
                $data['message'] = 'Datos guardados con éxito.';
                $data['clase'] = 'success';
                $data['state'] = 1;
            }

        }   
        echo json_encode($data);
    }

}
?>