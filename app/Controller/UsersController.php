<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'add');
    }

    public function index() {
        $this->setTitles('Usuarios', 'Listado', Router::url('/Dashboard/Usuarios', true));
    }

    public function getUsers() {

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('User');

            $conditions = [];
            if(!empty($this->params['url']['sSearch'])){
                
                $conditions = [
                    'User.states_id' => 1,
                    [   
                        'OR' => [ 
                            'User.nombres LIKE' => '%'.$this->params['url']['sSearch'].'%', 
                            'User.apellidos LIKE' => '%'.$this->params['url']['sSearch'].'%', 
                            'User.email LIKE' => '%'.$this->params['url']['sSearch'].'%',
                            'User.telefono LIKE' => '%'.$this->params['url']['sSearch'].'%' 
                        ]
                    ]
                ];

            }else{
                $conditions = [ 
                    'User.states_id' => 1
                ];
            }

            $eEcho = 1;
            if( !empty($this->params['url']['sEcho']) ){
                $eEcho = $this->params['url']['sEcho'];
            }

            $limit = 10;
            if( !empty($this->params['url']['iDisplayLength']) ){
                $limit = $this->params['url']['iDisplayLength'];
            }

            $dataUsers = $this->User->find('all', [
                'fields' => [
                    'User.id',
                    'User.nombres',
                    'User.apellidos',
                    'User.telefono',
                    'User.email'
                ],
                'conditions' => $conditions,
                'offset' => intval($this->params['url']['iDisplayStart']),
                'limit' => intval($this->params['url']['iDisplayLength']),
                'recursive' => 1
            ]);

            $dataUsersCount = $this->User->find('all', [
                'fields' => [
                    'User.id',
                    'User.nombres',
                    'User.apellidos',
                    'User.telefono',
                    'User.email'
                ],
                'conditions' => $conditions
            ]);

            foreach ($dataUsers as $dataUser) {
                $option = '';
                $option.='<div class="btn-group">
                        <div class="dropdown">
                            <button class="btn btn-md btn-primary btn-active-primary dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                Opciones <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu" style="">
                                <li class="dropdown-header">Seleccione</li>';                                    
                                //$option.='<li><a href="'.Router::url( '/Usuarios/Ver/'.$dataUser['User']['id'], false).'">Ver</a></li>';
                                $option.='<li><a href="'.Router::url( '/Dashboard/Usuarios/Editar/'.$dataUser['User']['id'], false).'">Editar</a></li>';
                                $option.='<li><a class="deleteItem" data-item="'.$dataUser['User']['id'].'" href="#">Eliminar</a></li>';
                            $option.='</ul>
                        </div>
                    </div>';

                $dataTemp = [
                    'Nombres' => $dataUser['User']['nombres'],
                    'Apellidos' => $dataUser['User']['apellidos'],
                    'Email' => $dataUser['User']['email'],
                    'Telefono' => $dataUser['User']['telefono'],
                    'Button' => $option
                ];
                array_push($data, $dataTemp);
            }

            $results = [
                "sEcho" => $eEcho,
                "iTotalRecords" => count($dataUsersCount),
                "iTotalDisplayRecords" => count($dataUsersCount),
                "aaData" => $data
            ];
      
            echo json_encode($results);
        }
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['password'] = 12345;
            $this->request->data['User']['username'] = $this->request->data['User']['email'];

            $emailWord = trim($this->request->data['User']['email']);
            $emailWord = str_replace(" ", "", $emailWord);

            $userExist = $this->User->find('first', [
                'conditions' => [
                    'User.email LIKE' => '%'.$emailWord.'%'
                ]
            ]);

            if($userExist){
                $this->Flash->bad('El email ingresado ya se encuentra registrado, intente con otro.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
            }

            if ($this->User->save($this->request->data['User'])) {
                $this->request->data['User'] = array_merge(
                    $this->request->data['User'],
                    array('id' => $this->User->id)
                );
                unset($this->request->data['User']['password']);
                $this->Auth->login($this->request->data['User']);

                $this->Flash->success('Usuario registrado y logeado, su nombre de suaurio es el email ingresado y su clave inicial para logearte es 12345.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
                
            }
            $this->Flash->bad('Error al realizar el registro, intentelo nuevamente mas tarde.');
            return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
        }
    }

    public function addUser() {
        if ($this->request->is('post')) {
            $this->User->create();
            
            if(!empty($this->request->data['User']['pass1'])){
                if(empty($this->request->data['User']['pass2'])){
                    $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
                }
            }

            if(!empty($this->request->data['User']['pass2'])){
                if(empty($this->request->data['User']['pass1'])){
                    $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
                }
            }

            if($this->request->data['User']['pass1']!=$this->request->data['User']['pass2']){
                $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
            }

            if(empty($this->request->data['User']['pass2']) && empty($this->request->data['User']['pass1'])){
                $this->request->data['User']['password'] = $this->request->data['User']['pass2'];
            }else{
                $this->request->data['User']['password'] = 12345;
            }

            $this->request->data['User']['username'] = $this->request->data['User']['email'];

            $emailWord = trim($this->request->data['User']['email']);
            $emailWord = str_replace(" ", "", $emailWord);

            $userExist = $this->User->find('first', [
                'conditions' => [
                    'User.email LIKE' => '%'.$emailWord.'%'
                ]
            ]);

            if($userExist){
                $this->Flash->error('El email ingresado ya se encuentra registrado, intente con otro.');
                return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
            }

            if ($this->User->save($this->request->data['User'])) {
                $this->request->data['User'] = array_merge(
                    $this->request->data['User'],
                    array('id' => $this->User->id)
                );
                unset($this->request->data['User']['password']);
                $this->Auth->login($this->request->data['User']);

                $this->Flash->ok('Usuario registrado y logeado, tu clave inicial para logearte es 12345.');
                return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
                
            }
            $this->Flash->error('Error al realizar el registro, intentelo nuevamente mas tarde.');
            return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
        }

        $this->setTitles('Usuarios', 'Nuevo Usuario', Router::url('/Dashboard/Usuarios', true));
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Flash->success('Usuario Logeado.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
            }
            $this->Flash->bad('Usuario o contraseña incorrecta.');
            return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
        }
    }

    public function logout() {
        header('pragma: no-cache'); 
        header('Cache-Control: no-cache, must-revalidate');
        $this->response->disableCache();
        $this->Session->delete('Auth.User');
        $this->Session->delete('User');
        $this->Session->destroy();
        $this->Cookie->destroy();
        return $this->redirect($this->Auth->logout());
    }

    public function reestablecer() {
        $this->set(array('title_for_layout' => 'Reestablecer contraseña'));
        $this->layout = 'reestablecer';
    }

    public function edit() {
        if ($this->request->is('post') || $this->request->is('put')) {
           

            if(!empty($this->request->data['User']['password'])){
                if(empty($this->request->data['User']['password2'])){
                    $this->Flash->bad('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
                }
            }

            if(!empty($this->request->data['User']['password2'])){
                if(empty($this->request->data['User']['password'])){
                    $this->Flash->bad('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
                }
            }

            if($this->request->data['User']['password']!=$this->request->data['User']['password2']){
                $this->Flash->bad('Las contraseñas deben ser identicas, intentelo nuevamente.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
            }

            if(empty($this->request->data['User']['password2']) && empty($this->request->data['User']['password'])){
                unset($this->request->data['User']['password']);
            }

            $emailWord = trim($this->request->data['User']['email']);
            $emailWord = str_replace(" ", "", $emailWord);

            $userExist = $this->User->find('first', [
                'conditions' => [
                    'User.email LIKE' => '%'.$emailWord.'%',
                    'User.id <>' => $this->Auth->user('id')
                ]
            ]);

            if($userExist){
                $this->Flash->bad('El email ingresado ya se encuentra registrado, intente con otro.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
            }

            $this->request->data['User']['username'] = $this->request->data['User']['email'];

            $this->request->data['User']['id'] = $this->Auth->user('id');
            if ($this->User->save($this->request->data['User'])) {

                $this->request->data['User'] = array_merge(
                    $this->request->data['User'],
                    array('id' => $this->Auth->user('id'))
                );
                unset($this->request->data['User']['password']);
                $this->Auth->login($this->request->data['User']);

                $this->Flash->success('Usuario editado con éxito.');
                return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
                
            }
            $this->Flash->bad('Error al realizar la edición, intentelo nuevamente mas tarde.');
            return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
        }

    }

    public function editar($id = null) {

        if ($this->request->is('put') || $this->request->is('post')) {
            
            if(!empty($this->request->data['User']['pass1'])){
                if(empty($this->request->data['User']['pass2'])){
                    $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
                }
            }

            if(!empty($this->request->data['User']['pass2'])){
                if(empty($this->request->data['User']['pass1'])){
                    $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                    return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
                }
            }

            if($this->request->data['User']['pass1']!=$this->request->data['User']['pass2']){
                $this->Flash->error('Las contraseñas deben ser identicas, intentelo nuevamente.');
                return $this->redirect(['controller'=> '/Dashboard/Usuarios', 'action' => 'index']);
            }

            if(empty($this->request->data['User']['pass2']) && empty($this->request->data['User']['pass1'])){
                unset($this->request->data['User']['password']);
            }

            $this->request->data['User']['username'] = $this->request->data['User']['email'];
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Usuarios', 'action' => '']);
            }
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Usuarios', 'action' => 'Listado']);
        }

        $usuario = $this->User->findById($id);

        if(!$usuario){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Usuarios', 'action' => 'Listado']);
        }

        $this->request->data = $usuario;
        $this->setTitles('Usuarios', 'Editar Usuario', Router::url('/Dashboard/Usuarios', true));

    }

    public function editUser($id=null) {

        if ($this->request->is('post') || $this->request->is('put')) {

            $userExist = $this->User->find('first', [
                'conditions' => [
                    'User.email LIKE' => '%'.$this->request->data['User']['email'].'%',
                    'User.id <>' => $this->Auth->user('id')
                ]
            ]);

            if($userExist){
                $this->Flash->error('El email ingresado ya se encuentra registrado, intente con otro.');
                return $this->redirect(['controller'=> 'Mi-Perfil', 'action' => 'editUser', $id]);
            }

            if(!empty($this->request->data['User']['pass1'])){
                
                if(empty($this->request->data['User']['pass2'])){
                    $this->Flash->error('Las contraselas deben ser iguales.');
                    return $this->redirect(['controller'=> 'Mi-Perfil', 'action' => 'editUser', $id]);
                }

                if($this->request->data['User']['pass1']!=$this->request->data['User']['pass2']){
                    $this->Flash->error('Las contraselas deben ser iguales.');
                    return $this->redirect(['controller'=> 'Mi-Perfil', 'action' => 'editUser', $id]);
                }
            }

            $this->request->data['User']['password'] = $this->request->data['User']['pass1'];
            $this->request->data['User']['id'] = $id;
            $this->User->id = $id;
            if($this->User->save($this->request->data)){
                $this->Flash->ok('Has actualizado tus datos con éxito.');
                return $this->redirect(['controller'=> 'Dashboard', 'action' => '']);
            }
            
        }

        if(!$id){
            $this->Flash->bad('Error, intentelo nuevamente.');
            return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
        }

        $this->loadModel('User');
        $usuario = $this->User->findById($this->Auth->user('id'));

        if(!$usuario){
            $this->Flash->bad('Error, intentelo nuevamente.');
            return $this->redirect(['controller'=> 'Monitoreo', 'action' => 'index']);
        }

        $this->request->data = $usuario;

    }

    public function DeleteItem(){
        $data = [];
        $data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
        $data['clase'] = 'danger';
        $data['state'] = 2;

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('User');

            $user = $this->User->find('all', [
                'conditions' => [
                    'User.states_id' => 1,
                    'User.id' => $this->request->data('id')
                ]
            ]);

            if($user){

                $this->User->updateAll(
                    ['User.states_id' => 2], 
                    [
                        'User.states_id' => 1,
                        'User.id' => $this->request->data('id')
                    ]
                );
                $data['message'] = 'Datos guardados con éxito.';
                $data['clase'] = 'success';
                $data['state'] = 1;
            }

        }   
        echo json_encode($data);
    }
    
}

?>