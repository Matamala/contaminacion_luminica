<?php
App::uses('AppController', 'Controller');
class RegionsController extends AppController {

   public function index(){
   		$this->setTitles('Regiones', 'Listado', Router::url('/Dashboard/Regiones', true));
   }

   public function getRegions(){
        $this->autoRender = false;
        if($this->request->is('ajax')){   
            $data = [];
            $this->loadModel('Region');
            $conditions = [];
            if(!empty($this->params['url']['sSearch'])){ 
                $conditions = [
                    'Region.states_id' => 1,
                    [   
                        'OR' => [ 
                            'Region.name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                            'Region.code LIKE' => '%'.$this->params['url']['sSearch'].'%'
                        ]
                    ]

                ];
            }else{
                $conditions = [ 
                    'Region.states_id' => 1
                ];
            }

            $eEcho = 1;
            if( !empty($this->params['url']['sEcho']) ){
                $eEcho = $this->params['url']['sEcho'];
            }

            $limit = 10;
            if( !empty($this->params['url']['iDisplayLength']) ){
                $limit = $this->params['url']['iDisplayLength'];
            }

            $dataRegiones = $this->Region->find('all', [
                'fields' => [
                    'Region.id',
                    'Region.name',
                    'Region.order',
                    'Region.code'
                ],
                'conditions' => $conditions,
                'offset' => intval($this->params['url']['iDisplayStart']),
                'limit' => intval($this->params['url']['iDisplayLength']),
                'recursive' => 1
            ]);

            $dataRegionesCount = $this->Region->find('all', [
                'fields' => [
                    'Region.id',
                    'Region.name',
                    'Region.order',
                    'Region.code'
                ],
                'conditions' => $conditions
            ]);

            foreach ($dataRegiones as $dataRegion) {

                $option = '';
                $option.='<div class="btn-group">
                        <div class="dropdown">
                            <button class="btn btn-md btn-primary btn-active-primary dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                Opciones <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu" style="">
                                <li class="dropdown-header">Seleccione</li>';                                    
                                $option.='<li><a href="'.Router::url( '/Dashboard/Regiones/Editar/'.$dataRegion['Region']['id'], false).'">Editar</a></li>';
                                $option.='<li><a class="deleteItem" data-item="'.$dataRegion['Region']['id'].'" href="#">Eliminar</a></li>';
                            $option.='</ul>
                        </div>
                    </div>';

                $dataTemp = [
                    'Region' => $dataRegion['Region']['name'],
                    'Code' => $dataRegion['Region']['code'],
                    'Order' => $dataRegion['Region']['order'],
                    'Button' => $option
                ];
                array_push($data, $dataTemp);
            }

            $results = [
                "sEcho" => $eEcho,
                "iTotalRecords" => count($dataRegionesCount),
                "iTotalDisplayRecords" => count($dataRegionesCount),
                "aaData" => $data
            ];
      
            echo json_encode($results);
        }
    }

   public function add(){
        if ($this->request->is('post')) {
            $this->loadModel('Region');
            $this->Region->create();
            $this->request->data['Region']['states_id'] = 1;
            $this->request->data['Region']['register_by'] = $this->Auth->User('id');
            if ($this->Region->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dasboard/Regiones', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        $this->setTitles('Regiones', 'Nueva Región', Router::url('/Dashboard/Regiones', true));
    }

    public function edit($id = null){
        $this->loadModel('Region');
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Region']['register_by'] = $this->Auth->User('id');
            $this->Region->id = $id;
            if ($this->Region->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Regiones', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dasboard/Regiones', 'action' => '']);
        }

        $region = $this->Region->findById($id);

        if(!$region){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dasboard/Regiones', 'action' => '']);
        }

        $this->request->data = $region;
        $this->setTitles('Regiones', 'Editar Región', Router::url('/Dashboard/Regiones', true));

    }

    public function view($id = null){
        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dasboard/Regiones', 'action' => '']);
        }

        $this->loadModel('Region');
        $region = $this->Region->findById($id);

        if(!$region){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dasboard/Regiones', 'action' => '']);
        }

        $this->request->data = $region;

    }

    public function DeleteItem(){
        $data = [];
        $data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
        $data['clase'] = 'danger';
        $data['state'] = 2;

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('Region');

            $item = $this->Region->find('all', [
                'conditions' => [
                    'Region.states_id' => 1,
                    'Region.id' => $this->request->data('id')
                ]
            ]);

            if($item){

                $this->Region->updateAll(
                    ['Region.states_id' => 2], 
                    [
                        'Region.states_id' => 1,
                        'Region.id' => $this->request->data('id')
                    ]
                );
                $data['message'] = 'Datos guardados con éxito.';
                $data['clase'] = 'success';
                $data['state'] = 1;
            }

        }   
        echo json_encode($data);
    }

}

?>