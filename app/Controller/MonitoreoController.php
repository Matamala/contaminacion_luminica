<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class MonitoreoController extends AppController {
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('mediciones', 'contact', 'about', 'addMeasure','addMeasure2', 'envioEmail');
    }

    public function index(){
        $this->layout = 'maps';
        $this->set(['title_for_layout' => 'Home']);

        //Set Usuario
        $this->loadModel('User');
        if(!empty($this->Auth->user('id'))){
        	$dataUser = $this->User->findById($this->Auth->user('id'));	
            $this->request->data['User']['nombres'] = $dataUser['User']['nombres'];
            $this->request->data['User']['apellidos'] = $dataUser['User']['apellidos'];
            $this->request->data['User']['email'] = $dataUser['User']['email'];
            $this->request->data['User']['telefono'] = $dataUser['User']['telefono'];

            $this->loadModel('UserRange');
            $zonesRange = $this->UserRange->find('all', [
                'fields' => [
                    'UserRange.zones_id',
                    'UserRange.range_start',
                    'UserRange.range_finished'
                ],
                'conditions' => [
                    'UserRange.users_id' => $this->Auth->user('id'),
                    'UserRange.states_id' => 1
                ]
            ]);

            foreach ($zonesRange as $rango) {
                $this->request->data['Zone'][$rango['UserRange']['zones_id']] = 1;
                $this->request->data['Range']['range_start_'.$rango['UserRange']['zones_id']] = $rango['UserRange']['range_start'];
                $this->request->data['Range']['range_finished_'.$rango['UserRange']['zones_id']] = $rango['UserRange']['range_finished'];
            }

            

        }else{
        	$dataUser = "";
        }

        $this->loadModel('Zone');
        $this->loadModel('Medidas');
        $zones = $this->Zone->find('all',[
            'conditions' => [
                'Zone.states_id' => 1
            ],
            'recursive' => -2
        ]);

        $dataZones = [];
        $i=0;
        foreach ($zones as $zona) {
            $dataZones[$i] = $zona;

            $medidas = $this->Medidas->find('all', [
                'conditions' => [
                    'Medidas.states_id' => 1,
                    'Medidas.zones_id' => $zona['Zone']['id']
                ],
                'order' => [ 'Medidas.created DESC' ],
                'limit' => 10
            ]);
            $dataZones[$i]['Medidas'] = $medidas; 
            $i++;
        }

        $this->set('Zonas', $dataZones);
        $this->set('dataUser', $dataUser);
    }

    public function mediciones(){
        $this->autoRender = false;
        $data['states'] = 2;
        if($this->request->is('ajax')){

            $this->loadModel('Zone');
            $this->loadModel('Medidas');

            $id_zone = $this->request->data('itemId');
            $tipo = $this->request->data('tipo');

            
            //$id_zone = 8;
            //$tipo = 'mes';

            $zona = $this->Zone->find('all', [
                'conditions' => [
                    'Zone.id' => $id_zone,
                    'Zone.states_id' => 1
                ],
                'recursive' => -2
            ]);

            switch($tipo){

                case 'ahora':
                    $hoy = date('Y-m-d');
                    $mediciones = $this->Medidas->find('all', [
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created LIKE' => '%'.$hoy.'%'
                        ],
                        'order' => [ 'Medidas.created DESC' ],
                        'limit' => 25,
                        'recursive' => -2
                    ]);
                break;

                case 'dia':
                    $dia = date('Y-m-d', strtotime($this->request->data('date')));
                    $mediciones = $this->Medidas->find('all', [
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created BETWEEN ? AND ?' => [$dia.' 00:00:00', $dia.' 05:00:00']  
                        ],
                        'order' => [ 'Medidas.created ASC' ],
                        'recursive' => -2
                    ]);
                break;

                case 'semana':/* POR SEMANA */
                    #$year = date("Y");
                    #$week = date("W");
                    # obtenemos el timestamp del primer dia del año
                    #$timestamp = mktime(0, 0, 0, 1, 1, $year);
                    # sumamos el timestamp de la suma de las semanas actuales
                    #$timestamp+= $week*7*24*60*60;
                    # restamos la posición inicial del primer dia del año
                    #$ultimoDia = $timestamp-date("w", mktime(0, 0, 0, 1, 1, $year))*24*60*60;
                    # le restamos los dias que hay hasta llegar al lunes
                    #$primerDia = $ultimoDia-86400*(date('N',$ultimoDia)-1);
                    # mostramos la fecha correcta
                    #$diaInicio = date("Y-m-d", $primerDia);
                    #$diaFinal = date("Y-m-d", $ultimoDia);

                    $diaSemana = explode(" al ", $this->request->data('date'));
                    $diaInicio = date('Y-m-d', strtotime($diaSemana[0]));
                    $diaFinal = date('Y-m-d', strtotime($diaSemana[1]));

                    $mediciones = $this->Medidas->find('all', [
                        'fields' => [
                            'DATE_FORMAT(Medidas.fecha, "%Y-%m-%d") AS fecha',
                            'AVG(Medidas.temp) AS temp',
                            'AVG(Medidas.hz) AS hz',
                            'AVG(Medidas.hpa) AS hpa',
                        ],
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created BETWEEN ? AND ?' => [$diaInicio, $diaFinal]  
                        ],
                        'order' => [ 'Medidas.created ASC' ],
                        'recursive' => -2,
                        'group' => ["DATE_FORMAT(Medidas.fecha, '%Y-%m-%d')"]
                    ]);
                break;

                case 'mes':
                    $date = explode('-', $this->request->data('date'));
                    $date = $date[1].'-'.$date[0];
                    //echo 'La fecha es : '.$date.'----';

                    $month = $date;
                    $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
                    $last_day = date('Y-m-d', strtotime("{$aux} - 1 day"));

                    //echo 'Medidas.created BETWEEN ? AND ? => ['.$date.'-01 ,'.$last_day.']';

                    $mediciones = $this->Medidas->find('all', [
                        'fields' => [
                            'DATE_FORMAT(Medidas.fecha, "%Y-%m-%d") AS fecha',
                            'AVG(Medidas.temp) AS temp',
                            'AVG(Medidas.hz) AS hz',
                            'AVG(Medidas.hpa) AS hpa',
                        ],
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created BETWEEN ? AND ?' => [$date.'-01'  , $last_day],
                            'TIME(Medidas.created) BETWEEN ? AND ?' => ['00:00:00'  , '05:00:00'],  
                        ],
                        'order' => [ 'Medidas.created ASC' ],
                        'recursive' => -2,
                        'group' => ["DATE_FORMAT(Medidas.fecha, '%Y-%m-%d')"]
                    ]);
                break;

                case 'ano':/* POR AÑO */
                    $date = $this->request->data('date');
                    $mediciones = $this->Medidas->find('all', [
                        'fields' => [
                            'DATE_FORMAT(Medidas.fecha, "%m%") AS fecha',
                            'AVG(Medidas.temp) AS temp',
                            'AVG(Medidas.hz) AS hz',
                            'AVG(Medidas.hpa) AS hpa',
                        ],
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created BETWEEN ? AND ?' => [$date.'01-01'  , $date.'-12-31'],
                            'TIME(Medidas.created) BETWEEN ? AND ?' => ['00:00:00'  , '05:00:00'],    
                        ],
                        'order' => [ 'Medidas.created ASC' ],
                        'recursive' => -2,
                        'group' => ["DATE_FORMAT(Medidas.created, '%m%')"]
                    ]);
                break;

                default:
                    $hoy = date('Y-m-d');
                    $mediciones = $this->Medidas->find('all', [
                        'conditions' => [
                            'Medidas.zones_id' => $id_zone,
                            'Medidas.states_id' => 1,
                            'Medidas.created LIKE' => '%'.$hoy.'%'
                        ],
                        'order' => [ 'Medidas.created DESC' ],
                        'limit' => 25,
                        'recursive' => -2
                    ]);
                break;

            }

            $i=0;
            $contData = [];
            $categorias = [];
            $tempData = [];
            $contData = [];
            $presionData = [];
            $dataCont = [];
            $dataLum = [];
            $dataPresion = [];

            $meses = [];
            $meses['1'] = 'Enero';
            $meses['01'] = 'Enero';
            $meses['2'] = 'Febrero';
            $meses['02'] = 'Febrero';
            $meses['3'] = 'Marzo';
            $meses['03'] = 'Marzo';
            $meses['4'] = 'Abril';
            $meses['04'] = 'Abril';
            $meses['5'] = 'Mayo';
            $meses['05'] = 'Mayo';
            $meses['6'] = 'Junio';
            $meses['06'] = 'Junio';
            $meses['7'] = 'Julio';
            $meses['07'] = 'Julio';
            $meses['8'] = 'Agosto';
            $meses['08'] = 'Agosto';
            $meses['9'] = 'Septiembre';
            $meses['09'] = 'Septiembre';
            $meses['10'] = 'Octubre';
            $meses['11'] = 'Noviembre';
            $meses['12'] = 'Diciembre';

            $n = 23;
            foreach ($mediciones as $Meidida) {

               if($tipo=='ahora'){
        
                    if($i==0){
                        $temperatura = '{period: "'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hz'], 2, '.', '').'"}';
                        $contaminacion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['temp'], 2, '.', '').'"}';
                        $presion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '').'"}';
                        $i++;
                    }else{
                        $temperatura.= ',{period: "'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['temp'], 2, '.', '').'"}';
                        $contaminacion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hz'], 2, '.', '').'"}';
                        $presion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '').'"}';
                        $i++;
                    }

                    if($i>1){
                        //$categorias[] = date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']));
                        //$categorias= date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']));    

                        array_unshift($categorias, date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])));

                        $tempData[] = [
                            'name' => 'Medida',
                            'data' => number_format((float)$Meidida['Medidas']['temp'], 2, '.', '')
                        ];

                        $contData[] = [
                            'name' => 'Medida',
                            'data' => number_format((float)$Meidida['Medidas']['hz'], 2, '.', '')
                        ];

                        $presionData[] = [
                            'name' => 'Medida',
                            'data' => number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '')
                        ];

                        $dataCont[] = [
                           'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                           'contaminacion'  =>  number_format((float)$Meidida['Medidas']['hz'], 2, '.', '')
                        ];

                        $dataLum[] = [
                           'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                           'temperatura'  => number_format((float)$Meidida['Medidas']['temp'], 2, '.', '')
                        ];

                        $dataPresion[] = [
                           'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                           'presion'  => number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '')
                        ];
                        $n++;
                    }

                    
                }

                if($tipo=='dia'){
        
                    if($i==0){
                        $temperatura = '{period: "'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hz'], 2, '.', '').'"}';
                        $contaminacion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['temp'], 2, '.', '').'"}';
                        $presion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '').'"}';
                        $i++;
                    }else{
                        $temperatura.= ',{period: "'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['temp'], 2, '.', '').'"}';
                        $contaminacion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hz'], 2, '.', '').'"}';
                        $presion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])).'", up:"'.number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '').'"}';
                    }

                    $categorias[] = date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha']));
                        
                    $tempData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida['Medidas']['temp'], 2, '.', '')
                    ];

                    $contData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida['Medidas']['hz'], 2, '.', '')
                    ];

                    $presionData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '')
                    ];

                    $dataCont[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                       'contaminacion'  =>  number_format((float)$Meidida['Medidas']['hz'], 2, '.', '')
                    ];

                    $dataLum[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                       'temperatura'  => number_format((float)$Meidida['Medidas']['temp'], 2, '.', '')
                    ];

                    $dataPresion[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida['Medidas']['fecha'])),
                       'presion'  => number_format((float)$Meidida['Medidas']['hpa'], 2, '.', '')
                    ];

                    $n--;
                }


                if($tipo=='semana' || $tipo=='mes' || $tipo=='ano'){

                    if($i==0){
                        $temperatura = '{period: "'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['hz'], 2, '.', '').'"}';
                        $contaminacion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['temp'], 2, '.', '').'"}';
                        $presion = '{period:"'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['hpa'], 2, '.', '').'"}';
                        $i++;
                    }else{
                        $temperatura.= ',{period: "'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['temp'], 2, '.', '').'"}';
                        $contaminacion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['hz'], 2, '.', '').'"}';
                        $presion.= ',{period:"'.date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])).'", up:"'.number_format((float)$Meidida[0]['hpa'], 2, '.', '').'"}';
                    }

                    if($tipo=='mes'){
                        $categorias[] = date('d-m-Y', strtotime($Meidida[0]['fecha']));
                    }

                    if($tipo=='ano'){
                        $categorias[] = $meses[date('m', strtotime($Meidida[0]['fecha']))];
                    }

                    if($tipo=='semana'){
                        $categorias[] = date('d-m-Y', strtotime($Meidida[0]['fecha']));
                    }

                    $tempData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida[0]['temp'], 2, '.', '')
                    ];

                    $contData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida[0]['hz'], 2, '.', '')
                    ];

                    $presionData[] = [
                        'name' => 'Medida',
                        'data' => number_format((float)$Meidida[0]['hpa'], 2, '.', '')
                    ];

                    $dataCont[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])),
                       'contaminacion'  =>  number_format((float)$Meidida[0]['hz'], 2, '.', '')
                    ];

                    $dataLum[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])),
                       'temperatura'  => number_format((float)$Meidida[0]['temp'], 2, '.', '')
                    ];

                    $dataPresion[] = [
                       'periodo'  => date('d-m-Y H:i:s', strtotime($Meidida[0]['fecha'])),
                       'presion'  => number_format((float)$Meidida[0]['hpa'], 2, '.', '')
                    ];
                }
            
            }

            $data['zonaname'] = $zona[0]['Zone']['zone'];
            $data['seriesCont'] = $contData;
            $data['seriesTemp'] = $tempData;
            $data['seriesPresion'] = $presionData;
            $data['mediciones'] = $mediciones;
            $data['temperatura'] = $dataLum;
            $data['presion'] = $dataPresion;
            $data['contaminacion'] = $dataCont;
            $data['categorias'] = $categorias;
            $data['zona'] = $zona;
            $data['states'] = 1;
            //$this->request->data('itemId')
            //$this->request->data('idFb')
        }
        echo json_encode($data);
    }

    public function contact(){
        $this->set(['title_for_layout' => 'Contacto']);
    }

    public function about(){
        $this->set(['title_for_layout' => 'Sobre Nosotros']);
    }

    public function addMeasure(){
        $this->autoRender = false;
        $this->loadModel('Medidas');
        $this->loadModel('Zone');

        if(!empty($this->request->data('NODO'))){

            $findZone = $this->Zone->find('all', [
                'conditions' => [
                    'Zone.firebase_name' => $this->request->data('NODO'),
                    'Zone.states_id' => 1
                ],
                'recursive' => -2
            ]);

            if($findZone){
                $data = [];
                $data['fecha'] = $this->request->data('FECHA');
                $data['nodo'] = $this->request->data('NODO');
                $data['zones_id'] = $findZone[0]['Zone']['id'];
                $data['hz'] = $this->request->data('Msqm');
                $data['temp'] = $this->request->data('TEMP');
                $data['hpa'] = $this->request->data('hPa');
                $data['latitud'] = $this->request->data('latitud');
                $data['longitud'] = $this->request->data('longitud');
                $data['states_id'] = 1;
                $this->Medidas->create();
                
                if($this->Medidas->save($data)){
                    
                    $msqm = $this->request->data('Msqm');
                    $fecha = date('Y-m-d');
                    $this->loadModel('UserRange');
                    $dataCompacts = $this->UserRange->find('all', [
                        'conditions' => [
                            'UserRange.range_finished >' => $msqm,
                            'UserRange.zones_id' => $findZone[0]['Zone']['id'],
                            'UserRange.states_id' => 1
                        ]
                    ]); 

                    foreach ($dataCompacts as $dataCompact) {    
                        
                        if($dataCompact['UserRange']['time_finished']<$dataCompact['UserRange']['time_start']){
                            $fechaInicio = date ( 'Y-m-d H:i:s' , strtotime ($fecha.' '.$dataCompact['UserRange']['time_start']));
                            $nuevafecha = date("Y-m-d H:i:s", strtotime($fecha." ".$dataCompact['UserRange']['time_finished']."+ 1 days"));      
                            $fechaFinal = $nuevafecha;
                        }else{
                            $fechaInicio = date ( 'Y-m-d H:i:s' , strtotime ($fecha.' '.$dataCompact['UserRange']['time_start']));
                            $fechaFinal = date ( 'Y-m-d H:i:s' , strtotime ($fecha.' '.$dataCompact['UserRange']['time_finished']));
                        }    

                        
                        $fechaCalculo = date('Y-m-d H:i:s', strtotime($this->request->data('FECHA')));     
                        if($fechaCalculo>=$fechaInicio && $fechaCalculo<=$fechaFinal){
                            $nombre_completo = $dataCompact['User']['nombres'].' '.$dataCompact['User']['apellidos'];
                            $email = trim($dataCompact['User']['email']);
                            $nodo_nombre = $dataCompact['Zone']['zone'];
                            $nodo_id = $dataCompact['Zone']['id'];    
                            
                            
                            $horaExp = date('d-m-Y H:i:s', strtotime($this->request->data('FECHA')));
                            $Email = new CakeEmail();
                            $Email->template('myview', 'mytemplate')
                                ->subject('Acerca de')
                                ->emailFormat('html')
                                ->viewVars([
                                    'nombre_completo' => $nombre_completo,
                                    'nodo_nombre' => $findZone[0]['Zone']['zone'], 
                                    'nodo_id' => $findZone[0]['Zone']['id'],
                                    'latitud' => $findZone[0]['Zone']['latitud'],
                                    'longitud' => $findZone[0]['Zone']['longitud'],
                                    'hora' => $horaExp,
                                    'msqm' => $this->request->data('Msqm'),
                                    'umbral_definido' => $dataCompact['UserRange']['range_finished'],
                                    'temperatura' => $this->request->data('TEMP'),
                                    'hpa' => $this->request->data('hPa')
                                ])
                                ->to($email)
                                ->from(['info@ibot.cl' => 'CONTAMINACIÓN LIMÍNICA'])
                                ->send(); 

                            $this->loadModel('SentAlert');
                            $SentAlert = [];
                            $SentAlert['start'] = $fechaInicio;
                            $SentAlert['finiched'] = $fechaFinal;
                            $SentAlert['fecha_calculo'] = $fechaCalculo;
                            $SentAlert['users_ranges_id'] = $dataCompact['UserRange']['id'];
                            $SentAlert['users_id'] = $dataCompact['UserRange']['users_id']; 
                            $SentAlert['zone_id'] = $dataCompact['UserRange']['zones_id'];
                            $SentAlert['alert_value_msqm'] = $msqm;
                            $SentAlert['alert_my_value'] = $dataCompact['UserRange']['range_finished'];
                            $SentAlert['states_id'] = 1;
                            $this->SentAlert->create();
                            $this->SentAlert->save($SentAlert);
                        }
                    }
                }
            }
        }
    }

}

?>