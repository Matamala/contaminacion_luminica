<?php
App::uses('AppController', 'Controller');
class CommunesController extends AppController {

   public function index(){
   		$this->setTitles('Comunas', 'Listado', Router::url('/Dashboard/Comunas', true));
   }

   public function getCommunes(){
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('Commune');

            $conditions = [];
            if(!empty($this->params['url']['sSearch'])){ 
                $conditions = [
                    'Commune.states_id' => 1,
                    [   
                        'OR' => [ 
                            'Commune.name LIKE' => '%'.$this->params['url']['sSearch'].'%',
                            'City.name LIKE' => '%'.$this->params['url']['sSearch'].'%'
                        ]
                    ]

                ];
            }else{
                $conditions = [ 
                    'Commune.states_id' => 1
                ];
            }

            $eEcho = 1;
            if( !empty($this->params['url']['sEcho']) ){
                $eEcho = $this->params['url']['sEcho'];
            }

            $limit = 10;
            if( !empty($this->params['url']['iDisplayLength']) ){
                $limit = $this->params['url']['iDisplayLength'];
            }

            $dataComunas = $this->Commune->find('all', [
                'fields' => [
                    'Commune.id',
                    'Commune.name',
                    'City.name'
                ],
                'conditions' => $conditions,
                'offset' => intval($this->params['url']['iDisplayStart']),
                'limit' => intval($this->params['url']['iDisplayLength']),
                'recursive' => 1
            ]);

            $dataComunasCount = $this->Commune->find('all', [
                'fields' => [
                    'Commune.id',
                    'Commune.name',
                    'City.name'
                ],
                'conditions' => $conditions
            ]);

            foreach ($dataComunas as $dataComuna) {

                $option = '';
                $option.='<div class="btn-group">
                        <div class="dropdown">
                            <button class="btn btn-md btn-primary btn-active-primary dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                Opciones <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu" style="">
                                <li class="dropdown-header">Seleccione</li>';                                    
                                $option.='<li><a href="'.Router::url( '/Dashboard/Comunas/Editar/'.$dataComuna['Commune']['id'], false).'">Editar</a></li>';
                                $option.='<li><a class="deleteItem" data-item="'.$dataComuna['Commune']['id'].'" href="#">Eliminar</a></li>';
                            $option.='</ul>
                        </div>
                    </div>';

                $dataTemp = [
                    'Comuna' => $dataComuna['Commune']['name'],
                    'Ciudad' => $dataComuna['City']['name'],
                    'Button' => $option
                ];
                array_push($data, $dataTemp);
            }

            $results = [
                "sEcho" => $eEcho,
                "iTotalRecords" => count($dataComunasCount),
                "iTotalDisplayRecords" => count($dataComunasCount),
                "aaData" => $data
            ];
      
            echo json_encode($results);
       }
    }

   public function add(){
        if ($this->request->is('post')) {
            $this->loadModel('Commune');
            $this->Commune->create();
            $this->request->data['Commune']['states_id'] = 1;
            $this->request->data['Commune']['register_by'] = $this->Auth->User('id');
            if ($this->Commune->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        $this->loadModel('City');
        $ciudades = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);
        $this->set('Ciudades', $ciudades);

        $this->setTitles('Comunas', 'Nueva Comuna', Router::url('/Dashboard/Comunas', true));
    }

    public function edit($id = null){
        $this->loadModel('Commune');
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Commune']['register_by'] = $this->Auth->User('id');
            $this->Commune->id = $id;
            if ($this->Commune->save($this->request->data)) {
                $this->Flash->ok('Datos guardados con exito.');
                return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
            }

            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
        }

        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
        }

        $Comuna = $this->Commune->findById($id);

        if(!$Comuna){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
        }

        $this->request->data = $Comuna;

        $this->loadModel('City');
        $ciudades = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);
        $this->set('Ciudades', $ciudades);
        $this->setTitles('Comunas', 'Editar Comuna', Router::url('/Dashboard/Comunas', true));

    }

    public function view($id = null){
        if(!$id){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
        }

        $Comuna = $this->Commune->findById($id);

        if(!$Comuna){
            $this->Flash->error('Ha ocurrido un error. Por favor, intente nuevamente.');
            return $this->redirect(['controller' => '/Dashboard/Comunas', 'action' => '']);
        }

        $this->request->data = $Comuna;

        $this->loadModel('City');
        $ciudades = $this->City->find('list', [
            'fields' => [ 'City.id', 'City.name' ], 
            'conditions' => [ 'City.states_id' => 1 ],
            'order' => [ 'City.name ASC' ]
        ]);
        $this->set('Ciudades', $ciudades);

    }

    public function DeleteItem(){
        $data = [];
        $data['message'] = 'Ha ocurrido un error, intentelo nuevamente.';
        $data['clase'] = 'danger';
        $data['state'] = 2;

        if($this->request->is('ajax')){
            $this->autoRender = false;
            $data = [];

            $this->loadModel('Commune');

            $item = $this->Commune->find('all', [
                'conditions' => [
                    'Commune.states_id' => 1,
                    'Commune.id' => $this->request->data('id')
                ]
            ]);

            if($item){

                $this->Commune->updateAll(
                    ['Commune.states_id' => 2], 
                    [
                        'Commune.states_id' => 1,
                        'Commune.id' => $this->request->data('id')
                    ]
                );
                $data['message'] = 'Datos guardados con éxito.';
                $data['clase'] = 'success';
                $data['state'] = 1;
            }

        }   
        echo json_encode($data);
    }
}

?>