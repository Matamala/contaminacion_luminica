<?php
App::uses('AppController', 'Controller');
class HomeController extends AppController {

   public function index(){

   		$this->loadModel('Zone');

   		$nodos = $this->zone->find('count', [
   			'Zone.states_id' => 1
   		]);


   		$this->set('NumeroNodos', $nodos);

   }

}

?>