<?php
App::uses('AppController', 'Controller');
class DashboardsController extends AppController {
    
    public function index(){
        $this->loadModel('Zone');
        $nodos = $this->Zone->find('count', [
            'conditions' => [
                'Zone.states_id' => 1
            ]
        ]);
        $this->set('NumeroNodos', $nodos);

        $this->setTitles('Home', '', Router::url('/Dashboard', true));
    }

    public function misZonas(){
        
        $this->loadModel('Zone');
        $this->loadModel('UserRange');
        $this->loadModel('Medidas');
        $this->loadModel('Region');
        $this->loadModel('Commune');
        $this->loadModel('City');

        $this->paginate = [
            'conditions' => [
                'UserRange.states_id' => 1, 
                'UserRange.users_id' => $this->Auth->user('id'),
                'UserRange.states_id' =>  1
            ],
            'recursive' => -2,
            'limit' => 3
        ];

        // we are using the 'User' model
        $items = $this->paginate('UserRange');
        $nodos = [];
        $i=0;
        foreach ($items as $item) {
            
            $nodos[$i] = $item;

            $this->Medidas->recursive = -2;
            $medidas = $this->Medidas->find('all', [
                'conditions' => [
                    'Medidas.zones_id' => $item['Zone']['id'],
                    'Medidas.states_id' => 1
                ],
                'recursive' => -2
            ]);
            $nodos[$i]['Medidas'] = $medidas;    

            $this->Region->recursive = -2;
            $region = $this->Region->findById($item['Zone']['regions_id']);
            $nodos[$i]['Region'] = $region;

            $this->City->recursive = -2;
            $ciudad = $this->City->findById($item['Zone']['citys_id']);
            $nodos[$i]['City'] = $ciudad;

            $this->Commune->recursive = -2;
            $comuna = $this->Commune->findById($item['Zone']['communes_id']);
            $nodos[$i]['Commune'] = $comuna;
            $i++;
        } 

        $this->set('Nodos', $nodos);
        $this->setTitles('Nodos', 'Mis Nodos', Router::url('/Dashboard/Mis-Nodos', true));
    }

    public function getCitys(){
        $this->autoRender = false;
        $data = [];
        $data['response'] = '<option value="">Seleccione ciudad</option>';
        $data['status'] = 2;
        if($this->request->is('ajax')){
            if(!empty($this->request->data('regions_id'))){
                $this->loadModel('City');
                $items = $this->City->find('all', [
                    'conditions' => [
                        'City.regions_id' => $this->request->data('regions_id'),
                        'City.states_id' => 1
                    ],
                    'order' => [ 'City.name ASC' ],
                    'recursive' => -2 
                ]);

                if($items){
                    foreach ($items as $item) {
                        $data['response'].='<option value="'.$item['City']['id'].'">'.$item['City']['name'].'</option>';
                    }
                    $data['status'] = 1;
                }else{
                    $data['status'] = 2;
                }
            }
        }
    
        echo json_encode($data);
    }

    public function getCommunes(){
        $this->autoRender = false;
        $data = [];
        $data['response'] = '<option value="">Seleccione comuna</option>';
        $data['status'] = 2;
        if($this->request->is('ajax')){
            if(!empty($this->request->data('citys_id'))){
                $this->loadModel('Commune');
                $items = $this->Commune->find('all', [
                    'conditions' => [
                        'Commune.citys_id' => $this->request->data('citys_id'),
                        'Commune.states_id' => 1
                    ],
                    'order' => [ 'Commune.name ASC' ],
                    'recursive' => -2 
                ]);

                if($items){
                    foreach ($items as $item) {
                        $data['response'].='<option value="'.$item['Commune']['id'].'">'.$item['Commune']['name'].'</option>';
                    }
                    $data['status'] = 1;
                }else{
                    $data['status'] = 2;
                }
            }
        }
    
        echo json_encode($data);
    }

    public function addZone(){
        
    }

    public function editZone($id=null){
        
    }

    public function zonasMonitoreadas(){
        
        $this->loadModel('Zone');
        $this->paginate = [
            'conditions' => ['Zone.states_id' => 1],
            'limit' => 3
        ];
         
        // we are using the 'User' model
        $nodos = $this->paginate('Zone');
         
        // pass the value to our view.ctp
        $this->set('Nodos', $nodos);

        $this->setTitles('Nodos', 'Nodos Monitoreados', Router::url('/Dashboard/Nodos-Monitoreadas', true));

    }

    public function galeryImages(){
        $this->setTitles('Home', 'Imagenes', Router::url('/Dashboard', true));
    }

}

?>